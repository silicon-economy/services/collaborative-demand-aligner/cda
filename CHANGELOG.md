# Changelog
All notable changes to this project will be documented in this file.

## [X.X.X] - XXXX-XX-XX
### Added
- arc42 documentation chapter 10

### Changed

- Change demand and request detail width

## [1.0.1] - [2022-08-30]
### Added
- fonts are now embedded instead of loaded from external sites
- Chapter 1, 3, 11, Glossary and Preface of arc42 documentation added
- add ProducerController
- API for incoming responses from suppliers implemented
- Add internal controller for the supplier to handle confirm/reject button clicks for requests
- REST communication between producer and supplier victoria when sending a request
- Frontend service for confirmation or rejection processing

### Changed
- delivery week service fixed
- sonar rule (CWE-546) for supiscious comments (TODO) disabled
- Adapt colors and icons according the ui-mock.
- refactored backend
- updated mockdata (contains aligned IDs for requests)
refactored frontend
- Adapt ExceptionHandler
- Exclude correct sonar rule (Todo comments)
- Update frontend dependencies to reduce vulnerabilitites
- controller and service on the producer's side when the supplier confirms/rejects a request
- supplier sends response to the producers endpoint
- insert rendered diagrams into documentation

### Removed
- custom SonarQube configuration

## [1.0.0] - [2022-07-26]

### Added
- delivery week service added
- external REST endpoint on the supplier side and logic to handle incoming requests from a producer
- REST endpoint & DTOs implemented for request detail data implemented in backend
- InboundRequestOverviewService implemented in frontend
- add OverviewService that provides methods for both overview services
- add service for colors & icons for the inbound request
- add new colors & icons to the styles.scss
- API for inbound requests implemented
- DTOs for supplier requests
- documentation for chapter 7
- setup for using PostgresQL database
- add inbound request extension to the frontend datamodel
- add `InboundRequestStatus` enum within its `AggregatedInboundRequestStatus` to the frontend enums package
- add request detail view for supplier

### Changed
- harmonizing naming in backend & frontend DTOs
- merge Outbound- and InboundRequestService to a single RequestService (backend)
- copy-n-paste artifacts removed
- license file updated
- credentials removed from files for local development
- api (backend & fontend) changed (raised to v2)
- rename the color service for outbound requests
- restructured the frontend interfaces packages (split into inbound & outbound)
- adapt the angular barrels (index.ts) to the new package structure
- add full company data to inbound request dto
- SonarQube configuration: do not suggest to use local type inference in Java anymore

### Fixed
- fix close detail view within the same row

## [0.0.1] - [2022-06-10]

### Added
- CI job for doing a release added
- wrong websocket protocol fixed (wss vs. ws)
- default values and feedback added for empty tiles in frontend
- function to send requests to suppliers on the demand detail view page
- detail view shows request status with colors
- add status of total demand to detail DTO
- add drag and drop functionality to detail-view-table
- add unit of demand to detail DTO
- add `InboundRequest`s to data model
- add scroll functionality to demand overview table
- add auto change of production week by clicking on demand cell
- dedicated aggregated request status of multiple outbound requests

### Changed
- CI image for frontend set fixed to trion/ng-cli-karma:13.3.7
- styling of first cells in overview changed
- the old version of the legend table in demand overview was replaced
- styleguide is replaced with latest version
- update to spring boot version 2.6.6
- appearance of demand-overview-table cells changed
- renamed `SupplierRequest` to `OutboundRequest`
- demand detail view component was new styled
- demand detail view component was integrated into demand view
- updated mock data

### Fixed
- Fix border around demands in demand view

### Removed
- removed jobsummary field

## [0.0.0] - 2022-04-04

### Added
- Init Changelog.md
