[[section-introduction]]
== Introduction

=== Silicon Economy

****
In a Silicon Economy, services are executed and used via platforms. A Silicon Economy Platform consists of five different kinds of major components, namely one or more SE Services, one or more IDS connectors, a Logistics Broker, an optional IoT Broker and an optional Blockchain Full-Node. This platform is the environment of SE Services, its location does not matter (e.g., IT environment of a company, a cloud environment, etc...).

SE Services are professional (logistics) services, consisting of IT and/or physical services. They are part of a functional (logistics) process and can be linked and subsequently orchestrated in the sense of that process.

.SE Big Picture
image::images/01_se_big_picture.svg[SE Big Picture]

Furthermore, SE Services can be integrated into existing structures, and must be able to be executed and monitored. SE Services are documented. SE Services must be able to be booked and accounted for and have a (technical) description that includes all information about the function of the service and the conditions of use. Finally, SE Services must be able to be supplied with data and must be able to be called via the IDS.
****

=== Collaborative Demand Aligner as a Silicon Economy Plattform Component

The Collaborative Demand Aligner (CDA) component is a SE Service of a Silicon Economy Platform. The CDA supports collaborative planning in the supply chain by communicating demands transparently and re-communicating availabilities, which implicitly takes supplier capacities into account. The App allows for a secure and trustworthy exchange of demand data and delivery commitments. Thus, it builds the basis for collaborative production planning.
The aim is an increase in planning security to ensure adherence to schedules while minimizing coordination's efforts.

=== Requirements Overview

From an end users’ point of view, the CDA App facilitates the collaboration between producer and supplier. Demand data can be exchanged. The number of planning runs and deviations can be reduced. Overall the planning is more accurate and customers are happier. We summarize the requirements for the Collaborative Demand Aligner, which are detailed in the Jira.

. What is the Collaborative Demand Aligner
.. A basic component of a Silicon Economy Platform
.. A solution to facilitate collaborative planning between producer and supplier
.. A solution to exchange demand data

. Essential Features
.. Demand overview/ dashboard for producer
.. Request overview/ dashboard for supplier

=== Quality Goals

****
The following table describes the top six quality goals of a Collaborative Demand Aligner classified by link:https://en.wikipedia.org/wiki/ISO/IEC_9126[ISO/IEC 9126-1] characteristics. The order of goals gives a rough idea of their importance.
****

[options="header"]
|===

|Quality Goal |Motivation/Description

|Functionality
|To be accepted, the CDA component must provide the functionality required for the collaborative planning process.

|Usability
|In order to safe time and prevent mistakes the dashboard must include all necessary information and functions in a structured way.

|Interoperability
|The component must be interoperable with the production planning and ERP systems. Efficiency The CDA service must gather and process data fast to obtain transparency and handle time sensitive products efficiently.PortabilityThe CDA component must be portable so that companies can adopt it and add company-specific functions with minimal effort.

|Efficiency
|The CDA service must gather and process data fast to obtain transparency and handle time sensitive products efficiently.

|Portability
|The CDA component must be portable so that companies can adopt it and add company-specific functions with minimal effort.

|Maintainability
|To allow the use of the component as an open source component it must be maintainable.

|Reliability
|In order to be used in an industrial environment, the services must offer high reliability.

|Understandability
|For the end user, the visualized information as well as the required steps/actions have to be clear and easy to understand

|===

=== Stakeholders

[role="arc42help"]
****
The following table lists the most important stakeholders of an CDA (person, roles and/or organizations) and their respective expectations, goal and intentions:
****

[options="header"]
|===

|Role |Description |Goal, Intention |Example

|Supplier
|A SE Service user, who requires information from the producer about his demands
|Needs a secure data exchange and clear overview over the producer's demands
|Sees an overview whether the producer's demands meet his capacities

|Producer
|A SE Service user, who requires information from the supplier about his capacities
|Needs a secure data exchange and clear overview over the supplier's capacities
|Sees an overview whether the supplier's capacities meet his demands

|Service Developer
|Develops or reuses an SE Service
|Needs a high quality as well as detailed documentation of the source code and interfaces to implement extensions of the services.
|The service developer picks up a service of the Collaborative Demand Aligner App and wants to extend it with further functionalities

|===
