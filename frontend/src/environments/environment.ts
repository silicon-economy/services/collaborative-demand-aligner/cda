/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  // Check if fields are defined before reading them, they are not in e.g. headless chrome
  production: window["env"] && window["env"]["production"] ? window["env"]["production"] : false,
  // API_HOST has to include the protocol since we can not rely on servers redirecting us to https when hardcoding http
  apiHost: window["env"] && window["env"]["API_HOST"] ? window["env"]["API_HOST"] : 'http://localhost',
  apiPort: window["env"] && window["env"]["API_PORT"] ? window["env"]["API_PORT"] : '4200',
  demandOverviewApi:"/api/v2/producer/demand/overview",
  wsProtocol: 'ws', // the websocket protcol either ws or wss,
};
