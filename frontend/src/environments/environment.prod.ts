/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export const environment = {
    production: true,
    // TODO replace the host and port with the correct data for production
  apiHost: window["env"] && window["env"]["API_HOST"] ? window["env"]["API_HOST"] : 'http://localhost',
  apiPort: window["env"] && window["env"]["API_PORT"] ? window["env"]["API_PORT"] : '8080',
  demandOverviewApi:"/api/v2/producer/demand/overview",
  wsProtocol: 'wss', // the websocket protcol either ws or wss,
};
