/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'demands',
    loadChildren: () => import('./producer/pages/demands/demands.module').then(m => m.DemandsModule)
  },
  {
    path: 'demand/:year/:week/:articleNumber',
    loadChildren: () => import('./producer/pages/demand-detail/demand-detail.module').then(m => m.DemandDetailModule)
  },
  {
    path: 'requests',
    loadChildren: () => import('./supplier/pages/requests/requests.module').then(m => m.RequestsModule)
  },
  {
    path: 'request/:year/:week/:articleNumber',
    loadChildren: () => import('./supplier/pages/request-detail/request-detail.module').then(m => m.RequestDetailModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

