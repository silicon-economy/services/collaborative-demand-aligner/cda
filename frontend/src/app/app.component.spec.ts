/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatDrawer, MatDrawerMode, MatSidenavModule} from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { SidenavService } from 'src/app/common/services/sidenav.service';
import { ThemeService } from 'src/app/common/services/theme.service';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Observable, of } from 'rxjs';
import {MatSnackBar} from "@angular/material/snack-bar";

class FakeThemeService {
  toggleMode(): void {}
  getNextModeIcon(): string { return 'dark_mode'; }
  getModeStr(): string { return 'light-theme'; }
  getModeObj() { return { name: 'light-theme', icon: 'light_mode' }; }
  getNextModeObj() { return { name: 'dark-theme', icon: 'dark_mode' }; }
}

class FakeSidenavService {
  toggleSidenavState() {}
  getSidenavState(): boolean { return true; }
}

class FakeBreakpointObserver {
  observe(): Observable<BreakpointState> {
    return of({
      matches: false,
      breakpoints: { },  
    });
  };
}

class MatSnackBarStub{
  open(){
    return {
      onAction: () => of({})
    }
  }
}

describe('AppComponent', () => {
  let app: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatToolbarModule,
        MatIconModule,
        MatListModule,
        BrowserAnimationsModule,
        MatSidenavModule,
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        { provide: ThemeService, useClass: FakeThemeService },
        { provide: SidenavService, useClass: FakeSidenavService },
        { provide: BreakpointObserver, useClass: FakeBreakpointObserver },
        { provide: MatSnackBar , useClass: MatSnackBarStub},
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;
  });

  it('should create the app', () => {
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should toggle the sidenav state', () => {
    // arrange
    app.breakpointStateLtMd = false;

    const sidenavService = TestBed.inject(SidenavService);
    const toggleSidenavStateSpy = spyOn(sidenavService, 'toggleSidenavState');

    // act
    app.ngOnInit();
    app.sidenavStateToggle();

    // expect
    expect(toggleSidenavStateSpy).toHaveBeenCalled();
    expect(fixture.componentInstance.sidenavState).toEqual(true);
  });

  it('should toggle the blue themes mode', () => {
    // arrange
    const addSpy = spyOn(document.body.classList, 'add');
    const removeSpy = spyOn(document.body.classList, 'remove');

    const themeService = TestBed.inject(ThemeService);
    const toggleModeSpy = spyOn(themeService, 'toggleMode');

    // act
    app.toggleThemeMode();

    // assert
    expect(addSpy).toHaveBeenCalledWith('blue-dark-theme');
    expect(removeSpy).toHaveBeenCalledWith('blue-light-theme');
    expect(app.themeModeIcon).toEqual('dark_mode');
  });

  it('should toggle the sidenav state with true bool', () => {
    // arrange
    app.breakpointStateLtMd = true;

    const sidenavService = TestBed.inject(SidenavService);
    const toggleSidenavStateSpy = spyOn(sidenavService, 'toggleSidenavState');

    // act
    app.sidenavStateToggle();

    // expect
    expect(toggleSidenavStateSpy).not.toHaveBeenCalled();
  });

  it('should toggle the sidenav mode with "over" ', () => {
    const sidenavModeMock: MatDrawerMode = 'over';
    // act
    app['activateHandsetLayout']();

    // assert
    expect(sidenavModeMock).toEqual(app.sidenavMode);

  })

  it('should toggle the sidenav mode with "side" ', () => {
    // arrange
    const sidenavModeMock: MatDrawerMode = 'side';

    // act
    app['deactivateHandsetLayout']();

    // assert
    expect(sidenavModeMock).toEqual(app.sidenavMode);
  })

  it('should set the correct headline', () => {
    // arrange
    const title: string = 'new title';

    // act
    app.setHeadline(title);

    // assert
    expect(app.headline).toEqual(title);
  })
});
