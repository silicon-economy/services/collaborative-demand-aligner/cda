/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ComponentFixture, TestBed} from '@angular/core/testing';

import {RequestsComponent} from './requests.component';
import {AggregatedRequestStatus} from "../../model/enum";
import {RequestCellData, RequestOverviewService} from "../../services/request-overview.service";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {RxStompService} from "@stomp/ng2-stompjs";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Observable, of, throwError} from "rxjs";
import {Message} from "@stomp/stompjs";
import { RequestDetailComponent } from "../request-detail/request-detail.component";
import {
    ChangeDetectorRef,
    ComponentFactory,
    ComponentRef,
    ElementRef, EmbeddedViewRef,
    Injector, NgModuleRef, QueryList, TemplateRef,
    Type,
    ViewContainerRef,
    ViewRef
} from "@angular/core";
import { MatTableModule } from "@angular/material/table";
import { RouterModule } from "@angular/router";
import { RouterTestingModule } from "@angular/router/testing";
import { MatToolbarModule } from "@angular/material/toolbar";
import { DragDropModule } from "@angular/cdk/drag-drop";
import { ConfigurationService } from "../../../common/services/configuration.service";

class MatSnackBarStub{
    open(){
        return {
            onAction: () => of({})
        }
    }
}

class FakeStompService {
    watch(): Observable<Message> {
        return of();
    }
}

const statusAllAnswered: AggregatedRequestStatus = AggregatedRequestStatus.ALL_ANSWERED;

const cellMock1: RequestCellData = {
    articleNumber: "123",
    content: 'content',
    week: 25,
    year: 2022,
    status: statusAllAnswered,
    requestsCompleted: true,
}
const columnIndexMock: number = 0;
const rowIndexMock: number = 0;

describe('RequestsComponent', () => {
    let component: RequestsComponent;
    let requestComponent: RequestDetailComponent;
    let fixture: ComponentFixture<RequestsComponent>;
    let detailFixture: ComponentFixture<RequestDetailComponent>;

    class FakeDetailComponentRef extends ComponentRef<any> {
        get location(): ElementRef<any> {
            throw new Error('Method not implemented.');
        }
        get injector(): Injector {
            throw new Error('Method not implemented.');
        }
        get instance(): RequestDetailComponent {
            return requestComponent;
        }
        get hostView(): ViewRef {
            throw new Error('Method not implemented.');
        }
        get changeDetectorRef(): ChangeDetectorRef {
            throw new Error('Method not implemented.');
        }
        get componentType(): Type<any> {
            throw new Error('Method not implemented.');
        }
        destroy(): void {
            throw new Error('Method not implemented.');
        }
        onDestroy(callback: Function): void {
            throw new Error('Method not implemented.');
        }

    }

    class FakeViewContainerRef extends ViewContainerRef {
        createComponent(componentFactory: ComponentFactory<any>, index?: number, injector?: Injector, projectableNodes?: any[][], ngModule?: NgModuleRef<any>): ComponentRef<any> {
            return new FakeDetailComponentRef();
        }
        get element(): ElementRef<any> {
            throw new Error('Method not implemented.');
        }
        get injector(): Injector {
            throw new Error('Method not implemented.');
        }
        get parentInjector(): Injector {
            throw new Error('Method not implemented.');
        }
        clear(): void {
            console.log('clear');
        }
        get(index: number): ViewRef | null {
            throw new Error('Method not implemented.');
        }
        get length(): number {
            throw new Error('Method not implemented.');
        }
        createEmbeddedView<C>(templateRef: TemplateRef<C>, context?: C, index?: number): EmbeddedViewRef<C> {
            throw new Error('Method not implemented.');
        }
        insert(viewRef: ViewRef, index?: number): ViewRef {
            throw new Error('Method not implemented.');
        }
        move(viewRef: ViewRef, currentIndex: number): ViewRef {
            throw new Error('Method not implemented.');
        }
        indexOf(viewRef: ViewRef): number {
            throw new Error('Method not implemented.');
        }
        remove(index?: number): void {
            throw new Error('Method not implemented.');
        }
        detach(index?: number): ViewRef | null {
            throw new Error('Method not implemented.');
        }
    }

    const viewRef = new FakeViewContainerRef();

    class FakeQueryList extends QueryList<ViewContainerRef> {
        toArray(): ViewContainerRef[] {
            return [viewRef];
        }
    }

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [RequestsComponent],
            imports: [
                HttpClientTestingModule,
                HttpClientTestingModule,
                MatTableModule,
                RouterModule.forRoot([]),
                RouterTestingModule.withRoutes([]),
                MatToolbarModule,
                DragDropModule,
            ],
            providers: [
                RequestOverviewService,
                ConfigurationService,
                { provide: HttpTestingController, RxStompService },
                { provide: MatSnackBar , useClass: MatSnackBarStub },
                { provide: RxStompService, useClass: FakeStompService },
        ]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(RequestsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        detailFixture = TestBed.createComponent(RequestDetailComponent);
        requestComponent = detailFixture.componentInstance;
        detailFixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });


    it('should return the cell-with-icon string or an empty string', () => {
        const mock1Return = component.isCellWithIcon(cellMock1)
        const mock2Return = component.isCellWithIcon()

        expect(mock1Return).toEqual('cell-with-icon');
        expect(mock2Return).toEqual('');
    });

    it('should return the request-border string or an empty string', () => {
        component.selectedColumn = 0;
        component.selectedRow = 0;

        const mock1Return = component.setBorder(0, 0);
        const mock2Return = component.setBorder(1, 1,);

        expect(mock1Return).toEqual('request-border ');
        expect(mock2Return).toEqual('');
    });

    it('should return a string containing scss classes', () => {
        const mock1return = component.cellClasses(cellMock1, columnIndexMock, rowIndexMock);

        expect(mock1return).toContain('');
        expect(mock1return).toContain('pointer-cursor mat-elevation-z5 ');
        expect(mock1return).toContain('color-');
    });

    it('should trigger popupService.errorPopup() on stomp message', () => {
        // arrange
        const requestService = TestBed.inject(RequestOverviewService);
        const spyWatch = spyOn(requestService, 'getRequestOverviews').and.returnValue(throwError('error: 400'));
        const spyRefresh = spyOn<any>(component["popupService"], 'errorPopup');

        // act
        component.refreshData();

        // assert
        expect(spyWatch).toHaveBeenCalled();
        expect(spyRefresh).toHaveBeenCalledTimes(1);
    });

    it('should not open request detail view', () => {
        // arrange
        component.selectedColumn = -1;
        component.selectedRow = -1;
        component.expandedRow = -1;
        component.drag = false;

        const cell: RequestCellData = {};
        cell.week = 7;
        cell.year = 2022;
        cell.articleNumber = '4711';

        const cellData: RequestCellData[][] = [[cell]];

        // act
        component.toggleDetailView(0, 0, cell);

        // assert
        expect(component.selectedRow).toEqual(-1);
        expect(component.selectedColumn).toEqual(-1);
        expect(component.expandedRow).toEqual(-1);
    });

    it('should open request detail view', () => {
        // arrange
        const cell: RequestCellData = { week: 7, year: 2022, articleNumber: '4711', content: 'something' };
        component.selectedColumn = -1;
        component.selectedRow = -1;
        component.expandedRow = -1;
        component.drag = false;
        component.data = { rows: [[cell]], columns: [] }
        component.rowContainers = new FakeQueryList();

        console.log(`cell: ${JSON.stringify(cell)}`);
        // act
        component.toggleDetailView(0, 0, cell);

        // assert
        expect(component.selectedRow).toEqual(0);
        expect(component.selectedColumn).toEqual(0);
        expect(component.expandedRow).toEqual(0);
    });

    it('should close request detail view', () => {
        // arrange
        const cell: RequestCellData = { week: 7, year: 2022, articleNumber: '4711', content: 'something' };
        component.data = { rows: [[cell]], columns: [] }
        component.rowContainers = new FakeQueryList();
        const clearSpy = spyOn(viewRef, 'clear');
        component.selectedColumn = 0;
        component.selectedRow = 0;
        component.expandedRow = 0;
        component.drag = false;

        // act
        component.toggleDetailView(0, 0, cell);
        expect(clearSpy).toHaveBeenCalled();
        // assert
        expect(component.selectedRow).toEqual(-1);
        expect(component.selectedColumn).toEqual(-1);
        expect(component.expandedRow).toEqual(-1);
    });
});
