/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RequestsComponent} from './requests.component';
import {MatTableModule} from '@angular/material/table';
import {FlexLayoutModule} from '@angular/flex-layout';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {MatToolbarModule} from "@angular/material/toolbar";
import {RequestsRoutingModule} from "./requests-routing.module";

@NgModule({
    declarations: [
        RequestsComponent
    ],
    imports: [
        CommonModule,
        RequestsRoutingModule,
        MatTableModule,
        FlexLayoutModule,
        DragDropModule,
        MatToolbarModule,
    ]
})
export class RequestsModule {
}
