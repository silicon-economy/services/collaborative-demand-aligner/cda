/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {
    Component,
    ComponentFactory, ComponentFactoryResolver, HostListener,
    OnDestroy,
    OnInit,
    QueryList, ViewChild,
    ViewChildren,
    ViewContainerRef
} from '@angular/core';
import {ColorAndIconService} from "../../services/color-and-icon.service";
import {DateCalculationService} from "../../../common/services/date-calculation-service";
import {CdkDragDrop, moveItemInArray} from "@angular/cdk/drag-drop";
import {AggregatedRequestStatus} from "../../model/enum";
import {
    RequestCellData,
    RequestOverviewService,
    RequestOverviewTableData
} from "../../services/request-overview.service";
import {Subscription} from "rxjs";
import {RxStompService} from "@stomp/ng2-stompjs";
import {PopupWindowService} from "../../../common/services/popup-window.service";
import {Initialize} from "../../../common/initialize";
import { RequestDetailComponent } from "../request-detail/request-detail.component";
import { MatTable } from "@angular/material/table";

@Component({
    selector: 'app-requests',
    templateUrl: './requests.component.html',
    styleUrls: ['./requests.component.scss']
})
export class RequestsComponent implements OnInit, OnDestroy {

    drag = false;

    @HostListener('mousedown') onMouseDown() {
        this.drag = false;
    }
    @HostListener('mousemove') onMouseMove() {
        this.drag = true;
    }

    // Delivery week row data
    toolbarDate: string = '';
    productionWeek: string = '';

    selectedRow = -1;
    selectedColumn = -1;
    expandedRow = -1;

    // Stomp
    topicEvents = ['/topic/refresh'];
    eventSubscription$ = new Subscription();

    // Drag Drop
    @ViewChild('table')
    table: MatTable<any> | undefined;

    // detail view
    @ViewChildren('tableRow', { read: ViewContainerRef })
    rowContainers: QueryList<ViewContainerRef> | undefined;


    /**
     * Object that holds all available requests.
     */
    data: RequestOverviewTableData = {columns: [], rows: []};

    constructor(
        private colorAndIconService: ColorAndIconService,
        private dateCalculationService: DateCalculationService,
        private requestOverviewService: RequestOverviewService,
        private stompService: RxStompService,
        private popupService: PopupWindowService,
        private resolver: ComponentFactoryResolver,
    ) {
        this.toolbarDate = dateCalculationService.getDateStringForToday();
        this.productionWeek = dateCalculationService.getWeekFromDate(new Date()).toString();
    }

    ngOnInit(): void {
       // init stomp
        Initialize.stomp(this.topicEvents, this.eventSubscription$, this.stompService, this, this.popupService);
    }

    ngOnDestroy(): void {
        this.eventSubscription$.unsubscribe();
    }

    onListDrop(event: CdkDragDrop<any[]>) {
        // Swap the elements around
        const previousIndex = this.data.rows.findIndex(
            (row) => row === event.item.data
        );
        moveItemInArray(this.data.rows, previousIndex, event.currentIndex);
        this.data.rows = this.data.rows.slice();
    }

    /**
     * Retrieves the icon color class linked to the specified status from the
     * {@link StatusColorAndIconService}.
     *
     * @param status aggregated request status
     * @returns the icon color class
     */
    getIconColorForAggregatedStatus(status: AggregatedRequestStatus): string {
        return this.colorAndIconService.getIconColorClassForAggregatedStatus(status);
    }

    /**
     * Retrieves the icon type linked to the specified status from the
     * {@link StatusColorAndIconService}.
     *
     * If status does not result in an icon, an empty string is
     * returned.
     *
     * @param status aggregated request status
     * @returns the icon class
     */
    getIconForAggregatedStatus(status: AggregatedRequestStatus): string {
        return this.colorAndIconService.getIconClassForAggregatedStatus(
            status,
        );
    }

    /**
     * Handles to open the demand detail view within the demands' table while a demand cell is
     * clicked. If there is no content in the cell, no content will be returned.
     *
     * @param columnIndex - Column index of the selected cell
     * @param rowIndex - Row index of the selected cell
     * @param cellData - Content of the cell
     */
    toggleDetailView(columnIndex: number, rowIndex: number, cellData?: RequestCellData): void {
        // If no content is in this cell
        if (!cellData?.content) {
            return;
        }

        // Only do anything when drag is false
        if (!this.drag) {
            let sameCell = false;
            const currentCell = this.data.rows[rowIndex][columnIndex];

            if (this.selectedRow === rowIndex && this.selectedColumn === columnIndex) {
                sameCell = true;
            }

            if (this.expandedRow !== -1) {
                // clear old request detail content
                this.rowContainers!.toArray()[this.expandedRow].clear();
                this.resetRowsAndColumns()
            }
            if (sameCell) {
                // If same cell was clicked, don't open new one
                return;
            } else {
                // Request detail view component will be loaded
                this.selectedRow = rowIndex;
                this.selectedColumn = columnIndex;
                const requestDetailContainer = this.rowContainers!.toArray()[rowIndex];
                const requestDetailComponentFactory: ComponentFactory<any> =
                    this.resolver.resolveComponentFactory(RequestDetailComponent);
                const requestDetailComponent = requestDetailContainer.createComponent(requestDetailComponentFactory);

                requestDetailComponent.instance.year = currentCell.year;
                requestDetailComponent.instance.week = currentCell.week;
                requestDetailComponent.instance.articleNumber =
                    currentCell.articleNumber;
                requestDetailComponent.instance.selectedColumn =
                    (this.data.columns.length - this.selectedColumn < 6) ?
                        this.data.columns.length - 6 : this.selectedColumn;
                this.expandedRow = rowIndex;
                this.productionWeek = cellData.week!.toString();
                this.toolbarDate = this.dateCalculationService.getDateStringForWeekAndYear(cellData.week!, cellData.year!);
            }
        }
    }

    /**
     * Reset all for expanding needed row and column values to -1.
     */
    private resetRowsAndColumns(): void {
        this.expandedRow = -1;
        this.selectedRow = -1;
        this.selectedColumn = -1;
    }

    /**
     * Checks if the specified cell is displayed with an icon and returns the 'cell-with-icon' class
     * for proper formatting.
     *
     * @param cell table cell
     * @returns css class for a cell with an icon or empty string if no icon is displayed in the cell
     */
    isCellWithIcon(cell?: RequestCellData): string {
        if (cell) {
            return 'cell-with-icon';
        }
        return '';
    }

    /**
     * Fetches the latest {@link RequestOverviewTableData} from the {@link DemandOverviewService}.
     */
    refreshData(): void {
        this.requestOverviewService.getRequestOverviews().subscribe((data) => {
            this.data = this.requestOverviewService.toDataSourceArray(data);
        },
        (err) => {
            this.popupService.errorPopup(err);
        })
    }


    /**
     * Returns multiple classes for a cell at once. Includes classes for border, pointer and
     * background color.
     *
     * @param e table cell
     * @param columnIndex column index of cell
     * @param rowIndex row index of cell
     * @returns string containing (multiple) classes
     */
    cellClasses(e: RequestCellData, columnIndex: number, rowIndex: number): string {
        return (
            this.setBorder(columnIndex, rowIndex) +
            this.setPointerClass(e) +
            this.backgroundColorClass(e.status)
        );
    }

    /**
     * Sets a border css class to a cell when the cell is selected and it's details are shown.
     *
     * @param columnIndex - Column index of the selected cell
     * @param rowIndex - Row index of the selected cell
     * @return CSS class
     */
    setBorder(columnIndex: number, rowIndex: number): string {
        if (columnIndex === this.selectedColumn && rowIndex === this.selectedRow) {
            return 'request-border ';
        } else {
            return '';
        }
    }

    /**
     * Returns css classes of pointer and shadow for non-empty table cells.
     *
     * @param e table cell
     * @returns css classes
     */
    private setPointerClass(e?: RequestCellData): string {
        if (e?.content) {
            return 'pointer-cursor mat-elevation-z5 ';
        } else {
            return '';
        }
    }

    /**
     * Retrieves the background color linked to the specified status from the
     * {@link ColorAndIconService}.
     *
     * @param status request status
     * @returns background color class
     */
    private backgroundColorClass(status?: AggregatedRequestStatus): string {
        return this.colorAndIconService.getBackgroundColorClassForAggregatedStatus(
            status
        );
    }


}
