/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestDetailComponent } from './request-detail.component';
import { CompanyData } from "../../../common/model";
import { AggregatedRequestStatus, RequestStatus } from "../../model/enum";
import { Observable, of } from "rxjs";
import { Message } from "@stomp/stompjs";
import { RequestDetailData } from "../../model";
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";
import { MatCardModule } from "@angular/material/card";
import { MatToolbarModule } from "@angular/material/toolbar";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatTableModule } from "@angular/material/table";
import { RequestDetailRoutingModule } from "./request-detail-routing.module";
import { MatButtonModule } from "@angular/material/button";
import { MatDividerModule } from "@angular/material/divider";
import { MatIconModule } from "@angular/material/icon";
import { RxStompService } from "@stomp/ng2-stompjs";
import { RequestDetailService } from "../../services/request-detail.service";
import { MatSnackBarModule} from "@angular/material/snack-bar";

const mockCustomer1: CompanyData = {
  id: 'testid1',
  name: 'testName1',
};
const mockCustomer2: CompanyData = {
  id: 'testid2',
  name: 'testName2',
};
const mockCustomer3: CompanyData = {
  id: 'testid3',
  name: 'testName3',
};
const mockCustomer4: CompanyData = {
  id: 'testid4',
  name: 'testName4',
};
const mockCustomer5: CompanyData = {
  id: 'testid5',
  name: 'testName5',
};
const mockCustomer6: CompanyData = {
  id: 'testid6',
  name: 'testName6',
};

const mockCustomer7: CompanyData = {
  id: 'testid7',
  name: 'testName7',
};

const mockFakeCustomer: CompanyData = {
  id: 'testERROR',
  name: 'testName',
};

const mockFakeConfirmationTrue = {
  requestId: 'ss',
  confirmed: true,
};

const mockData: RequestDetailData = {
  articleNumber: '45',
  articleDescription: 'test',
  totalQuantity: 5,
  requests: [
    {
      id: 'ss',
      articleNumber: 'testArticleNo',
      quantity: 3,
      year: 2022,
      week: 6,
      status: RequestStatus.NEW_REQUEST,
      customer: mockCustomer1,
    },
    {
      id: 'ss',
      articleNumber: 'testArticleNo',
      quantity: 3,
      year: 2022,
      week: 6,
      status: RequestStatus.NEGATIVE_FEEDBACK,
      customer: mockCustomer2,
    },
    {
      id: 'ss',
      articleNumber: 'testArticleNo',
      quantity: 3,
      year: 2022,
      week: 6,
      status: RequestStatus.NO_FEEDBACK_REJECTED,
      customer: mockCustomer3,
    },
    {
      id: 'ss',
      articleNumber: 'testArticleNo',
      quantity: 3,
      year: 2022,
      week: 6,
      status: RequestStatus.POSITIVE_FEEDBACK,
      customer: mockCustomer4,
    },
    {
      id: 'ss',
      articleNumber: 'testArticleNo',
      quantity: 3,
      year: 2022,
      week: 6,
      status: RequestStatus.POSITIVE_FEEDBACK_REJECTED,
      customer: mockCustomer5,
    },
    {
      id: 'ss',
      articleNumber: 'testArticleNo',
      quantity: 3,
      year: 2022,
      week: 6,
      status: RequestStatus.POSITIVE_FEEDBACK_CONFIRMED,
      customer: mockCustomer6,
    },
  ],
  year: 2022,
  week: 10,
  totalStatus: AggregatedRequestStatus.ALL_ANSWERED,
  unit: 'unit',
};

const mockData2: RequestDetailData = {
  articleNumber: '45',
  articleDescription: 'test',
  totalQuantity: 5,
  requests: [
    {
      id: 'ss',
      articleNumber: 'testArticleNo',
      quantity: 3,
      year: 2022,
      week: 6,
      customer: mockCustomer7,
      status: RequestStatus.POSITIVE_FEEDBACK_CONFIRMED,
    },
  ],
  year: 2022,
  week: 10,
  totalStatus: AggregatedRequestStatus.FEEDBACK_INCOMPLETE,
  unit: 'unit',
};

const mockDataWithoutRequests: RequestDetailData = {
  articleNumber: '45',
  articleDescription: 'test',
  totalQuantity: 5,
  requests: [],
  year: 2022,
  week: 10,
  totalStatus: AggregatedRequestStatus.NEW_REQUEST,
  unit: 'unit',
};

class FakeRequestDetailService {
  getRequestDetail(): Observable<RequestDetailData> {
    return of(mockData);
  }
  
  createRequest(): void {}
}

class FakeStompService {
  watch(): Observable<Message> {
    return of();
  }
}

describe('RequestDetailComponent', () => {
  let component: RequestDetailComponent;
  let fixture: ComponentFixture<RequestDetailComponent>;
  let httpClient: HttpTestingController;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestDetailComponent ],
      imports: [
        HttpClientTestingModule,
        MatCardModule,
        MatToolbarModule,
        BrowserAnimationsModule,
        MatTableModule,
        RequestDetailRoutingModule,
        MatButtonModule,
        MatDividerModule,
        MatIconModule,
        MatSnackBarModule,
      ],
      providers: [
        { provide: RequestDetailService, useClass: FakeRequestDetailService },
        { provide: RxStompService, useClass: FakeStompService },
      ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestDetailComponent);
    component = fixture.componentInstance;
    component.articleNumber = mockData.articleNumber;
    component.week = mockData.week;
    component.year = mockData.year;
    httpClient = TestBed.inject(HttpTestingController);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should calculate quantity to request properly', () => {
    //arrange
    component.data = mockData;

    //act
    const dataToTest = component.quantityToRequest(mockCustomer1);
    const dataToTest2 = component.quantityToRequest(mockCustomer7);

    //assert
    expect(dataToTest).toEqual(3);
    expect(dataToTest2).toEqual(5);
  });

  it('should trigger refreshData() on stomp message', () => {
    // arrange
    const stompService = TestBed.inject(RxStompService);
    let message: Observable<Message> = of({} as Message);
    const spyWatch = spyOn(stompService, 'watch').and.returnValue(message);
    const spyRefresh = spyOn(component, 'refreshData');

    // act
    component.ngOnInit();

    // assert
    expect(spyWatch).toHaveBeenCalled();
    expect(spyRefresh).toHaveBeenCalledTimes(2); // once in fetchUrlParams and once because of stomp
  });

  it('should get request detail data on init', () => {
    // arrange
    const requestDetailService = TestBed.inject(RequestDetailService);
    const spy = spyOn(requestDetailService, 'getRequestDetail').and.returnValue(
        of(mockData)
    );

    // act
    component.ngOnInit();

    // assert
    expect(spy).toHaveBeenCalled();
    expect(component.data).toEqual(mockData);
  });

  it('should return the correct icon for customer', () => {
    // arrange

    // act
    const icon1 = component.getIconClassForCustomer(mockCustomer1);
    const icon2 = component.getIconClassForCustomer(mockCustomer2);
    const icon3 = component.getIconClassForCustomer(mockCustomer3);
    const icon4 = component.getIconClassForCustomer(mockCustomer4);
    const icon5 = component.getIconClassForCustomer(mockCustomer5);
    const icon6 = component.getIconClassForCustomer(mockCustomer6);
    const iconFake = component.getIconClassForCustomer(mockFakeCustomer);

    // assert
    expect(icon1).toEqual('mark_email_unread');
    expect(icon2).toEqual('');
    expect(icon3).toEqual('do_not_disturb_on');
    expect(icon4).toEqual('');
    expect(icon5).toEqual('do_not_disturb_on');
    expect(icon6).toEqual('check_circle');
    expect(iconFake).toEqual('');
  });

  it('should return the correct icon color for customer', () => {
    // arrange

    // act
    const icon1 = component.getIconColorClassForCustomer(mockCustomer1);
    const icon2 = component.getIconColorClassForCustomer(mockCustomer2);
    const icon3 = component.getIconColorClassForCustomer(mockCustomer3);
    const icon4 = component.getIconColorClassForCustomer(mockCustomer4);
    const icon5 = component.getIconColorClassForCustomer(mockCustomer5);
    const icon6 = component.getIconColorClassForCustomer(mockCustomer6);
    const iconFake = component.getIconColorClassForCustomer(mockFakeCustomer);

    // assert
    expect(icon1).toEqual('unread-icon');
    expect(icon2).toEqual('');
    expect(icon3).toEqual('disturb-icon');
    expect(icon4).toEqual('');
    expect(icon5).toEqual('disturb-icon');
    expect(icon6).toEqual('check-icon');
    expect(iconFake).toEqual('');
  });

  it('should return the correct background color class for customer', () => {
    // arrange
    const supplierWithRequest = mockCustomer4;
    const supplierWithoutRequest = mockFakeCustomer;

    // act
    const colorOfStatus = component.getBackgroundColorClassForCustomer(supplierWithRequest);
    const defaultColor = component.getBackgroundColorClassForCustomer(supplierWithoutRequest);

    // assert
    expect(colorOfStatus).toEqual('color-positive-feedback');
    expect(defaultColor).toEqual('');
  });

  it('should return the correct background color class for aggregated status', () => {
    // arrange

    // act
    const backgroundColorClass1 = component.getBackgroundColorClassForAggregatedStatus(mockData.totalStatus);
    const backgroundColorClass2 = component.getBackgroundColorClassForAggregatedStatus(mockData2.totalStatus);
    const backgroundColorClassFake = component.getBackgroundColorClassForAggregatedStatus(mockDataWithoutRequests.totalStatus);

    // assert
    expect(backgroundColorClass1).toEqual('color-all-answered');
    expect(backgroundColorClass2).toEqual('color-feedback-incomplete');
    expect(backgroundColorClassFake).toEqual('');
  });

  it('should disable/enable the confirm button correctly', () => {
    // arrange

    // act
    const button1 = component.disableConfirmButton(mockData.requests[0]);
    const button2 = component.disableConfirmButton(mockData.requests[1]);
    const button3 = component.disableConfirmButton(mockData.requests[2]);
    const button4 = component.disableConfirmButton(mockData.requests[3]);
    const button5 = component.disableConfirmButton(mockData.requests[4]);
    const button6 = component.disableConfirmButton(mockData.requests[5]);

    // assert
    expect(button1).toEqual(false);
    expect(button2).toEqual(false);
    expect(button3).toEqual(true);
    expect(button4).toEqual(true);
    expect(button5).toEqual(true);
    expect(button6).toEqual(true);
  });

  it('should enable the confirm remaining customers button', () => {
    // arrange
    component.data = mockData;

    // act
    const button = component.disableConfirmRemainingCustomersButton();

    // assert
    expect(button).toEqual(false);
  });

  it('should disable the confirm remaining customers button', () => {
    // arrange
    component.data = mockDataWithoutRequests;

    // act
    const button = component.disableConfirmRemainingCustomersButton();

    // assert
    expect(button).toEqual(true);
  });

  it('should disable/enable the reject button correctly', () => {
    // arrange

    // act
    const button1 = component.disableRejectButton(mockData.requests[0]);
    const button2 = component.disableRejectButton(mockData.requests[1]);
    const button3 = component.disableRejectButton(mockData.requests[2]);
    const button4 = component.disableRejectButton(mockData.requests[3]);
    const button5 = component.disableRejectButton(mockData.requests[4]);
    const button6 = component.disableRejectButton(mockData.requests[5]);

    // assert
    expect(button1).toEqual(false);
    expect(button2).toEqual(true);
    expect(button3).toEqual(true);
    expect(button4).toEqual(false);
    expect(button5).toEqual(true);
    expect(button6).toEqual(true);
  });

  it('should enable the reject remaining customers button', () => {
    // arrange
    component.data = mockData;

    // act
    const button = component.disableRejectRemainingCustomersButton();

    // assert
    expect(button).toEqual(false);
  });

  it('should disable the reject remaining customers button', () => {
    // arrange
    component.data = mockDataWithoutRequests;

    // act
    const button = component.disableRejectRemainingCustomersButton();

    // assert
    expect(button).toEqual(true);
  });

  it('should switch customer state', () => {
    // arrange
    const state = component.customersState;
    // act
    component.customersToggle();

    // assert
    expect(component.customersState).toEqual(!state);
  });

  it('should set customer state to true', () => {
    // arrange

    // act
    component.checkForRequests(mockData);

    // assert
    expect(component.customersState).toBe(true);
  });

  it('should set customer state to false', () => {
    // arrange

    // act
    component.checkForRequests(mockDataWithoutRequests);

    // assert
    expect(component.customersState).toBe(false);
  });

  it('should return the correct icon class for aggregated status', () => {
    // arrange

    // act
    const iconMarkEmailUnread = component.getIconForAggregatedStatus(
        AggregatedRequestStatus.NEW_REQUEST
    );
    const iconMail = component.getIconForAggregatedStatus(
        AggregatedRequestStatus.FEEDBACK_INCOMPLETE
    );
    const iconMarkEmailRead = component.getIconForAggregatedStatus(
        AggregatedRequestStatus.ALL_ANSWERED
    );

    // assert
    expect(iconMarkEmailUnread).toEqual('mark_email_unread');
    expect(iconMail).toEqual('mail');
    expect(iconMarkEmailRead).toEqual('mark_email_read');
  });

  it('should return the correct icon color class for aggregated status', () => {
    // arrange

    // act
    const iconMarkEmailUnread = component.getIconColorForStatus(
        AggregatedRequestStatus.NEW_REQUEST
    );
    const iconMail = component.getIconColorForStatus(
        AggregatedRequestStatus.FEEDBACK_INCOMPLETE
    );
    const iconMarkEmailRead = component.getIconColorForStatus(
        AggregatedRequestStatus.ALL_ANSWERED
    );

    // assert
    expect(iconMarkEmailUnread).toEqual('unread-icon');
    expect(iconMail).toEqual('incomplete-icon');
    expect(iconMarkEmailRead).toEqual('read-icon');
  });
  
  it('should send confirmation', () => {
    // arrange
    let requestsSent = 0;
    const spySendSingle = spyOn(component, 'sendRequest').and.callFake(
        (supplier) => {
          requestsSent++;
        }
    );
  
    // act
    component.sendConfirmation(mockData.requests[0]);
  
    // assert
    expect(spySendSingle).toHaveBeenCalledOnceWith(mockData.requests[0], true);
    expect(requestsSent).toBe(1);
  });

  it('should send rejection', () => {
    // arrange
    let requestsSent = 0;
    const spySendSingle = spyOn(component, 'sendRequest').and.callFake(
        (supplier) => {
          requestsSent++;
        }
    );
  
    // act
    component.sendRejection(mockData.requests[0]);
  
    // assert
    expect(spySendSingle).toHaveBeenCalledOnceWith(mockData.requests[0], false);
    expect(requestsSent).toBe(1);
  });
  
  it('should send confirmation to all remaining', () => {
    // arrange
    let requestsSent = 0;
    const spySendSingle = spyOn(component, 'sendConfirmation').and.callFake(
        (supplier) => {
          requestsSent++;
        }
    );
  
    component.data = mockData;
  
    // act
    component.sendConfirmationToRemainingProducer();
  
    // assert
    expect(spySendSingle).toHaveBeenCalledWith(mockData.requests[0]);
    expect(requestsSent).toBe(2);
  });
  
  it('should send rejection to all remaining', () => {
    // arrange
    let requestsSent = 0;
    const spySendSingle = spyOn(component, 'sendRejection').and.callFake(
        (supplier) => {
          requestsSent++;
        }
    );
  
    component.data = mockData;
  
    // act
    component.sendRejectionToRemainingProducer();
  
    // assert
    expect(spySendSingle).toHaveBeenCalledWith(mockData.requests[0]);
    expect(requestsSent).toBe(2);
  });
  
  it('should send request', () => {
    // arrange
    const requestDetailService = TestBed.inject(RequestDetailService);
    const mockHTTPResponse: string = '200 OK';
    const spy = spyOn(
        requestDetailService,
        'createRequest'
    ).and.returnValue(of(mockHTTPResponse));
  
    component.data = mockData;
  
    // act
    component.sendRequest(mockData.requests[0], true);
  
    // assert
    expect(spy).toHaveBeenCalledWith(mockFakeConfirmationTrue);
    expect(component.data).toEqual(mockData);
  });

});
