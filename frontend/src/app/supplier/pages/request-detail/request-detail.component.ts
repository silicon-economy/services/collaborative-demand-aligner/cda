/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from "@angular/animations";
import { Subscription } from "rxjs";
import { RxStompService } from "@stomp/ng2-stompjs";
import { CompanyData } from "../../../common/model";
import { AggregatedRequestStatus, RequestStatus } from "../../model/enum";
import { RequestDetailService } from "../../services/request-detail.service";
import { Request, RequestDetailData } from "../../model";
import { ColorAndIconService } from "../../services/color-and-icon.service";
import { PopupWindowService } from "../../../common/services/popup-window.service";
import { Initialize } from "../../../common/initialize";

@Component({
  selector: 'app-request-detail',
  templateUrl: './request-detail.component.html',
  styleUrls: ['./request-detail.component.scss'],
  animations: [
    /** Animation that rotates the indicator arrow. */
    trigger('indicatorRotation', [
      state('collapsed, void', style({ transform: 'rotate(0deg)' })),
      state('expanded', style({ transform: 'rotate(180deg)' })),
      transition(
        'expanded <=> collapsed, void => collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
      ),
    ]),
    /** Animation that expands and collapses the panel content. */
    trigger('listHeight', [
      state('collapsed, void', style({ height: '0px', visibility: 'hidden' })),
      state('expanded', style({ height: '*', visibility: 'visible' })),
      transition(
        'expanded <=> collapsed, void => collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
      ),
    ]),
  ],
})
export class RequestDetailComponent implements OnInit, OnDestroy {
  //Stomp
  topicEvents = ['/topic/refresh'];
  eventSubscription$ = new Subscription();

  data?: RequestDetailData;

  @Input()
  week: number = 0;

  @Input()
  year: number = 0;

  @Input()
  articleNumber: string = '';

  @Input()
  selectedColumn: number = 0;

  // tables
  displayedColumnsDetails = ['Name', 'Unit', 'Total Demand'];
  displayedColumnsCustomer = [
    'Customer Name',
    'Demand Requested',
    'Confirms',
    'Rejects',
  ];

  // accordion
  customersState = false;

  constructor(
    private requestDetailService: RequestDetailService,
    private stompService: RxStompService,
    private colorsAndIconsService: ColorAndIconService,
    private popupService: PopupWindowService,
  ) { }

  ngOnInit(): void {
    Initialize.stomp(this.topicEvents, this.eventSubscription$, this.stompService, this, this.popupService);
  }

  ngOnDestroy(): void {
    this.eventSubscription$.unsubscribe();
  }

  /**
   * Calculates the quantity of an article that shows as requested amount / amount to be requested.
   *
   * @param customer The company data of a customer
   * @returns Quantity of article
   */
  quantityToRequest(customer: CompanyData): any {
    const requestFound = this.findRequestSentFromCostumer(customer);
    if (requestFound) {
      return requestFound.quantity;
    } else {
      return this.data!.totalQuantity;
    }
  }

  /**
   * Returns Google icon class for a given {@link CompanyData Customer}.
   *
   * @param customer {@link CompanyData Customer} to check
   * @returns Class name of a Google icon class
   */
  getIconClassForCustomer(customer: CompanyData): string {
    return this.colorsAndIconsService.getIconClassForCustomer(
      customer,
      this.data
    );
  }

  /**
   * Looks for requests sent to this supplier and returns a color class for an icon
   * corresponding to the request's status.
   *
   * @param customer {@link CompanyData Customer} to check
   * @returns Icon color class
   */
  getIconColorClassForCustomer(customer: CompanyData): string {
    return this.colorsAndIconsService.getIconColorClassForCustomer(
      customer,
      this.data
    );
  }

  /**
   * Retrieves the background color class linked to the specified aggregated request status from the
   * {@link StatusColorAndIconService}.
   *
   * @param status - Aggregated request status
   * @returns Background color class
   */
  getBackgroundColorClassForAggregatedStatus(status: AggregatedRequestStatus): string {
    return this.colorsAndIconsService.getBackgroundColorClassForAggregatedStatus(
      status
    );
  }

  /**
   * Looks for requests sent to this supplier and returns the background color class
   * corresponding to the status.
   *
   * {@link CompanyData Customer} to check
   * @returns Background color class
   */
  getBackgroundColorClassForCustomer(customer: CompanyData): string {
    return this.colorsAndIconsService.getBackgroundColorClassForCustomer(
      customer,
      this.data
    );
  }

  /**
   * Retrieves the icon type linked to the specified status from the
   * {@link StatusColorAndIconService}.
   *
   * If status does not result in an icon, an empty string is
   * returned.
   *
   * @param status aggregated request status
   * @returns the icon class
   */
  getIconForAggregatedStatus(status: AggregatedRequestStatus): string {
    return this.colorsAndIconsService.getIconClassForAggregatedStatus(
      status,
    );
  }

  /**
   * Retrieves the icon color class linked to the specified status from the
   * {@link StatusColorAndIconService}.
   *
   * @param status aggregated request status
   * @returns icon color class
   */
  getIconColorForStatus(status: AggregatedRequestStatus): string {
    return this.colorsAndIconsService.getIconColorClassForAggregatedStatus(status);
  }

  /**
   * Decides if the button to send confirm responses to all remaining customers should be disabled. The
   * button should be disabled, if all buttons to response confirmation to a single customer are disabled.
   *
   * @return True if button to response confirmation to remaining customers should be disabled
   */
  disableConfirmRemainingCustomersButton(): boolean {
    let enabledButtons = 0;
    this.data!.requests.forEach((request) => {
      if (!this.disableConfirmButton(request)) {
        enabledButtons++;
      }
    });
    return enabledButtons === 0;
  }

  /**
   * Decides if the button to send reject responses to all remaining customers should be disabled. The
   * button should be disabled, if all buttons to response rejection to a single customer are disabled.
   *
   * @return True if button to response rejection to remaining suppliers should be disabled
   */
  disableRejectRemainingCustomersButton(): boolean {
    let enabledButtons = 0;
    this.data!.requests.forEach((request) => {
      if (!this.disableRejectButton(request)) {
        enabledButtons++;
      }
    });
    return enabledButtons === 0;
  }

  /**
   * Decides if the button to send a confirmation response to a specific customer should be disabled. The button
   * should be disabled if this customer got a response previously.
   *
   * @param request The Request to be checked
   * @returns True if button to response confirmation specified customer should be disabled
   */
  disableConfirmButton(request: Request): boolean {
    // disable button if response is completed
    if (request.status === RequestStatus.NEW_REQUEST
      || request.status === RequestStatus.NEGATIVE_FEEDBACK) {
      // disable button if a response was already sent to this customer
      const indexOfRequest = this.data!.requests.findIndex(
        (res) => res.id === request.id
      );
      if (indexOfRequest !== -1) {
        // this supplier was not requested yet, show button
        return false;
      }
    }
    return true;
  }

  /**
   * Decides if the button to send a rejection response to a specific customer should be disabled. The button
   * should be disabled if this customer got a response previously.
   *
   * @param request The Request to be checked
   * @returns True if button to response rejection specified customer should be disabled
   */
  disableRejectButton(request: Request): boolean {
    // disable button if requests completed
    if (request.status === RequestStatus.NEW_REQUEST
      || request.status === RequestStatus.POSITIVE_FEEDBACK) {
      // disable button if a response was already sent to this customer
      const indexOfRequest = this.data!.requests.findIndex(
        (res) => res.id === request.id
      );
      if (indexOfRequest !== -1) {
        // this supplier was not requested yet, show button
        return false;
      }
    }
    return true;
  }

  /**
   * Fetches the latest {@link RequestDetailData} from the {@link RequestDetailService}.
   */
  refreshData(): void {
    this.requestDetailService
      .getRequestDetail(this.year, this.week, this.articleNumber)
      .subscribe((data) => {
        this.data = data;
        this.checkForRequests(data);
      });
  }

  /**
   * Checks if at least one customer exists for this request. If yes, {@link customersState} will be
   * set to true.
   *
   * @param data {@link RequestDetailData} object from the database
   */
  checkForRequests(data: RequestDetailData): void {
    this.customersState = data.requests.length !== 0;
  }

  /**
   * Toggles the customer accordion to be open or close.
   */
  customersToggle(): void {
    this.customersState = !this.customersState;
  }

  /**
   * Searches for a request in {@link data} that was sent to this supplier.
   *
   * @param customer {@link CompanyData Customer} to check
   * @returns the request sent, or undefined if no request was found
   */
  private findRequestSentFromCostumer(customer: CompanyData): Request | undefined {
    return this.data!.requests.filter(
      (request) => request.customer.id === customer.id
    )[0];
  }

  /**
   * Sends a request to all suppliers who have not received a request yet.
   */
  sendConfirmationToRemainingProducer(): void {
    this.data?.requests.forEach((request) => {
      if (!this.disableConfirmButton(request)) {
        this.sendConfirmation(request);
      }
    });
  }

  /**
   * Sends a request to all suppliers who have not received a request yet.
   */
  sendRejectionToRemainingProducer(): void {
    this.data?.requests.forEach((request) => {
      if (!this.disableRejectButton(request)) {
        this.sendRejection(request);
      }
    });
  }

  sendConfirmation(request: Request) {
    this.sendRequest(request, true);
  }

  sendRejection(request: Request) {
    this.sendRequest(request, false);
  }

  /**
   * Sends a response to a request with the confirmation or rejection to the specified producer.
   *
   * @param request Request to send the response to
   * @param confirm
   */
  sendRequest(request: Request, confirm: boolean): void {
    const confirmation = {
      requestId: request.id,
      confirmed: confirm,
    };
    this.requestDetailService.createRequest(confirmation).subscribe(
      () => {
        // empty function body needed to provide the 'err' function as a second argument
        // for the 'subscribe' method. Otherwise, the error would not be recognized.
      },
      (err) => {
        this.popupService.errorPopup(err);
      });
  }

  /**
   * Calculate the styling by input selected Column to imitate floating
   * 
   * @returns the style configuration 
   */
  float(): { [klass: string]: string } {
    return {
      "padding-left": this.selectedColumn * 205 + 'px',
      "width": 1200 + (this.selectedColumn * 205) + 'px'
    };
  }
}
