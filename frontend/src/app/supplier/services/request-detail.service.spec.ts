/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { RequestDetailService } from './request-detail.service';
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";
import { AggregatedRequestStatus, RequestStatus } from "../model/enum";
import { HttpResponse } from "@angular/common/http";
import { RequestDetailData } from "../model";
import { RequestCellData } from "./request-overview.service";

describe('RequestDetailService', () => {
  let service: RequestDetailService;
  let httpClient: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(RequestDetailService);
    httpClient = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get the requested detail data', () => {
    // arrange
    const argumentData: RequestCellData = {
      articleNumber: '5871',
      content: '',
      week: 15,
      year: 2022,
    };

    const expectedElement: RequestDetailData = {
      year: 2022,
      week: 5,
      totalQuantity: 5,
      articleNumber: '5871',
      articleDescription: 'dummy desc',
      totalStatus: AggregatedRequestStatus.NEW_REQUEST,
      unit: 'unit',
      requests: [
        {
          id: 'test',
          articleNumber: 'testArticleNo',
          quantity: 5,
          year: 2022,
          week: 6,
          customer: {
            id: '123',
            name: 'name'
          },
          status: RequestStatus.NEW_REQUEST,
        },
      ]
    };

    // act
    service.getRequestDetail(2022, 15, '5871').subscribe((data) => {
      expect(data).toEqual(expectedElement);
    });

    // assert
    httpClient
        .expectOne(`${location.origin}/api/v2/supplier/requests/detail-view/2022-15/5871`)
        .event(new HttpResponse<{}>({ body: expectedElement }));
  });
  
  it('should create a request', () => {
    // arrange
    const confirmation = {
      requestId: "1337",
      confirmed: true,
    };
    let mockResponse: HttpResponse<{}> = new HttpResponse<{}>({ status: 200 });
    
    // act & assert
    service.createRequest(confirmation).subscribe((id) => {
      const req = httpClient
          .expectOne(`${location.origin}/api/v2/supplier/requests/confirmation`);
      req.event(mockResponse);
      expect(req.request.method).toEqual('POST');
      expect(req.request.body).toEqual(confirmation);
    });
  });
});
