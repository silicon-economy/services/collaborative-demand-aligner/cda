/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { ColorAndIconService } from './color-and-icon.service';
import { CompanyData } from "../../common/model";
import { RequestDetailData } from "../model";
import { AggregatedRequestStatus, RequestStatus } from "../model/enum";

const requestedCustomer: CompanyData = {
    id: 'Id1',
    name: 'Customer.1',
}

const unrequestedCustomer: CompanyData = {
    id: 'Id2',
    name: 'Customer.2',
}

const dataWithNewRequests: RequestDetailData = {
    articleNumber: '45',
    articleDescription: 'test',
    totalQuantity: 5,
    requests: [
        {
            id: 'anyUuid',
            articleNumber: 'testArticleNo',
            quantity: 3,
            year: 2022,
            week: 6,
            customer: requestedCustomer,
            status: RequestStatus.NEW_REQUEST,
        },
    ],
    year: 2022,
    week: 10,
    totalStatus: AggregatedRequestStatus.NEW_REQUEST,
    unit: 'unit',
}

describe('ColorAndIconService', () => {
    let service: ColorAndIconService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(ColorAndIconService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should get the icon class for customer (details)', () => {
        // act
        const iconRetrievedFromMap = service.getIconClassForCustomer(
            requestedCustomer,
            dataWithNewRequests
        );

        // assert
        expect(iconRetrievedFromMap).toEqual('mark_email_unread');
    });

    it('should get no icon class for customer (details)', () => {
        // act
        const noIconRetrievedFromMap = service.getIconClassForCustomer(
            unrequestedCustomer,
            dataWithNewRequests
        );

        // assert
        expect(noIconRetrievedFromMap).toEqual('');
    });


    it('should get the icon color class for aggregated status (overview)', () => {
        // arrange
        const status = AggregatedRequestStatus.NEW_REQUEST;

        // act
        const iconColor = service.getIconColorClassForAggregatedStatus(status);

        // assert
        expect(iconColor).toEqual('unread-icon');
    });

    it('should get no icon color class for aggregated status (overview)', () => {
        // arrange
        const status = AggregatedRequestStatus.NEW_REQUEST;

        // act
        const noColor = service.getIconColorClassForAggregatedStatus(undefined);

        // assert
        expect(noColor).toEqual('');
    });

    it('should get the icon color for status (customer details)', () => {
        // act
        const iconColor = service.getIconColorClassForCustomer(requestedCustomer, dataWithNewRequests);

        // assert
        expect(iconColor).toEqual('unread-icon');
    });

    it('should get no icon color for status (customer details)', () => {
        // act
        const noIconColor = service.getIconColorClassForCustomer(unrequestedCustomer, dataWithNewRequests);

        // assert
        expect(noIconColor).toEqual('');
    });

    it('should get icon for aggregated status (overview)', () => {
        // arrange & act
        const classStringNewRequest = service.getIconClassForAggregatedStatus(
            AggregatedRequestStatus.NEW_REQUEST);
        const classStringFeedbackIncomplete = service.getIconClassForAggregatedStatus(
            AggregatedRequestStatus.FEEDBACK_INCOMPLETE);
        const classStringAllAnswered = service.getIconClassForAggregatedStatus(
            AggregatedRequestStatus.ALL_ANSWERED);

        // assert
        expect(classStringNewRequest).toEqual('mark_email_unread');
        expect(classStringFeedbackIncomplete).toEqual('mail');
        expect(classStringAllAnswered).toEqual('mark_email_read');
    });

    it('should get no icon for aggregated status (overview)', () => {
        // arrange & act
        const noIcon = service.getIconClassForAggregatedStatus(undefined);

        // assert
        expect(noIcon).toEqual('');
    });

    it('should get the background color for status (customer details)', () => {
        // act & arrange
        const colorNewRequest = service.getBackgroundColorClassForStatus(RequestStatus.NEW_REQUEST);
        const colorPositiveFeedback = service.getBackgroundColorClassForStatus(RequestStatus.POSITIVE_FEEDBACK);
        const colorNegativeFeedback = service.getBackgroundColorClassForStatus(RequestStatus.NEGATIVE_FEEDBACK);
        const colorPositiveFeedbackConfirmed =
            service.getBackgroundColorClassForStatus(RequestStatus.POSITIVE_FEEDBACK_CONFIRMED);
        const colorPositiveFeedbackRejected =
            service.getBackgroundColorClassForStatus(RequestStatus.POSITIVE_FEEDBACK_REJECTED);
        const colorNoFeedbackRejected =
            service.getBackgroundColorClassForStatus(RequestStatus.NO_FEEDBACK_REJECTED);

        // assert
        expect(colorNewRequest).toEqual('');
        expect(colorPositiveFeedback).toEqual('color-positive-feedback');
        expect(colorNegativeFeedback).toEqual('color-negative-feedback');
        expect(colorPositiveFeedbackConfirmed).toEqual('color-positive-feedback');
        expect(colorPositiveFeedbackRejected).toEqual('color-positive-feedback');
        expect(colorNoFeedbackRejected).toEqual('');
    });

    it('should get no background color for status (customer details)', () => {
        // act & arrange
        const noColor = service.getBackgroundColorClassForStatus(undefined)

        // assert
        expect(noColor).toEqual("");
    });

    it('should get the background color for aggregated status (overview)', () => {
        // act & arrange
        const colorNewRequest =
            service.getBackgroundColorClassForAggregatedStatus(AggregatedRequestStatus.NEW_REQUEST);
        const colorFeedbackIncomplete =
            service.getBackgroundColorClassForAggregatedStatus(AggregatedRequestStatus.FEEDBACK_INCOMPLETE);
        const colorAllAnswered =
            service.getBackgroundColorClassForAggregatedStatus(AggregatedRequestStatus.ALL_ANSWERED);

        // assert
        expect(colorNewRequest).toEqual('');
        expect(colorFeedbackIncomplete).toEqual('color-feedback-incomplete');
        expect(colorAllAnswered).toEqual('color-all-answered');
    });

    it('should get no background color for aggregated status (overview)', () => {
        // act & arrange
        const noColor = service.getBackgroundColorClassForAggregatedStatus(undefined);

        // assert
        expect(noColor).toEqual('');
    });

    it('should get the background color for customer (details)', () => {
        // act & assert
        const colorRequested = service.getBackgroundColorClassForCustomer(requestedCustomer, dataWithNewRequests);

        // assert
        expect(colorRequested).toEqual('');
    });

    it('should get no background color for customer (details)', () => {
        // act & assert
        const colorUnrequested = service.getBackgroundColorClassForCustomer(unrequestedCustomer, dataWithNewRequests);
        const undefinedRequest = service.getBackgroundColorClassForCustomer(requestedCustomer, undefined);

        // assert
        expect(colorUnrequested).toEqual('');
        expect(undefinedRequest).toEqual('');
    });

});
