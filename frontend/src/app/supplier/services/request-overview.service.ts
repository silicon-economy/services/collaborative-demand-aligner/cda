/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {AggregatedRequestStatus} from "../model/enum";
import {Injectable} from "@angular/core";
import {RequestOverviewData} from "../model";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";
import {ConfigurationService} from "../../common/services/configuration.service";
import {createHttpOptions} from "../../common/configuration/http-options";
import {OverviewService} from "../../common/services/overview-service";

/**
 * Internal interface for keeping the table data all together in one object
 *
 * @author Bernd Breitenbach
 */
export interface RequestOverviewTableData {
    columns: string[];
    rows: RequestCellData[][];
}

/**
 * Interface for keeping data for one table cell
 *
 * @author Bernd Breitenbach
 */
export interface RequestCellData {
    articleNumber?: string;
    content?: string;
    week?: number;
    year?: number;
    status?: AggregatedRequestStatus;
    requestsCompleted?: boolean;
}

@Injectable({
    providedIn: 'root',
})
export class RequestOverviewService extends OverviewService {
    private httpOptions = createHttpOptions();

    constructor(
        private http: HttpClient,
        private configService: ConfigurationService,
    ) {
        super();
    }

    /**
     * Fetches all request overviews.
     *
     * @returns An observable that contains a list of request overviews.
     */
    getRequestOverviews(): Observable<RequestOverviewData> {
        const url = this.configService.requestOverviewApiUrl;
        return this.http.get<RequestOverviewData>(url, this.httpOptions).pipe(
            map((e: any) => {
                e.requestOverviewItems = new Map(Object.entries(e.requestOverviewItems));
                return e;
            })
        );
    }

    /**
     * Converts a RequestOverviewData object to OverviewTableData suitable for displaying in a table
     *
     * @param data
     */
    toDataSourceArray(data: RequestOverviewData): RequestOverviewTableData {
        const weeks = new Map<string, number>();
        const res: RequestOverviewTableData = {columns: [], rows: []};
        const columnWeeks = new Map<string, number>();

        data.requestOverviewItems.forEach((value, key) => {
            value.forEach((e) => {
                weeks.set(`${e.year}-${e.week}`, 0);
                columnWeeks.set(`${e.year}-${e.week}`, e.week);
            });
        });

        // Fills header row with article number and request week
        const arr = this.fillHeaderRowWithArticleNumberAndWeek(weeks, res, columnWeeks);

        data.requestOverviewItems.forEach((value, key) => {
            const row = new Array<RequestCellData>(arr.length + 1);
            // Preset with NEW_REQUEST (happens to avoid null pointer with empty cells)
            let i;
            for (i = 1; i < arr.length + 1; i++) {
                row[i] = {content: '', status: AggregatedRequestStatus.ALL_ANSWERED};
            }

            this.fillRowsWithContent(row, key, value, weeks);
            res.rows.push(row);
        });
        return res;
    }
}
