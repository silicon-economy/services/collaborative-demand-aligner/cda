/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { CompanyData } from "../../common/model";
import { AggregatedRequestStatus, RequestStatus } from "../model/enum";
import { Request, RequestDetailData } from "../model";

@Injectable({
    providedIn: 'root'
})
export class ColorAndIconService {

    /**
     * Assigns background colors for request detail components depending on their status
     *
     * Assign classes (in this case colors) dynamically to specific cells via html-classes
     * (see [ngClass] in matColumnDef="Total Requests" in request-detail.component.html for reference).
     */
    statusToBackgroundColorMap = new Map<RequestStatus | undefined, string>([
        [undefined, ''],
        [RequestStatus.POSITIVE_FEEDBACK, 'color-positive-feedback'],
        [RequestStatus.POSITIVE_FEEDBACK_CONFIRMED, 'color-positive-feedback'],
        [RequestStatus.POSITIVE_FEEDBACK_REJECTED, 'color-positive-feedback'],
        [RequestStatus.NEGATIVE_FEEDBACK, 'color-negative-feedback'],
    ]);

    /**
     * Assigns background colors for request components (overview) depending on their aggregated status
     *
     * Assign classes (in this case colors) dynamically to specific cells via html-classes
     * (see [ngClass] in matColumnDef="Total Requests" in request.component.html for reference).
     */
    aggregatedStatusToBackgroundColorMap = new Map<AggregatedRequestStatus | undefined, string>([
        [undefined, ''],
        [AggregatedRequestStatus.FEEDBACK_INCOMPLETE, 'color-feedback-incomplete'],
        [AggregatedRequestStatus.ALL_ANSWERED, 'color-all-answered'],
    ]);

    /**
     * Assigns icons for request detail components depending on their status
     *
     * Assign classes (in this case icons) dynamically to specific cells via html-classes
     * (see [ngClass] in matColumnDef="Total Requests" in request-detail.component.html for reference).
     */
    statusToIconMap = new Map<RequestStatus | undefined, string>([
        [undefined, ''],
        [RequestStatus.NEW_REQUEST, 'mark_email_unread'],
        [RequestStatus.POSITIVE_FEEDBACK_CONFIRMED, 'check_circle'],
        [RequestStatus.POSITIVE_FEEDBACK_REJECTED, 'do_not_disturb_on'],
        [RequestStatus.NO_FEEDBACK_REJECTED, 'do_not_disturb_on'],
    ]);

    /**
     * Assigns icons for request components (overview) depending on their aggregated status
     *
     * assign classes (in this case icons) dynamically to specific cells via html-classes
     * (see [ngClass] in matColumnDef="Total Requests" in request.component.html for reference).
     */
    aggregatedStatusToIconMap = new Map<AggregatedRequestStatus | undefined, string>([
        [undefined, ''],
        [AggregatedRequestStatus.NEW_REQUEST, 'mark_email_unread'],
        [AggregatedRequestStatus.FEEDBACK_INCOMPLETE, 'mail'],
        [AggregatedRequestStatus.ALL_ANSWERED, 'mark_email_read'],
    ]);

    /**
     * Assigns colors for any icon by its name
     *
     * Assign classes (in this case colors) dynamically to specific icons via html-classes
     */
    iconToIconColorMap = new Map<string | undefined, string>([
        [undefined, ''],
        ['check_circle', 'check-icon'],
        ['do_not_disturb_on', 'disturb-icon'],
        ['mark_email_unread', 'unread-icon'],
        ['mail', 'incomplete-icon'],
        ['mark_email_read', 'read-icon'],
    ]);

    /**
     * Returns the background color for a given status.
     *
     * @param status - {@link RequestStatus}
     * @returns class name of a color class (see styles.scss)
     */
    getBackgroundColorClassForStatus(status?: RequestStatus): string {
        return this.statusToBackgroundColorMap.get(status) || '';
    }

    /**
     * Returns the background color for a given aggregated status.
     *
     * @param status - {@link AggregatedRequestStatus}
     * @returns class name of a color class (see styles.scss)
     */
    getBackgroundColorClassForAggregatedStatus(status?: AggregatedRequestStatus): string {
        return this.aggregatedStatusToBackgroundColorMap.get(status) || '';
    }

    /**
     * For a given customer, the status of a possible request sent to that customer is mapped to a
     * background color class.
     *
     * @param customer - process a request to this customer
     * @param data - {@link RequestDetailData}
     * @returns class name of a color class or an empty sting if no request was found
     */
    getBackgroundColorClassForCustomer(customer: CompanyData, data: RequestDetailData | undefined): string {
        // search for a request sent to this customer
        const requestFound = this.findRequestSentToCustomer(customer, data);
        // return no color of no request was found
        return this.statusToBackgroundColorMap.get(requestFound?.status) || '';
    }

    /**
     * For a given customer, the status of a possible request sent to that customer is mapped to a
     * Google icon class.
     *
     * If there is no icon, an empty string is returned.
     *
     * @param customer the customer
     * @param data searches for the request in this {@link RequestDetailData}
     * @returns related icon class
     */
    getIconClassForCustomer(customer: CompanyData, data: RequestDetailData | undefined): string {
        // search for a request sent to this customer
        const requestFound = this.findRequestSentToCustomer(customer, data);
        // return no icon if no request was found
        return this.statusToIconMap.get(requestFound?.status) || '';
    }

    /**
     * Returns the Google icon class for a given aggregated status.
     *
     * If there is no icon, an empty string is returned.
     *
     * @param status aggregated request status
     * @returns icon class
     */
    getIconClassForAggregatedStatus(status?: AggregatedRequestStatus): string {
        return this.aggregatedStatusToIconMap.get(status) || '';
    }

    /**
     * Returns the icon color class for an icon retrieved via {@link getIconClassForCustomer}.
     *
     * @param customer the customer the icon is for
     * @param data searches for the request in this {@link RequestDetailData}
     * @returns color class for icon
     */
    getIconColorClassForCustomer(customer: CompanyData, data: RequestDetailData | undefined): string {
        return this.iconToIconColorMap.get(
            this.getIconClassForCustomer(customer, data)
        ) || '';
    }

    /**
     * Returns the icon color class for a given aggregated status.
     *
     * @param status - {@link AggregatedRequestStatus}
     * @return color class for icon
     */
    getIconColorClassForAggregatedStatus(status?: AggregatedRequestStatus): string {
        return this.iconToIconColorMap.get(this.getIconClassForAggregatedStatus(status))
            || '';
    }

    /**
     * Searches for a request that was sent to the specified customer.
     *
     * @param customer as related customer
     * @param data search for a request in this {@link RequestDetailData}
     * @returns the request sent, or undefined if no request was found
     */
    private findRequestSentToCustomer(
        customer: CompanyData,
        data: RequestDetailData | undefined
    ): Request | undefined {
        return data?.requests.filter(
            (request) => request.customer.id === customer.id
        )[0];
    }
}
