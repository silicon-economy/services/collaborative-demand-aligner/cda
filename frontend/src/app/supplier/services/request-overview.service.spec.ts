/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RequestOverviewItem, RequestOverviewData } from '../model';
import { AggregatedRequestStatus } from '../model/enum';
import { ConfigurationService } from '../../common/services/configuration.service';

import { RequestOverviewService } from './request-overview.service';

describe('RequestOverviewService', () => {
  let service: RequestOverviewService;

  let httpClient: HttpTestingController;
  let configService: ConfigurationService;

  const TEST_DATA: RequestOverviewData = {
    requestOverviewItems: new Map<string, RequestOverviewItem[]>([
      [
        '0815',
        [
          {
            year: 2021,
            week: 52,
            quantity: 12,
            status: AggregatedRequestStatus.ALL_ANSWERED,
            requestsCompleted: true,
          },
          {
            year: 2022,
            week: 1,
            quantity: 17,
            status: AggregatedRequestStatus.ALL_ANSWERED,
            requestsCompleted: true,
          },
          {
            year: 2022,
            week: 2,
            quantity: 22,
            status: AggregatedRequestStatus.ALL_ANSWERED,
            requestsCompleted: true,
          },
          {
            year: 2022,
            week: 4,
            quantity: 19,
            status: AggregatedRequestStatus.ALL_ANSWERED,
            requestsCompleted: true,
          },
          {
            year: 2022,
            week: 5,
            quantity: 23,
            status: AggregatedRequestStatus.ALL_ANSWERED,
            requestsCompleted: true,
          },
        ],
      ],
      [
        '0894',
        [
          {
            year: 2021,
            week: 52,
            quantity: 0,
            status: AggregatedRequestStatus.ALL_ANSWERED,
            requestsCompleted: true,
          },
          {
            year: 2022,
            week: 1,
            quantity: 33,
            status: AggregatedRequestStatus.ALL_ANSWERED,
            requestsCompleted: true,
          },
          {
            year: 2022,
            week: 2,
            quantity: 21,
            status: AggregatedRequestStatus.ALL_ANSWERED,
            requestsCompleted: true,
          },
          {
            year: 2022,
            week: 3,
            quantity: 27,
            status: AggregatedRequestStatus.ALL_ANSWERED,
            requestsCompleted: true,
          },
          {
            year: 2022,
            week: 4,
            quantity: 31,
            status: AggregatedRequestStatus.ALL_ANSWERED,
            requestsCompleted: true,
          },
          {
            year: 2022,
            week: 5,
            quantity: 32,
            status: AggregatedRequestStatus.ALL_ANSWERED,
            requestsCompleted: true,
          },
        ],
      ],
      [
        '4711',
        [
          {
            year: 2021,
            week: 52,
            quantity: 51,
            status: AggregatedRequestStatus.ALL_ANSWERED,
            requestsCompleted: true,
          },
          {
            year: 2022,
            week: 1,
            quantity: 77,
            status: AggregatedRequestStatus.ALL_ANSWERED,
            requestsCompleted: true,
          },
          {
            year: 2022,
            week: 2,
            quantity: 21,
            status: AggregatedRequestStatus.ALL_ANSWERED,
            requestsCompleted: true,
          },
          {
            year: 2022,
            week: 3,
            quantity: 60,
            status: AggregatedRequestStatus.ALL_ANSWERED,
            requestsCompleted: true,
          },
          {
            year: 2022,
            week: 4,
            quantity: 31,
            status: AggregatedRequestStatus.ALL_ANSWERED,
            requestsCompleted: true,
          },
        ],
      ],
    ]),
  };


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ConfigurationService],
    });
    httpClient = TestBed.inject(HttpTestingController);
    configService = TestBed.inject(ConfigurationService);
    service = TestBed.inject(RequestOverviewService);

  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  it('should get all request-overviews', () => {
    // act
    service.getRequestOverviews().subscribe((data: RequestOverviewData) => {
      expect(data).toEqual(TEST_DATA);

      // assert
      httpClient
        .expectOne(configService.demandOverviewApiUrl)
        .event(new HttpResponse<{}>({ body: TEST_DATA }));
    });
  });

  it('should convert request overview data to row data', () => {
    // act
    const cdata = service.toDataSourceArray(TEST_DATA);

    // assert
    expect(cdata.columns.length).toEqual(7);
    expect(cdata.rows.length).toEqual(3);
    cdata.rows.forEach((row) => {
      expect(row.length).toEqual(7);
    });
  });
});
