/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { createHttpOptions } from "../../common/configuration/http-options";
import { HttpClient } from "@angular/common/http";
import { ConfigurationService } from "../../common/services/configuration.service";
import { Observable } from "rxjs";
import {ConfirmationResponse, RequestDetailData} from "../model";

@Injectable({
  providedIn: 'root'
})
export class RequestDetailService {
  private httpOptions = createHttpOptions();

  constructor(
      private http: HttpClient,
      private configService: ConfigurationService
  ) {}

  /**
   * Fetches a request detail.
   *
   * @returns An observable that contains an object of request detail data.
   */
  getRequestDetail(
      year: number,
      week: number,
      articleNumber: string
  ): Observable<RequestDetailData> {
    const url =
        this.configService.requestDetailApiUrl +
        `${year}-${week}/${articleNumber}`;
    return this.http.get<RequestDetailData>(url, this.httpOptions);
  }
  
  createRequest(confirmationResponse: ConfirmationResponse): Observable<string> {
    const url = this.configService.sendConfirmationApiUrl;
    return this.http.post<string>(url, confirmationResponse);
  }
}
