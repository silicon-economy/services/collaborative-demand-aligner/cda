/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { RequestStatus } from "./enum";
import { CompanyData, WeeklyData } from "../../common/model";

/**
 * Request data
 *
 * @author Till-Philipp Patron
 * @author Fabian Serves
 */
export interface Request extends WeeklyData {

    /**
     * The request uuid
     */
    id: string;

    /**
     * The article number of the requested article
     */
    articleNumber: string;

    /**
     * The quantity
     */
    quantity: number;

    /**
     * The customer data
     */
    customer: CompanyData;

    /**
     * The request status
     */
    status: RequestStatus;

}
