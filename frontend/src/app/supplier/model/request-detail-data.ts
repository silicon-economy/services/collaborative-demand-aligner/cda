/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {WeeklyData} from "../../common/model";
import {Request} from "./request";
import { AggregatedRequestStatus } from "./enum";

/**
 * Detail data of requests regarding a given combination of year, week, and article.
 *
 * @author Till-Philipp Patron
 * @author Fabian Serves
 */
export interface RequestDetailData extends WeeklyData {

    /**
     *  The article number
     */
    articleNumber: string;

    /**
     *  The article description
     */
    articleDescription: string;

    /**
     *  The total number
     */
    totalQuantity: number;

    /**
     * The request data
     */
    requests: Request[];

    /**
     * The composite status for the total supply
     */
    totalStatus: AggregatedRequestStatus;

    /**
     * The article unit of this supply
     */
    unit: string;

}
