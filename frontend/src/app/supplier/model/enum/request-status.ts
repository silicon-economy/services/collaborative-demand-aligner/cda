/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Statuses for {@link Request Request}.
 *
 * @author Till-Philipp Patron
 * @author Fabian Serves
 */
export enum RequestStatus {

    NEW_REQUEST = "NEW_REQUEST",
    POSITIVE_FEEDBACK = 'POSITIVE_FEEDBACK',
    NEGATIVE_FEEDBACK = 'NEGATIVE_FEEDBACK',
    POSITIVE_FEEDBACK_CONFIRMED = 'POSITIVE_FEEDBACK_CONFIRMED',
    POSITIVE_FEEDBACK_REJECTED = 'POSITIVE_FEEDBACK_REJECTED',
    NO_FEEDBACK_REJECTED = 'NO_FEEDBACK_REJECTED',

}
