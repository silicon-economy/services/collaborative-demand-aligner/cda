/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Aggregated statuses for {@link RequestDetailData Request Detail Data}.
 *
 * These are the statuses concerning the entirety of requests for a specific week, year, and
 * article. It is aggregated by the statuses of multiple Requests.
 *
 * These aggregated request statuses are use for the {@link RequestOverviewItem RequestOverviewItem}.
 *
 * @see RequestStatus
 * @author Till-Philipp Patron
 * @author Fabian Serves
 */
export enum AggregatedRequestStatus {

    FEEDBACK_INCOMPLETE = "FEEDBACK_INCOMPLETE",
    NEW_REQUEST = "NEW_REQUEST",
    ALL_ANSWERED = "ALL_ANSWERED",

}
