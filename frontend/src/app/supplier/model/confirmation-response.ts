/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * The response of the supplier to an request being a confirmation or rejection.
 *
 * @author Steffen Biehs
 */
export interface ConfirmationResponse {
    
    /**
     * The id of the Request
     */
    requestId: string;
    
    /**
     * Status whether the request is confirmed or rejected
     */
    confirmed: boolean;
}
