/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {WeeklyData} from "../../common/model";
import {AggregatedRequestStatus} from "./enum";

/**
 * The request data for a given combination of year, week and article.
 *
 * @author Till-Philipp Patron
 * @author Fabian Serves
 */
export interface RequestOverviewItem extends WeeklyData {

    /** The quantity for a given year/week for a certain article */
    quantity: number;

    /** The aggregated status for this item */
    status: AggregatedRequestStatus;

    /** If this item is completed or the opportunity for sending requests is given */
    requestsCompleted: boolean;

}
