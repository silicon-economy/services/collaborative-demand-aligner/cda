/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {RequestOverviewItem} from "./request-overview-item";

/**
 * Data that belongs to the request overview.
 *
 * @author Till-Philipp Patron
 * @author Fabian Serves
 */
export interface RequestOverviewData {

    /**
     *  The map from article numbers to lists of request overview item data
     */
    requestOverviewItems: Map<string, RequestOverviewItem[]>;
}
