/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FlexLayoutModule } from '@angular/flex-layout';

import { MatToolbarModule } from "@angular/material/toolbar";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatIconModule } from "@angular/material/icon";
import { MatListModule } from "@angular/material/list";
import { MatButtonModule } from "@angular/material/button";
import { DemandOverviewService } from './producer/services/demand-overview.service';
import { HttpClientModule } from '@angular/common/http';
import { DemandDetailService } from "./producer/services/demand-detail.service";
import { InjectableRxStompConfig, RxStompService, rxStompServiceFactory } from '@stomp/ng2-stompjs';
import { MyStompConfig } from './common/configuration/mystompconfig';
import { Configuration } from './common/configuration/app.configuration';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { RequestOverviewService } from "./supplier/services/request-overview.service";
import { RequestDetailService } from "./supplier/services/request-detail.service";
import { MatSnackBarModule} from "@angular/material/snack-bar";

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatToolbarModule,
        FlexLayoutModule,
        MatSidenavModule,
        MatIconModule,
        MatListModule,
        MatButtonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        DragDropModule,
        MatSnackBarModule,
    ],
    providers: [
        DemandOverviewService,
        DemandDetailService,
        RequestOverviewService,
        RequestDetailService,
        {
            provide: InjectableRxStompConfig,
            useClass: MyStompConfig,
            deps: [Configuration]
        },
        {
            provide: RxStompService,
            useFactory: rxStompServiceFactory,
            deps: [InjectableRxStompConfig]
        },
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
