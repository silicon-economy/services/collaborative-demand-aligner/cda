/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class Configuration {


  public apiUrl: string;

  constructor() {
    this.apiUrl = (window as any)?.env?.API_URL || (window.location.origin + '/api');
  }

  stompBrokerUrl(): string {
    return `${environment.wsProtocol}://${window.location.hostname}:${window.location.port}/ws`; 
  }

  apiURLFor(trailing: string): string {
    return `${this.apiUrl}/${trailing}`;
  }
}
