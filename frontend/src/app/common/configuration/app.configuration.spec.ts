/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { Configuration } from './app.configuration';


describe('Configuration', () => {
  let testObject: Configuration;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
      ]
    });
    testObject = TestBed.inject(Configuration);
  });

  it('should be created', () => {
    expect(testObject).toBeTruthy();
  });

  it('should return URL', () => {
    let data = testObject.apiURLFor('');
    expect(data).toEqual('http://localhost:9876/api/');
  });
});