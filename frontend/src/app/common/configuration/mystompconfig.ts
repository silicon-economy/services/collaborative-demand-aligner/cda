/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { InjectableRxStompConfig } from '@stomp/ng2-stompjs';
import { Configuration } from './app.configuration';


export class MyStompConfig extends InjectableRxStompConfig {

  constructor(private config: Configuration) {
    super();
    this.heartbeatIncoming = 0;
    this.heartbeatOutgoing = 20000; // Typical value 20000 - every 20 seconds
    this.brokerURL = config.stompBrokerUrl();
  }
}