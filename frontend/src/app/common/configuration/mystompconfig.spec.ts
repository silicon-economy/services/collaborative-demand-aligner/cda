/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { environment } from 'src/environments/environment';
import { Configuration } from './app.configuration';
import { MyStompConfig } from './mystompconfig';


describe('MyStompConfig', () => {
  let testClass: MyStompConfig;
  const res = `${environment.wsProtocol}://${window.location.hostname}:${window.location.port}/ws`;

  beforeEach(() => {
    testClass = new MyStompConfig(new Configuration());
  });

  it('should create', () => {
    expect(testClass).toBeTruthy();
  });

  it('should have the correct values', () => {
    expect(testClass.heartbeatIncoming).toEqual(0);
    expect(testClass.heartbeatOutgoing).toEqual(20000);
    expect(testClass.brokerURL).toEqual(res);
  });
});
