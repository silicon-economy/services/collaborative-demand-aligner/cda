/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {MatSnackBar} from "@angular/material/snack-bar";
import {Injectable} from "@angular/core";

/**
 * Service for providing a popup window
 *
 * To implement this service, simply import it, inject it and
 *
 * @author T. Patron
 */
@Injectable({providedIn: 'root',
})
export class PopupWindowService {
    constructor(
        private snackBar: MatSnackBar) {
    }

    errorDuration: number = 3000;

    /**
     * Provides an error popup window with given message and preset duration that is also logged in console.
     * Simply call this method and fill it with an error-string to show the error in a popup window.
     *
     * @param err the message that will be shown in the popup
     */
    errorPopup(err: any) {
        console.log(`error:`, err);
        this.snackBar.open(`Error: ${err.status}:${err.statusText}`, '', {duration: this.errorDuration})
    }

    /**
     * Provides a raw popup that can be configured further with custom message and duration.
     * This method can be used for any type of popup window since it is not pre-defined.
     *
     * @param message that is shown in the popup window.
     * @param duration for which the popup window will be displayed.
     */
    popup(message: string, duration: number) {
        this.snackBar.open(message, '', {duration: duration});
    }
}
