/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';

/**
 * Service for providing all kinds of configuration values
 *
 * @author Anton Thomas Scholz, Bernd Breitenbach
 */
@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  get demandOverviewApiUrl(): string {
    // we don't take settings from environment anymore we're using reverse proxying
    // no more hassle with CORS errors ;-)
    return `${location.origin}/api/v2/producer/demand/overview`;
  }

  get demandDetailApiUrl(): string {
    return `${location.origin}/api/v2/producer/demand/detail-view/`;
  }

  get createRequestApiUrl(): string {
    return `${location.origin}/api/v2/producer/requests/`;
  }

  get requestOverviewApiUrl(): string {
    return `${location.origin}/api/v2/supplier/requests/overview`;
  }

  get requestDetailApiUrl(): string {
    return `${location.origin}/api/v2/supplier/requests/detail-view/`;
  }
  
  get sendConfirmationApiUrl(): string {
    return `${location.origin}/api/v2/supplier/requests/confirmation`;
  }


}
