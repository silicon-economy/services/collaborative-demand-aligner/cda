/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Injectable} from "@angular/core";

/**
 * Service to convert dates in different formats and data types and vice versa.
 *
 * @author Steffen Biehs
 */
@Injectable({
    providedIn: 'root'
})
export class DateCalculationService {

    /**
     * Generates a date string like `09.05.2022 - 13.05.2022` for a given week number and year.
     *
     * @param week - Week number as number
     * @param year - Year as number
     * @return string Formatted date string
     */
    getDateStringForWeekAndYear(week: number, year: number): string {
        const firstDayOfWeek = this.getStartOfWeek(week, year);
        const monday = firstDayOfWeek.toLocaleDateString('de-DE', {
            day: '2-digit',
            month: '2-digit',
            year: 'numeric',
        });
        const friday = new Date(
            firstDayOfWeek.setDate(firstDayOfWeek.getDate() + 4)
        ).toLocaleDateString('de-DE', {
            day: '2-digit',
            month: '2-digit',
            year: 'numeric',
        });
        return monday + ' - ' + friday;
    }

    /**
     * Calculates the first day of a given week and year.
     *
     * @param week - Week number as number
     * @param year - Year as number
     * @return Day as {@link Date} object
     */
    getStartOfWeek(week: number, year: number): Date {
        const simple = new Date(year, 0, 1 + (week - 1) * 7);
        const dayOfWeek = simple.getDay();
        const weekStart = simple;
        if (dayOfWeek <= 4)
            weekStart.setDate(simple.getDate() - simple.getDay() + 1);
        else weekStart.setDate(simple.getDate() + 8 - simple.getDay());
        return weekStart;
    }

    /**
     * Generates a date string like `09.05.2022 - 13.05.2022` for today's week number and year.
     *
     * @return string Formatted date string
     */
    getDateStringForToday(): string {
        const today = new Date();
        const week = this.getWeekFromDate(today);
        const year = today.getUTCFullYear();

        return this.getDateStringForWeekAndYear(week, year);
    }

    /**
     * Calculates the current week from a given date
     *
     * @param today 's date
     */
    getWeekFromDate(today: Date): number {
        const d = new Date(
            Date.UTC(today.getFullYear(), today.getMonth(), today.getDate())
        );
        const dayNum = d.getUTCDay() || 7;
        d.setUTCDate(d.getUTCDate() + 4 - dayNum);
        const yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
        return Math.ceil(((d.getTime() - yearStart.getTime()) / 86400000 + 1) / 7);
    }

}
