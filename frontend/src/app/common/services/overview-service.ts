/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Injectable} from "@angular/core";
import {DemandCellData, DemandOverviewTableData} from "../../producer/services/demand-overview.service";
import {RequestCellData, RequestOverviewTableData} from "../../supplier/services/request-overview.service";
import {RequestOverviewItem} from "../../supplier/model";
import {DemandOverviewItem} from "../../producer/model";

/**
 * Service to provide methods used by the demand and request overview services
 *
 * Should be extended by {@link DemandOverviewService} and {@link RequestOverviewService}.
 */
@Injectable({
    providedIn: 'root',
})
export class OverviewService {

    /**
     * Fills header row with article number and weeks
     *
     * @param weeks map
     * @param res overview table data
     * @param columnWeeks column weeks
     * @protected
     */
    protected fillHeaderRowWithArticleNumberAndWeek(
        weeks: Map<string, number>,
        res: DemandOverviewTableData | RequestOverviewTableData,
        columnWeeks: Map<string, number>): string[] {
        const arr = Array.from(weeks.keys());
        arr.sort();
        if ("columns" in res) {
            res.columns[0] = 'Article No.';
        }
        arr.forEach((e, i) => {
            weeks.set(e, i + 1);
            if ("columns" in res) {
                res.columns[i + 1] = `${columnWeeks.get(e)}`;
            }
        });
        return arr;
    }

    /**
     * Fills rows with content
     *
     * This includes article numbers and requested items.
     *
     * @param row cell data array
     * @param key article number
     * @param value overview item array
     * @param weeks weeks
     * @protected
     */
    protected fillRowsWithContent(
        row: DemandCellData[] | RequestCellData[],
        key: string,
        value: DemandOverviewItem[] | RequestOverviewItem[],
        weeks: Map<string, number>) {
        row[0] = {content: key};
        value.forEach((e) => {
            const idx = weeks.get(`${e.year}-${e.week}`) as number;
            row[idx] = {
                content: `${e.quantity}`,
                week: e.week,
                year: e.year,
                articleNumber: key,
                status: e.status,
                requestsCompleted: e.requestsCompleted,
            };
        });
    }


}
