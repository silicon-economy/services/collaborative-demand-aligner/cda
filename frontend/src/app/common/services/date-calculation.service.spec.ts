/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { DateCalculationService } from './date-calculation-service';

describe('DateCalculationService', () => {
    let service: DateCalculationService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(DateCalculationService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should return first day of week', () => {
        // arrange
        const day = 9;
        const week = 19;
        const year = 2022;

        // act
        const result = service.getStartOfWeek(week, year);

        // assert
        expect(result.getDate()).toEqual(day);
    });

    it('should return first day of first week in year that is in year before', () => {
        // arrange
        const day = 30;
        const week = 1;
        const year = 2020;

        // act
        const result = service.getStartOfWeek(week, year);

        // assert
        expect(result.getDate()).toEqual(day);
    });

    it('should return first week in year', () => {
        // arrange
        const dateString = 'Sat Jan 8 2022 15:46:42 GMT+0200 (Mitteleuropäische Sommerzeit)';
        const date = new Date(dateString);
        const week = 1;

        // act
        const result = service.getWeekFromDate(date);

        // assert
        expect(result).toEqual(week);
    });

    it('should return last week in year', () => {
        // arrange
        const dateString = 'Sat Jan 1 2022 15:46:42 GMT+0200 (Mitteleuropäische Sommerzeit)';
        const date = new Date(dateString);
        const week = 52;

        // act
        const result = service.getWeekFromDate(date);

        // assert
        expect(result).toEqual(week);
    });

    it('should return correct date string format', () => {
        // arrange
        const prodWeek = '09.05.2022 - 13.05.2022';
        const week = 19;
        const year = 2022;

        // act
        const result = service.getDateStringForWeekAndYear(week, year);

        // assert
        expect(result).toEqual(prodWeek);
    });

    it('should return correct date string format for week in two different months', () => {
        // arrange
        const prodWeek = '31.01.2022 - 04.02.2022';
        const week = 5;
        const year = 2022;

        // act
        const result = service.getDateStringForWeekAndYear(week, year);

        // assert
        expect(result).toEqual(prodWeek);
    });

    it('should return correct date string format for last week in two different years', () => {
        // arrange
        const prodWeek = '28.12.2020 - 01.01.2021';
        const week = 53;
        const year = 2020;

        // act
        const result = service.getDateStringForWeekAndYear(week, year);

        // assert
        expect(result).toEqual(prodWeek);
    });

    it('should return correct date string format for first week in two different years', () => {
        // arrange
        const prodWeek = '30.12.2019 - 03.01.2020';
        const week = 1;
        const year = 2020;

        // act
        const result = service.getDateStringForWeekAndYear(week, year);

        // assert
        expect(result).toEqual(prodWeek);
    });

});