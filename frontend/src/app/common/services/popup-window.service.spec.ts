/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {TestBed} from '@angular/core/testing';
import {PopupWindowService} from "./popup-window.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {of} from "rxjs";

const mockMessage: string = "message";
const mockDuration: number = 5000;

class MatSnackBarStub{
    open(){
        return {
            onAction: () => of({})
        }
    }
}

describe('PopupWindowService', () => {
    let service: PopupWindowService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers : [ { provide: MatSnackBar , useClass: MatSnackBarStub }]
        }).compileComponents();
        service = TestBed.inject(PopupWindowService);

    })

    it('should be truthy', () => {
        expect(service).toBeTruthy();
    })

    it('should open an error popup window', () => {
        // arrange
        const snackBar = TestBed.inject(MatSnackBar);
        const spy = spyOn(snackBar, "open");

        // act
        service.errorPopup(mockMessage);

        // assert
        expect(spy).toHaveBeenCalled();
    });

    it('should open a raw popup window', () => {
        // arrange
        const snackBar = TestBed.inject(MatSnackBar);
        const spy = spyOn(snackBar, "open");

        // act
        service.popup(mockMessage, mockDuration);

        // assert
        expect(spy).toHaveBeenCalled();
    })
});
