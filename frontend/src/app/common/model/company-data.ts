/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Company data
 *
 * @author Bernd Breitenbach
 */
export interface CompanyData {
    /**
     *  Unique identifier for the company
     */
    id: string;
    
    /**
     * The company name (can be both: supplier or customer)
     */
    name: string;

}
