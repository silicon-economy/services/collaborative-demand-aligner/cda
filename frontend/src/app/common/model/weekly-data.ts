/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Interface for weekly data. 
 * Extend this with interface/classes for data storage
 * 
 * @author Bernd Breitenbach
 */
export interface WeeklyData {
    /**
     * The year for this data item
     */
    year: number;

    /**
     * The week for this data item
     */
    week: number;
}
