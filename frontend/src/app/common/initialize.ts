/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Message} from "@stomp/stompjs";
import {Subscription} from "rxjs";
import {RxStompService} from "@stomp/ng2-stompjs";
import {RequestsComponent} from "../supplier/pages/requests/requests.component";
import {DemandsComponent} from "../producer/pages/demands/demands.component";
import {PopupWindowService} from "./services/popup-window.service";
import {DemandDetailComponent} from "../producer/pages/demand-detail/demand-detail.component";
import { RequestDetailComponent } from "../supplier/pages/request-detail/request-detail.component";

/**
 * Abstract class that contains a single method to initialize a stomp subscription.
 */
export abstract class Initialize {

    static stomp(topicEvents: string[], eventSubscription$: Subscription, stompService: RxStompService,
                 component: RequestsComponent | DemandsComponent | DemandDetailComponent | RequestDetailComponent,
                 popupService: PopupWindowService) {
        topicEvents.forEach((event) => {
            eventSubscription$.add(
                stompService.watch(event).subscribe((message: Message) => {
                        if (message) {
                            component.refreshData();
                        }
                    },
                    (err) => {
                        popupService.errorPopup(err);
                    })
            );
        });
        component.refreshData();
    }

}
