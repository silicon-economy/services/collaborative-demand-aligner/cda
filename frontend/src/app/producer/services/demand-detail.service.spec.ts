/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpResponse } from '@angular/common/http';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { DemandDetailData } from '../model';
import { RequestInput } from 'src/app/producer/model/';
import { RequestStatus } from '../model/enum';

import { DemandDetailService } from './demand-detail.service';
import { DemandCellData } from './demand-overview.service';

describe('DemandDetailService', () => {
  let service: DemandDetailService;
  let httpClient: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(DemandDetailService);
    httpClient = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  // TODO: Fix test
  it('should get the requested detail data', () => {
    // arrange
    const argumentData: DemandCellData = {
      articleNumber: '5871',
      content: '',
      week: 15,
      year: 2022,
    };

    const expectedElement: DemandDetailData = {
      year: 2022,
      week: 5,
      totalQuantity: 5,
      requests: [
        {
          uuid: 'test',
          articleNumber: 'testArticleNo',
          quantity: 5,
          year: 2022,
          week: 6,
          supplierId: '2',
          status: RequestStatus.NO_RESPONSE,
        },
      ],
      articleNumber: '5871',
      articleDescription: 'dummy desc',
      demands: [
        {
          id: '5',
          productionNumber: '55',
          quantity: 5,
          deliveryDate: new Date(),
        },
      ],
      possibleSuppliers: [
        {
          id: '5',
          name: 'test',
        },
      ],
      totalStatus: RequestStatus.NO_RESPONSE,
      unit: 'unit',
      requestsCompleted: false,
    };

    // act
    service.getDemandDetail(2022, 15, '5871').subscribe((data) => {
      expect(data).toEqual(expectedElement);
    });

    // assert
    httpClient
      .expectOne(`${location.origin}/api/v2/producer/demand/detail-view/2022-15/5871`)
      .event(new HttpResponse<{}>({ body: expectedElement }));
  });

  it('should create a request', () => {
    // arrange
    const requestInput: RequestInput = {
      articleId: 'testArticleNumber',
      quantity: 17,
      supplierId: 'testSupplier',
      year: 2000,
      week: 2,
    };
    const mockRequestId: string = '6c8f7fbf-8ec8-46e4-b72e-e35d9c6e0b30';

    // act & assert
    service.createRequest(requestInput).subscribe((id) => {
      expect(id).toEqual(mockRequestId);
      httpClient
        .expectOne(`${location.origin}/api/v2/producer/requests`)
        .event(new HttpResponse<{}>({ body: mockRequestId }));
    });
  });
});
