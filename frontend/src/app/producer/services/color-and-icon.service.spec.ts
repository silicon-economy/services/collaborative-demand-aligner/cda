/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { ColorAndIconService } from './color-and-icon.service';
import { DemandDetailData } from '../model';
import {CompanyData} from "../../common/model";
import {RequestStatus} from "../model/enum";

const requestedSupplier: CompanyData = {
  id: 'Id1',
  name: 'Sup.1',
};

const unrequestedSupplier: CompanyData = {
  id: 'Id2',
  name: 'Sup.2',
};

const dataWithUncompletedRequests: DemandDetailData = {
  articleNumber: '45',
  articleDescription: 'test',
  totalQuantity: 5,
  demands: [],
  possibleSuppliers: [requestedSupplier, unrequestedSupplier],
  requests: [
    {
      uuid: 'ss',
      articleNumber: 'testArticleNo',
      quantity: 3,
      year: 2022,
      week: 6,
      supplierId: 'Id1',
      status: RequestStatus.NO_RESPONSE,
    },
  ],
  year: 2022,
  week: 10,
  totalStatus: RequestStatus.NO_RESPONSE,
  unit: 'unit',
  requestsCompleted: false,
};

const dataWithCompletedRequests: DemandDetailData = {
  articleNumber: '45',
  articleDescription: 'test',
  totalQuantity: 5,
  demands: [],
  possibleSuppliers: [requestedSupplier, unrequestedSupplier],
  requests: [
    {
      uuid: 'ss',
      articleNumber: 'testArticleNo',
      quantity: 3,
      year: 2022,
      week: 6,
      supplierId: 'testid',
      status: RequestStatus.POSITIVE_FEEDBACK_CONFIRMED,
    },
  ],
  year: 2022,
  week: 10,
  totalStatus: RequestStatus.POSITIVE_FEEDBACK_CONFIRMED,
  unit: 'unit',
  requestsCompleted: true,
};

describe('ColorAndIconService', () => {
  let service: ColorAndIconService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ColorAndIconService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get the icon class for supplier status', () => {
    // act
    const iconRetrievedFromMap = service.getIconForSupplierStatus(
      requestedSupplier,
      dataWithUncompletedRequests
    );
    const iconFromEdgeCase = service.getIconForSupplierStatus(
      unrequestedSupplier,
      dataWithCompletedRequests
    );

    // assert
    expect(iconRetrievedFromMap).toEqual('schedule');
    expect(iconFromEdgeCase).toEqual('do_not_disturb_on');
  });

  it('should get no icon class for supplier status', () => {
    // act
    const noIcon = service.getIconForSupplierStatus(
      unrequestedSupplier,
      dataWithUncompletedRequests
    );

    // assert
    expect(noIcon).toEqual('');
  });

  it('should get the icon color class for supplier status', () => {
    // act
    const iconRetrievedFromMap = service.getIconColorForSupplierStatus(
      requestedSupplier,
      dataWithUncompletedRequests
    );
    const iconFromEdgeCase = service.getIconColorForSupplierStatus(
      unrequestedSupplier,
      dataWithCompletedRequests
    );

    // assert
    expect(iconRetrievedFromMap).toEqual('schedule-icon');
    expect(iconFromEdgeCase).toEqual('disturb-icon');
  });

  it('should get no icon color class for supplier status', () => {
    // act
    const noIcon = service.getIconColorForSupplierStatus(
      unrequestedSupplier,
      dataWithUncompletedRequests
    );

    // assert
    expect(noIcon).toEqual('');
  });

  it('should get the icon color class for the status', () => {
    // arrange
    const status = RequestStatus.POSITIVE_FEEDBACK;
    const requestCompleted = false

    // act
    const iconColor = service.getIconColorForStatus(status, requestCompleted);

    // assert
    expect(iconColor).toEqual('rule-icon');
  });

  it('should get the correct icon for aggregated status', () => {
    const classString2 = service.getIconForAggregatedStatus(
      RequestStatus.NEGATIVE_FEEDBACK,
      false
    );
    const classString3 = service.getIconForAggregatedStatus(
      RequestStatus.NO_RESPONSE,
      true
    );
    const classString4 = service.getIconForAggregatedStatus(
      RequestStatus.NO_RESPONSE,
      false
    );
    const classString6 = service.getIconForAggregatedStatus(
      RequestStatus.POSITIVE_FEEDBACK,
      false
    );
    const classString7 = service.getIconForAggregatedStatus(
      RequestStatus.POSITIVE_FEEDBACK_CONFIRMED,
      true
    );

    // assert
    expect(classString2).toEqual('rule');
    expect(classString3).toEqual('schedule');
    expect(classString4).toEqual('rule');
    expect(classString6).toEqual('rule');
    expect(classString7).toEqual('check_circle');
  });

  it('should no icon for aggregated status', () => {
    const classString1 = service.getIconForAggregatedStatus(
      RequestStatus.NEGATIVE_FEEDBACK,
      true
    );
    const classString5 = service.getIconForAggregatedStatus(
      RequestStatus.POSITIVE_FEEDBACK,
      true
    );
    const classString8 = service.getIconForAggregatedStatus(
      RequestStatus.NO_INTERACTION,
      false
    );

    // assert
    expect(classString1).toEqual('');
    expect(classString5).toEqual('');
    expect(classString8).toEqual('');
  });

  it('should return correct background color class for supplier', () => {
    // act
    const colorClass1 = service.backgroundColorClassForSupplier(
      requestedSupplier,
      dataWithUncompletedRequests
    );
    const colorClass2 = service.backgroundColorClassForSupplier(
      unrequestedSupplier,
      dataWithUncompletedRequests
    );

    // assert
    expect(colorClass1).toEqual('color-no-response');
    expect(colorClass2).toEqual('');
  });

  it('should return no background color class for supplier', () => {
    // act
    const colorClass2 = service.backgroundColorClassForSupplier(
      unrequestedSupplier,
      dataWithUncompletedRequests
    );

    // assert
    expect(colorClass2).toEqual('');
  });
});
