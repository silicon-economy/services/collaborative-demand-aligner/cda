/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Injectable} from '@angular/core';
import {DemandDetailData, Request} from '../model';
import {CompanyData} from "../../common/model";
import {RequestStatus} from "../model/enum";

@Injectable({
    providedIn: 'root',
})
export class ColorAndIconService {

    /**
     * Assigns background colors for demand detail components depending on their status
     *
     * Assign classes (in this case colors) dynamically to specific cells via html-classes
     * (see [ngClass] in matColumnDef="Total Demands" in demand-detail.component.html for reference).
     */
    statusToBackgroundColorMap = new Map<RequestStatus | undefined, string>([
        [undefined, ''],
        [RequestStatus.NO_RESPONSE, 'color-no-response'],
        [RequestStatus.NO_RESPONSE_REJECTED, 'color-no-response'],
        [RequestStatus.POSITIVE_FEEDBACK, 'color-positive-feedback'],
        [RequestStatus.POSITIVE_FEEDBACK_CONFIRMED, 'color-positive-feedback'],
        [RequestStatus.POSITIVE_FEEDBACK_REJECTED, 'color-positive-feedback'],
        [RequestStatus.NEGATIVE_FEEDBACK, 'color-negative-feedback'],
    ]);

    /**
     * Assigns icons for demand detail components depending on their status
     *
     * Assign classes (in this case icons) dynamically to specific cells via html-classes
     * (see [ngClass] in matColumnDef="Total Demands" in demand-detail.component.html for reference).
     */
    statusToIconMap = new Map<RequestStatus | undefined, string>([
        [undefined, ''],
        [RequestStatus.NO_RESPONSE, 'schedule'],
        [RequestStatus.NO_RESPONSE_REJECTED, 'do_not_disturb_on'],
        [RequestStatus.POSITIVE_FEEDBACK_CONFIRMED, 'check_circle'],
        [RequestStatus.POSITIVE_FEEDBACK_REJECTED, 'do_not_disturb_on'],
    ]);

    /**
     * Assigns colors for any icon by its name
     *
     * Assign classes (in this case colors) dynamically to specific icons via html-classes
     */
    iconToIconColorMap = new Map<string | undefined, string>([
        [undefined, ''],
        ['check_circle', 'check-icon'],
        ['do_not_disturb_on', 'disturb-icon'],
        ['schedule', 'schedule-icon'],
        ['rule', 'rule-icon'],
    ]);

    /**
     * Returns the background color class for a given status.
     *
     * @param status - {@link RequestStatus}
     * @returns Class name of a color class
     */
    backgroundColorClassForStatus(status?: RequestStatus): string {
        return this.statusToBackgroundColorMap.get(status) || '';
    }

    /**
     * For a given supplier, the status of a possible request sent to that supplier is mapped to a
     * background color class.
     *
     * @param supplier - process a request to this supplier
     * @param data - {@link DemandDetailData}
     * @returns Class name of a color class
     */
    backgroundColorClassForSupplier(
        supplier: CompanyData,
        data: DemandDetailData | undefined
    ): string {
        // search for a request sent to this supplier
        const requestFound = this.findRequestSentToSupplier(supplier, data);
        // return no color if no request was found
        return this.statusToBackgroundColorMap.get(requestFound?.status) || '';
    }

    /**
     * For a given supplier, the status of a possible request sent to that supplier is mapped to a
     * Google icon class. If the supplier is unrequested and the requests are completed, show the
     * do-not-disturb icon.
     *
     * @param supplier the supplier
     * @param data searches for the request in this {@link DemandDetailData}
     * @returns icon class
     */
    getIconForSupplierStatus(supplier: CompanyData, data: DemandDetailData | undefined): string {
        // search for a request sent to this supplier
        const requestFound = this.findRequestSentToSupplier(supplier, data);
        if (!requestFound) {
            // display do-not-disturb-icon if the requests are completed
            if (data?.requestsCompleted) {
                return 'do_not_disturb_on';
            }
            return '';
        }
        return this.statusToIconMap.get(requestFound.status) || '';
    }

    /**
     * Returns the Google icon class for a given aggregated status and information if the requests are
     * completed. If there is no icon, an empty string is returned.
     *
     * @param status aggregated request status
     * @param requestCompleted if requests are completed
     * @returns icon class
     */
    getIconForAggregatedStatus(status: RequestStatus, requestCompleted: boolean): string {
        if (status) {
            // always return rule when requests are incomplete, except for when status is no interaction
            if (!requestCompleted && status !== RequestStatus.NO_INTERACTION) {
                return 'rule';
            }
            if (status === RequestStatus.POSITIVE_FEEDBACK_CONFIRMED) {
                return 'check_circle';
            }
            if (status === RequestStatus.NO_RESPONSE && requestCompleted) {
                return 'schedule';
            }
        }
        return '';
    }

    /**
     * Returns the icon color class for an icon retrieved via {@link getIconForSupplierStatus}.
     *
     * @param supplier the supplier the icon is for
     * @param data searches for the request in this {@link DemandDetailData}
     * @returns color class for icon
     */
    getIconColorForSupplierStatus(
        supplier: CompanyData,
        data: DemandDetailData | undefined
    ): string {
        return (
            this.iconToIconColorMap.get(
                this.getIconForSupplierStatus(supplier, data)
            ) || ''
        );
    }

    /**
     * Returns the icon color class for a given status.
     *
     * @param status - {@link RequestStatus}
     * @param requestCompleted if requests are completed
     * @return color class for icon
     */
    getIconColorForStatus(status: RequestStatus, requestCompleted: boolean): string {
        return this.iconToIconColorMap.get(this.getIconForAggregatedStatus(status, requestCompleted))
            || '';
    }

    /**
     * Searches for a request that was sent to the specified supplier.
     *
     * @param supplier the supplier
     * @param data search for a request in this {@link DemandDetailData}
     * @returns the request sent, or undefined if no request was found
     */
    private findRequestSentToSupplier(
        supplier: CompanyData,
        data: DemandDetailData | undefined
    ): Request | undefined {
        return data?.requests.filter(
            (request) => request.supplierId === supplier.id
        )[0];
    }
}
