/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { DemandOverviewService } from './demand-overview.service';
import { ConfigurationService } from '../../common/services/configuration.service';
import { DemandOverviewItem, DemandOverviewData } from '../model';
import { RequestStatus } from '../model/enum';
import { HttpResponse } from '@angular/common/http';

describe('DemandOverviewService', () => {
  let service: DemandOverviewService;
  let httpClient: HttpTestingController;
  let configService: ConfigurationService;

  const TEST_DATA: DemandOverviewData = {
    demandOverviewItems: new Map<string, DemandOverviewItem[]>([
      [
        '0815',
        [
          {
            year: 2021,
            week: 52,
            quantity: 12,
            status: RequestStatus.NO_RESPONSE,
            requestsCompleted: false,
          },
          {
            year: 2022,
            week: 1,
            quantity: 17,
            status: RequestStatus.NO_RESPONSE,
            requestsCompleted: false,
          },
          {
            year: 2022,
            week: 2,
            quantity: 22,
            status: RequestStatus.NO_RESPONSE,
            requestsCompleted: false,
          },
          {
            year: 2022,
            week: 4,
            quantity: 19,
            status: RequestStatus.NO_RESPONSE,
            requestsCompleted: false,
          },
          {
            year: 2022,
            week: 5,
            quantity: 23,
            status: RequestStatus.NO_RESPONSE,
            requestsCompleted: false,
          },
        ],
      ],
      [
        '0894',
        [
          {
            year: 2021,
            week: 52,
            quantity: 0,
            status: RequestStatus.NO_RESPONSE,
            requestsCompleted: false,
          },
          {
            year: 2022,
            week: 1,
            quantity: 33,
            status: RequestStatus.NO_RESPONSE,
            requestsCompleted: false,
          },
          {
            year: 2022,
            week: 2,
            quantity: 21,
            status: RequestStatus.NO_RESPONSE,
            requestsCompleted: false,
          },
          {
            year: 2022,
            week: 3,
            quantity: 27,
            status: RequestStatus.NO_RESPONSE,
            requestsCompleted: false,
          },
          {
            year: 2022,
            week: 4,
            quantity: 31,
            status: RequestStatus.NO_RESPONSE,
            requestsCompleted: false,
          },
          {
            year: 2022,
            week: 5,
            quantity: 32,
            status: RequestStatus.NO_RESPONSE,
            requestsCompleted: false,
          },
        ],
      ],
      [
        '4711',
        [
          {
            year: 2021,
            week: 52,
            quantity: 51,
            status: RequestStatus.NO_RESPONSE,
            requestsCompleted: false,
          },
          {
            year: 2022,
            week: 1,
            quantity: 77,
            status: RequestStatus.NO_RESPONSE,
            requestsCompleted: false,
          },
          {
            year: 2022,
            week: 2,
            quantity: 21,
            status: RequestStatus.NO_RESPONSE,
            requestsCompleted: false,
          },
          {
            year: 2022,
            week: 3,
            quantity: 60,
            status: RequestStatus.NO_RESPONSE,
            requestsCompleted: false,
          },
          {
            year: 2022,
            week: 4,
            quantity: 31,
            status: RequestStatus.NO_RESPONSE,
            requestsCompleted: false,
          },
        ],
      ],
    ]),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ConfigurationService],
    });
    service = TestBed.inject(DemandOverviewService);
    httpClient = TestBed.inject(HttpTestingController);
    configService = TestBed.inject(ConfigurationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get all demand-overviews', () => {
    // act
    service.getDemandOverviews().subscribe((data: DemandOverviewData) => {
      expect(data).toEqual(TEST_DATA);

      // assert
      httpClient
        .expectOne(configService.demandOverviewApiUrl)
        .event(new HttpResponse<{}>({ body: TEST_DATA }));
    });
  });

  it('should convert demand overview data to row data', () => {
    const cdata = service.toDataSourceArray(TEST_DATA);
    expect(cdata.columns.length).toEqual(7);
    expect(cdata.rows.length).toEqual(3);
    cdata.rows.forEach((row) => {
      expect(row.length).toEqual(7);
    });
  });
});
