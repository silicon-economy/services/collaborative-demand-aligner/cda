/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RequestInput } from 'src/app/producer/model/request-input';
import { DemandDetailData } from '../model';
import { ConfigurationService } from '../../common/services/configuration.service';
import { createHttpOptions } from '../../common/configuration/http-options';

@Injectable({
  providedIn: 'root',
})
export class DemandDetailService {
  private httpOptions = createHttpOptions();

  constructor(
    private http: HttpClient,
    private configService: ConfigurationService
  ) {}

  /**
   * Fetches a demand detail.
   *
   * @returns An observable that contains an object of demand detail data.
   */
  getDemandDetail(
    year: number,
    week: number,
    articleNumber: string
  ): Observable<DemandDetailData> {
    const url =
      this.configService.demandDetailApiUrl +
      `${year}-${week}/${articleNumber}`;
    return this.http.get<DemandDetailData>(url, this.httpOptions);
  }

  createRequest(requestInput: RequestInput): Observable<string> {
    const url = this.configService.createRequestApiUrl;
    return this.http.post<string>(url, requestInput);
  }
}
