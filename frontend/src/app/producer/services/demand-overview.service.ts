/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {DemandOverviewData} from '../model';
import {ConfigurationService} from '../../common/services/configuration.service';
import {createHttpOptions} from '../../common/configuration/http-options';
import {RequestStatus} from "../model/enum";
import {OverviewService} from "../../common/services/overview-service";

/**
 * Interface for keeping data for one table cell
 *
 * @author Bernd Breitenbach
 */
export interface DemandCellData {
    articleNumber?: string;
    content?: string;
    week?: number;
    year?: number;
    status?: RequestStatus;
    requestsCompleted?: boolean;
}

/**
 * Internal interface for keeping the table data all together in one object
 *
 * @author Bernd Breitenbach
 */
export interface DemandOverviewTableData {
    columns: string[];
    rows: DemandCellData[][];
}

/**
 * REST-Service that provides the ability to interact with the Shipments API.
 *
 * @author Anton Thomas Scholz, Bernd Breitenbach
 */
@Injectable({
    providedIn: 'root',
})
export class DemandOverviewService extends OverviewService {
    private httpOptions = createHttpOptions();

    constructor(
        private http: HttpClient,
        private configService: ConfigurationService,
    ) {
        super();
    }

    /**
     * Fetches all demand overviews.
     *
     * @returns An observable that contains a list of demand overviews.
     */
    getDemandOverviews(): Observable<DemandOverviewData> {
        const url = this.configService.demandOverviewApiUrl;
        return this.http.get<DemandOverviewData>(url, this.httpOptions).pipe(
            map((e: any) => {
                e.demandOverviewItems = new Map(Object.entries(e.demandOverviewItems));
                return e;
            })
        );
    }

    /**
     * Converts a DemandOverviewData object to OverviewTableData suitable for displaying in a table
     */
    toDataSourceArray(data: DemandOverviewData): DemandOverviewTableData {
        const weeks = new Map<string, number>();
        const res: DemandOverviewTableData = {columns: [], rows: []};
        const columnWeeks = new Map<string, number>();

        data.demandOverviewItems.forEach((value, key) => {
            value.forEach((e) => {
                weeks.set(`${e.year}-${e.week}`, 0);
                columnWeeks.set(`${e.year}-${e.week}`, e.week);
            });
        });

        // Fills header row with article number and request week
        const arr = this.fillHeaderRowWithArticleNumberAndWeek(weeks, res, columnWeeks);

        data.demandOverviewItems.forEach((value, key) => {
            const row = new Array<DemandCellData>(arr.length + 1);
            // Preset with POSITIVE_FEEDBACK (happens to avoid null pointer with empty cells)
            let i;
            for (i = 1; i < arr.length + 1; i++) {
                row[i] = {content: '', status: RequestStatus.POSITIVE_FEEDBACK};
            }

            // Fills rows with content
            this.fillRowsWithContent(row, key, value, weeks)
            res.rows.push(row);
        });
        return res;
    }
}
