/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {
  Component,
  ComponentFactory,
  ComponentFactoryResolver,
  HostListener,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
  ViewContainerRef,
} from '@angular/core';
import {
  DemandCellData,
  DemandOverviewService,
  DemandOverviewTableData,
} from 'src/app/producer/services/demand-overview.service';
import { RxStompService } from '@stomp/ng2-stompjs';
import { Subscription } from 'rxjs';
import { MatTable } from '@angular/material/table';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { DemandDetailComponent } from '../demand-detail/demand-detail.component';
import { RequestStatus } from "../../model/enum";
import { DateCalculationService } from "../../../common/services/date-calculation-service";
import { PopupWindowService } from '../../../common/services/popup-window.service';
import {Initialize} from "../../../common/initialize";
import {ColorAndIconService} from "../../services/color-and-icon.service";

@Component({
  selector: 'app-demands',
  templateUrl: './demands.component.html',
  styleUrls: ['./demands.component.scss'],
})
export class DemandsComponent implements OnInit, OnDestroy {
  // Prevent click event from routing to details, when user draged a table row

  drag = false;

  @HostListener('mousedown') onMouseDown() {
    this.drag = false;
  }
  @HostListener('mousemove') onMouseMove() {
    this.drag = true;
  }

  // Stomp
  topicEvents = ['/topic/refresh'];
  eventSubscription$ = new Subscription();

  // Drag Drop
  @ViewChild('table')
  table: MatTable<any> | undefined;

  // detail view
  @ViewChildren('tableRow', { read: ViewContainerRef })
  rowContainers: QueryList<ViewContainerRef> | undefined;

  selectedRow = -1;
  selectedColumn = -1;
  expandedRow = -1;

  toolbarDate: string = '';
  productionWeek: string = '';

  /**
   * Object that holds all available demands.
   */
  data: DemandOverviewTableData = { columns: [], rows: [] };

  constructor(
    private demandsService: DemandOverviewService,
    private stompService: RxStompService,
    private colorAndIconService: ColorAndIconService,
    private resolver: ComponentFactoryResolver,
    private dateCalculationService: DateCalculationService,
    private popupService: PopupWindowService,
  ) {
    this.toolbarDate = dateCalculationService.getDateStringForToday();
    this.productionWeek = dateCalculationService.getWeekFromDate(new Date()).toString();
  }

  ngOnInit(): void {
    // init stomp
    Initialize.stomp(this.topicEvents, this.eventSubscription$, this.stompService, this, this.popupService);
  }

  ngOnDestroy(): void {
    this.eventSubscription$.unsubscribe();
  }

  /** 
   * Retrieves the icon type linked to the specified status from the 
   * {@link ColorAndIconService}. If status does not result in an icon, an empty string is
   * returned.
   * 
   * @param status aggregated request status
   * @param requestsCompleted if requests are completed
   * @returns the icon class
   */
  getIconForAggregatedStatus(status: RequestStatus, requestsCompleted: boolean): string {
    return this.colorAndIconService.getIconForAggregatedStatus(
      status,
      requestsCompleted
    );
  }

  /**
   * Retrieves the icon color class linked to the specified status from the 
   * {@link ColorAndIconService}.
   * 
   * @param status request status
   * @param requestCompleted if requests are completed
   * @returns the icon color class
   */
  getIconColorForStatus(status: RequestStatus, requestCompleted: boolean): string {
    return this.colorAndIconService.getIconColorForStatus(status, requestCompleted);
  }

  /**
   * Checks if the specified cell is displayed with an icon and returns the 'cell-with-icon' class 
   * for proper formatting.
   * 
   * @param cell table cell
   * @returns css class for a cell with an icon or empty string if no icon is displayed in the cell
   */
  isCellWithIcon(cell?: DemandCellData): string {
    if (cell) {
      if (
        this.colorAndIconService.getIconForAggregatedStatus(
          cell.status!,
          cell.requestsCompleted!
        )
      ) {
        return 'cell-with-icon';
      }
    }
    return '';
  }

  /**
   * Returns multiple classes for a cell at once. Includes classes for border, pointer and 
   * background color.
   * 
   * @param e table cell
   * @param columnIndex column index of cell
   * @param rowIndex row index of cell
   * @returns string containing (multiple) classes
   */
  cellClasses(e: DemandCellData, columnIndex: number, rowIndex: number): string {
    return (
      this.setBorder(columnIndex, rowIndex) +
      this.setPointerClass(e) +
      this.backgroundColorClass(e.status)
    );
  }

  /**
   * Sets a border css class to a cell when the cell is selected and it's details are shown.
   *
   * @param columnIndex - Column index of the selected cell
   * @param rowIndex - Row index of the selected cell
   * @return CSS class
   */
  setBorder(columnIndex: number, rowIndex: number): string {
    if (columnIndex === this.selectedColumn && rowIndex === this.selectedRow) {
      return 'demand-border ';
    } else {
      return '';
    }
  }

  /** 
   * Fetches the latest {@link DemandOverviewTableData} from the {@link DemandOverviewService}.
   */
  refreshData(): void {
    this.demandsService.getDemandOverviews().subscribe((data) => {
      this.data = this.demandsService.toDataSourceArray(data);
    },
      (err) => {
        this.popupService.errorPopup(err);
      });
  }

  /**
   * Handles to open the demand detail view within the demands' table while a demand cell is 
   * clicked. If there is no content in the cell, no content will be returned.
   *
   * @param columnIndex - Column index of the selected cell
   * @param rowIndex - Row index of the selected cell
   * @param e - Content of the cell
   */
  toggleDetailView(columnIndex: number, rowIndex: number, e?: DemandCellData): void {
    // If no content is in this cell
    if (!e?.content) {
      return;
    }

    // Only do anything when drag is false
    if (!this.drag) {
      let sameCell = false;
      const currentCell = this.data.rows[rowIndex][columnIndex];

      if (this.selectedRow === rowIndex && this.selectedColumn === columnIndex) {
        sameCell = true;
      }

      if (this.expandedRow !== -1) {
        // clear old demand request content
        this.rowContainers!.toArray()[this.expandedRow].clear();
        this.expandedRow = -1;
        this.selectedRow = -1;
        this.selectedColumn = -1;
      }

      if (sameCell) {
        // If same row was clicked, don't open new one
        return;
      } else {
        // Demand detail view component will be loaded
        this.selectedRow = rowIndex;
        this.selectedColumn = columnIndex;
        const container = this.rowContainers!.toArray()[rowIndex];
        const factory: ComponentFactory<any> =
          this.resolver.resolveComponentFactory(DemandDetailComponent);
        const demandDetailComponent = container.createComponent(factory);

        demandDetailComponent.instance.year = currentCell.year;
        demandDetailComponent.instance.week = currentCell.week;
        demandDetailComponent.instance.articleNumber =
          currentCell.articleNumber;
        demandDetailComponent.instance.selectedColumn =
          (this.data.columns.length - this.selectedColumn < 6) ?
            this.data.columns.length - 6 : this.selectedColumn;
        this.expandedRow = rowIndex;
        this.productionWeek = e.week!.toString();
        this.toolbarDate = this.dateCalculationService.getDateStringForWeekAndYear(e.week!, e.year!);
      }
    }
  }

  onListDrop(event: CdkDragDrop<any[]>) {
    // Swap the elements around
    const previousIndex = this.data.rows.findIndex(
      (row) => row === event.item.data
    );
    moveItemInArray(this.data.rows, previousIndex, event.currentIndex);
    this.data.rows = this.data.rows.slice();
  }

  /**
   * Retrieves the background color linked to the specified status from the 
   * {@link ColorAndIconService}.
   * 
   * @param status request status 
   * @returns background color class
   */
  private backgroundColorClass(status?: RequestStatus): string {
    return this.colorAndIconService.backgroundColorClassForStatus(
      status
    );
  }

  /**
   * Returns css classes of pointer and shadow for non-empty table cells.
   * 
   * @param e table cell
   * @returns css classes
   */
  private setPointerClass(e?: DemandCellData): string {
    if (e?.content) {
      return 'pointer-cursor mat-elevation-z5 ';
    } else {
      return '';
    }
  }
}
