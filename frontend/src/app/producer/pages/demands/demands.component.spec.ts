/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatTableModule } from '@angular/material/table';
import { Router, RouterModule } from '@angular/router';
import { RequestStatus } from '../../model/enum';
import {
  DemandCellData,
  DemandOverviewService,
} from 'src/app/producer/services/demand-overview.service';
import { ConfigurationService } from 'src/app/common/services/configuration.service';
import { RxStompService } from '@stomp/ng2-stompjs';
import { Message } from '@stomp/stompjs';
import { DemandsComponent } from './demands.component';
import {EMPTY, Observable, of, throwError} from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { MatToolbarModule } from '@angular/material/toolbar';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { DemandDetailComponent } from '../demand-detail/demand-detail.component';
import { ChangeDetectorRef, ComponentFactory, ComponentRef, ElementRef, EmbeddedViewRef, Injector, NgModuleRef, QueryList, TemplateRef, Type, ViewContainerRef, ViewRef } from '@angular/core';
import { MatSnackBar, MatSnackBarModule } from "@angular/material/snack-bar";

class FakeStompService {
  watch(): Observable<Message> {
    return of();
  }
}

describe('DemandsComponent', () => {
  let component: DemandsComponent;
  let detailComponent: DemandDetailComponent;
  let fixture: ComponentFixture<DemandsComponent>;
  let detailFixture: ComponentFixture<DemandDetailComponent>;
  let router: Router;
  let matSnackBar: MatSnackBar;

  class FakeDetailComponentRef extends ComponentRef<any> {
    get location(): ElementRef<any> {
      throw new Error('Method not implemented.');
    }
    get injector(): Injector {
      throw new Error('Method not implemented.');
    }
    get instance(): DemandDetailComponent {
      return detailComponent;
    }
    get hostView(): ViewRef {
      throw new Error('Method not implemented.');
    }
    get changeDetectorRef(): ChangeDetectorRef {
      throw new Error('Method not implemented.');
    }
    get componentType(): Type<any> {
      throw new Error('Method not implemented.');
    }
    destroy(): void {
      throw new Error('Method not implemented.');
    }
    onDestroy(callback: Function): void {
      throw new Error('Method not implemented.');
    }

  }

  class FakeViewContainerRef extends ViewContainerRef {
    createComponent(componentFactory: ComponentFactory<any>, index?: number, injector?: Injector, projectableNodes?: any[][], ngModule?: NgModuleRef<any>): ComponentRef<any> {
      return new FakeDetailComponentRef();
    }
    get element(): ElementRef<any> {
      throw new Error('Method not implemented.');
    }
    get injector(): Injector {
      throw new Error('Method not implemented.');
    }
    get parentInjector(): Injector {
      throw new Error('Method not implemented.');
    }
    clear(): void {
      console.log('clear');
    }
    get(index: number): ViewRef | null {
      throw new Error('Method not implemented.');
    }
    get length(): number {
      throw new Error('Method not implemented.');
    }
    createEmbeddedView<C>(templateRef: TemplateRef<C>, context?: C, index?: number): EmbeddedViewRef<C> {
      throw new Error('Method not implemented.');
    }
    insert(viewRef: ViewRef, index?: number): ViewRef {
      throw new Error('Method not implemented.');
    }
    move(viewRef: ViewRef, currentIndex: number): ViewRef {
      throw new Error('Method not implemented.');
    }
    indexOf(viewRef: ViewRef): number {
      throw new Error('Method not implemented.');
    }
    remove(index?: number): void {
      throw new Error('Method not implemented.');
    }
    detach(index?: number): ViewRef | null {
      throw new Error('Method not implemented.');
    }
  }

  const viewRef = new FakeViewContainerRef();

  class FakeQueryList extends QueryList<ViewContainerRef> {
    toArray(): ViewContainerRef[] {
      return [viewRef];
    }
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DemandsComponent, DemandDetailComponent],
      imports: [
        HttpClientTestingModule,
        MatTableModule,
        RouterModule.forRoot([]),
        RouterTestingModule.withRoutes([]),
        MatToolbarModule,
        DragDropModule,
        MatSnackBarModule,
      ],
      providers: [
        DemandOverviewService,
        ConfigurationService,
        { provide: RxStompService, useClass: FakeStompService },
      ],
    }).compileComponents();
    router = TestBed.inject(Router);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemandsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    detailFixture = TestBed.createComponent(DemandDetailComponent);
    detailComponent = detailFixture.componentInstance;
    detailFixture.detectChanges();
    matSnackBar = TestBed.inject(MatSnackBar);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return the right class', () => {
    expect(
      component.cellClasses(
        {
          articleNumber: '0',
          week: 1,
          year: 2022,
          content: 'not empty',
        },
        0,
        0
      )
    ).toEqual('pointer-cursor mat-elevation-z5 ');
  
    expect(
        component.cellClasses(
            {
              articleNumber: '0',
              week: 1,
              year: 2022,
              content: 'not empty',
              status: RequestStatus.NO_RESPONSE,
            },
            0,
            0
        )
    ).toEqual('pointer-cursor mat-elevation-z5 color-no-response');
  
    expect(
        component.cellClasses(
            {
              articleNumber: '0',
              week: 1,
              year: 2022,
              content: 'not empty',
              status: RequestStatus.POSITIVE_FEEDBACK_REJECTED,
            },
            0,
            0
        )
    ).toEqual('pointer-cursor mat-elevation-z5 color-positive-feedback');
  });

  it('should trigger refreshData() on stomp message', () => {
    // arrange
    const stompService = TestBed.inject(RxStompService);
    let message: Observable<Message> = of({} as Message);
    const spyWatch = spyOn(stompService, 'watch').and.returnValue(message);
    const spyRefresh = spyOn(component, 'refreshData');

    // act
    component.ngOnInit();

    // assert
    expect(spyWatch).toHaveBeenCalled();
    expect(spyRefresh).toHaveBeenCalledTimes(2); // once directly in ngOnInit and once because of stomp
  });

  it('should trigger popupService.errorPopup() on stomp message', () => {
    // arrange
    const demandService = TestBed.inject(DemandOverviewService);
    const spyWatch = spyOn(demandService, 'getDemandOverviews').and.returnValue(throwError('error: 400'));
    const spyRefresh = spyOn<any>(component["popupService"], 'errorPopup');

    // act
    component.refreshData();

    // assert
    expect(spyWatch).toHaveBeenCalled();
    expect(spyRefresh).toHaveBeenCalledTimes(1);
  });

  it('should return right class', () => {
    // act
    const classStringOfEmpty = component.isCellWithIcon(undefined);
    const classStringWithIcon = component.isCellWithIcon({
      status: RequestStatus.POSITIVE_FEEDBACK_CONFIRMED,
      requestsCompleted: true,
    });

    // assert
    expect(classStringOfEmpty).toEqual('');
    expect(classStringWithIcon).toEqual('cell-with-icon');
  });

  it('should switch states the right way', () => {
    // act
    component.onMouseMove();

    // assert
    expect(component.drag).toEqual(true);

    // act
    component.onMouseDown();

    // assert
    expect(component.drag).toEqual(false);
  });

  it('should set cell border', () => {
    // arrange
    component.selectedColumn = 1;
    component.selectedRow = 1;

    // act
    const result = component.setBorder(1, 1);

    // assert
    expect(result).toEqual('demand-border ');
  });

  it('should unset cell border', () => {
    // arrange
    component.selectedColumn = 1;
    component.selectedRow = 1;

    // act
    const result = component.setBorder(1, 2);

    // assert
    expect(result).toEqual('');
  });

  it('should not open demand detail view', () => {
    // arrange
    component.selectedColumn = -1;
    component.selectedRow = -1;
    component.expandedRow = -1;
    component.drag = false;

    const cell: DemandCellData = {};
    cell.week = 7;
    cell.year = 2022;
    cell.articleNumber = '4711';

    const cellData: DemandCellData[][] = [[cell]];

    // act
    component.toggleDetailView(0, 0, cell);

    // assert
    expect(component.selectedRow).toEqual(-1);
    expect(component.selectedColumn).toEqual(-1);
    expect(component.expandedRow).toEqual(-1);
  });

  it('should open demand detail view', () => {
    // arrange
    const cell: DemandCellData = { week: 7, year: 2022, articleNumber: '4711', content: 'something' };
    component.selectedColumn = -1;
    component.selectedRow = -1;
    component.expandedRow = -1;
    component.drag = false;
    component.data = { rows: [[cell]], columns: [] }
    component.rowContainers = new FakeQueryList();
    // act
    component.toggleDetailView(0, 0, cell);

    // assert
    expect(component.selectedRow).toEqual(0);
    expect(component.selectedColumn).toEqual(0);
    expect(component.expandedRow).toEqual(0);
  });

  it('should close demand detail view', () => {
    // arrange
    const cell: DemandCellData = { week: 7, year: 2022, articleNumber: '4711', content: 'something' };
    component.data = { rows: [[cell]], columns: [] }
    component.rowContainers = new FakeQueryList();
    const clearSpy = spyOn(viewRef, 'clear');
    component.selectedColumn = 0;
    component.selectedRow = 0;
    component.expandedRow = 0;
    component.drag = false;

    // act
    component.toggleDetailView(0, 0, cell);
    expect(clearSpy).toHaveBeenCalled();
    // assert
    expect(component.selectedRow).toEqual(-1);
    expect(component.selectedColumn).toEqual(-1);
    expect(component.expandedRow).toEqual(-1);
  });

  it('should retrieve icon color for status (from service)', () => {
    // arrange
    const status = RequestStatus.NEGATIVE_FEEDBACK;
    const requestCompleted = false

    // act
    const iconColor = component.getIconColorForStatus(status, requestCompleted);

    // assert
    expect(iconColor).toEqual('rule-icon');
  });

});
