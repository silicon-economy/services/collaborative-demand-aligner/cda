/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { DemandDetailComponent } from "./demand-detail.component";
import { DemandDetailRoutingModule } from "./demand-detail-routing.module";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatCardModule } from "@angular/material/card";
import { MatButtonModule } from "@angular/material/button";
import { MatDividerModule } from "@angular/material/divider";
import { MatIconModule } from "@angular/material/icon";

@NgModule({
  declarations: [
    DemandDetailComponent
  ],
    imports: [
        CommonModule,
        DemandDetailRoutingModule,
        MatTableModule,
        MatToolbarModule,
        MatCardModule,
        MatButtonModule,
        MatDividerModule,
        MatIconModule
    ]
})
export class DemandDetailModule { }
