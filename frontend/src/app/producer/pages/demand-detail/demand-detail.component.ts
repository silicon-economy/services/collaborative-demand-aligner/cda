/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {DemandDetailData, Request} from '../../model';
import {DemandDetailService} from '../../services/demand-detail.service';
import {
    animate,
    state,
    style,
    transition,
    trigger,
} from '@angular/animations';
import {RxStompService} from '@stomp/ng2-stompjs';
import {Subscription} from 'rxjs';
import {ColorAndIconService} from '../../services/color-and-icon.service';
import {CompanyData} from "../../../common/model";
import {RequestStatus} from "../../model/enum";
import {MatSnackBar} from "@angular/material/snack-bar";
import {PopupWindowService} from "../../../common/services/popup-window.service";
import {Initialize} from "../../../common/initialize";

@Component({
    selector: 'app-demand-detail',
    templateUrl: './demand-detail.component.html',
    styleUrls: ['./demand-detail.component.scss'],
    animations: [
        /** Animation that rotates the indicator arrow. */
        trigger('indicatorRotation', [
            state('collapsed, void', style({transform: 'rotate(0deg)'})),
            state('expanded', style({transform: 'rotate(180deg)'})),
            transition(
                'expanded <=> collapsed, void => collapsed',
                animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
            ),
        ]),
        /** Animation that expands and collapses the panel content. */
        trigger('listHeight', [
            state('collapsed, void', style({height: '0px', visibility: 'hidden'})),
            state('expanded', style({height: '*', visibility: 'visible'})),
            transition(
                'expanded <=> collapsed, void => collapsed',
                animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
            ),
        ]),
    ],
})
export class DemandDetailComponent implements OnInit, OnDestroy {
    //Stomp
    topicEvents = ['/topic/refresh'];
    eventSubscription$ = new Subscription();

    data?: DemandDetailData;

    @Input()
    year: number = 0;

    @Input()
    week: number = 0;

    @Input()
    articleNumber: string = '';

    @Input()
    selectedColumn: number = 0;

    // tables
    displayedColumnsDetails = ['Name', 'Total Demand', 'Unit'];
    displayedColumnsSuppliers = [
        'Supplier Name',
        'Demand Request',
        'Requests',
        'Confirms',
    ];
    displayedColumnsJobs = ['Job No.', 'Demand'];

    // accordion
    suppliersState = false;
    jobsState = false;


    constructor(
        private demandDetailService: DemandDetailService,
        private stompService: RxStompService,
        private statusColorsAndIconsService: ColorAndIconService,
        private snackBar: MatSnackBar,
        private popupService: PopupWindowService,
    ) {
    }

    ngOnInit(): void {
        Initialize.stomp(this.topicEvents, this.eventSubscription$, this.stompService, this, this.popupService);
    }

    ngOnDestroy(): void {
        this.eventSubscription$.unsubscribe();
    }

    /**
     * Calculates the quantity of an article that shows as requested amount / amount to be requested.
     *
     * @param supplier the supplier the request is/was sent to
     * @returns quantity of article
     */
    quantityToRequest(supplier: CompanyData): any {
        const requestFound = this.findRequestSentToSupplier(supplier);
        if (requestFound) {
            return requestFound.quantity;
        } else {
            return this.data?.totalQuantity;
        }
    }

  /**
   * Check if the confirm button for a given supplier is available to click or not by checking if 
   * the {@link RequestStatus} within the given supplier is positive or not.
   *
   * @param supplier {@link CompanyData Supplier} to check
   * @return True if request status positive. Otherwise false
   */
  isConfirmButtonAvailable(supplier: CompanyData): boolean {
    // search for a request sent to this supplier
    const requestFound = this.findRequestSentToSupplier(supplier);
    return requestFound?.status === RequestStatus.POSITIVE_FEEDBACK;
  }

  /**
   * Returns Google icon class for a given {@link CompanyData Supplier}.
   *
   * @param supplier Supplier
   * @returns Class name of a Google icon class
   */
  getIconForSupplierStatus(supplier: CompanyData): string {
    return this.statusColorsAndIconsService.getIconForSupplierStatus(
      supplier,
      this.data
    );
  }

  /**
   * Gets the fitting Google icon to display on the total demand.
   * 
   * @param status Aggregated status
   * @param requestCompleted Is requests are completed
   * @returns Google icon class
   */
  getIconForAggregatedStatus(status: RequestStatus, requestCompleted: boolean): string {
    return this.statusColorsAndIconsService.getIconForAggregatedStatus(
      status,
      requestCompleted
    );
  }

    /**
     * Looks for requests sent to the specified supplier and returns a color class for an icon
     * corresponding to the request's status.
     *
     * @param supplier the supplier the icon is for
     * @returns icon color class
     */
    getIconColorForSupplierStatus(supplier: CompanyData): string {
        return this.statusColorsAndIconsService.getIconColorForSupplierStatus(
            supplier,
            this.data
        );
    }

    /**
     * Retrieves the icon color class linked to the specified status from the
     * {@link ColorAndIconService}.
     *
     * @param status request status
     * @param requestCompleted if requests are completed
     * @returns icon color class
     */
    getIconColorForStatus(status: RequestStatus, requestCompleted: boolean): string {
        return this.statusColorsAndIconsService.getIconColorForStatus(status, requestCompleted);
    }

    /**
     * Retrieves the background color class linked to the specified status from the
     * {@link ColorAndIconService}.
     *
     * @param status request status
     * @returns background color class
     */
    backgroundColorClassForStatus(status: RequestStatus): string {
        return this.statusColorsAndIconsService.backgroundColorClassForStatus(
            status
        );
    }

    /**
     * Looks for requests sent to the specified supplier and returns the background color class
     * corresponding to the status.
     *
     * @param supplier the supplier the color is for
     * @returns background color class
     */
    backgroundColorClass(supplier: CompanyData): string {
        return this.statusColorsAndIconsService.backgroundColorClassForSupplier(
            supplier,
            this.data
        );
    }

    /**
     * Decides if the button to send requests to all remaining suppliers should be disabled. The
     * button should be disabled, if all buttons to request a single supplier are disabled.
     *
     * @return true if button to request remaining suppliers should be disabled
     */
    disableRequestRemainingButton(): boolean {
        let enabledButtons = 0;
        this.data?.possibleSuppliers.forEach((supplier) => {
            if (!this.disableRequestButton(supplier)) {
                enabledButtons++;
            }
        });
        return enabledButtons === 0;
    }

    /**
     * Decides if the button to send a request to a specific supplier should be disabled. The button
     * should be disabled if this supplier was requested previously or requestsCompleted is true.
     *
     * @param supplier the supplier addressed by the button
     * @returns true if button to request specified supplier should be disabled
     */
    disableRequestButton(supplier: CompanyData): boolean {
        // disable button if requests completed
        if (!this.data?.requestsCompleted) {
            // disable button if a request was already sent to this supplier
            const indexOfRequest = this.data?.requests.findIndex(
                (request) => request.supplierId === supplier.id
            );
            if (indexOfRequest === -1) {
                // this supplier was not requested yet, show button
                return false;
            }
        }
        return true;
    }

    /**
     * Fetches the latest {@link DemandDetailData} from the {@link DemandDetailService}.
     */
    refreshData(): void {
        this.demandDetailService
            .getDemandDetail(this.year, this.week, this.articleNumber)
            .subscribe((data) => {
                this.data = data;
                this.checkForRequests(data);
            },
                (err) => {
                this.popupService.errorPopup(err);
            });
    }

    /**
     * Checks if at least one request exists for this demand. If yes, {@link suppliersState} will be
     * set to true.
     *
     * @param data - {@link DemandDetailData} object from the database
     */
    checkForRequests(data: DemandDetailData): void {
        this.suppliersState = data.requests.length !== 0;
    }

    /**
     * Toggles the supplier accordion to be open or close.
     */
    suppliersToggle(): void {
        this.suppliersState = !this.suppliersState;
    }

    /**
     * Toggles the jobs accordion to be open or close.
     */
    jobsToggle(): void {
        this.jobsState = !this.jobsState;
    }

    /**
     * Sends a request with the contents of this detail view to the specified supplier.
     *
     * @param supplier who the request is addressed at
     */
    sendRequest(supplier: CompanyData): void {
        const requestToCreate = {
            articleId: this.articleNumber,
            quantity: this.quantityToRequest(supplier),
            supplierId: supplier.id,
            year: this.year,
            week: this.week,
        };
        this.demandDetailService.createRequest(requestToCreate).subscribe(
            () => {
                // empty function body needed to provide the 'err' function as a second argument
                // for the 'subscribe' method. Otherwise, the error would not be recognized.
            },
            (err) => {
                this.popupService.errorPopup(err);
            });
    }

    /**
     * Sends a request to all suppliers who have not received a request yet.
     */
    sendRequestToRemainingSuppliers(): void {
        this.data?.possibleSuppliers.forEach((supplier) => {
            if (!this.disableRequestButton(supplier)) {
                this.sendRequest(supplier);
            }
        });
    }

    /**
     * Searches for a request in {@link data} that was sent to the specified supplier.
     *
     * @param supplier the supplier
     * @returns the request sent, or undefined if no request was found
     */
    private findRequestSentToSupplier(supplier: CompanyData): Request | undefined {
        return this.data?.requests.filter(
            (request) => request.supplierId === supplier.id
        )[0];
    }

    /**
     * Calculate the styling by input selected Column to imitate floating
     * 
     * @returns the style configuration 
     */
    float(): { [klass: string]: string } {
        return {
            "padding-left": this.selectedColumn * 205 + 'px',
            "width": 1200 + (this.selectedColumn * 205) + 'px'
        };
    }
}
