/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DemandDetailData } from '../../model';
import { DemandDetailComponent } from './demand-detail.component';
import { RxStompService } from '@stomp/ng2-stompjs';
import { Message } from '@stomp/stompjs';
import {EMPTY, Observable, of, throwError} from 'rxjs';
import { RequestStatus } from '../../model/enum';
import { DemandDetailService } from 'src/app/producer/services/demand-detail.service';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { DemandDetailRoutingModule } from './demand-detail-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import {CompanyData} from "../../../common/model";
import {MatSnackBar,MatSnackBarModule} from "@angular/material/snack-bar";

const mockSupplierData: CompanyData = {
  id: 'testid',
  name: 'testName',
};

const mockSupplierData2: CompanyData = {
  id: 'testERROR',
  name: 'testName',
};

const mockData: DemandDetailData = {
  articleNumber: '45',
  articleDescription: 'test',
  totalQuantity: 5,
  demands: [],
  possibleSuppliers: [mockSupplierData, mockSupplierData2],
  requests: [
    {
      uuid: 'ss',
      articleNumber: 'testArticleNo',
      quantity: 3,
      year: 2022,
      week: 6,
      supplierId: 'testid',
      status: RequestStatus.NO_RESPONSE,
    },
  ],
  year: 2022,
  week: 10,
  totalStatus: RequestStatus.NO_RESPONSE,
  unit: 'unit',
  requestsCompleted: false,
};

const mockDataContainingPositiveConfirmedRequest: DemandDetailData = {
  articleNumber: '45',
  articleDescription: 'test',
  totalQuantity: 5,
  demands: [],
  possibleSuppliers: [mockSupplierData, mockSupplierData2],
  requests: [
    {
      uuid: 'ss',
      articleNumber: 'testArticleNo',
      quantity: 3,
      year: 2022,
      week: 6,
      supplierId: 'testid',
      status: RequestStatus.POSITIVE_FEEDBACK_CONFIRMED,
    },
  ],
  year: 2022,
  week: 10,
  totalStatus: RequestStatus.POSITIVE_FEEDBACK_CONFIRMED,
  unit: 'unit',
  requestsCompleted: true,
};

const mockDataWithoutRequests: DemandDetailData = {
  articleNumber: '45',
  articleDescription: 'test',
  totalQuantity: 5,
  demands: [],
  possibleSuppliers: [mockSupplierData, mockSupplierData2],
  requests: [],
  year: 2022,
  week: 10,
  totalStatus: RequestStatus.NO_RESPONSE,
  unit: 'unit',
  requestsCompleted: false,
};

// content is linked to content of mockData and mockSupplierData
const mockRequestInput = {
  articleId: '45',
  quantity: 5,
  supplierId: 'testid',
  year: 2022,
  week: 10,
};

class FakeDemandDetailService {
  getDemandDetail(): Observable<DemandDetailData> {
    return of(mockData);
  }

  createRequest(): void {}
}

class FakeStompService {
  watch(): Observable<Message> {
    return of();
  }
}

describe('DemandDetailComponent', () => {
  let component: DemandDetailComponent;
  let fixture: ComponentFixture<DemandDetailComponent>;
  let httpClient: HttpTestingController;
  let matSnackBar: MatSnackBar;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DemandDetailComponent],
      imports: [
        HttpClientTestingModule,
        MatCardModule,
        MatToolbarModule,
        BrowserAnimationsModule,
        MatTableModule,
        DemandDetailRoutingModule,
        MatButtonModule,
        MatDividerModule,
        MatIconModule,
        MatSnackBarModule,
      ],
      providers: [
        { provide: DemandDetailService, useClass: FakeDemandDetailService },
        { provide: RxStompService, useClass: FakeStompService },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemandDetailComponent);
    component = fixture.componentInstance;
    component.articleNumber = mockData.articleNumber;
    component.week = mockData.week;
    component.year = mockData.year;
    httpClient = TestBed.inject(HttpTestingController);
    matSnackBar = TestBed.inject(MatSnackBar);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should calculate quantity to request properly', () => {
    //arrange
    component.data = mockData;

    //act
    const dataToTest = component.quantityToRequest(mockSupplierData);
    const dataToTest2 = component.quantityToRequest(mockSupplierData2);

    //assert
    expect(dataToTest).toEqual(3);
    expect(dataToTest2).toEqual(5);
  });


  it ('should calculate total quantity for when there is no request found', () => {
    //arrange
    component.data = mockData;
    spyOn<any>(component, 'findRequestSentToSupplier').and.returnValue(undefined);

    //act
    const dataToTest = component.quantityToRequest(mockSupplierData);

    //assert
    expect(dataToTest).toEqual(5);
  });

  it('should disable request button based on if the supplier was request yet', () => {
    //arrange
    component.data = mockData;

    //act
    const disableButton = component.disableRequestButton(mockSupplierData);
    const disableButton2 = component.disableRequestButton(mockSupplierData2);

    //assert
    expect(disableButton).toBe(true);
    expect(disableButton2).toBe(false);
  });

  it('should disable request button based on if all requests are completed', () => {
    //arrange
    component.data = mockDataContainingPositiveConfirmedRequest;

    //act
    const disableButton = component.disableRequestButton(mockSupplierData);
    const disableButton2 = component.disableRequestButton(mockSupplierData2);

    //assert
    expect(disableButton).toBe(true);
    expect(disableButton2).toBe(true);
  });

  it('should trigger refreshData() on stomp message', () => {
    // arrange
    const stompService = TestBed.inject(RxStompService);
    let message: Observable<Message> = of({} as Message);
    const spyWatch = spyOn(stompService, 'watch').and.returnValue(message);
    const spyRefresh = spyOn(component, 'refreshData');

    // act
    component.ngOnInit();

    // assert
    expect(spyWatch).toHaveBeenCalled();
    expect(spyRefresh).toHaveBeenCalledTimes(2); // once in fetchUrlParams and once because of stomp
  });

  it('should trigger popupService.errorPopup() on stomp message', () => {
    // arrange
    const demandDetailService = TestBed.inject(DemandDetailService);
    const spyWatch = spyOn(demandDetailService, 'getDemandDetail').and.returnValue(throwError('error 400'));
    const spyRefresh = spyOn<any>(component["popupService"], 'errorPopup');

    // act
    component.refreshData();

    // assert
    expect(spyWatch).toHaveBeenCalled();
    expect(spyRefresh).toHaveBeenCalledTimes(1);
  });

  it('should get demand detail data on init', () => {
    // arrange
    const demandDetailService = TestBed.inject(DemandDetailService);
    const spy = spyOn(demandDetailService, 'getDemandDetail').and.returnValue(
      of(mockData)
    );

    // act
    component.ngOnInit();

    // assert
    expect(spy).toHaveBeenCalled();
    expect(component.data).toEqual(mockData);
  });

  it('should send request', () => {
    // arrange
    const demandDetailService = TestBed.inject(DemandDetailService);
    const mockRequestId: string = 'MockRequestUUID';
    const spy = spyOn(
      demandDetailService,
      'createRequest'
    ).and.returnValue(of(mockRequestId));
    // request total quantity
    const stubQuantityToRequest = spyOn(
      component,
      'quantityToRequest'
    ).and.returnValue(mockData.totalQuantity);
    component.data = mockData;

    // act
    component.sendRequest(mockSupplierData);

    // assert
    expect(spy).toHaveBeenCalledWith(mockRequestInput);
    expect(component.data).toEqual(mockData);
  });

  it('should send requests only to suppliers who were not requested before', () => {
    // arrange
    let requestsSent = 0;
    const spySendSingle = spyOn(component, 'sendRequest').and.callFake(
      (supplier) => {
        requestsSent++;
      }
    );

    // act
    component.sendRequestToRemainingSuppliers();

    // assert
    expect(requestsSent).toBe(1);
    expect(spySendSingle).toHaveBeenCalledOnceWith(mockSupplierData2);
  });

  it('should return the cell classes', () => {
    // arrange
    const supplierWithRequest = mockSupplierData;
    const supplierWithoutRequest = mockSupplierData2;

    // act
    const colorOfStatus = component.backgroundColorClass(supplierWithRequest);
    const defaultColor = component.backgroundColorClass(supplierWithoutRequest);

    // assert
    expect(colorOfStatus).toEqual('color-no-response');
    expect(defaultColor).toEqual('');
  });

  it('should switch supplier state', () => {
    // arrange
    const state = component.suppliersState;
    // act
    component.suppliersToggle();

    // assert
    expect(component.suppliersState).toEqual(!state);
  });

  it('should switch jobs state', () => {
    // arrange
    const state = component.jobsState;

    // act
    component.jobsToggle();

    // assert
    expect(component.jobsState).toEqual(!state);
  });

  it('should set supplier state to true', () => {
    // arrange

    // act
    component.checkForRequests(mockData);

    // assert
    expect(component.suppliersState).toBe(true);
  });

  it('should set supplier state to false', () => {
    // arrange

    // act
    component.checkForRequests(mockDataWithoutRequests);

    // assert
    expect(component.suppliersState).toBe(false);
  });
});
