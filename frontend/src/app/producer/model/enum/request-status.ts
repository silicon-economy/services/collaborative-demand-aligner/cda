/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Status for demand requests
 *
 * @author Bernd Breitenbach
 */
export enum RequestStatus {
  NO_INTERACTION = 'NO_INTERACTION',
  NO_RESPONSE = 'NO_RESPONSE',
  NO_RESPONSE_REJECTED = 'NO_RESPONSE_REJECTED',
  POSITIVE_FEEDBACK = 'POSITIVE_FEEDBACK',
  POSITIVE_FEEDBACK_CONFIRMED = 'POSITIVE_FEEDBACK_CONFIRMED',
  POSITIVE_FEEDBACK_REJECTED = 'POSITIVE_FEEDBACK_REJECTED',
  NEGATIVE_FEEDBACK = 'NEGATIVE_FEEDBACK',
}
