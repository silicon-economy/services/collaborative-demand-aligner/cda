/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DemandOverviewItem } from "./demand-overview-item";

/**
 * Data that belongs to the demand overview.
 *
 * @author Anton Thomas Scholz, Bernd Breitenbach
 */
export interface DemandOverviewData {
    /**
     *  The map from article numbers to lists of demand overiew item data
     */
    demandOverviewItems:Map<string,DemandOverviewItem[]>;
}
