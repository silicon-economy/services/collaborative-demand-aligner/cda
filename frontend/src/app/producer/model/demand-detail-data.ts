/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DemandData } from './demand-data';
import { CompanyData, WeeklyData } from '../../common/model';
import { Request } from './request';
import { RequestStatus } from "./enum";

/**
 * Detail data that belongs to the demand based on the DTO model of the backend.
 *
 * @author Bernd Breitenbach
 */
export interface DemandDetailData extends WeeklyData {

  /**
   *  The article number
   */
  articleNumber: string;

  /**
   *  The article description
   */
  articleDescription: string;

  /**
   *  The total number
   */
  totalQuantity: number;

  /**
   *  The demand data
   */
  demands: DemandData[];

  /**
   * The supplier data
   */
  possibleSuppliers: CompanyData[];

  /**
   * The request data
   */
  requests: Request[];

  /**
   * The composite status for the total demand
   */
  totalStatus: RequestStatus;

  /**
   * The article unit of this demand
   */
  unit: string;

  /** 
   * If this item is completed or the opportunity for sending requests is given 
   */
  requestsCompleted: boolean;
}
