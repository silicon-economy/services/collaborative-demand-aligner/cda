/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { WeeklyData } from '../../common/model';
import {RequestStatus} from "./enum";

/**
 * The quantity data for a given combination of year, week and article.
 *
 * @author Bernd Breitenbach
 */
export interface DemandOverviewItem extends WeeklyData {
  /** The quantity for a given year/week for a certain article */
  quantity: number;

  /** The aggregated status for this item */
  status: RequestStatus;

  /** If this item is completed or the opportunity for sending requests is given */
  requestsCompleted: boolean;
}
