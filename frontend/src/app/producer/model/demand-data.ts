/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Data that belongs to the demand based on the DTO model of the backend.
 *
 * @author Bernd Breitenbach
 */
export interface DemandData {

  /**
   * The unique identifier
   */
   id: string;

   /**
    * The production Number
    */
   productionNumber: string;

   /**
    * the quantity
    */
   quantity: number;


   /**
    * The latest delivery date
    */
   deliveryDate: Date;

}
