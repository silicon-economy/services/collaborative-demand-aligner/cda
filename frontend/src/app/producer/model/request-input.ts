/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { WeeklyData } from '../../common/model';

/**
 * Input data to create a Request in backend
 *
 */
export interface RequestInput extends WeeklyData {
  /**
   * The article number of the requested article
   */
  articleId: string;

  /**
   * The quantity
   */
  quantity: number;

  /**
   * The supplier id
   */
  supplierId: string;
}
