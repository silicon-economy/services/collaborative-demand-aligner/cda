/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {RequestStatus} from "./enum";

/**
 * Request data
 *
 * @author Bernd Breitenbach
 */
export interface Request {
  /**
   * The request uuid
   */
  uuid: string;

  /**
   * The article number of the requested article
   */
  articleNumber: string;

  /**
   * The quantity
   */
  quantity: number;

  /**
   * The year of the latest delivery time
   */
  year: number;

  /**
   * The calendar week of the latest delivery time
   */
  week: number;

  /**
   * The supplier id
   */
  supplierId: string;

  /**
   * The request status
   */
  status: RequestStatus;
}
