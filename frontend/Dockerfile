FROM node:14-alpine as builder

COPY package.json package-lock.json ./
RUN npm ci && mkdir /ng-app && mv ./node_modules ./ng-app
WORKDIR /ng-app
COPY . .

RUN npm run ng build -- --configuration production --source-map --output-hashing none --output-path=dist

FROM nginxinc/nginx-unprivileged

ARG API_BACKEND
ARG DNS_RESOLVER
ARG production

ENV API_BACKEND $API_BACKEND
ENV DNS_RESOLVER $DNS_RESOLVER
ENV production $production

RUN env | sort

USER root
COPY nginx/default.conf.template /tmp/default.conf.template
RUN envsubst '$API_BACKEND,$DNS_RESOLVER' < /tmp/default.conf.template > /etc/nginx/conf.d/default.conf

RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /ng-app/dist /usr/share/nginx/html
RUN touch /usr/share/nginx/html/assets/env.js && chown nginx:nginx /usr/share/nginx/html/assets/env.js && chmod go+rw /usr/share/nginx/html/assets/env.js


USER nginx

EXPOSE 8088:8088
CMD ["/bin/sh",  "-c",  "exec nginx -g 'daemon off;'"]
