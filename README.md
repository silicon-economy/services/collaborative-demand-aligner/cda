> This project has been archived. It is no longer maintained or updated. If you have any questions about the project, please contact info@openlogisticsfoundation.org.


# Collaborative Demand Aligner

The Collaborative Demand Aligner component is a SE Service of a Silicon Economy Platform. The documentation contains the description of all Collaborative Demand Aligner components, their context, interaction, constraints and implementation.

The current release of the Collaborative Demand Aligner Application enables the aligment of demands between manufacturer/producer and their suppliers.

The purpose of this interactive communication is the consideration of supplier capacity limitations in production planning and vice versa.

The App will allow a secure and trustworthy exchange of demand data and delivery commitments as the basis for collaborative production planning.
The aim is an increase of planning reliablity and supply chains resiliance to ensure adherence to schedules while minimizing coordinations efforts.

# Starting locally

To fire up the application consisting of `frontend` and `backend` you just have to enter `docker-compose up --build` in the root directory.

To start only the `frontend` or `backend` just change into the corresponding directory and do the `docker-compose up --build` there.
Then the docker-compose file there will be used.

To use a PostgresQL database instead of the integrated H2 one, start a local instance by entering `docker-compose -f docker-compose-postgresql.yml up`. 
You also have to specify that the `postgresql` profile should be used e.g. by starting the backend with `mvn spring-boot:run  -Dspring-boot.run.profiles=postgresql`.

# Swagger UI

To enter the Swagger UI, open the browser and navigate to `http://localhost:8080/swagger-ui/index.html`
 

