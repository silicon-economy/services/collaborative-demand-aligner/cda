/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.model.enums;

import java.util.Arrays;
import lombok.Getter;

/**
 * This are the possible statuses of a single sent request.
 *
 * @see AggregatedRequestStatus
 * @author jhohnsel
 */
public enum RequestStatus {
  NO_RESPONSE(2),
  POSITIVE_FEEDBACK(3),
  NEGATIVE_FEEDBACK(6),
  POSITIVE_FEEDBACK_CONFIRMED(7),
  POSITIVE_FEEDBACK_REJECTED(8),
  NO_RESPONSE_REJECTED(9);

  @Getter private final int number;

  RequestStatus(int value) {
    this.number = value;
  }

  public static RequestStatus parseFromInt(int number) throws IllegalArgumentException {
    var statusMatchingNumber =
        Arrays.asList(RequestStatus.values()).stream()
            .filter(status -> number == status.getNumber())
            .findAny();
    if (statusMatchingNumber.isPresent()) {
      return statusMatchingNumber.get();
    }
    // unkown number
    throw new IllegalArgumentException(
        String.format(
            "Error occurred while parsing %s, unknown status number: %s",
            RequestStatus.class, number));
  }
}
