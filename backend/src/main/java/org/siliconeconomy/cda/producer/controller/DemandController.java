/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.cda.common.controller.BaseController;
import org.siliconeconomy.cda.producer.model.dto.output.DemandDetailViewDTO;
import org.siliconeconomy.cda.producer.model.dto.output.DemandOverviewDTO;
import org.siliconeconomy.cda.producer.service.DemandDetailViewService;
import org.siliconeconomy.cda.producer.service.DemandOverviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * This class represents an internal (UI to backend) controller to handle requests for the demand
 * views in the frontend. These are the Demand Overview and Demand Detail View.
 *
 * @author Steffen Biehs
 * @author jhohnsel
 */
@RestController
@Log4j2
public class DemandController implements BaseController {

  private final DemandOverviewService demandOverviewService;
  private final DemandDetailViewService detailViewService;

  @Autowired
  public DemandController(
      DemandOverviewService demandOverviewService, DemandDetailViewService detailViewService) {
    this.demandOverviewService = demandOverviewService;
    this.detailViewService = detailViewService;
  }

  // ###############################################################################
  // Demand Overview
  // ###############################################################################

  /**
   * This endpoint returns a {@link DemandOverviewDTO} object as JSON. If something went wrong, an
   * appropriate HTTP response will be returned.
   *
   * @return {@link ResponseEntity} with the {@link DemandOverviewDTO} object or an error
   */
  @Operation(summary = "Get the demand overview data")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "The demand overview data was returned",
            content = @Content(schema = @Schema(implementation = DemandOverviewDTO.class))),
        @ApiResponse(
            responseCode = "500",
            description = "The demand overview data could not be loaded",
            content = @Content(examples = @ExampleObject("error message")))
      })
  @GetMapping(
      value = "/producer/demand/overview",
      produces = {"application/json"})
  public ResponseEntity<Object> getDemandOverviewData() {
    try {
      var demandOverviewDTO = demandOverviewService.getAllDemandOverviewDTOs();
      return new ResponseEntity<>(demandOverviewDTO, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(
          "An internal server error occurs while fetching data for the demand overview",
          HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // ###############################################################################
  // Demand Detail View
  // ###############################################################################

  /**
   * This endpoint returns a {@link DemandDetailViewDTO} for the requested week and article. If
   * something went wrong, an appropriate HTTP response will be returned.
   *
   * @param year the year of the requested detail view
   * @param week the calendar week of the requested detail view
   * @param articleNumber identifies the requested {@link org.siliconeconomy.cda.model.Article
   *     Article} of the detail view
   * @return {@link ResponseEntity} with the {@link DemandDetailViewDTO} object or an error
   * @see DemandDetailViewService#getDemandDetailView(Integer, Integer, String)
   */
  @Operation(
      summary = "Get the demand detail view data",
      parameters = {
        @Parameter(name = "year", example = "2022"),
        @Parameter(name = "week", example = "5"),
        @Parameter(name = "articleNumber", example = "4711"),
      },
      responses = {
        @ApiResponse(
            responseCode = "200",
            content = @Content(schema = @Schema(implementation = DemandDetailViewDTO.class))),
        @ApiResponse(
            responseCode = "500",
            description = "The demand detail view data could not be loaded",
            content = @Content(examples = @ExampleObject("error message")))
      })
  @GetMapping(
      path = "/producer/demand/detail-view/{year}-{week}/{articleNumber}",
      produces = "application/json")
  public ResponseEntity<Object> getDemandDetailView(
      @PathVariable("year") Integer year,
      @PathVariable("week") Integer week,
      @PathVariable("articleNumber") String articleNumber) {
    try {
      DemandDetailViewDTO dto = detailViewService.getDemandDetailView(year, week, articleNumber);
      return new ResponseEntity<>(dto, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(
          "An internal server error occurred while fetching data for the demand detail view",
          HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
