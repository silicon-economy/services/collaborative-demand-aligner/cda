/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import java.util.UUID;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.cda.common.controller.BaseController;
import org.siliconeconomy.cda.common.exception.Details;
import org.siliconeconomy.cda.common.exception.EntityNotFoundException;
import org.siliconeconomy.cda.common.exception.IllegalDataFormatException;
import org.siliconeconomy.cda.common.exception.RestCommunicationException;
import org.siliconeconomy.cda.producer.model.Request;
import org.siliconeconomy.cda.producer.model.dto.input.DataForRequestDTO;
import org.siliconeconomy.cda.producer.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * This class represents an internal (UI to backend) controller to handle requests regarding the
 * {@link Request}.
 *
 * @author Steffen Biehs
 * @author jhohnsel
 */
@RestController("ProducerRequestController")
@Log4j2
public class RequestController implements BaseController {
  private static final ObjectMapper MAPPER = new ObjectMapper();
  private final RequestService requestService;

  @Autowired
  public RequestController(RequestService requestService) {
    this.requestService = requestService;
  }

  /**
   * This endpoint triggers the communication to send a new {@link Request} to the specified
   * supplier. After the response is processed, it is saved to the database and returns the {@link
   * UUID} of the new {@link Request}.
   *
   * @param dto data to specify the request coming from the producers frontend
   * @return {@link UUID} of the created {@link Request}
   * @throws IllegalDataFormatException handled by the exception handler
   * @throws EntityNotFoundException handled by the exception handler
   * @throws RestCommunicationException handled by the exception handler
   */
  @Operation(
      summary = "Create a request that is sent to the supplier",
      responses = {
        @ApiResponse(
            responseCode = "201",
            description = "The request was created",
            content =
                @Content(
                    examples =
                        @ExampleObject(
                            "{\"requestId\": \"1747ff7a-a693-4197-88c9-8798383c4585\"}"))),
        @ApiResponse(
            responseCode = "400",
            description =
                "The request could not be created, an error occured in the communication with the "
                    + "supplier.",
            content =
                @Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = Details.class))),
        @ApiResponse(
            responseCode = "404",
            description = "The request could not be created, some needed information is unkown.",
            content =
                @Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = Details.class)))
      })
  @PostMapping(
      path = "/producer/requests",
      consumes = {MediaType.APPLICATION_JSON_VALUE},
      produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(HttpStatus.CREATED)
  public JsonNode sendRequest(@RequestBody DataForRequestDTO dto)
      throws IllegalDataFormatException, EntityNotFoundException, RestCommunicationException {
    Request request = requestService.sendRequest(dto);
    return MAPPER.createObjectNode().put("requestId", request.getId().toString());
  }
}
