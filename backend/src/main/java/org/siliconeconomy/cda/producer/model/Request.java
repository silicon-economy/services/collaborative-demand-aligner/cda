/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.common.model.Company;
import org.siliconeconomy.cda.producer.model.deserializer.RequestDeserializer;
import org.siliconeconomy.cda.producer.model.enums.RequestStatus;

/**
 * Holds information of an outgoing request that is sent to a supplier. Requests are made to meet
 * {@link Demand demands}.
 *
 * @author jhohnsel
 */
@Entity(name = "ProducerRequest")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonDeserialize(using = RequestDeserializer.class)
@EqualsAndHashCode
public class Request {

  @Id private UUID id;

  /** The article requested. */
  @ManyToOne private Article article;

  /** The requested quantity of the request. */
  private Integer quantity;

  /** The year of the production time. */
  private Integer productionYear;

  /** The calendar week of the production time. */
  private Integer productionWeek;

  /** The supplier the request was sent to. */
  @ManyToOne private Company supplier;

  /** The latest status of the request. */
  private RequestStatus status;
}
