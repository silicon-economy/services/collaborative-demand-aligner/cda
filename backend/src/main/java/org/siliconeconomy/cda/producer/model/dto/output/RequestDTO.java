/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.model.dto.output;

import java.util.UUID;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.siliconeconomy.cda.producer.model.Request;
import org.siliconeconomy.cda.producer.model.enums.RequestStatus;

/**
 * A DTO holding information on a {@link Request}.
 *
 * @see DemandDetailViewDTO
 * @author jhohnsel
 */
@Getter
@EqualsAndHashCode
public class RequestDTO {
  private final UUID id;
  private final String articleNumber;
  private final Integer quantity;
  private final Integer year;
  private final Integer week;
  private final String supplierId;
  private final RequestStatus status;

  public RequestDTO(Request request) {
    this.id = request.getId();
    this.articleNumber = request.getArticle().getArticleNumber();
    this.quantity = request.getQuantity();
    this.year = request.getProductionYear();
    this.week = request.getProductionWeek();
    this.supplierId = request.getSupplier().getId();
    this.status = request.getStatus();
  }
}
