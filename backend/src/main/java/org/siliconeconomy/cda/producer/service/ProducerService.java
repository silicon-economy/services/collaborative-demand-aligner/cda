/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.service;

import java.util.UUID;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.cda.common.exception.EntityNotFoundException;
import org.siliconeconomy.cda.common.exception.InvalidStatusChangeException;
import org.siliconeconomy.cda.common.model.dto.ConfirmationResponseDTO;
import org.siliconeconomy.cda.producer.model.Request;
import org.siliconeconomy.cda.producer.model.enums.RequestStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service to handle confirmation requests sent by suppliers.
 *
 * @author Till-Philipp Patron
 */
@Service
@Log4j2
public class ProducerService {
  private final RequestService requestService;

  @Autowired
  public ProducerService(RequestService requestService) {
    this.requestService = requestService;
  }

  /**
   * Validates the answer to a {@link Request} given by a {@link ConfirmationResponseDTO} and saves
   * the changes in the request.
   *
   * @param response the response to a request that is validated and that determines the new request
   *     status of the request
   * @throws InvalidStatusChangeException if status in the request is definitive
   * @throws EntityNotFoundException if no request is found for the id in the response
   */
  public void parseAndSaveConfirmation(ConfirmationResponseDTO response)
      throws InvalidStatusChangeException, EntityNotFoundException {
    Request request = getRequestIfPresent(response.getRequestId());
    checkIfStatusIsChangeable(request.getStatus());
    changeStatus(request, response.isConfirmed());
  }

  /**
   * Checks if the given id is invalid due to no existing {@link Request} entity for that id. If the
   * id is invalid, an {@link EntityNotFoundException} is thrown.
   *
   * @param id the id to identify the regarding {@link Request}.
   * @return the found {@link Request}, is never null.
   * @throws EntityNotFoundException is thrown in case there is no entity with that id.
   */
  protected Request getRequestIfPresent(UUID id) throws EntityNotFoundException {
    Request request;
    try {
      // this might throw a javax.persistence.EntityNotFoundException
      request = requestService.getRequest(id);
      if (request == null) {
        // enter the catch block to handle this null case
        throw new EntityNotFoundException("");
      }
    } catch (Exception ex) {
      var errorMessage =
          String.format("There is no request for id '%s' present in the database", id);
      log.error(errorMessage);
      throw new EntityNotFoundException(errorMessage);
    }
    return request;
  }

  /**
   * Checks by the specified {@link RequestStatus} if the {@link Request} is confirmable/rejectable.
   * If not, an {@link InvalidStatusChangeException} is thrown.
   *
   * @param currentStatus the current status of the request that is to be changed
   * @throws InvalidStatusChangeException if the status is definitive
   */
  protected void checkIfStatusIsChangeable(RequestStatus currentStatus)
      throws InvalidStatusChangeException {
    if (currentStatus == RequestStatus.POSITIVE_FEEDBACK_CONFIRMED
        || currentStatus == RequestStatus.POSITIVE_FEEDBACK_REJECTED
        || currentStatus == RequestStatus.NO_RESPONSE_REJECTED) {
      var errorMessage =
          String.format(
              "Status change in request invalid, cannot change status '%s'.", currentStatus);
      log.error(errorMessage);
      throw new InvalidStatusChangeException(errorMessage);
    }
  }

  /**
   * Changes the status of the specified {@link Request} depending on whether the request is
   * confirmed or rejected.
   *
   * @param request changes the status of this request
   * @param isConfirmed if true the new status is {@link RequestStatus#POSITIVE_FEEDBACK}, otherwise
   *     the new status is {@link RequestStatus#NEGATIVE_FEEDBACK}
   */
  protected void changeStatus(Request request, boolean isConfirmed) {
    RequestStatus newStatus;
    if (isConfirmed) {
      newStatus = RequestStatus.POSITIVE_FEEDBACK;
    } else {
      newStatus = RequestStatus.NEGATIVE_FEEDBACK;
    }
    requestService.updateRequestStatus(request, newStatus);
  }
}
