/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.model.dto.output;

import java.util.List;
import java.util.stream.Collectors;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.common.model.dto.CompanyDTO;
import org.siliconeconomy.cda.producer.model.Demand;
import org.siliconeconomy.cda.producer.model.Request;
import org.siliconeconomy.cda.producer.model.enums.AggregatedRequestStatus;
import org.springframework.stereotype.Component;

/**
 * A DTO containing the needed information to display a demand detail view.
 *
 * @author jhohnsel
 */
@Data
@NoArgsConstructor
// make it a component to enable autowiring in DemandDetailViewService for easier testing
@Component
public class DemandDetailViewDTO {
  private int year;
  private int week;
  private String articleNumber;
  private String articleDescription;
  private int totalQuantity;
  private List<DemandDTO> demands;
  private List<CompanyDTO> possibleSuppliers;
  private List<RequestDTO> requests;
  /** Refers to the total demand in this week and is the result of the statuses of all requests. */
  private AggregatedRequestStatus totalStatus;
  /** If this item is completed or the opportunity for actions (sending requests) is given. */
  private boolean requestsCompleted;

  private String unit;

  /**
   * Constructs a {@link DemandDetailViewDTO} using the information of the specified parameters.It
   * should be ensured beforehand that each {@link Demand} in the demands parameter and each {@link
   * Request} in the requests parameter are compatible with the calendar week and article specified
   * by the other parameters.
   *
   * @param year the year of the detail view
   * @param week the calendar week of the detail view
   * @param article the article in the demand detail view
   * @param demands List of demands that match the article and week combination
   * @param requests List of requests sent to suppliers that match the article and week combination
   * @param totalStatus composite status of the entire demand
   * @param requestsCompleted if completed or the opportunity for sending requests is given
   * @return a new DemandDetailViewDTO instance
   */
  public DemandDetailViewDTO of(
      int year,
      int week,
      Article article,
      List<Demand> demands,
      List<Request> requests,
      AggregatedRequestStatus totalStatus,
      boolean requestsCompleted) {
    var instance = new DemandDetailViewDTO();
    instance.year = year;
    instance.week = week;
    instance.articleNumber = article.getArticleNumber();
    instance.articleDescription = article.getDescription();
    instance.totalQuantity =
        demands.stream().map(Demand::getQuantity).reduce(Integer::sum).orElse(0);
    instance.demands = demands.stream().map(DemandDTO::new).collect(Collectors.toList());
    instance.possibleSuppliers =
        article.getSuppliers().stream().map(CompanyDTO::new).collect(Collectors.toList());
    instance.requests = requests.stream().map(RequestDTO::new).collect(Collectors.toList());
    instance.totalStatus = totalStatus;
    instance.unit = article.getUnit();
    instance.requestsCompleted = requestsCompleted;
    return instance;
  }
}
