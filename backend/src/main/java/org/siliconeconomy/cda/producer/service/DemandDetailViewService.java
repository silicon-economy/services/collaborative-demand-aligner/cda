/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.service;

import java.util.List;
import java.util.Optional;
import lombok.NonNull;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.common.model.repositories.ArticleRepository;
import org.siliconeconomy.cda.producer.model.Demand;
import org.siliconeconomy.cda.producer.model.Request;
import org.siliconeconomy.cda.producer.model.dto.output.DemandDetailViewDTO;
import org.siliconeconomy.cda.producer.model.repositories.DemandRepository;
import org.siliconeconomy.cda.producer.model.repositories.RequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This service holds logic to process requests of the {@link
 * org.siliconeconomy.cda.controller.DemandController DemandController}.
 *
 * @author jhohnsel
 */
@Service
public class DemandDetailViewService {

  private final DemandRepository demandRepo;
  private final ArticleRepository articleRepo;
  private final RequestRepository requestRepo;
  private final RequestService requestService;
  // autowire this for easier testing of method getDemandDetailView()
  private final DemandDetailViewDTO demandDetailViewDTO;

  @Autowired
  public DemandDetailViewService(
      DemandRepository demandRepo,
      ArticleRepository articleRepo,
      RequestRepository requestRepo,
      RequestService requestService,
      DemandDetailViewDTO demandDetailViewDTO) {
    this.demandRepo = demandRepo;
    this.articleRepo = articleRepo;
    this.requestRepo = requestRepo;
    this.requestService = requestService;
    this.demandDetailViewDTO = demandDetailViewDTO;
  }

  /**
   * Collects the needed information to display a specific demand detail view page. The parameters
   * are checked to be non-null.
   *
   * @param year the year of the week
   * @param week the calendar week of the requested detail view
   * @param articleNumber the articleNumber of the {@link
   *     org.siliconeconomy.cda.common.model.Article Article} of the requested detail view
   * @return a {@link DemandDetailViewDTO} containing the needed information to display a detail
   *     view
   */
  public DemandDetailViewDTO getDemandDetailView(
      @NonNull Integer year, @NonNull Integer week, @NonNull String articleNumber) {
    // find article with specified number
    Optional<Article> optArticle = articleRepo.findById(articleNumber);
    if (optArticle.isEmpty()) {
      throw new IllegalArgumentException(
          String.format("No article with articleNumber '%s' found", articleNumber));
    }
    var article = optArticle.get();
    // retrieve all demands and requests matching the specified week and specified article from repo
    List<Demand> demands = demandRepo.findAllByWeekAndArticleNumber(year, week, articleNumber);
    List<Request> requests = requestRepo.findAllByWeekAndArticleNumber(year, week, articleNumber);
    return demandDetailViewDTO.of(
        year,
        week,
        article,
        demands,
        requests,
        requestService.getCompositeRequestStatus(year, week, article.getArticleNumber()),
        requestService.areRequestsCompleted(year, week, article.getArticleNumber()));
  }
}
