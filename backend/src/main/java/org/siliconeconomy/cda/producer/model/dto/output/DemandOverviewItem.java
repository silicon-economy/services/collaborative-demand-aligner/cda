/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.model.dto.output;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.siliconeconomy.cda.producer.model.enums.AggregatedRequestStatus;

/**
 * Represents a data transfer object for a single demand overview item.
 *
 * @author Steffen Biehs
 */
@Getter
@EqualsAndHashCode
@Setter
@AllArgsConstructor
@Builder
public class DemandOverviewItem {

  private int quantity;
  private int year;
  private int week;
  private AggregatedRequestStatus status;
  /** If this item is completed or the opportunity for actions (sending requests) is given. */
  private boolean requestsCompleted;
}
