/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.model.repositories;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.siliconeconomy.cda.common.util.YearAndWeek;
import org.siliconeconomy.cda.producer.model.Demand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository of {@link Demand}.
 *
 * @author jhohnsel
 */
@Repository
public interface DemandRepository extends JpaRepository<Demand, UUID> {

  /**
   * Filters all the {@link Demand Demands} from the repository where {@link Demand#deliveryDate} is
   * contained in the specified calendar week and the {@link
   * org.siliconeconomy.cda.common.model.Article Article} of the demand matches the specified
   * articleNumber.
   *
   * @param year the year
   * @param week the calendar week
   * @param articleNumber the articleNumber of an {@link org.siliconeconomy.cda.common.model.Article
   *     Article}
   * @return a List of demands from the repository that match the specified conditions (week- and
   *     article-wise)
   */
  public default List<Demand> findAllByWeekAndArticleNumber(
      int year, int week, String articleNumber) {
    return findAll().stream()
        .filter(
            demand ->
                YearAndWeek.containsDate(year, week, demand.getDeliveryDate())
                    && demand.getArticle().getArticleNumber().equals(articleNumber))
        .collect(Collectors.toList());
  }
}
