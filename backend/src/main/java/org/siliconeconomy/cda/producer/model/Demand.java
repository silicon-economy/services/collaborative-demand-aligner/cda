/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.time.LocalDateTime;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.producer.model.deserializer.DemandDeserializer;

/**
 * A demand is the need of a certain {@link Article} in a certain quantity at a certain date. It is
 * linked to a certain order referenced by the {@link #productionNumber}.
 *
 * @author jhohnsel
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonDeserialize(using = DemandDeserializer.class)
public class Demand {

  @Id
  @GeneratedValue(generator = "UUID")
  @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
  private UUID id;

  /** A reference to the production order to which this demand is assigned. */
  private String productionNumber;

  /** The demanded article. */
  @ManyToOne private Article article;

  /** The quantity of the demanded article. */
  private Integer quantity;

  /** The latest date by which the article must be delivered. */
  private LocalDateTime deliveryDate;
}
