/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.model.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.common.setup.SetupData;
import org.siliconeconomy.cda.producer.model.Demand;

/**
 * Custom deserializer of a {@link Demand}. Resolves the articleNumber node to an {@link Article}
 * object from the {@link SetupData}.
 *
 * @author jhohnsel
 */
@Log4j2
public class DemandDeserializer extends StdDeserializer<Demand> {

  private transient SetupData setupData = new SetupData();

  public DemandDeserializer() {
    this(null);
  }

  public DemandDeserializer(Class<?> vc) {
    super(vc);
  }

  @Override
  public Demand deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
    JsonNode node = jp.getCodec().readTree(jp);
    String productionNumber = node.get("Production number").asText();
    Integer quantity = Integer.parseInt(node.get("Quantity").asText());
    var deliveryDate = LocalDateTime.parse(node.get("Latest date of delivery").asText());
    JsonNode idNode = node.get("id");
    UUID id;
    if (idNode != null && !idNode.isNull()) {
      id = UUID.fromString(idNode.asText());
    } else {
      // if no id is present, assign random UUID
      id = UUID.randomUUID();
      log.debug(String.format("Generated Demand UUID '%s'", id));
    }
    // retrieve article with matiching articleNumber from setupData
    String articleNo = node.get("Article No").asText();
    var article = setupData.findArticleByArticleNumber(articleNo);

    return new Demand(id, productionNumber, article, quantity, deliveryDate);
  }
}
