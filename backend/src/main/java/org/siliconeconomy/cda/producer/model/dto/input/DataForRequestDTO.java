/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.model.dto.input;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

/**
 * Represents a data transfer object for an incoming <code> Request </code> object.
 *
 * @author Steffen Biehs
 */
@Getter
@AllArgsConstructor
@Builder
@NonNull
public class DataForRequestDTO {
  @Schema(example = "4716")
  private String articleId;

  @Schema(example = "46")
  private Integer quantity;

  @Schema(example = "2022")
  private Integer year;

  @Schema(example = "5")
  private Integer week;

  @Schema(example = "125")
  private String supplierId;
}
