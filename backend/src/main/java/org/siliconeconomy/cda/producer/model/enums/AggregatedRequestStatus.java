/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.model.enums;

import java.util.Arrays;
import lombok.Getter;

/**
 * These are the status concerning the overall demand of a specific week and article. It is
 * aggregated by the statuses of multiple sent requests.
 *
 * @see RequestStatus
 * @author jhohnsel
 */
public enum AggregatedRequestStatus {
  NO_INTERACTION(1),
  NO_RESPONSE(2),
  POSITIVE_FEEDBACK(3),
  NEGATIVE_FEEDBACK(6),
  POSITIVE_FEEDBACK_CONFIRMED(7);

  @Getter private final int number;

  AggregatedRequestStatus(int value) {
    this.number = value;
  }

  public static AggregatedRequestStatus parseFromInt(int number) throws IllegalArgumentException {
    var statusMatchingNumber =
        Arrays.asList(AggregatedRequestStatus.values()).stream()
            .filter(status -> number == status.getNumber())
            .findAny();
    if (statusMatchingNumber.isPresent()) {
      return statusMatchingNumber.get();
    }
    // unkown number
    throw new IllegalArgumentException(
        String.format(
            "Error occurred while parsing %s, unknown status number: %s",
            AggregatedRequestStatus.class, number));
  }
}
