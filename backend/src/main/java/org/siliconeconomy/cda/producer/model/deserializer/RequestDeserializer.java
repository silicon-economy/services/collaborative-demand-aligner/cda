/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.model.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.common.setup.SetupData;
import org.siliconeconomy.cda.common.util.YearAndWeek;
import org.siliconeconomy.cda.producer.model.Request;
import org.siliconeconomy.cda.producer.model.enums.RequestStatus;

/**
 * Custom deserializer of a {@link Request}. Resolves the articleNumber node to an {@link Article}
 * object from the {@link SetupData}.
 *
 * @author jhohnsel
 */
@Log4j2
public class RequestDeserializer extends StdDeserializer<Request> {

  private transient SetupData setupData = new SetupData();

  public RequestDeserializer() {
    this(null);
  }

  public RequestDeserializer(Class<?> vc) {
    super(vc);
  }

  @Override
  public Request deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
    JsonNode node = jp.getCodec().readTree(jp);
    Integer quantity = Integer.parseInt(node.get("Requested quantity").asText());
    var deliveryDate = LocalDateTime.parse(node.get("Latest date of delivery").asText());
    var status = RequestStatus.parseFromInt(node.get("Status").asInt());
    var id = UUID.fromString(node.get("UUID").asText());
    // retrieve article with matching articleNumber from setupData
    String articleNo = node.get("Article No.").asText();
    var article = setupData.findArticleByArticleNumber(articleNo);
    // retrieve supplier with matching id from setupData
    String supplierId = node.get("Supplier").asText();
    var supplier = setupData.findCompanyById(supplierId);

    var yearAndWeek = YearAndWeek.of(deliveryDate);
    var request = new Request();
    request.setId(id);
    request.setQuantity(quantity);
    request.setProductionYear(yearAndWeek[0]);
    request.setProductionWeek(yearAndWeek[1]);
    request.setArticle(article);
    request.setSupplier(supplier);
    request.setStatus(status);
    return request;
  }
}
