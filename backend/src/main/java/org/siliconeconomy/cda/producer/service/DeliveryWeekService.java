/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.service;

import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.WeekFields;
import java.util.Locale;
import org.springframework.stereotype.Service;

/**
 * Service for calculating a production week (and year) for a given delivery
 * week and vice versa.
 *
 * @author Bernd Breitenbach
 */
@Service
public class DeliveryWeekService {

  private static final int YEAR = 0;
  private static final int WEEK = 1;
  private static final String RANGE_ERROR_MESSAGE = "week is out of range [1...53]";

  
  /**
   * Calculates the delivery week & year for a given production week & year
   *
   * @param year the year
   * @param week the week in the range of [1..53]
   * @return a two element integer array with year and week
   */
  public int[] productionToDeliveryWeek(final int year, final int week) {
    if (week < 1 || week > 53) {
      throw new IllegalArgumentException(RANGE_ERROR_MESSAGE);
    }

    int[] res = { year, week - 1 };

    if (week == 1) {
      res[YEAR] = year - 1;
      res[WEEK] = LocalDate.of(year - 1, Month.DECEMBER, 31)
          .get(WeekFields.of(Locale.GERMANY).weekOfYear());
    }
    return res;
  }

  /**
   * Calculates the production week & year for a given delivery week & year
   *
   * @param year the year
   * @param week the week in the range of [1..53]
   * @return a two element integer array with year and week
   */
  public int[] deliveryToProductionWeek(final int year, final int week) {

    if (week < 1 || week > 53) {
      throw new IllegalArgumentException(RANGE_ERROR_MESSAGE);
    }

    int[] res = { year, week + 1 };

    if (week == 52) {
      var lastWeek = LocalDate.of(year, Month.DECEMBER, 31).get(WeekFields.of(Locale.GERMANY).weekOfYear());
      if (lastWeek == week) { // current week is the last one in the year
        res[YEAR] = year + 1;
        res[WEEK] = 1;
      } // otherwise week can only be equal to 53 -> we keep week & year
    } else if (week == 53) {
      res[WEEK] = 1;
      res[YEAR] = year + 1;
    }
    return res;
  }
}
