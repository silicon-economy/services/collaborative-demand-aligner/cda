/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.siliconeconomy.cda.common.controller.BaseController;
import org.siliconeconomy.cda.common.exception.Details;
import org.siliconeconomy.cda.common.exception.EntityNotFoundException;
import org.siliconeconomy.cda.common.exception.InvalidStatusChangeException;
import org.siliconeconomy.cda.common.model.dto.ConfirmationResponseDTO;
import org.siliconeconomy.cda.producer.service.ProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * External controller to receive requests from the supplier company.
 *
 * <p>There is an endpoint to communicate a confirmation or rejection to a sent response.
 *
 * @author Till-Philipp Patron
 */
@RestController
public class ProducerController implements BaseController {

  private final ProducerService producerService;

  @Autowired
  public ProducerController(ProducerService producerService) {
    this.producerService = producerService;
  }

  /**
   * This endpoint takes the answer of a supplier regarding a sent request referenced by the {@link
   * ConfirmationResponseDTO} and updates the request status on a confirmation or rejection
   * accordingly.
   *
   * @param responseDTO the response made by the supplier being a confirmation or rejection of a
   *     request
   * @return Http status indicating if the request status update was processed succesfully
   * @throws EntityNotFoundException if no request is found for the specified request id
   * @throws InvalidStatusChangeException if the request is currently in a status that does not
   *     allow a confirmation or rejection by the supplier
   */
  @Operation(
      summary = "Accepts the confirmation or rejection of a supplier to a previously sent request.",
      responses = {
        @ApiResponse(
            responseCode = "200",
            description = "The message could be processed successfully.",
            content = @Content(examples = @ExampleObject(value = "OK"))),
        @ApiResponse(
            responseCode = "400",
            description = "The request status cannot be changed.",
            content = @Content(schema = @Schema(implementation = Details.class))),
        @ApiResponse(
            responseCode = "404",
            description = "The request ID is unknown.",
            content = @Content(schema = @Schema(implementation = Details.class))),
      })
  @PostMapping(
      path = "/producer/external/confirmation",
      consumes = "application/json",
      produces = "application/json")
  public HttpStatus receiveConfirmation(@RequestBody ConfirmationResponseDTO responseDTO)
      throws InvalidStatusChangeException, EntityNotFoundException {
    producerService.parseAndSaveConfirmation(responseDTO);
    return HttpStatus.OK;
  }
}
