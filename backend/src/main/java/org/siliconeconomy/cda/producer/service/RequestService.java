/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.service;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.cda.common.exception.EntityNotFoundException;
import org.siliconeconomy.cda.common.exception.IllegalDataFormatException;
import org.siliconeconomy.cda.common.exception.RestCommunicationException;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.common.model.Company;
import org.siliconeconomy.cda.common.model.dto.RequestRawDTO;
import org.siliconeconomy.cda.common.model.repositories.ArticleRepository;
import org.siliconeconomy.cda.common.model.repositories.CompanyRepository;
import org.siliconeconomy.cda.common.service.DataRefreshStompService;
import org.siliconeconomy.cda.producer.model.Request;
import org.siliconeconomy.cda.producer.model.dto.input.DataForRequestDTO;
import org.siliconeconomy.cda.producer.model.enums.AggregatedRequestStatus;
import org.siliconeconomy.cda.producer.model.enums.RequestStatus;
import org.siliconeconomy.cda.producer.model.repositories.RequestRepository;
import org.siliconeconomy.cda.supplier.model.dto.output.RequestDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * This service holds logic to process requests of the controllers. Methods that change the content
 * in the database signalize this by emitting a stomp message via {@link DataRefreshStompService}.
 *
 * @author Steffen Biehs
 * @author jhohnsel
 */
@Service("ProducerRequestService")
@Log4j2
public class RequestService {

  private final ArticleRepository articleRepo;
  private final RequestRepository requestRepo;
  private final CompanyRepository companyRepo;
  private final DataRefreshStompService dataRefreshStompService;
  private final DeliveryWeekService deliveryWeekService;
  private final SupplierGatewayService supplierGatewayService;

  @Value("${producer.paul.company.id}")
  private String myCompanyId;

  @Autowired
  public RequestService(
      ArticleRepository articleRepo,
      RequestRepository requestRepo,
      CompanyRepository companyRepo,
      DataRefreshStompService dataRefreshStompService,
      DeliveryWeekService deliveryWeekService,
      SupplierGatewayService supplierGatewayService) {
    this.articleRepo = articleRepo;
    this.requestRepo = requestRepo;
    this.companyRepo = companyRepo;
    this.dataRefreshStompService = dataRefreshStompService;
    this.deliveryWeekService = deliveryWeekService;
    this.supplierGatewayService = supplierGatewayService;
  }

  /**
   * Sends a new request to the specified supplier, validates the response and saves the new
   * instance. A stomp message is emitted on save.
   *
   * @param requestToSend input data that was created in the frontend and results in the transmitted
   *     {@link RequestRawDTO}
   * @return new {@link Request} that was sent to the supplier and saved in the database
   * @throws IllegalDataFormatException if the response from the supplier is not compatible with the
   *     request
   * @throws EntityNotFoundException if specified entities cannot be found in the repos
   * @throws RestCommunicationException if the supplier responded with an error code
   */
  public Request sendRequest(DataForRequestDTO requestToSend)
      throws IllegalDataFormatException, EntityNotFoundException, RestCommunicationException {
    Optional<Company> supplier = companyRepo.findById(requestToSend.getSupplierId());
    if (supplier.isPresent()) {
      // send the request to the supplier
      RequestRawDTO body = createBodyForRequest(requestToSend);
      RequestDTO responseFromSupplier =
          supplierGatewayService.sendNewRequestToSupplier(supplier.get(), body);
      // validate response content-wise
      validateSupplierResponseAgainstRequestOrThrow(body, responseFromSupplier);

      // save the request as sent to the database
      Request requestSent = createRequestObjectFromSentRequest(body, supplier.get());
      saveRequest(requestSent);
      return requestSent;
    } else {
      throw new EntityNotFoundException("The supplier this request should be sent to is unknown.");
    }
  }

  /**
   * Returns a {@link Request} for a given ID.
   *
   * @param requestId the given ID
   * @return {@link Request}
   */
  public Request getRequest(UUID requestId) {
    return requestRepo.getById(requestId);
  }

  /**
   * Updates a {@link RequestStatus} within a {@link Request} and saves it in the database. A stomp
   * message is emitted.
   *
   * @param request passed {@link Request}
   * @param status status value to be updated
   * @return updated {@link Request}
   * @see #saveRequest(Request)
   */
  public Request updateRequestStatus(Request request, RequestStatus status) {
    request.setStatus(status);
    saveRequest(request);
    return request;
  }

  /**
   * For the specified week and article number, the {@link AggregatedRequestStatus} of multiple
   * {@link Request requests} is evaluated.
   *
   * @param year year of calendar week
   * @param week calendar week
   * @param articleNumber article number of an {@link Article}
   * @return the composite status of {@link Request requests}, otherwise {@link
   *     AggregatedRequestStatus#NO_INTERACTION}
   */
  public AggregatedRequestStatus getCompositeRequestStatus(
      int year, int week, String articleNumber) {
    var requests = requestRepo.findAllByWeekAndArticleNumber(year, week, articleNumber);
    AggregatedRequestStatus totalStatus;
    if (requests.isEmpty()) {
      totalStatus = AggregatedRequestStatus.NO_INTERACTION;
    }
    /*
     * when a request with a positive (confirmed) feedback is present, it reflects
     * in the total
     * status directly
     */
    else if (requests.stream()
        .anyMatch(request -> request.getStatus() == RequestStatus.POSITIVE_FEEDBACK_CONFIRMED)) {
      totalStatus = AggregatedRequestStatus.POSITIVE_FEEDBACK_CONFIRMED;
    } else if (requests.stream()
        .anyMatch(request -> request.getStatus() == RequestStatus.POSITIVE_FEEDBACK)) {
      totalStatus = AggregatedRequestStatus.POSITIVE_FEEDBACK;
    }
    // check for unanswered requests
    else if (requests.stream()
        .anyMatch(request -> request.getStatus() == RequestStatus.NO_RESPONSE)) {
      totalStatus = AggregatedRequestStatus.NO_RESPONSE;
    }
    // total status is negative if and only if all requests were declined
    else if (requests.stream()
        .allMatch(request -> request.getStatus() == RequestStatus.NEGATIVE_FEEDBACK)) {
      totalStatus = AggregatedRequestStatus.NEGATIVE_FEEDBACK;
    } else {
      // should never reach this branch with a valid combination of request statuses
      throw new IllegalStateException("Logical error, cannot interpret statuses of requests");
    }
    return totalStatus;
  }

  /**
   * Checks if for the specified week and article the opportunity of sending requests is given. This
   * is not given, if all suppliers have been requested or a firm confirmation ({@link
   * RequestStatus#POSITIVE_FEEDBACK_CONFIRMED}) has been given to one supplier.
   *
   * @param year year of calendar week
   * @param week calendar week
   * @param articleNumber article number of an {@link Article}
   * @return false if the opportunity for sending requests is given, true otherwise
   */
  public boolean areRequestsCompleted(int year, int week, String articleNumber) {
    var sentRequests = requestRepo.findAllByWeekAndArticleNumber(year, week, articleNumber);
    // return true if a supplier has been given a firm confirmation
    boolean containsFirmConfirmation =
        sentRequests.stream()
            .anyMatch(
                request -> request.getStatus().equals(RequestStatus.POSITIVE_FEEDBACK_CONFIRMED));
    if (containsFirmConfirmation) {
      return true;
    }
    // return if all suppliers were requested already
    var allPossibleSuppliers =
        articleRepo
            .findById(articleNumber)
            .orElseThrow(
                () -> new NoSuchElementException("unknown article number: " + articleNumber))
            .getSuppliers();
    return allPossibleSuppliers.stream()
        .allMatch(
            supplier ->
                sentRequests.stream().anyMatch(request -> request.getSupplier().equals(supplier)));
  }

  /**
   * Prepares the http body when a request will be sent to a supplier.
   *
   * @param requestToSend content of the request to be sent
   * @return request data in fitting format for the api
   * @throws EntityNotFoundException if article number is unknown
   */
  protected RequestRawDTO createBodyForRequest(DataForRequestDTO requestToSend)
      throws EntityNotFoundException {
    Optional<Article> article = articleRepo.findById(requestToSend.getArticleId());
    if (article.isPresent()) {
      int[] deliveryYearAndWeek =
          deliveryWeekService.productionToDeliveryWeek(
              requestToSend.getYear(), requestToSend.getWeek());
      return RequestRawDTO.builder()
          .id(UUID.randomUUID()) // this is where the id for the new request is set
          .articleNumber(article.get().getArticleNumber())
          .quantity(requestToSend.getQuantity())
          .year(deliveryYearAndWeek[0])
          .week(deliveryYearAndWeek[1])
          .customerId(myCompanyId)
          .build();
    } else {
      throw new EntityNotFoundException("Unknown article in request to be sent.");
    }
  }

  /**
   * Validates the response from the supplier against the sent request data. In success, a fitting
   * {@link Request} object is returned ready to be saved into the repository. Otherwise, an
   * exception is thrown.
   *
   * @param requestSent data that was sent to the supplier
   * @param response content of the response of the supplier
   * @throws IllegalDataFormatException
   */
  protected void validateSupplierResponseAgainstRequestOrThrow(
      RequestRawDTO requestSent, RequestDTO response) throws IllegalDataFormatException {
    // compare request and response content-wise
    boolean sameId = requestSent.getId().equals(response.getId());
    boolean sameArticle = requestSent.getArticleNumber().equals(response.getArticleNumber());
    boolean sameQuantity = requestSent.getQuantity().equals(response.getQuantity());
    boolean sameYear = requestSent.getYear().equals(response.getYear());
    boolean sameWeek = requestSent.getWeek().equals(response.getWeek());
    boolean sameCustomer = requestSent.getCustomerId().equals(response.getCustomer().getId());
    if (!(sameId && sameArticle && sameQuantity && sameYear && sameWeek && sameCustomer)) {
      String errorMessage = "Sent request data is not compatible with the response from supplier.";
      log.error(errorMessage);
      throw new IllegalDataFormatException(errorMessage);
    }
  }

  /**
   * Transforms the sent data into a {@link Request} object.
   *
   * @param requestRawDTO body of the request that was sent to the supplier
   * @param supplier the supplier the request was sent to as {@link Company}
   * @return {@link Request} object to be saved in the repo
   * @throws EntityNotFoundException
   */
  protected Request createRequestObjectFromSentRequest(
      RequestRawDTO requestRawDTO, Company supplier) throws EntityNotFoundException {
    Optional<Article> article = articleRepo.findById(requestRawDTO.getArticleNumber());
    if (article.isPresent()) {
      int[] productionWeek =
          deliveryWeekService.deliveryToProductionWeek(
              requestRawDTO.getYear(), requestRawDTO.getWeek());
      return Request.builder()
          .id(requestRawDTO.getId())
          .article(article.get())
          .quantity(requestRawDTO.getQuantity())
          .productionYear(productionWeek[0])
          .productionWeek(productionWeek[1])
          .supplier(supplier)
          .status(RequestStatus.NO_RESPONSE)
          .build();
    } else {
      throw new EntityNotFoundException("Unknown article in the sent request.");
    }
  }

  /**
   * Saves a {@link Request} to the database and emits a stomp message to signalize that the content
   * in the database was changed.
   *
   * @param request {@link Request} to be saved
   * @see DataRefreshStompService
   */
  private void saveRequest(Request request) {
    requestRepo.save(request);
    dataRefreshStompService.triggerRefresh();
  }
}
