/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.model.dto.output;

import java.util.List;
import java.util.Map;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Represents a data transfer object for the demand overview.
 *
 * @author Steffen Biehs
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class DemandOverviewDTO {

  /** Maps: Article number -> List of {@link DemandOverviewItem} related to this article number. */
  private Map<String, List<DemandOverviewItem>> demandOverviewItems;
}
