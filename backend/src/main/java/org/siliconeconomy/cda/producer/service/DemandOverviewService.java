/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.siliconeconomy.cda.common.util.YearAndWeek;
import org.siliconeconomy.cda.producer.model.Demand;
import org.siliconeconomy.cda.producer.model.dto.output.DemandOverviewDTO;
import org.siliconeconomy.cda.producer.model.dto.output.DemandOverviewItem;
import org.siliconeconomy.cda.producer.model.repositories.DemandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the DemandController
 *
 * @author Steffen Biehs
 */
@Service
public class DemandOverviewService {

  private final DemandRepository demandRepository;
  private final RequestService requestService;

  @Autowired
  public DemandOverviewService(DemandRepository demandRepository, RequestService requestService) {
    this.demandRepository = demandRepository;
    this.requestService = requestService;
  }

  /**
   * Returns a {@link DemandOverviewDTO} object. The lists within the object are based on all {@link
   * Demand} objects from the database in form of lists for a table.
   *
   * @return {@link DemandOverviewDTO}
   */
  public DemandOverviewDTO getAllDemandOverviewDTOs() {
    List<Demand> demands = demandRepository.findAll();

    return aggregateDemandOverviewDTO(demands);
  }

  /**
   * This method iterates over the list of {@link Demand} objects. In every iteration step, helper
   * methods create the needed data structures to create a {@link DemandOverviewDTO} at the end of
   * the method in the desired format.
   *
   * @param demands List of {@link Demand} objects
   * @return The finally created {@link DemandOverviewDTO} object
   */
  private DemandOverviewDTO aggregateDemandOverviewDTO(List<Demand> demands) {
    Map<String, List<DemandOverviewItem>> demandOverviewItems = new HashMap<>();
    demands.forEach(demand -> assignDemandToHashMap(demand, demandOverviewItems));
    var demandOverviewDTO = new DemandOverviewDTO();
    demandOverviewDTO.setDemandOverviewItems(demandOverviewItems);
    return demandOverviewDTO;
  }

  /**
   * Transfers the quantity of a specific article into a {@link DemandOverviewItem} object and adds
   * it to the passed {@link HashMap}.
   *
   * @param demand {@link Demand} object
   * @param map {@link HashMap} of type {@code &lt;String, List&lt;DemandOverviewItem&gt;&gt;}
   */
  private void assignDemandToHashMap(Demand demand, Map<String, List<DemandOverviewItem>> map) {
    // generate the year and week of the demand object
    int[] yearWeek = YearAndWeek.of(demand.getDeliveryDate());
    int q = demand.getQuantity();
    String artNo = demand.getArticle().getArticleNumber();

    map.putIfAbsent(artNo, new ArrayList<>());
    List<DemandOverviewItem> list = map.get(artNo);

    // Iterate over list of DemandOverviewItems
    var match = false;
    for (DemandOverviewItem dto : list) {
      // check if the date of the current demand is already in the list
      if (dto.getYear() == yearWeek[0] && dto.getWeek() == yearWeek[1]) {
        // if yes, add the quantity of the demand to the dto of the list
        dto.setQuantity(dto.getQuantity() + q);
        match = true;
        break;
      }
    }
    // check if there were no match, create a new DemandOverviewItem in the list
    if (!match) {
      DemandOverviewItem dto =
          DemandOverviewItem.builder()
              .quantity(q)
              .year(yearWeek[0])
              .week(yearWeek[1])
              .status(requestService.getCompositeRequestStatus(yearWeek[0], yearWeek[1], artNo))
              .requestsCompleted(
                  requestService.areRequestsCompleted(yearWeek[0], yearWeek[1], artNo))
              .build();
      list.add(dto);
    }
  }
}
