/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.model.dto.output;

import java.time.format.DateTimeFormatter;
import java.util.UUID;
import lombok.Getter;
import org.siliconeconomy.cda.producer.model.Demand;

/**
 * A DTO holding information on a {@link Demand}. This class covers all the information on a demand
 * that is shown on the demand detail view page.
 *
 * @see DemandDetailViewDTO
 * @author jhohnsel
 */
@Getter
public class DemandDTO {
  private UUID id;
  private String productionNumber;
  private Integer quantity;
  private String deliveryDate;

  public DemandDTO(Demand demand) {
    this.id = demand.getId();
    this.productionNumber = demand.getProductionNumber();
    this.quantity = demand.getQuantity();
    this.deliveryDate = demand.getDeliveryDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
  }
}
