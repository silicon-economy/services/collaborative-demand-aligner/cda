/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.service;

import java.net.URI;
import java.util.NoSuchElementException;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.cda.common.exception.RestCommunicationException;
import org.siliconeconomy.cda.common.model.Company;
import org.siliconeconomy.cda.common.model.dto.CompanyDTO;
import org.siliconeconomy.cda.common.model.dto.RequestRawDTO;
import org.siliconeconomy.cda.supplier.model.dto.output.RequestDTO;
import org.siliconeconomy.cda.supplier.model.enums.RequestStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

/**
 * This service performs (external) REST requests addressed to a supplier.
 *
 * @author jhohnsel
 */
@Service
@Log4j2
public class SupplierGatewayService {

  @Value("${supplier.external.url}")
  private String supplierVictoriaBaseUrl;

  @Getter(AccessLevel.PACKAGE) // only for test class
  @Value("${supplier.external.url.request.path}")
  private String requestPath;

  @Value("${supplier.victoria.company.id}")
  @Getter(AccessLevel.PACKAGE) // only for test class
  private String supplierVictoriaCompanyId;

  /**
   * Sends the specified request to the specified supplier. If the supplier equals Victoria, this
   * results in a real REST call, while a mocked http response is provided throughout the method for
   * any other supplier.
   *
   * @param supplier who the request is sent to
   * @param body body for the http request to send, contains the request information in format
   *     expected by the supplier
   * @return body of the http response from the supplier, contains the request information
   *     interpreted by the supplier in their format
   * @throws RestCommunicationException if the supplier responded with an error code 4xx or 5xx
   */
  public RequestDTO sendNewRequestToSupplier(Company supplier, RequestRawDTO body)
      throws RestCommunicationException {
    ResponseEntity<RequestDTO> supplierResponse;
    if (supplier.getId().equals(supplierVictoriaCompanyId)) {
      // get base URL of the supplier and add appendix of the api
      URI supplierUri = URI.create(getExternalBaseUrlForSupplier(supplier) + requestPath);
      // post request
      try {
        supplierResponse =
            new RestTemplate()
                .exchange(supplierUri, HttpMethod.POST, new HttpEntity<>(body), RequestDTO.class);
      } catch (HttpClientErrorException | HttpServerErrorException ex) {
        String errorMessage =
            "Response from supplier contains http error code (4xx, 5xx): " + ex.getMessage();
        log.error(errorMessage);
        throw new RestCommunicationException(ex);
      }
    } else {
      // as this is a demo case, mock the response for other suppliers than Victoria
      supplierResponse = mockSupplierResponse(body);
    }
    return supplierResponse.getBody();
  }

  /**
   * Mocks the external REST communication between the producer and a supplier by creating the http
   * response manually. Is used in the case that a supplier other than Victoria is requested.
   *
   * @param body the http request body the producer would have sent
   * @return response the supplier would have made based on the requested data
   */
  protected ResponseEntity<RequestDTO> mockSupplierResponse(RequestRawDTO body) {
    CompanyDTO customer = CompanyDTO.builder().id(body.getCustomerId()).build();
    RequestDTO requestDTO =
        new RequestDTO(
            body.getId(),
            body.getArticleNumber(),
            body.getQuantity(),
            body.getYear(),
            body.getWeek(),
            customer,
            RequestStatus.NEW_REQUEST);
    HttpHeaders headers = new HttpHeaders();
    return new ResponseEntity<>(requestDTO, headers, HttpStatus.CREATED);
  }

  /**
   * Retrieves the url for the specified supplier. For now, only for supplier Victoria a url is
   * returned and an exception is thrown for any other supplier.
   *
   * @param supplier url of this supplier is retrieved
   * @return base url (external) of the supplier as string
   * @throws NoSuchElementException if supplier is not victoria
   */
  protected String getExternalBaseUrlForSupplier(Company supplier) {
    // In a productive environment, every supplier would be linked with their url. For the demo
    // case, only the supplier Victoria is linked with a url
    if (supplier.getId().equals(supplierVictoriaCompanyId)) {
      return supplierVictoriaBaseUrl;
    }
    String errorMessage = String.format("No URL for supplier %s known.", supplier.getId());
    log.error(errorMessage);
    throw new NoSuchElementException(errorMessage);
  }
}
