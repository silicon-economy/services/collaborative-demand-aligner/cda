/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.supplier.model.dto.output;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.siliconeconomy.cda.supplier.model.enums.AggregatedRequestStatus;

/**
 * A DTO containing the needed information to display a request detail view.
 *
 * @author Bernd Breitenbach
 */
@Data
@AllArgsConstructor
@Builder
public class RequestDetailViewDTO {
  /** The article number */
  private String articleNumber;

  /** The article description */
  private String articleDescription;

  /** The total number */
  private Integer totalQuantity;

  /** The request data */
  private List<RequestDTO> requests;

  /** The composite status for the total supply */
  private AggregatedRequestStatus totalStatus;

  /** The article unit of this supply */
  private String unit;
}
