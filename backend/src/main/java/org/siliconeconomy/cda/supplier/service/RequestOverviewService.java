/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.supplier.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.siliconeconomy.cda.supplier.model.Request;
import org.siliconeconomy.cda.supplier.model.dto.output.RequestOverviewDTO;
import org.siliconeconomy.cda.supplier.model.dto.output.RequestOverviewItem;
import org.siliconeconomy.cda.supplier.model.enums.AggregatedRequestStatus;
import org.siliconeconomy.cda.supplier.model.repositories.RequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for the RequestController
 *
 * @author Bernd Breitenbach
 */
@Service
public class RequestOverviewService {

  private final RequestRepository requestRepository;

  private final RequestService requestService;

  @Autowired
  public RequestOverviewService(
      RequestRepository requestRepository, RequestService requestService) {
    this.requestRepository = requestRepository;
    this.requestService = requestService;
  }

  /**
   * Returns a {@link RequestOverviewDTO} object. The lists within the object are based on all
   * {@link Request} objects from the database in the form of lists for a table.
   *
   * @return {@link RequestOverviewDTO}
   */
  public RequestOverviewDTO getAllRequestOverviewDTOs() {
    List<Request> requests = requestRepository.findAll();

    return aggregateRequestOverviewDTO(requests);
  }

  /**
   * This method iterates over the list of {@link Request} objects. In every iteration step, helper
   * methods create the needed data structures to create a {@link RequestOverviewDTO} at the end of
   * the method in the desired format.
   *
   * @param requests List of {@link Request} objects
   * @return The finally created {@link RequestOverviewDTO} object
   */
  private RequestOverviewDTO aggregateRequestOverviewDTO(List<Request> requests) {
    Map<String, List<RequestOverviewItem>> requestOverviewItems = new HashMap<>();

    requests.forEach(request -> assignRequestToHashMap(request, requestOverviewItems));

    var requestOverviewDTO = new RequestOverviewDTO();
    requestOverviewDTO.setRequestOverviewItems(requestOverviewItems);
    return requestOverviewDTO;
  }

  /**
   * Transfers the quantity of a specific article into a {@link RequestOverviewItem} object and adds
   * it to the passed {@link HashMap}.
   *
   * @param request {@link Request} object
   * @param map {@link HashMap} of type {@code &lt;String, List&lt;RequestOverviewItem&gt;&gt;}
   */
  private void assignRequestToHashMap(Request request, Map<String, List<RequestOverviewItem>> map) {
    // generate the year and week of the request object
    int q = request.getQuantity();
    String artNo = request.getArticle().getArticleNumber();

    map.putIfAbsent(artNo, new ArrayList<>());
    List<RequestOverviewItem> list = map.get(artNo);

    // Iterate over list of RequestOverviewItems
    var match = false;
    for (RequestOverviewItem dto : list) {
      // check if the date of the current request is already in the list
      if (dto.getYear() == request.getDeliveryYear()
          && dto.getWeek() == request.getDeliveryWeek()) {
        // if yes, add the quantity of the request to the dto of the list
        dto.setQuantity(dto.getQuantity() + q);
        match = true;
        break;
      }
    }
    // check if there were no match, create a new RequestOverviewItem in the
    // list
    if (!match) {
      AggregatedRequestStatus status =
          requestService.getCompositeRequestStatus(
              request.getDeliveryYear(), request.getDeliveryWeek(), artNo);
      RequestOverviewItem dto =
          RequestOverviewItem.builder()
              .quantity(q)
              .year(request.getDeliveryYear())
              .week(request.getDeliveryWeek())
              .status(status)
              .requestsCompleted(
                  requestService.areRequestsAnswered(
                      request.getDeliveryYear(), request.getDeliveryWeek(), artNo))
              .build();
      list.add(dto);
    }
  }
}
