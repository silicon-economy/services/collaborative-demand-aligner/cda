/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.supplier.model.enums;

import java.util.Arrays;
import lombok.Getter;

/**
 * Statuses of a single incoming request.
 *
 * @author jhohnsel
 */
public enum RequestStatus {
  NEW_REQUEST(102),
  POSITIVE_FEEDBACK(103),
  NEGATIVE_FEEDBACK(106),
  POSITIVE_FEEDBACK_CONFIRMED(107),
  POSITIVE_FEEDBACK_REJECTED(108),
  NO_FEEDBACK_REJECTED(109);

  @Getter private final int number;

  RequestStatus(int value) {
    this.number = value;
  }

  public static RequestStatus parseFromInt(int number) throws IllegalArgumentException {
    var statusMatchingNumber =
        Arrays.asList(RequestStatus.values()).stream()
            .filter(status -> number == status.getNumber())
            .findAny();
    if (statusMatchingNumber.isPresent()) {
      return statusMatchingNumber.get();
    }
    // unkown number
    throw new IllegalArgumentException(
        String.format(
            "Error occurred while parsing %s, unknown status number: %s",
            RequestStatus.class, number));
  }
}
