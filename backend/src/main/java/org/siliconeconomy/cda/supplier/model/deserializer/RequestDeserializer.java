/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.supplier.model.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.common.setup.SetupData;
import org.siliconeconomy.cda.common.util.YearAndWeek;
import org.siliconeconomy.cda.supplier.model.Request;
import org.siliconeconomy.cda.supplier.model.enums.RequestStatus;

/**
 * Custom deserializer of a {@link Request}. Resolves the articleNumber node to an {@link Article}
 * object from the {@link SetupData}.
 *
 * @author jhohnsel
 */
@Log4j2
public class RequestDeserializer extends StdDeserializer<Request> {
  private transient SetupData setupData = new SetupData();

  public RequestDeserializer() {
    this(null);
  }

  public RequestDeserializer(Class<?> vc) {
    super(vc);
  }

  @Override
  // custom deserialization
  public Request deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
    JsonNode jsonNode = jp.getCodec().readTree(jp);
    Integer quantity = Integer.parseInt(jsonNode.get("Requested quantity").asText());
    var latestDeliveryDate = LocalDateTime.parse(jsonNode.get("Latest date of delivery").asText());
    var requestStatus = RequestStatus.parseFromInt(jsonNode.get("Status").asInt());
    var requestId = UUID.fromString(jsonNode.get("UUID").asText());
    // retrieve article with matching articleNumber from setupData
    String articleNumber = jsonNode.get("Article No.").asText();
    var article = setupData.findArticleByArticleNumber(articleNumber);
    // retrieve customer with matching id from setupData
    String customerId = jsonNode.get("Customer").asText();
    var customer = setupData.findCompanyById(customerId);

    var yearAndWeek = YearAndWeek.of(latestDeliveryDate);
    var request = new Request();
    request.setId(requestId);
    request.setQuantity(quantity);
    request.setDeliveryYear(yearAndWeek[0]);
    request.setDeliveryWeek(yearAndWeek[1]);
    request.setArticle(article);
    request.setCustomer(customer);
    request.setStatus(requestStatus);
    return request;
  }
}
