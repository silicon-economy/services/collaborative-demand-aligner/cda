/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.supplier.model.dto.output;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.UUID;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.siliconeconomy.cda.common.model.dto.CompanyDTO;
import org.siliconeconomy.cda.supplier.model.Request;
import org.siliconeconomy.cda.supplier.model.enums.RequestStatus;

/**
 * A DTO holding information on a {@link Request}.
 *
 * @author jhohnsel
 */
@Getter
@EqualsAndHashCode
@Builder
@Setter
public class RequestDTO {
  private UUID id;
  private String articleNumber;
  private Integer quantity;
  private Integer year;
  private Integer week;
  private CompanyDTO customer;
  private RequestStatus status;

  public RequestDTO(Request request) {
    this.id = request.getId();
    this.articleNumber = request.getArticle().getArticleNumber();
    this.quantity = request.getQuantity();
    this.year = request.getDeliveryYear();
    this.week = request.getDeliveryWeek();
    this.customer = new CompanyDTO(request.getCustomer());
    this.status = request.getStatus();
  }

  /**
   * This constructor is used when deserializing from JSON.
   *
   * @param id
   * @param articleNumber
   * @param quantity
   * @param year
   * @param week
   * @param customer
   * @param status
   */
  @JsonCreator
  public RequestDTO(
      @JsonProperty("id") UUID id,
      @JsonProperty("articleNumber") String articleNumber,
      @JsonProperty("quantity") Integer quantity,
      @JsonProperty("year") Integer year,
      @JsonProperty("week") Integer week,
      @JsonProperty("customer") CompanyDTO customer,
      @JsonProperty("status") RequestStatus status) {
    this.id = id;
    this.articleNumber = articleNumber;
    this.quantity = quantity;
    this.year = year;
    this.week = week;
    this.customer = customer;
    this.status = status;
  }
}
