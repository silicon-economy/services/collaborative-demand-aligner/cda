/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.supplier.service;

import java.util.List;
import java.util.Optional;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.cda.common.exception.EntityNotFoundException;
import org.siliconeconomy.cda.common.exception.IllegalDataFormatException;
import org.siliconeconomy.cda.supplier.model.Request;
import org.siliconeconomy.cda.common.model.dto.RequestRawDTO;
import org.siliconeconomy.cda.supplier.model.enums.RequestStatus;
import org.siliconeconomy.cda.supplier.model.repositories.RequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service to handle {@link Request} related operations.
 *
 * @author Fabian Serves & Till-Philipp Patron
 */
@Service
@Log4j2
public class SupplierService {

  /**
   * Database interface for {@link Request}
   */
  private final RequestRepository requestRepository;

  /**
   * Service to handle {@link Request}.
   */
  private final RequestService requestService;

  // Autowired Constructor
  @Autowired
  public SupplierService(RequestRepository requestRepository, RequestService requestService) {
    this.requestRepository = requestRepository;
    this.requestService = requestService;
  }

  /**
   * Validates a {@link RequestRawDTO} and converts it into an {@link Request} if valid. Input is
   * validated against already existing {@link Request Requests}.
   *
   * @param requestRawDTO given DTO that will be validated & converted to a {@link Request}.
   * @return converted {@link Request} object.
   * @throws IllegalDataFormatException see {@link RequestService#saveRequest(Request)}
   * @throws EntityNotFoundException see {@link RequestService#saveRequest(Request)}
   */
  public Request processIncomingRequest(RequestRawDTO requestRawDTO)
      throws IllegalDataFormatException, EntityNotFoundException {
    // check if input request id is new
    Optional<Request> savedRequestSameId = requestRepository.findById(requestRawDTO.getId());
    if (savedRequestSameId.isPresent()) {
      // a request with this id exists already, check if it should be updated or is an error
      validateAgainstExistingRequest(requestRawDTO, savedRequestSameId.get());
      log.info(
          String.format(
              "An existing %s will be updated, its status is set to %s.",
              Request.class.getSimpleName(), RequestStatus.NEW_REQUEST.name()));
      // update the existing request according to input data and 'reset' the status to new
      return requestService.updateRequestStatus(
          requestRawDTO.toRequest(), RequestStatus.NEW_REQUEST);
    } else {
      // request id is new
      validateNewRequest(requestRawDTO);
      Request requestFromDto = requestRawDTO.toRequest();
      log.info(
          String.format(
              "Created new %s with id %s.", Request.class.getSimpleName(), requestRawDTO.getId()));
      return requestService.saveRequest(requestFromDto);
    }
  }

  /**
   * Validates a {@link RequestRawDTO} when its id is new. There must be no saved {@link Request}
   * with the same article, week and customer combination.
   *
   * @param requestRawDTO given request input
   * @throws IllegalDataFormatException if the data is inappropriate.
   */
  protected void validateNewRequest(RequestRawDTO requestRawDTO) throws IllegalDataFormatException {
    List<Request> requestsSameWeekArticleCustomer =
        requestRepository.findAllByWeekAndArticleAndCustomer(
            requestRawDTO.getYear(),
            requestRawDTO.getWeek(),
            requestRawDTO.getArticleNumber(),
            requestRawDTO.getCustomerId());
    if (!requestsSameWeekArticleCustomer.isEmpty()) {
      var errorMessage =
          String.format(
              "There is already a current request for article number %s and customer with id %s for"
                  + " week %s/%s. Please update the existing request instead.",
              requestRawDTO.getArticleNumber(),
              requestRawDTO.getCustomerId(),
              requestRawDTO.getWeek(),
              requestRawDTO.getYear());
      throw new IllegalDataFormatException(errorMessage);
    }
  }

  /**
   * Validates a {@link RequestRawDTO} against an already existing {@link Request} that has the same
   * id. It is invalid if the requests differ in week/year or the customer.
   *
   * @param requestRawDTO given request input
   * @param request existing request with the same id
   * @throws IllegalDataFormatException if the data is inappropriate.
   */
  protected void validateAgainstExistingRequest(RequestRawDTO requestRawDTO, Request request)
      throws IllegalDataFormatException {
    if (!requestRawDTO.getWeek().equals(request.getDeliveryWeek())
        || !requestRawDTO.getYear().equals(request.getDeliveryYear())
        || !requestRawDTO.getCustomerId().equals(request.getCustomer().getId())) {
      // updates of the request to a different week or customer are not allowed
      var errorMessage =
          String.format(
              "There already exists a request with ID %s (week %s/%s and customer id %s). You "
                  + "cannot change the week to %s/%s or the customer id to %s.",
              request.getId(),
              request.getDeliveryWeek(),
              request.getDeliveryYear(),
              request.getCustomer().getId(),
              requestRawDTO.getWeek(),
              requestRawDTO.getYear(),
              requestRawDTO.getCustomerId());
      throw new IllegalDataFormatException(errorMessage);
    } else if (!requestRawDTO.getArticleNumber().equals(request.getArticle().getArticleNumber())
        || !requestRawDTO.getQuantity().equals(request.getQuantity())) {
      // differences in article or quantity are no errors, request will be updated
      log.warn(
          String.format(
              "There already exists a request with ID %s. (Article, Quantity) will be updated from "
                  + "(%s,%s) to (%s,%s).",
              request.getId(),
              request.getArticle().getArticleNumber(),
              request.getQuantity(),
              requestRawDTO.getArticleNumber(),
              requestRawDTO.getQuantity()));
    }
  }
}
