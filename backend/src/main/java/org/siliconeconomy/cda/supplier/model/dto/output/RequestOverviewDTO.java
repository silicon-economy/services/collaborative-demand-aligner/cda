/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.supplier.model.dto.output;

import java.util.List;
import java.util.Map;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Represents a data transfer object for the request overview.
 *
 * @author Bernd Breitenbach
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class RequestOverviewDTO {
  /** Maps: Article number -> List of {@link RequestOverviewItem} related to this article number */
  private Map<String, List<RequestOverviewItem>> requestOverviewItems;
}
