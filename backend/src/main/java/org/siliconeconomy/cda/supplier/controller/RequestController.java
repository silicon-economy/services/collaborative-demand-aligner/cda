/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.supplier.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.cda.common.controller.BaseController;
import org.siliconeconomy.cda.common.exception.Details;
import org.siliconeconomy.cda.common.exception.EntityNotFoundException;
import org.siliconeconomy.cda.common.exception.IllegalDataFormatException;
import org.siliconeconomy.cda.common.exception.RestCommunicationException;
import org.siliconeconomy.cda.common.model.dto.ConfirmationResponseDTO;
import org.siliconeconomy.cda.supplier.model.Request;
import org.siliconeconomy.cda.supplier.model.dto.output.RequestDTO;
import org.siliconeconomy.cda.supplier.model.dto.output.RequestDetailViewDTO;
import org.siliconeconomy.cda.supplier.model.dto.output.RequestOverviewDTO;
import org.siliconeconomy.cda.supplier.model.enums.RequestStatus;
import org.siliconeconomy.cda.supplier.service.RequestOverviewService;
import org.siliconeconomy.cda.supplier.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * This class represents an internal (UI to backend) controller to handle requests regarding the
 * {@link Request} and the {@link RequestStatus}.
 *
 * @author Steffen Biehs, Bernd Breitenbach
 */
@RestController("SupplierRequestController")
@Log4j2
public class RequestController implements BaseController {

  private final RequestService requestService;
  private final RequestOverviewService requestOverviewService;

  @Autowired
  public RequestController(
      RequestService requestService, RequestOverviewService requestOverviewService) {
    this.requestService = requestService;
    this.requestOverviewService = requestOverviewService;
  }

  /**
   * This endpoint returns a {@link RequestOverviewDTO} object as JSON. If something went wrong, an
   * appropriate HTTP response will be returned.
   *
   * @return {@link ResponseEntity} with the {@link RequestOverviewDTO} object or an error
   */
  @Operation(summary = "Get the request overview data")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "The request overview data was returned",
            content = @Content(schema = @Schema(implementation = RequestOverviewDTO.class))),
        @ApiResponse(
            responseCode = "500",
            description = "The request overview data could not be loaded",
            content = @Content(examples = @ExampleObject("error message")))
      })
  @GetMapping(
      value = "/supplier/requests/overview",
      produces = {"application/json"})
  public ResponseEntity<Object> getRequestOverviewData() {
    try {
      var requestOverviewDTO = requestOverviewService.getAllRequestOverviewDTOs();
      return new ResponseEntity<>(requestOverviewDTO, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(
          "An internal server error occurs while fetching data for the request overview",
          HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * This endpoint returns a {@link RequestDetailViewDTO} for the requested week and article.
   *
   * @param year the year of the details
   * @param week the week of the details
   * @param articleNumber the article number
   * @return {@link RequestDetailViewDTO}
   */
  @Operation(
      summary = "Get the request detail view data",
      responses = {
        @ApiResponse(
            responseCode = "200",
            description = "The requests details are returned",
            content = @Content(schema = @Schema(implementation = RequestDTO.class))),
        @ApiResponse(
            responseCode = "400",
            description = "The requests details could not be returned. The given ID was null",
            content = @Content(examples = @ExampleObject("error message")))
      })
  @GetMapping(
      path = "/supplier/requests/detail-view/{year}-{week}/{articleNumber}",
      produces = "application/json")
  public ResponseEntity<Object> getRequestDetailData(
      @PathVariable("year") Integer year,
      @PathVariable("week") Integer week,
      @PathVariable("articleNumber") String articleNumber) {
    try {
      RequestDetailViewDTO requestDetailViewDTO =
          requestService.getRequestsDetailData(year, week, articleNumber);
      return new ResponseEntity<>(requestDetailViewDTO, HttpStatus.OK);
    } catch (IllegalArgumentException e) {
      return new ResponseEntity<>(
          "The request could not be returned. The given ID was null", HttpStatus.BAD_REQUEST);
    } catch (Exception e) {
      return new ResponseEntity<>(
          "The request could not be returned.", HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * This endpoint sends the answer to a request (confirmation or rejection) to a customer.
   *
   * @param confirmationResponseDTO Contains the confirmation boolean value and the request id
   * @return {@link HttpStatus} indicating the success of sending the message
   * @throws IllegalDataFormatException handled by the exception handler
   * @throws EntityNotFoundException handled by the exception handler
   * @throws RestCommunicationException handled by the exception handler
   * @see RequestService#sendConfirmationToProducer(ConfirmationResponseDTO)
   */
  @Operation(
      summary = "Send a confirmation or rejection to a customer",
      responses = {
        @ApiResponse(
            responseCode = "200",
            description = "Message was sent to the producer successfully",
            content = @Content(schema = @Schema(implementation = RequestOverviewDTO.class))),
        @ApiResponse(
            responseCode = "400",
            description =
                "An error occured in the process of sending the message or processing the changes "
                    + "in the request.",
            content = @Content(schema = @Schema(implementation = Details.class))),
        @ApiResponse(
            responseCode = "404",
            description = "A referenced entity was not found.",
            content = @Content(schema = @Schema(implementation = Details.class)))
      })
  @PostMapping(
      path = "/supplier/requests/confirmation",
      consumes = "application/json",
      produces = "application/json")
  public HttpStatus confirmationClicked(
      @RequestBody ConfirmationResponseDTO confirmationResponseDTO)
      throws IllegalDataFormatException, EntityNotFoundException, RestCommunicationException {
    requestService.sendConfirmationToProducer(confirmationResponseDTO);
    return HttpStatus.OK;
  }
}
