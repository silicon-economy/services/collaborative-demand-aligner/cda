/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.supplier.model.repositories;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.siliconeconomy.cda.supplier.model.Request;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository of {@link Request}.
 *
 * @author jhohnsel, Bernd Breitenbach
 */
@Repository("SupplierRequestRepository")
public interface RequestRepository extends JpaRepository<Request, UUID> {

  /**
   * Filters all the {@link Request Requests} from the repository where {@link Request#week week}
   * and {@link Request#year year} are equal to the specified calendar week and year and the {@link
   * org.siliconeconomy.cda.common.model.Article Article} of the request matches the specified
   * articleNumber.
   *
   * @param year the year
   * @param week the calendar week
   * @param articleNumber the articleNumber of an {@link org.siliconeconomy.cda.common.model.Article
   *     Article}
   * @return a List of requests from the repository that match the specified conditions (week- and
   *     article-wise)
   */
  public default List<Request> findAllByWeekAndArticleNumber(
      int year, int week, String articleNumber) {
    return findAll().stream()
        .filter(
            request ->
                year == request.getDeliveryYear()
                    && week == request.getDeliveryWeek()
                    && request.getArticle().getArticleNumber().equals(articleNumber))
        .collect(Collectors.toList());
  }

  /**
   * Filters all the {@link Request Requests} from the repository where
   *
   * <ul>
   *   <li>{@link Request#week week} and {@link Request#year year} are equal to the specified
   *       calendar week and year and
   *   <li>the {@link org.siliconeconomy.cda.common.model.Article Article} of the request matches
   *       the specified {@code articleNumber}
   *   <li>the {@link org.siliconeconomy.cda.common.model.Company Company} of the request matches
   *       the specified {@code companyId}
   * </ul>
   *
   * @param year the year
   * @param week the calendar week
   * @param articleNumber the identifier of an {@link org.siliconeconomy.cda.common.model.Article
   *     Article}
   * @param customerId the identifier of a {@link org.siliconeconomy.cda.common.model.Company
   *     Company}
   * @return a List of requests from the repository that match the specified conditions
   */
  public default List<Request> findAllByWeekAndArticleAndCustomer(
      int year, int week, String articleNumber, String customerId) {
    return findAllByWeekAndArticleNumber(year, week, articleNumber).stream()
        .filter(request -> request.getCustomer().getId().equals(customerId))
        .collect(Collectors.toList());
  }
}
