/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.supplier.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.cda.common.exception.EntityNotFoundException;
import org.siliconeconomy.cda.common.exception.IllegalDataFormatException;
import org.siliconeconomy.cda.common.exception.RestCommunicationException;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.common.model.dto.ConfirmationResponseDTO;
import org.siliconeconomy.cda.common.model.repositories.ArticleRepository;
import org.siliconeconomy.cda.common.model.repositories.CompanyRepository;
import org.siliconeconomy.cda.common.service.DataRefreshStompService;
import org.siliconeconomy.cda.supplier.model.Request;
import org.siliconeconomy.cda.supplier.model.dto.output.RequestDTO;
import org.siliconeconomy.cda.supplier.model.dto.output.RequestDetailViewDTO;
import org.siliconeconomy.cda.supplier.model.enums.AggregatedRequestStatus;
import org.siliconeconomy.cda.supplier.model.enums.RequestStatus;
import org.siliconeconomy.cda.supplier.model.repositories.RequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

/**
 * This service holds logic to process requests of the RequestController. Methods that change the
 * content in the database signalize this by emitting a stomp message via {@link
 * DataRefreshStompService}.
 *
 * @author Steffen Biehs
 */
@Service("SupplierRequestService")
@Log4j2
public class RequestService {

  private final RequestRepository requestRepo;
  private final ArticleRepository articleRepository;
  private final CompanyRepository companyRepository;
  private final DataRefreshStompService dataRefreshStompService;

  @Value("${producer.external.url}")
  private String producerPaulBaseUrl;

  @Value("${producer.external.url.confirmation.path}")
  private String confirmationPath;

  @Value("${producer.paul.company.id}")
  private String producerPaulId;

  @Autowired
  public RequestService(
      RequestRepository requestRepo,
      ArticleRepository articleRepository,
      CompanyRepository companyRepository,
      DataRefreshStompService dataRefreshStompService) {
    this.requestRepo = requestRepo;
    this.articleRepository = articleRepository;
    this.companyRepository = companyRepository;
    this.dataRefreshStompService = dataRefreshStompService;
  }

  /**
   * Updates a {@link RequestStatus} within a {@link Request} and saves it in the database. A stomp
   * message is emitted.
   *
   * @param request passed {@link Request}
   * @param status status value to be updated
   * @return updated {@link Request}
   * @throws EntityNotFoundException if related article or company is not present in database.
   * @throws IllegalDataFormatException if object cannot be saved to database.
   * @see #saveRequest(Request)
   */
  public Request updateRequestStatus(Request request, RequestStatus status)
      throws EntityNotFoundException, IllegalDataFormatException {
    request.setStatus(status);
    saveRequest(request);
    return request;
  }

  /**
   * Saves a {@link Request} to the database and emits a stomp message to signalize that the content
   * in the database was changed. Dependencies of the {@link Request} to an {@link Article} and
   * Company entity are solved.
   *
   * @param request {@link Request} to be saved
   * @return the saved entity
   * @throws EntityNotFoundException if related article or company is not present in database.
   * @throws IllegalDataFormatException if the database constraints are violated.
   * @see DataRefreshStompService
   */
  public Request saveRequest(Request request)
      throws EntityNotFoundException, IllegalDataFormatException {
    // solve dependencies
    request.setArticle(
        retrieveEntityByIdOrThrow(request.getArticle().getArticleNumber(), articleRepository));
    request.setCustomer(
        retrieveEntityByIdOrThrow(request.getCustomer().getId(), companyRepository));
    try {
      var savedRequest = requestRepo.save(request);
      log.info(
          String.format(
              "Saved %s with id %s successfully.", Request.class.getSimpleName(), request.getId()));
      dataRefreshStompService.triggerRefresh();
      return savedRequest;
    } catch (RuntimeException exception) {
      var errorMessage = "The database constraints may have been violated. ";
      throw new IllegalDataFormatException(errorMessage);
    }
  }

  /**
   * For the specified week and article number, the {@link AggregatedRequestStatus} of multiple
   * {@link Request requests} is evaluated.
   *
   * @param year year of calendar week
   * @param week calendar week
   * @param articleNumber article number of an {@link Article}
   * @return the composite status of {@link Request requests}, otherwise {@link
   *     AggregatedRequestStatus#ALL_ANSWERED}
   */
  public AggregatedRequestStatus getCompositeRequestStatus(
      int year, int week, String articleNumber) {
    var requests = requestRepo.findAllByWeekAndArticleNumber(year, week, articleNumber);
    return getCompositeRequestStatus(requests);
  }

  /**
   * Checks if for the specified week and article the opportunity of sending requests is given. This
   * is not given if all suppliers have been requested or a firm confirmation ({@link
   * RequestStatus#POSITIVE_FEEDBACK_CONFIRMED}) has been given to one supplier.
   *
   * @param year year of calendar week
   * @param week calendar week
   * @param articleNumber article number of an {@link Article}
   * @return false if the opportunity for sending requests is given, true otherwise
   */
  public boolean areRequestsAnswered(int year, int week, String articleNumber) {
    return getCompositeRequestStatus(year, week, articleNumber)
        != AggregatedRequestStatus.ALL_ANSWERED;
  }

  /**
   * Returns a DTO for the requests details for a given year, week and article
   *
   * @param year the year
   * @param week the week
   * @param articleNumber the number of the article
   * @return the DTO or null if no requests are found
   * @see RequestDetailViewDTO
   */
  public RequestDetailViewDTO getRequestsDetailData(
      Integer year, Integer week, String articleNumber) {
    var totalQuantity = 0;
    RequestDetailViewDTO result = null;
    List<RequestDTO> dtos = new ArrayList<>();
    String articleDescription = null;
    String unit = null;
    var requests = requestRepo.findAllByWeekAndArticleNumber(year, week, articleNumber);
    if (!requests.isEmpty()) {
      for (var request : requests) {
        dtos.add(new RequestDTO(request));
        if (articleDescription == null && request.getArticle() != null) {
          articleDescription = request.getArticle().getDescription();
          unit = request.getArticle().getUnit();
        }
        totalQuantity += request.getQuantity();
      }
      result =
          RequestDetailViewDTO.builder()
              .articleDescription(articleDescription)
              .unit(unit)
              .articleNumber(articleNumber)
              .requests(dtos)
              .totalQuantity(totalQuantity)
              .totalStatus(getCompositeRequestStatus(requests))
              .build();
    }
    return result;
  }

  /**
   * Send a {@link ConfirmationResponseDTO} to a producer to confirm or reject a producer's request.
   *
   * @param confirmationResponseDTO contains the response information
   * @throws IllegalDataFormatException if an error occured while saving the updated {@link Request}
   * @throws EntityNotFoundException if a referenced entity is not found
   * @throws RestCommunicationException if the customer responded with an error code
   */
  public void sendConfirmationToProducer(ConfirmationResponseDTO confirmationResponseDTO)
      throws IllegalDataFormatException, EntityNotFoundException, RestCommunicationException {
    Request request =
        retrieveEntityByIdOrThrow(confirmationResponseDTO.getRequestId(), requestRepo);
    if (request.getCustomer().getId().equals(producerPaulId)) {
      // perfom REST call to Paul
      try {
        new RestTemplate()
            .postForEntity(
                getConfirmationBaseUrlForProducerPaul(), confirmationResponseDTO, HttpStatus.class);
      } catch (HttpClientErrorException | HttpServerErrorException ex) {
        String errorMessage =
            "Response from producer contains http error code (4xx, 5xx): " + ex.getMessage();
        log.error(errorMessage);
        throw new RestCommunicationException(ex);
      }
    }
    // for other customers than Paul just continue as if they sent OK back
    changeRequestStatus(confirmationResponseDTO, request);
  }

  /**
   * For a given list of {@link Request requests} the {@link AggregatedRequestStatus} is evaluated.
   *
   * @param requests a list of requests
   * @return the composite status of {@link Request requests}, otherwise {@link
   *     AggregatedRequestStatus#ALL_ANSWERED}
   */
  protected AggregatedRequestStatus getCompositeRequestStatus(List<Request> requests) {
    AggregatedRequestStatus res = null;
    for (Request r : requests) {
      if (res == null) {
        if (r.getStatus() == RequestStatus.NEW_REQUEST) {
          res = AggregatedRequestStatus.NEW_REQUEST;
        } else {
          res = AggregatedRequestStatus.ALL_ANSWERED;
        }
      } else if (res == AggregatedRequestStatus.NEW_REQUEST
          && r.getStatus() != RequestStatus.NEW_REQUEST) {
        res = AggregatedRequestStatus.FEEDBACK_INCOMPLETE;
        break; // we can leave the loop now because it will not change anymore
      } else if (res == AggregatedRequestStatus.ALL_ANSWERED
          && r.getStatus() == RequestStatus.NEW_REQUEST) {
        res = AggregatedRequestStatus.FEEDBACK_INCOMPLETE;
      }
    }
    if (res == null) {
      res = AggregatedRequestStatus.ALL_ANSWERED;
    }
    return res;
  }

  /**
   * Depending on the value of {@link ConfirmationResponseDTO#confirmed}, the {@link RequestStatus}
   * in the given {@link Request} is changed respectively.
   *
   * @param confirmationResponseDTO data that determines the new request status
   * @param request the {@link Request} the status is changed of
   * @throws IllegalDataFormatException if the update of the request failed
   * @throws EntityNotFoundException if the update of the request failed
   * @see RequestService#updateRequestStatus(Request, RequestStatus)
   */
  protected void changeRequestStatus(
      ConfirmationResponseDTO confirmationResponseDTO, Request request)
      throws IllegalDataFormatException, EntityNotFoundException {
    RequestStatus newStatus;
    if (confirmationResponseDTO.isConfirmed()) {
      newStatus = RequestStatus.POSITIVE_FEEDBACK;
    } else {
      newStatus = RequestStatus.NEGATIVE_FEEDBACK;
    }
    updateRequestStatus(request, newStatus);
  }

  /**
   * Returns the full URL to Pauls endpoint to send a confirmation or rejection.
   *
   * @return URL to said endpoint as String
   */
  protected String getConfirmationBaseUrlForProducerPaul() {
    return producerPaulBaseUrl + confirmationPath;
  }

  /**
   * Tries to retrieve an entity by id from a {@link CrudRepository} or throws an exception.
   *
   * @param id of the requested entity.
   * @param repository any {@link CrudRepository}
   * @return the entity from the repository if present
   * @throws EntityNotFoundException if entity is not present in the repository
   */
  private <T, U> T retrieveEntityByIdOrThrow(U id, CrudRepository<T, U> repository)
      throws EntityNotFoundException {
    Optional<T> object = repository.findById(id);
    if (object.isEmpty()) {
      var errorMessage =
          String.format(
              "There is no entity with id '%s' present in the related %s.",
              id, repository.getClass().getSimpleName());
      throw new EntityNotFoundException(errorMessage);
    }
    return object.get();
  }
}
