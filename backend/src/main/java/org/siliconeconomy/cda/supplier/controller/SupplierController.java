/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.supplier.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.cda.common.controller.BaseController;
import org.siliconeconomy.cda.common.exception.Details;
import org.siliconeconomy.cda.common.exception.EntityNotFoundException;
import org.siliconeconomy.cda.common.exception.IllegalDataFormatException;
import org.siliconeconomy.cda.common.model.dto.RequestRawDTO;
import org.siliconeconomy.cda.supplier.model.Request;
import org.siliconeconomy.cda.supplier.model.dto.output.RequestDTO;
import org.siliconeconomy.cda.supplier.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * External controller to receive requests from the producer/customer.
 *
 * <p>There are endpoints to trigger a request for a supplier and to confirm the request.
 *
 * @author Fabian Serves
 */
@RestController
@Log4j2
public class SupplierController implements BaseController {

  // Contains methods for incoming and outgoing requests
  private final SupplierService supplierService;

  // Autowired Constructor
  @Autowired
  public SupplierController(SupplierService supplierService) {
    this.supplierService = supplierService;
  }

  /**
   * This endpoint creates a new {@link Request}.
   *
   * @param requestRawDTO as a new {@link Request}.
   * @return created {@link Request}.
   * @throws EntityNotFoundException if article or company is not present in database.
   * @throws IllegalDataFormatException if the {@link RequestRawDTO} is inappropriate.
   */
  @Operation(
      tags = "Supplier External",
      summary = "Create new request or update existing.",
      responses = {
        @ApiResponse(
            responseCode = "201",
            description = "A new request was created or got updated."),
        @ApiResponse(
            responseCode = "400",
            description =
                "The new request could not be created. The provided data is inappropriate to create"
                    + " a new request, because the given id or quantity is not valid.",
            content =
                @Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = Details.class))),
        @ApiResponse(
            responseCode = "404",
            description =
                "The new request could not be created. The provided data is inappropriate to create"
                    + " a new request, because Article or Company is not present in database.",
            content =
                @Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = Details.class)))
      })
  @ResponseStatus(HttpStatus.CREATED)
  @PostMapping(path = "/supplier/external/request", consumes = MediaType.APPLICATION_JSON_VALUE)
  public RequestDTO createRequest(@RequestBody RequestRawDTO requestRawDTO)
      throws IllegalDataFormatException, EntityNotFoundException {
    Request request = supplierService.processIncomingRequest(requestRawDTO);
    return new RequestDTO(request);
  }
}
