/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.supplier.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.common.model.Company;
import org.siliconeconomy.cda.supplier.model.deserializer.RequestDeserializer;
import org.siliconeconomy.cda.supplier.model.enums.RequestStatus;

/**
 * Contains information about an incoming request sent by a customer.
 *
 * @author jhohnsel
 */
@Entity(name = "SupplierRequest")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonDeserialize(using = RequestDeserializer.class)
@ToString
public class Request {

  @Id private UUID id;

  /** The article requested. */
  @ManyToOne private Article article;

  /** The requested quantity of the request. */
  @Positive @NotNull private Integer quantity;

  /** The deliveryYear of the delivery time. */
  private Integer deliveryYear;

  /** The calendar deliveryWeek of the delivery time. */
  private Integer deliveryWeek;

  /** The customer who sent the request. */
  @ManyToOne private Company customer;

  /** The latest status of the request. */
  private RequestStatus status;
}
