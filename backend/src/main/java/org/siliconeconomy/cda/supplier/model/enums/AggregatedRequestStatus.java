/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.supplier.model.enums;

import java.util.Arrays;
import lombok.Getter;

/**
 * These are the status concerning the entirety of (multiple) requests for a specific week and
 * article. It is aggregated by the statuses of multiple incoming requests.
 *
 * @see RequestStatus
 * @author jhohnsel
 */
public enum AggregatedRequestStatus {
  FEEDBACK_INCOMPLETE(110),
  NEW_REQUEST(111),
  ALL_ANSWERED(112);

  @Getter private final int number;

  AggregatedRequestStatus(int value) {
    this.number = value;
  }

  public static AggregatedRequestStatus parseFromInt(int number) throws IllegalArgumentException {
    var statusMatchingNumber =
        Arrays.asList(AggregatedRequestStatus.values()).stream()
            .filter(status -> number == status.getNumber())
            .findAny();
    if (statusMatchingNumber.isPresent()) {
      return statusMatchingNumber.get();
    }
    // unkown number
    throw new IllegalArgumentException(
        String.format(
            "Error occurred while parsing %s, unknown status number: %s",
            AggregatedRequestStatus.class, number));
  }
}
