/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.common.setup;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvParser;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.io.ClassPathResource;

/**
 * Utility class to load objects from csv files.
 *
 * @author jhohnsel
 */
@Log4j2
public class CsvImporter {

  private static final CsvMapper MAPPER;
  // initialze mapper
  static {
    MAPPER = new CsvMapper();
    MAPPER.findAndRegisterModules();
  }

  public <T> List<T> loadObjectsFromCsv(Class<T> type, String fileName) {
    try (var resource = new ClassPathResource(fileName).getInputStream(); ) {
      CsvSchema schema = CsvSchema.emptySchema().withHeader().withColumnSeparator(';');
      MappingIterator<T> readValues =
          MAPPER
              .readerFor(type)
              .with(schema)
              .readValues(new InputStreamReader(resource, StandardCharsets.UTF_8));
      List<T> readAll = readValues.readAll();
      log.debug(
          String.format(
              "Read %s objects of type '%s' from csv file '%s'", readAll.size(), type, fileName));
      return readAll;
    } catch (IOException ex) {
      log.error(String.format("Error while parsing csv file '%s'", fileName), ex);
      return Collections.emptyList();
    }
  }

  public List<String[]> loadManyToManyRelationships(String fileName) {
    try {
      // use a different mapper than the static one as it needs to have special features
      var mapper = new CsvMapper();
      CsvSchema manyToManySchema =
          CsvSchema.emptySchema().withSkipFirstDataRow(true).withColumnSeparator(';');
      mapper.enable(CsvParser.Feature.WRAP_AS_ARRAY);
      var resource = new ClassPathResource(fileName).getInputStream();
      MappingIterator<String[]> readValues =
          mapper.readerFor(String[].class).with(manyToManySchema).readValues(resource);
      return readValues.readAll();
    } catch (IOException ex) {
      log.error(
          String.format(
              "Error occurred while loading many to many relationship from file '%s'", fileName),
          ex);
      return Collections.emptyList();
    }
  }
}
