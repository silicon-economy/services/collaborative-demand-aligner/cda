/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.common.setup;

import java.util.List;
import javax.annotation.PostConstruct;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.producer.model.Demand;
import org.siliconeconomy.cda.common.model.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Setup data in database when application starting. The setup order of the different entities is
 * crucial.
 *
 * @see SetupData
 * @see SetupService
 * @author jhohnsel
 */
@Component
public class Setup {

  private final SetupService setupService;
  private final SetupData setupData;

  @Autowired
  public Setup(SetupService setupService) {
    this.setupService = setupService;
    setupData = new SetupData();
  }

  @PostConstruct
  public void setupData() {
    // must set up companies and articles first
    setupCompaniesAndArticles();
    // setup of demands and requests may be in any order
    setupDemands();
    setupOutboundRequests();
    setupInboundRequests();
  }

  private void setupCompaniesAndArticles() {
    // must setup companies before articles
    List<Company> companies = setupData.getCompanies();
    for (Company company : companies) {
      setupService.setupCompany(company);
    }
    // setup articles after companies
    List<Article> articles = setupData.getArticles();
    articles.forEach(setupService::setupArticle);
  }

  private void setupDemands() {
    List<Demand> demands = setupData.getDemands();
    demands.forEach(setupService::setupDemand);
  }

  private void setupOutboundRequests() {
    List<org.siliconeconomy.cda.producer.model.Request> requests = setupData.getOutboundRequests();
    requests.forEach(setupService::setupOutboundRequest);
  }

  private void setupInboundRequests() {
    List<org.siliconeconomy.cda.supplier.model.Request> inboundRequests =
        setupData.getInboundRequests();
    inboundRequests.forEach(setupService::setupInboundRequest);
  }
}
