/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.common.model.repositories;

import org.siliconeconomy.cda.common.model.Article;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository of {@link Article}.
 *
 * @author jhohnsel
 */
@Repository
public interface ArticleRepository extends CrudRepository<Article, String> {}
