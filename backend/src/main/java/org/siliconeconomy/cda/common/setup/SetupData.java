/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.common.setup;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import lombok.AccessLevel;
import lombok.Getter;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.common.model.Company;
import org.siliconeconomy.cda.producer.model.Demand;

/**
 * Loads the setup data from csv files into working memory and resolves relationships. A {@link
 * CsvImporter} is used for loading the csv files.
 *
 * @author jhohnsel
 */
@Getter(AccessLevel.PACKAGE)
public class SetupData {

  private static final CsvImporter CSV_IMPORTER = new CsvImporter();

  private static final String DIR = "database-initialization/";
  private static final String ARTICLE_PATH = DIR + "articles.csv";
  private static final String COMPANY_PATH = DIR + "companies.csv";
  private static final String ARTICLE_AND_SUPPLIER_PATH = DIR + "article-supplier-matching.csv";
  private static final String DEMAND_PATH = DIR + "demands.csv";
  private static final String OUTBOUND_REQUEST_PATH = DIR + "outbound-requests.csv";
  private static final String INBOUND_REQUEST_PATH = DIR + "inbound-requests.csv";

  private List<Article> cachedArticles;
  private List<Company> cachedCompanies;

  public List<Company> getCompanies() {
    if (cachedCompanies == null || cachedCompanies.isEmpty()) {
      List<Company> companies = CSV_IMPORTER.loadObjectsFromCsv(Company.class, COMPANY_PATH);
      cachedCompanies = companies;
    }
    return cachedCompanies;
  }

  public List<Article> getArticles() {
    if (cachedArticles == null || cachedArticles.isEmpty()) {
      // seperately load articles, suppliers and their many to many relationships
      List<Company> allCompanies = getCompanies();
      List<Article> articles = CSV_IMPORTER.loadObjectsFromCsv(Article.class, ARTICLE_PATH);
      List<String[]> articleSupplierCombinations =
          CSV_IMPORTER.loadManyToManyRelationships(ARTICLE_AND_SUPPLIER_PATH);
      // insert the many to many relationship into each article
      for (String[] articleSupplierCombination : articleSupplierCombinations) {
        var article = findArticleByArticleNumber(articles, articleSupplierCombination[0]);
        List<Company> suppliers = article.getSuppliers();
        if (suppliers == null) {
          suppliers = new ArrayList<>();
        }
        suppliers.add(findCompanyById(allCompanies, articleSupplierCombination[1]));
        article.setSuppliers(suppliers);
      }
      cachedArticles = articles;
    }
    return cachedArticles;
  }

  public List<Demand> getDemands() {
    return CSV_IMPORTER.loadObjectsFromCsv(Demand.class, DEMAND_PATH);
  }

  public List<org.siliconeconomy.cda.producer.model.Request> getOutboundRequests() {
    return CSV_IMPORTER.loadObjectsFromCsv(
        org.siliconeconomy.cda.producer.model.Request.class, OUTBOUND_REQUEST_PATH);
  }

  public List<org.siliconeconomy.cda.supplier.model.Request> getInboundRequests() {
    return CSV_IMPORTER.loadObjectsFromCsv(
        org.siliconeconomy.cda.supplier.model.Request.class, INBOUND_REQUEST_PATH);
  }

  /**
   * Throws NoSuchElementException if the articleNumber is not known.
   *
   * @param articleNumber
   * @return
   */
  public Article findArticleByArticleNumber(String articleNumber) {
    List<Article> allArticles = getArticles();
    return findArticleByArticleNumber(allArticles, articleNumber);
  }

  /**
   * Throws NoSuchElementException if the id of the company is not known.
   *
   * @param companyId
   * @return
   */
  public Company findCompanyById(String companyId) {
    List<Company> allCompanies = getCompanies();
    return findCompanyById(allCompanies, companyId);
  }

  private Article findArticleByArticleNumber(List<Article> articles, String articleNumber) {
    Optional<Article> optArticle =
        articles.stream()
            .filter(article -> article.getArticleNumber().equals(articleNumber))
            .findFirst();
    if (optArticle.isPresent()) {
      return optArticle.get();
    }
    throw new NoSuchElementException("Found no article with articleNumber " + articleNumber);
  }

  private Company findCompanyById(List<Company> companies, String companyId) {
    Optional<Company> optCompany =
        companies.stream().filter(company -> company.getId().equals(companyId)).findFirst();
    if (optCompany.isPresent()) {
      return optCompany.get();
    }
    throw new NoSuchElementException("Found no company with id " + companyId);
  }
}
