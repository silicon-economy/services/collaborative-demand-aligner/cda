/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.common.controller;

import org.springframework.web.bind.annotation.RequestMapping;

/**
 * All controllers should implement this controller. Don't add a {@link RequestMapping} annotation
 * to the implementing controller class, declare the path only on method level.
 */
@RequestMapping(path = "/api/v2")
public interface BaseController {}
