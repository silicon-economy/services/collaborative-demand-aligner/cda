/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.common.setup;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.common.model.Company;
import org.siliconeconomy.cda.common.model.repositories.ArticleRepository;
import org.siliconeconomy.cda.common.model.repositories.CompanyRepository;
import org.siliconeconomy.cda.producer.model.Demand;
import org.siliconeconomy.cda.producer.model.repositories.DemandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Saves objects to the database. Fields of the object to save that are entities themselves are
 * replaced by the coresponding instance from the repository.
 *
 * @see Setup
 * @author jhohnsel
 */
@Service
@Log4j2
public class SetupService {

  private final ArticleRepository articleRepository;
  private final CompanyRepository companyRepository;
  private final DemandRepository demandRepository;
  private final org.siliconeconomy.cda.producer.model.repositories.RequestRepository
      outboundRequestRepository;
  private final org.siliconeconomy.cda.supplier.model.repositories.RequestRepository
      inboundRequestRepository;

  @Autowired
  public SetupService(
      ArticleRepository articleRepository,
      CompanyRepository companyRepository,
      DemandRepository demandRepository,
      org.siliconeconomy.cda.producer.model.repositories.RequestRepository
          outboundRequestRepository,
      org.siliconeconomy.cda.supplier.model.repositories.RequestRepository
          inboundRequestRepository) {
    this.articleRepository = articleRepository;
    this.companyRepository = companyRepository;
    this.demandRepository = demandRepository;
    this.outboundRequestRepository = outboundRequestRepository;
    this.inboundRequestRepository = inboundRequestRepository;
  }

  @Transactional
  public void setupCompany(Company company) {
    if (companyRepository.findById(company.getId()).isEmpty()) {
      companyRepository.save(company);
      log.debug(String.format("Saved company with id '%s' to repository", company.getId()));
    }
  }

  @Transactional
  public void setupArticle(Article article) {
    if (articleRepository.findById(article.getArticleNumber()).isEmpty()) {
      // get the suppliers of this article from the repository
      List<Company> suppliers = article.getSuppliers();
      List<Company> persistedSuppliers = new ArrayList<>();
      for (Company supplier : suppliers) {
        Optional<Company> optSupplier = companyRepository.findById(supplier.getId());
        if (optSupplier.isPresent()) {
          persistedSuppliers.add(optSupplier.get());
        } else {
          persistedSuppliers.add(supplier);
        }
      }
      article.setSuppliers(persistedSuppliers);
      articleRepository.save(article);
      log.debug(
          String.format(
              "Saved article with article number '%s' to repository", article.getArticleNumber()));
    }
  }

  @Transactional
  public void setupDemand(Demand demand) {
    if (demandRepository.findById(demand.getId()).isEmpty()) {
      // get the article of this demand from the repository
      String articleNumber = demand.getArticle().getArticleNumber();
      var articleFromRepo = articleRepository.findById(articleNumber);
      if (articleFromRepo.isPresent()) {
        demand.setArticle(articleFromRepo.get());
      }
      demandRepository.save(demand);
      log.debug(String.format("Saved demand with id '%s' to repository", demand.getId()));
    }
  }

  @Transactional
  public void setupOutboundRequest(org.siliconeconomy.cda.producer.model.Request request) {
    if (outboundRequestRepository.findById(request.getId()).isEmpty()) {
      // get the article of this demand from the repository
      String articleNumber = request.getArticle().getArticleNumber();
      var articleFromRepo = articleRepository.findById(articleNumber);
      if (articleFromRepo.isPresent()) {
        request.setArticle(articleFromRepo.get());
      }
      // get the supplier of this demand from the repository
      String supplierId = request.getSupplier().getId();
      var supplierFromRepo = companyRepository.findById(supplierId);
      if (supplierFromRepo.isPresent()) {
        request.setSupplier(supplierFromRepo.get());
      }
      outboundRequestRepository.save(request);
      log.debug(
          String.format("Saved (outbound) request with id '%s' to repository", request.getId()));
    }
  }

  @Transactional
  public void setupInboundRequest(org.siliconeconomy.cda.supplier.model.Request inboundRequest) {
    if (inboundRequestRepository.findById(inboundRequest.getId()).isEmpty()) {
      // get the article of this demand from the repository
      String articleNumber = inboundRequest.getArticle().getArticleNumber();
      var articleFromRepo = articleRepository.findById(articleNumber);
      if (articleFromRepo.isPresent()) {
        inboundRequest.setArticle(articleFromRepo.get());
      }
      // get the supplier of this demand from the repository
      String customerId = inboundRequest.getCustomer().getId();
      var customerFromRepo = companyRepository.findById(customerId);
      if (customerFromRepo.isPresent()) {
        inboundRequest.setCustomer(customerFromRepo.get());
      }
      inboundRequestRepository.save(inboundRequest);
      log.debug(
          String.format(
              "Saved (inbound) request with id '%s' to repository", inboundRequest.getId()));
    }
  }
}
