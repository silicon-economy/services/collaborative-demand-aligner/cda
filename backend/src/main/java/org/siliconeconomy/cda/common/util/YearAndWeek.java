/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.common.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.temporal.WeekFields;
import java.util.Locale;

/**
 * Contains static utilitiy methods to work with calendar weeks of a year and {@link LocalDateTime}.
 *
 * @author jhohnsel
 */
public class YearAndWeek {

  // this class shall not be instantiated and only static methods should be used
  private YearAndWeek() {}

  /**
   * Calculates the calendar week of a date by german conventions.
   *
   * @param localDateTime the date to be converted into a calendar week
   * @return an int array where the first entry is the year and the second entry is the calendar
   *     week
   */
  public static int[] of(LocalDateTime localDateTime) {
    var yearAndWeek = new int[2];
    int year = localDateTime.getYear();
    int week = localDateTime.get(WeekFields.of(Locale.GERMANY).weekOfYear());
    if (week == 0) {
      // this is the case when the first week starts after the start of the year, set
      // it to the year
      // before and its last calendar week
      year -= 1; // year before
      // last week of year before
      week = LocalDate.of(year, Month.DECEMBER, 31).get(WeekFields.of(Locale.GERMANY).weekOfYear());
    }
    yearAndWeek[0] = year;
    yearAndWeek[1] = week;
    return yearAndWeek;
  }

  /**
   * Checks if the specified date is contained in the specified week of the year by german
   * conventions.
   *
   * @param year the year of the calendar week to check
   * @param week the calendar week to check
   * @param dateToCheck a date as {@link LocalDateTime}
   * @return true if the date is contained in the calendar week, false otherwise
   */
  public static boolean containsDate(int year, int week, LocalDateTime dateToCheck) {
    int[] yearAndWeekOfDate = of(dateToCheck);
    return (yearAndWeekOfDate[0] == year) && (yearAndWeekOfDate[1] == week);
  }

  /**
   * Returns a composite key of a year/week tuple e.g. for using in maps
   *
   * @param year the year
   * @param week the week in range [0,53]
   * @return the integer key
   */
  public static int composeKey(int year, int week) {
    return year * 100 + week;
  }
}