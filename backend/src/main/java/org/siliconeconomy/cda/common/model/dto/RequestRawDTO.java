/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.common.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.common.model.Company;
import org.siliconeconomy.cda.supplier.model.Request;
import org.siliconeconomy.cda.supplier.model.enums.RequestStatus;

/**
 * Represents a data transfer object that a producer sends to a supplier when a request is
 * communicated.
 *
 * @author Fabian Serves
 */
@Getter
@AllArgsConstructor
@Builder
@NonNull
public class RequestRawDTO {

  // UUID
  private UUID id;

  // Article id
  @Schema(example = "4711")
  private String articleNumber;

  // Quantity of the given article
  @Schema(example = "15")
  private Integer quantity;

  // Delivery year
  @Schema(example = "2022")
  private Integer year;

  // Delivery week
  @Schema(example = "7")
  private Integer week;

  // Customer id (Indicates from whom the request has been made)
  @Schema(example = "1")
  private String customerId;

  /**
   * Creates a new {@link Request} instance from this {@link RequestRawDTO}. The status is set to
   * {@link RequestStatus#NEW_REQUEST}.
   *
   * <p>The created {@link Request} contains new {@link Article} and {@link Company} instances. Make
   * sure to resolve the dependencies before persisiting the {@link Request}.
   *
   * @return new {@link Request} instance
   */
  public Request toRequest() {
    return Request.builder()
        .id(this.getId())
        .article(Article.builder().articleNumber(this.getArticleNumber()).build())
        .quantity(this.getQuantity())
        .deliveryYear(this.getYear())
        .deliveryWeek(this.getWeek())
        .customer(Company.builder().id(this.getCustomerId()).build())
        .status(RequestStatus.NEW_REQUEST)
        .build();
  }
}
