/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.common.model.repositories;

import org.siliconeconomy.cda.common.model.Company;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository of {@link Company}.
 *
 * @author jhohnsel
 */
@Repository
public interface CompanyRepository extends CrudRepository<Company, String> {}
