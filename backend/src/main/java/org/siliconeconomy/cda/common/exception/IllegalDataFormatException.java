/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.common.exception;

/**
 * Exception for illegal data format.
 *
 * @author Fabian Serves
 */
public class IllegalDataFormatException extends Exception {

  /**
   * Constructs a new custom {@link IllegalDataFormatException} with its message.
   *
   * @param message the detail message.
   */
  public IllegalDataFormatException(String message) {
    super(message);
  }
}
