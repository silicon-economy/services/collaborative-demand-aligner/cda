/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.common.exception;

public class InvalidStatusChangeException extends Exception {

  /**
   * Constructs a new custom {@link InvalidStatusChangeException} with its message.
   *
   * @param message the detail message.
   */
  public InvalidStatusChangeException(String message) {
    super(message);
  }
}
