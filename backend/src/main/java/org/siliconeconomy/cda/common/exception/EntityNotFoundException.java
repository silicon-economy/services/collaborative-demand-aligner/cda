/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.common.exception;

/**
 * Exception for entities unexpectedly not present in the database.
 *
 * @author Fabian Serves
 */
public class EntityNotFoundException extends Exception {

  /**
   * Constructs a new custom EntityNotFoundException with its message.
   *
   * @param message the detail message.
   */
  public EntityNotFoundException(String message) {
    super(message);
  }
}
