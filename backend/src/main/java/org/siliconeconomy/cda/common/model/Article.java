/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.common.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.siliconeconomy.cda.producer.model.Demand;
import org.siliconeconomy.cda.producer.model.Request;

/**
 * Holds information about an article.
 *
 * @see Demand
 * @see Request
 * @author jhohnsel
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Article {

  @Id
  @JsonProperty("Article No")
  private String articleNumber;

  /** A human readable description of the article. */
  @JsonProperty("Description")
  private String description;

  /** The unit in which this article is measured. */
  @JsonProperty("Unit")
  private String unit;

  /** A list of companies who can potentially supply this article. */
  @ManyToMany
  @JoinTable(
      name = "ARTICLE_SUPPLIERS",
      joinColumns = {@JoinColumn(name = "article_id")},
      inverseJoinColumns = {@JoinColumn(name = "supplier_id")})
  private List<Company> suppliers;
}
