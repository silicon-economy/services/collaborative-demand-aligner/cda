/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.common.exception;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

/**
 * Template for exception details forwarded to the Rest-Endpoint.
 *
 * @author Fabian Serves
 */
@AllArgsConstructor
@Builder
@Getter
public class Details {

  // The error details timestamp as date.
  private LocalDateTime timestamp;

  // The error details message as string.
  private String message;

  // The request uri of the origin request.
  private String requestUri;
}
