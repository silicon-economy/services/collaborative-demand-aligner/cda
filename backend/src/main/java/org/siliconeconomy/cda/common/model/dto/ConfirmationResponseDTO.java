/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.common.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * A DTO holding information on the answer to a prior request for articles that confirms or rejects
 * the request. The prior request is referred to by its id.
 *
 * @author Till-Philipp Patron
 */
@Getter
@Builder
@AllArgsConstructor
@EqualsAndHashCode
public class ConfirmationResponseDTO {

  /** the id of the request this answer refers to */
  @Schema(example = "43df5ce6-f02c-44d7-8ec9-9007cd13413e")
  private final UUID requestId;

  /** status whether the request is confirmed or rejected */
  private final boolean confirmed;
}
