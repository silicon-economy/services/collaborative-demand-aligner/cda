/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.common.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Holds information about a company. A company may be referred to as a supplier or as a customer.
 *
 * @author jhohnsel
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Company {
  @Id
  @JsonProperty("Company ID")
  private String id;

  /** The name of the company. */
  @JsonProperty("Company name")
  private String name;

  /** A list of articles that the supplier can potentially supply. */
  @ManyToMany(mappedBy = "suppliers")
  private List<Article> articles;
}
