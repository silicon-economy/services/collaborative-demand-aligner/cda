/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.common.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

/**
 * This service is used to send stomp messages to the frontend when the content in the database has
 * changed.
 *
 * @author jhohnsel
 */
@Service
@Log4j2
public class DataRefreshStompService {

  @Autowired private SimpMessagingTemplate messageTemplate;

  @Value("${cda.ws.topic.refresh}")
  private String topic;

  /**
   * Sends stomp message to signalize that some data has changed and the frontend should refresh its
   * displayed content.
   */
  public void triggerRefresh() {
    messageTemplate.convertAndSend(topic, "refresh site");
    log.debug("send stomp message: refresh");
  }
}
