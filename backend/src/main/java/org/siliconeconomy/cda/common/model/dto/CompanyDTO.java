/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.common.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.siliconeconomy.cda.common.model.Company;

/**
 * A DTO holding information on a {@link Company}.
 *
 * @see DemandDetailViewDTO
 * @author jhohnsel
 */
@Getter
@Builder
@EqualsAndHashCode
public class CompanyDTO {
  private String id;
  private String name;

  public CompanyDTO(Company company) {
    this.id = company.getId();
    this.name = company.getName();
  }

  /**
   * This constructor is used when deserializing from JSON.
   *
   * @param id
   * @param name
   */
  @JsonCreator
  public CompanyDTO(@JsonProperty("id") String id, @JsonProperty("name") String name) {
    this.id = id;
    this.name = name;
  }
}
