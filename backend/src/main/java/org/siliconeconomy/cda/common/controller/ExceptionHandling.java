/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.common.controller;

import com.fasterxml.jackson.databind.JsonMappingException;
import java.time.LocalDateTime;
import java.util.NoSuchElementException;
import lombok.extern.log4j.Log4j2;
import org.siliconeconomy.cda.common.exception.Details;
import org.siliconeconomy.cda.common.exception.EntityNotFoundException;
import org.siliconeconomy.cda.common.exception.IllegalDataFormatException;
import org.siliconeconomy.cda.common.exception.InvalidStatusChangeException;
import org.siliconeconomy.cda.common.exception.RestCommunicationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

/** Contains {@link ExceptionHandler exception handlers} for different exceptions. */
@RestControllerAdvice
@Log4j2
public class ExceptionHandling {

  @ExceptionHandler(JsonMappingException.class)
  public ResponseEntity<Object> handleJsonMappingException(JsonMappingException exception) {
    return new ResponseEntity<>("Error while parsing JSON", HttpStatus.BAD_REQUEST);
  }

  /**
   * This method handles the {@link EntityNotFoundException}.
   *
   * @param exception as {@link EntityNotFoundException}
   * @param request related {@link WebRequest}
   * @return error message
   */
  @ExceptionHandler(EntityNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public ResponseEntity<Details> handleEntityNotFoundException(
      EntityNotFoundException exception, WebRequest request) {
    var details = buildDetails(exception, request);
    return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
  }

  /**
   * This method handles the {@link IllegalDataFormatException}.
   *
   * @param exception as {@link IllegalDataFormatException}
   * @param request related {@link WebRequest}
   * @return error message
   */
  @ExceptionHandler(IllegalDataFormatException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ResponseEntity<Details> handleIllegalDataFormatException(
      IllegalDataFormatException exception, WebRequest request) {
    var details = buildDetails(exception, request);
    return new ResponseEntity<>(details, HttpStatus.BAD_REQUEST);
  }

  /**
   * This method handles the {@link RestCommunicationException}.
   *
   * @param exception as {@link RestCommunicationException}
   * @param request related {@link WebRequest}
   * @return error message
   */
  @ExceptionHandler(RestCommunicationException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ResponseEntity<Details> handleRestCommunicationException(
      RestCommunicationException exception, WebRequest request) {
    var details = buildDetails(exception, request);
    return new ResponseEntity<>(details, HttpStatus.BAD_REQUEST);
  }

  /**
   * This method handles the {@link NoSuchElementException}.
   *
   * @param exception as {@link NoSuchElementException}
   * @param request related {@link WebRequest}
   * @return error message
   */
  @ExceptionHandler(NoSuchElementException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public ResponseEntity<Details> handleNoSuchElementException(
      NoSuchElementException exception, WebRequest request) {
    var details = buildDetails(exception, request);
    return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
  }

  /**
   * This method handles the {@link InvalidStatusChangeException}.
   *
   * @param exception as {@link InvalidStatusChangeException}
   * @param request related {@link WebRequest}
   * @return error message
   */
  @ExceptionHandler(InvalidStatusChangeException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ResponseEntity<Details> handleInvalidStatusChangeException(
      InvalidStatusChangeException exception, WebRequest request) {
    var details = buildDetails(exception, request);
    return new ResponseEntity<>(details, HttpStatus.BAD_REQUEST);
  }

  /**
   * This method handles the {@link IllegalArgumentException}.
   *
   * @param exception as {@link IllegalArgumentException}
   * @param request related {@link WebRequest}
   * @return error message
   */
  @ExceptionHandler(IllegalArgumentException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ResponseEntity<Details> handleIllegalArgumentException(
      IllegalArgumentException exception, WebRequest request) {
    var details = buildDetails(exception, request);
    return new ResponseEntity<>(details, HttpStatus.BAD_REQUEST);
  }

  /**
   * Builds error message to forward to the rest endpoint.
   *
   * @param exception given exception
   * @param request related {@link WebRequest}
   * @return built details
   */
  private Details buildDetails(Exception exception, WebRequest request) {
    return Details.builder()
        .timestamp(LocalDateTime.now())
        .message(exception.getMessage())
        .requestUri(request.getDescription(false))
        .build();
  }
}
