/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.common.exception;

/** Exception for when the response of a REST request has an error http status code. */
public class RestCommunicationException extends Exception {

  /**
   * Constructs a new custom {@link RestCommunicationException} with its cause.
   *
   * @param cause the cause, e.g. could be a HttpClientErrorException or HttpServerErrorException
   */
  public RestCommunicationException(Throwable cause) {
    super(cause);
  }
}
