/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.model.enums;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Unit Test of class {@link AggregatedRequestStatus}.
 *
 * @author jhohnsel
 */
class AggregatedRequestStatusTest {

  @Test
  void parseFromInt_whenUnknownNumber() {
    // arrange
    int invalidRequestStatus = 0;

    // act & assert
    Assertions.assertThatExceptionOfType(IllegalArgumentException.class)
        .isThrownBy(() -> AggregatedRequestStatus.parseFromInt(invalidRequestStatus));
  }

  @Test
  void parseFromInt_whenValidNumber() {
    // arrange
    int noInteraction = 1;

    // act & assert
    Assertions.assertThat(AggregatedRequestStatus.parseFromInt(noInteraction))
        .isEqualTo(AggregatedRequestStatus.NO_INTERACTION);
  }
}
