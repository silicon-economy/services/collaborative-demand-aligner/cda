/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.model.enums;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Unit Test of class {@link RequestStatus}.
 *
 * @author jhohnsel
 */
class RequestStatusTest {

  @Test
  void parseFromInt_whenUnknownNumber() {
    // arrange
    int invalidRequestStatus = 0;

    // act & assert
    Assertions.assertThatExceptionOfType(IllegalArgumentException.class)
        .isThrownBy(() -> RequestStatus.parseFromInt(invalidRequestStatus));
  }

  @Test
  void parseFromInt_whenValidNumber() {
    // arrange
    int noResponse = 2;

    // act & assert
    Assertions.assertThat(RequestStatus.parseFromInt(noResponse))
        .isEqualTo(RequestStatus.NO_RESPONSE);
  }
}
