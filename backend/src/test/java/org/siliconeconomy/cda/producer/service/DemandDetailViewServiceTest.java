/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.service;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.common.model.repositories.ArticleRepository;
import org.siliconeconomy.cda.producer.model.Demand;
import org.siliconeconomy.cda.producer.model.Request;
import org.siliconeconomy.cda.producer.model.dto.output.DemandDetailViewDTO;
import org.siliconeconomy.cda.producer.model.repositories.DemandRepository;
import org.siliconeconomy.cda.producer.model.repositories.RequestRepository;

/**
 * Unit Test of class {@link DemandDetailViewService}.
 *
 * @author jhohnsel
 */
class DemandDetailViewServiceTest {

  @Mock private static DemandRepository demandRepo;
  @Mock private static ArticleRepository articleRepo;
  @Mock private static RequestRepository requestRepo;
  @Mock private static RequestService requestService; // this mock is needed even when unused
  @Mock private DemandDetailViewDTO demandDetailViewDTO;
  @InjectMocks private static DemandDetailViewService service;
  private static final String INVALID_ARTICLE_NUMBER = "invalid";
  private static final String VALID_ARTICLE_NUMBER = "valid";
  private AutoCloseable closeable;

  @BeforeEach
  public void openMocks() {
    closeable = MockitoAnnotations.openMocks(this);
    // mock methods
    when(articleRepo.findById(INVALID_ARTICLE_NUMBER)).thenReturn(Optional.empty());
    when(articleRepo.findById(VALID_ARTICLE_NUMBER))
        .thenReturn(Optional.of(Article.builder().articleNumber(VALID_ARTICLE_NUMBER).build()));
    when(demandRepo.findAllByWeekAndArticleNumber(anyInt(), anyInt(), eq(VALID_ARTICLE_NUMBER)))
        .thenReturn(List.of(new Demand()));
    when(requestRepo.findAllByWeekAndArticleNumber(anyInt(), anyInt(), eq(VALID_ARTICLE_NUMBER)))
        .thenReturn(List.of(new Request()));
    when(demandDetailViewDTO.of(anyInt(), anyInt(), any(), any(), any(), any(), anyBoolean()))
        .thenReturn(demandDetailViewDTO);
  }

  @AfterEach
  public void close() throws Exception {
    closeable.close();
  }

  @Test
  void getDemandDetailView_whenInvalidArticleNumber() {
    // arrange
    int year = 2022;
    int week = 5;

    // act & assert
    assertThatExceptionOfType(IllegalArgumentException.class)
        .isThrownBy(() -> service.getDemandDetailView(year, week, INVALID_ARTICLE_NUMBER));
  }

  @Test
  void getDemandDetailView_validArticleNumber() {
    // arrange
    int year = 2022;
    int week = 5;

    // act & assert
    Assertions.assertThatNoException()
        .isThrownBy(() -> service.getDemandDetailView(year, week, VALID_ARTICLE_NUMBER));
  }
}
