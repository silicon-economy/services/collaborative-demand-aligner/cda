/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.cda.common.model.dto.ConfirmationResponseDTO;
import org.siliconeconomy.cda.producer.service.ProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

/**
 * Unit Test of class {@link ProducerController}.
 *
 * @author Till-Philipp Patron
 * @author jhohnsel
 */
@SpringBootTest
@AutoConfigureMockMvc
class ProducerControllerTest {
  private static final String BASE_PATH = "/api/v2/";
  private static final String CONFIRMATION_PATH = "/producer/external/confirmation";
  private static final ObjectMapper MAPPER = new ObjectMapper();

  @Autowired private MockMvc mockMvc;
  @MockBean private ProducerService producerService;
  private ConfirmationResponseDTO confirmationResponseDTO;

  @BeforeEach
  void setup() {
    confirmationResponseDTO =
        ConfirmationResponseDTO.builder().requestId(UUID.randomUUID()).confirmed(true).build();
  }

  @Test
  void receiveConfirmation_HttpStatusOK() throws Exception {
    // arrange
    doNothing().when(producerService).parseAndSaveConfirmation(any());

    // act & assert
    mockMvc
        .perform(
            post(BASE_PATH + CONFIRMATION_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(MAPPER.writeValueAsString(confirmationResponseDTO)))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    verify(producerService).parseAndSaveConfirmation(confirmationResponseDTO);
  }
}
