/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.cda.common.exception.EntityNotFoundException;
import org.siliconeconomy.cda.common.exception.InvalidStatusChangeException;
import org.siliconeconomy.cda.common.model.dto.ConfirmationResponseDTO;
import org.siliconeconomy.cda.producer.model.Request;
import org.siliconeconomy.cda.producer.model.enums.RequestStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

/**
 * Unit Test of class {@link ProducerService}.
 *
 * @author Till-Philipp Patron
 */
@SpringBootTest
class ProducerServiceTest {
  @MockBean private static RequestService requestService;

  @Autowired private ProducerService producerService;

  private UUID requestId;
  private ConfirmationResponseDTO confirmationResponse;
  private ConfirmationResponseDTO rejectionResponse;
  private Request request;
  private Request requestAfterConfirmation;
  private Request requestAfterRejection;

  @BeforeEach
  public void setup() {
    requestId = UUID.randomUUID();

    confirmationResponse =
        ConfirmationResponseDTO.builder().requestId(requestId).confirmed(true).build();
    rejectionResponse =
        ConfirmationResponseDTO.builder().requestId(requestId).confirmed(false).build();

    request = Request.builder().id(requestId).status(RequestStatus.NO_RESPONSE).build();
    requestAfterConfirmation =
        Request.builder().id(requestId).status(RequestStatus.POSITIVE_FEEDBACK).build();

    requestAfterRejection =
        Request.builder().id(requestId).status(RequestStatus.NEGATIVE_FEEDBACK).build();
  }

  @Test
  void parseAndSaveConfirmation_whenConfirmation()
      throws EntityNotFoundException, InvalidStatusChangeException {
    // arrange
    when(requestService.getRequest(any())).thenReturn(request);
    when(requestService.updateRequestStatus(any(), any())).thenReturn(requestAfterConfirmation);

    // act
    producerService.parseAndSaveConfirmation(confirmationResponse);

    // assert
    verify(requestService).getRequest(requestId);
    verify(requestService).updateRequestStatus(request, RequestStatus.POSITIVE_FEEDBACK);
  }

  @Test
  void parseAndSaveConfirmation_whenRejection()
      throws EntityNotFoundException, InvalidStatusChangeException {
    // arrange
    when(requestService.getRequest(any())).thenReturn(request);
    when(requestService.updateRequestStatus(any(), any())).thenReturn(requestAfterRejection);

    // act
    producerService.parseAndSaveConfirmation(rejectionResponse);

    // assert
    verify(requestService).getRequest(requestId);
    verify(requestService).updateRequestStatus(request, RequestStatus.NEGATIVE_FEEDBACK);
  }

  @Test
  void getRequestIfPresent_whenRequestIsPresent() throws EntityNotFoundException {
    // arrange
    when(requestService.getRequest(any())).thenReturn(request);

    // act
    var result = producerService.getRequestIfPresent(confirmationResponse.getRequestId());

    // assert
    // implicitly also asserts that no exception is thrown
    assertThat(result).isEqualTo(request);
  }

  @Test
  void getRequestIfPresent_whenRequestIsNotPresent() {
    // arrange
    when(requestService.getRequest(any())).thenReturn(null);

    // act & assert
    assertThatExceptionOfType(EntityNotFoundException.class)
        .isThrownBy(() -> producerService.getRequestIfPresent(confirmationResponse.getRequestId()));
  }

  @Test
  void getRequestIfPresent_whenRepoThrows() {
    // arrange
    when(requestService.getRequest(any()))
        .thenThrow(new javax.persistence.EntityNotFoundException());

    // act & assert
    assertThatExceptionOfType(EntityNotFoundException.class)
        .isThrownBy(() -> producerService.getRequestIfPresent(confirmationResponse.getRequestId()));
  }

  @Test
  void checkIfStatusIsChangeable_whenPOSITIVE_FEEDBACK_CONFIRMED() {
    // act & assert
    assertThatExceptionOfType(InvalidStatusChangeException.class)
        .isThrownBy(
            () ->
                producerService.checkIfStatusIsChangeable(
                    RequestStatus.POSITIVE_FEEDBACK_CONFIRMED))
        .withMessage(
            "Status change in request invalid, cannot change status 'POSITIVE_FEEDBACK_CONFIRMED'.");
  }

  @Test
  void checkIfStatusIsChangeable_whenPOSITIVE_FEEDBACK_REJECTED() {
    // act & assert
    assertThatExceptionOfType(InvalidStatusChangeException.class)
        .isThrownBy(
            () ->
                producerService.checkIfStatusIsChangeable(RequestStatus.POSITIVE_FEEDBACK_REJECTED))
        .withMessage(
            "Status change in request invalid, cannot change status 'POSITIVE_FEEDBACK_REJECTED'.");
  }

  @Test
  void checkIfStatusIsChangeable_whenNO_RESPONSE_REJECTED() {
    // act & assert
    assertThatExceptionOfType(InvalidStatusChangeException.class)
        .isThrownBy(
            () -> producerService.checkIfStatusIsChangeable(RequestStatus.NO_RESPONSE_REJECTED))
        .withMessage(
            "Status change in request invalid, cannot change status 'NO_RESPONSE_REJECTED'.");
  }

  @Test
  void checkIfStatusIsChangeable_whenChangeableStatus() {
    // act & assert
    assertThatNoException()
        .isThrownBy(() -> producerService.checkIfStatusIsChangeable(RequestStatus.NO_RESPONSE));
  }

  @Test
  void changeStatus_whenConfirmed() {
    // arrange
    when(requestService.updateRequestStatus(any(), any())).thenReturn(requestAfterConfirmation);

    // act
    producerService.changeStatus(request, true);

    // assert
    verify(requestService).updateRequestStatus(request, RequestStatus.POSITIVE_FEEDBACK);
  }

  @Test
  void changeStatus_whenRejected() {
    // arrange
    when(requestService.updateRequestStatus(any(), any())).thenReturn(requestAfterRejection);

    // act
    producerService.changeStatus(request, false);

    // assert
    verify(requestService).updateRequestStatus(request, RequestStatus.NEGATIVE_FEEDBACK);
  }
}
