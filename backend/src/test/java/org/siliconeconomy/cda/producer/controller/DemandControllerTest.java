/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.common.util.YearAndWeek;
import org.siliconeconomy.cda.producer.model.Demand;
import org.siliconeconomy.cda.producer.model.dto.output.DemandDetailViewDTO;
import org.siliconeconomy.cda.producer.model.dto.output.DemandOverviewDTO;
import org.siliconeconomy.cda.producer.model.dto.output.DemandOverviewItem;
import org.siliconeconomy.cda.producer.service.DemandDetailViewService;
import org.siliconeconomy.cda.producer.service.DemandOverviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * Unit Test of class {@link DemandController}.
 *
 * @author Steffen Biehs
 * @author jhohnsel
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
class DemandControllerTest {

  @MockBean DemandOverviewService demandOverviewService;
  @MockBean DemandDetailViewService demandDetailViewService;

  @Autowired DemandController demandController;

  @Autowired ApplicationContext context;

  @Test
  void getDemandOverviewData_response500() {
    // arrange
    Mockito.when(demandOverviewService.getAllDemandOverviewDTOs())
        .thenThrow(new IllegalArgumentException());
    var expectedResult =
        new ResponseEntity<>(
            "An internal server error occurs while fetching data for the demand overview",
            HttpStatus.INTERNAL_SERVER_ERROR);

    // act
    var result = demandController.getDemandOverviewData();

    // assert
    assertThat(result).isEqualTo(expectedResult);
  }

  @Test
  void getDemandOverviewData_response200() {
    // arrange
    Article article = Article.builder().articleNumber("0815").build();

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    Demand demand1 =
        Demand.builder()
            .article(article)
            .quantity(1)
            .productionNumber("123")
            .deliveryDate(LocalDate.parse("04.02.2022", formatter).atStartOfDay())
            .build();

    Demand demand2 =
        Demand.builder()
            .article(article)
            .quantity(2)
            .productionNumber("456")
            .deliveryDate(LocalDate.parse("04.02.2022", formatter).atStartOfDay())
            .build();

    var demandOverviewDTO = new DemandOverviewDTO();

    int[] yearWeek = YearAndWeek.of(demand1.getDeliveryDate());

    Map<String, List<DemandOverviewItem>> map = new LinkedHashMap<>();
    var dto = DemandOverviewItem.builder().quantity(3).year(yearWeek[0]).week(yearWeek[1]).build();
    List<DemandOverviewItem> list2 = new ArrayList<>();
    list2.add(dto);
    map.put(demand1.getArticle().getArticleNumber(), list2);
    demandOverviewDTO.setDemandOverviewItems(map);

    Mockito.when(demandOverviewService.getAllDemandOverviewDTOs()).thenReturn(demandOverviewDTO);
    var expectedResult = new ResponseEntity<>(demandOverviewDTO, HttpStatus.OK);

    // act
    var result = demandController.getDemandOverviewData();

    // assert
    assertThat(result).isEqualTo(expectedResult);
  }

  @Test
  void getDemandDetailView_response500() {
    // arrange
    when(demandDetailViewService.getDemandDetailView(anyInt(), anyInt(), any()))
        .thenThrow(new IllegalArgumentException());

    // act
    ResponseEntity<Object> response =
        demandController.getDemandDetailView(2022, 5, "articleNumber");

    // assert
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    assertThat(response.getBody())
        .isEqualTo(
            "An internal server error occurred while fetching data for the demand detail view");
  }

  @Test
  void getDemandDetailView_response200() {
    // arrange
    when(demandDetailViewService.getDemandDetailView(anyInt(), anyInt(), any()))
        .thenReturn(new DemandDetailViewDTO());

    // act
    ResponseEntity<Object> response =
        demandController.getDemandDetailView(2022, 5, "articleNumber");

    // assert
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isInstanceOf(DemandDetailViewDTO.class);
  }
}
