/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.model.deserializer;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.cda.producer.model.Request;

/**
 * Unit test of class {@link RequestDeserializer}.
 *
 * @author jhohnsel
 */
class RequestDeserializerTest {

  private static ObjectMapper mapper;
  private static RequestDeserializer deserializer;

  @BeforeAll
  public static void setup() {
    mapper = new ObjectMapper();
    deserializer = new RequestDeserializer();
  }

  /** * Deserialize a {@link Request} from JSON. */
  @Test
  void deserialize_whenCorrect() throws IOException {
    // arrange
    String uuid = "d6ff81ae-4c8b-4e3d-8d52-bb6931ad9482";
    String json =
        "{\"UUID\":\""
            + uuid
            + "\",\"Latest date of delivery\":\"2022-02-04T12:00:00\",\"Article No.\":4711,"
            + "\"Requested quantity\":18,\"Supplier\":123,\"Status\":2}";

    // act
    Object deserialisedRequest = deserialiseRequest(json);

    // assert
    assertThat(deserialisedRequest).isInstanceOf(Request.class);
    assertThat(((Request) deserialisedRequest).getId()).isEqualByComparingTo(UUID.fromString(uuid));
  }

  private Object deserialiseRequest(String json) throws IOException {
    try (InputStream stream = new ByteArrayInputStream(json.getBytes()); ) {
      JsonParser jsonParser = mapper.getFactory().createParser(stream);
      DeserializationContext context = mapper.getDeserializationContext();
      return deserializer.deserialize(jsonParser, context);
    }
  }
}