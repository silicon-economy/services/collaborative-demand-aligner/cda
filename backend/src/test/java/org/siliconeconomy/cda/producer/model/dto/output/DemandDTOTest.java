/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.model.dto.output;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.producer.model.Demand;

/**
 * @author jhohnsel
 */
class DemandDTOTest {

  /** Test constructor from {@link Demand} to {@link DemandDTO}. */
  @Test
  void constructorFromDemandToDTO() {
    // arrange
    LocalDateTime localDateTime = LocalDateTime.parse("2022-02-28T00:00");
    UUID demandId = UUID.randomUUID();
    String productionNumber = UUID.randomUUID().toString();
    int quantity = 9876;
    String articleNo = UUID.randomUUID().toString();

    Demand demand =
        new Demand(
            demandId,
            productionNumber,
            Article.builder().articleNumber(articleNo).build(),
            quantity,
            localDateTime);

    // act
    DemandDTO dto = new DemandDTO(demand);

    // assert
    assertThat(dto.getId()).isEqualTo(demandId);
    assertThat(dto.getProductionNumber()).isEqualTo(productionNumber);
    assertThat(dto.getQuantity()).isEqualTo(quantity);
    assertThat(dto.getDeliveryDate()).isEqualTo("28.02.2022");
  }
}
