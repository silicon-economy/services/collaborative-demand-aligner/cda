/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.cda.common.exception.EntityNotFoundException;
import org.siliconeconomy.cda.common.exception.IllegalDataFormatException;
import org.siliconeconomy.cda.common.exception.RestCommunicationException;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.common.model.Company;
import org.siliconeconomy.cda.producer.model.Request;
import org.siliconeconomy.cda.producer.model.dto.input.DataForRequestDTO;
import org.siliconeconomy.cda.producer.model.enums.RequestStatus;
import org.siliconeconomy.cda.producer.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

/**
 * Unit Test of class {@link RequestController}.
 *
 * @author Steffen Biehs
 * @author jhohnsel
 */
@SpringBootTest
@AutoConfigureMockMvc
class RequestControllerTest {

  private static final String BASE_PATH = "/api/v2/";
  private static final String SEND_REQUEST_PATH = "/producer/requests";
  private static final ObjectMapper MAPPER = new ObjectMapper();

  @MockBean private RequestService requestService;
  @Autowired private MockMvc mockMvc;

  private String dataForRequestDTOJson;
  private Request request;

  @BeforeEach
  public void setup() throws JsonProcessingException {
    // shared data
    final String articleNumber = "0815";
    final String supplierId = "12345";
    final int quantity = 18;
    final int year = 2022;
    final int week = 5;

    // prepare input data
    DataForRequestDTO dataForRequestDTO =
        DataForRequestDTO.builder()
            .articleId(articleNumber)
            .quantity(quantity)
            .year(year)
            .week(week)
            .supplierId(supplierId)
            .build();
    dataForRequestDTOJson = MAPPER.writeValueAsString(dataForRequestDTO);

    // prepare output data
    Article article = Article.builder().articleNumber(articleNumber).build();
    Company supplier = Company.builder().id(supplierId).build();
    request =
        Request.builder()
            .id(UUID.randomUUID())
            .article(article)
            .quantity(quantity)
            .productionYear(year)
            .productionWeek(week)
            .supplier(supplier)
            .status(RequestStatus.NO_RESPONSE)
            .build();
  }

  @Test
  void sendRequest() throws Exception {
    // arrange
    when(requestService.sendRequest(any())).thenReturn(request);

    // act & assert
    mockMvc
        .perform(
            post(BASE_PATH + SEND_REQUEST_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(dataForRequestDTOJson))
        .andExpect(status().isCreated())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.requestId", is(request.getId().toString())));
  }

  @Test
  void sendRequest_whenIllegalDataFormatException() throws Exception {
    // arrange
    String exceptionMessage =
        "Sent request data is not compatible with the response from supplier.";
    when(requestService.sendRequest(any()))
        .thenThrow(new IllegalDataFormatException(exceptionMessage));

    // act & assert
    mockMvc
        .perform(
            post(BASE_PATH + SEND_REQUEST_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(dataForRequestDTOJson))
        .andExpect(status().isBadRequest())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.message", is(exceptionMessage)));
  }

  @Test
  void sendRequest_whenEntityNotFoundException() throws Exception {
    // arrange
    String exceptionMessage = "The supplier this request should be sent to is unkown.";
    when(requestService.sendRequest(any()))
        .thenThrow(new EntityNotFoundException(exceptionMessage));

    // act & assert
    mockMvc
        .perform(
            post(BASE_PATH + SEND_REQUEST_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(dataForRequestDTOJson))
        .andExpect(status().isNotFound())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.message", is(exceptionMessage)));
  }

  @Test
  void sendRequest_whenRestCommunicationException() throws Exception {
    // arrange
    when(requestService.sendRequest(any()))
        .thenThrow(new RestCommunicationException(new RuntimeException()));

    // act & assert
    mockMvc
        .perform(
            post(BASE_PATH + SEND_REQUEST_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(dataForRequestDTOJson))
        .andExpect(status().isBadRequest())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON));
  }
}
