/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

class DeliveryWeekServiceTest {

  private DeliveryWeekService service = new DeliveryWeekService();

  @ParameterizedTest
  @ValueSource(ints = { -100, -1, 0, 54, 100 })
  void testDeliveryToProductionWeek_IllegalArgument(int week) {
    // arrange

    // act & assert
    assertThrows(
        IllegalArgumentException.class,
        () -> {
          service.deliveryToProductionWeek(2022, week);
        });
  }

  @ParameterizedTest
  @CsvSource({
      "2022,52,2023,1",
      "2022,12,2022,13",
      "2021,52,2022,1",
      "2020,52,2020,53",
      "2020,53,2021,1"
  })
  void testDeliveryToProductionWeek(int year, int week, int yearExpected, int weekExpected) {
    // arrange

    // act
    int[] res = service.deliveryToProductionWeek(year, week);

    // assert
    assertThat(res[0]).isEqualTo(yearExpected);
    assertThat(res[1]).isEqualTo(weekExpected);
  }

  @ParameterizedTest
  @ValueSource(ints = { -100, -1, 0, 54, 100 })
  void testProductionToDeliveryWeek_IllegalArgument(int week) {
    // arrange

    // act & assert
    assertThrows(
        IllegalArgumentException.class,
        () -> {
          service.deliveryToProductionWeek(2022, week);
        });
  }

  @ParameterizedTest
  @CsvSource({ "2022,12,2022,11", "2021,53,2021,52", "2021,1,2020,53", "2022,1,2021,52" })
  void testProductionToDeliveryWeek(int year, int week, int yearExpected, int weekExpected) {
    // arrange

    // act
    int[] res = service.productionToDeliveryWeek(year, week);

    // assert
    assertThat(res[0]).isEqualTo(yearExpected);
    assertThat(res[1]).isEqualTo(weekExpected);
  }
}
