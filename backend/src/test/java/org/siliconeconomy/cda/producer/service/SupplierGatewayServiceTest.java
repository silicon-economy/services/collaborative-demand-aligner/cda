/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.UUID;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.siliconeconomy.cda.common.exception.RestCommunicationException;
import org.siliconeconomy.cda.common.model.Company;
import org.siliconeconomy.cda.common.model.dto.CompanyDTO;
import org.siliconeconomy.cda.common.model.dto.RequestRawDTO;
import org.siliconeconomy.cda.supplier.model.dto.output.RequestDTO;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.ResponseEntity;

/** Test of the {@link SupplierGatewayService}. */
@SpringBootTest
class SupplierGatewayServiceTest {
  private static final ObjectMapper MAPPER = new ObjectMapper();
  private static final String COMPANY_ID_PAUL = "3";
  private static MockWebServer mockWebServer;
  private static String baseUrlMockWebServer;

  @SpyBean private SupplierGatewayService supplierGatewayService;
  private Company supplierVictoria;
  private Company otherSupplier;
  private RequestDTO requestDTO;
  private RequestRawDTO requestRawDTO;

  @BeforeAll
  static void startMockWebServer() throws IOException {
    mockWebServer = new MockWebServer();
    mockWebServer.start();
    baseUrlMockWebServer = String.format("http://localhost:%s", mockWebServer.getPort());
  }

  @BeforeEach
  void setup() {
    supplierVictoria =
        Company.builder().id(supplierGatewayService.getSupplierVictoriaCompanyId()).build();
    otherSupplier = Company.builder().id("different id").build();
    CompanyDTO producerPaul = CompanyDTO.builder().id(COMPANY_ID_PAUL).build();

    final UUID requestId = UUID.randomUUID();
    final String articleNumber = "article";
    final int quantity = 18;
    final int year = 2022;
    final int deliveryWeek = 4;
    // data sent to the supplier
    requestRawDTO =
        RequestRawDTO.builder()
            .id(requestId)
            .articleNumber(articleNumber)
            .quantity(quantity)
            .year(year)
            .week(deliveryWeek)
            .customerId(COMPANY_ID_PAUL)
            .build();
    // data responded by the supplier
    requestDTO =
        RequestDTO.builder()
            .id(requestId)
            .articleNumber(articleNumber)
            .quantity(quantity)
            .year(year)
            .week(deliveryWeek)
            .customer(producerPaul)
            .status(org.siliconeconomy.cda.supplier.model.enums.RequestStatus.NEW_REQUEST)
            .build();
  }

  @AfterAll
  static void shutDown() throws IOException {
    mockWebServer.shutdown();
  }

  @Test
  void sendNewRequestToSupplier_whenSupplierIsVictoria_201()
      throws RestCommunicationException, InterruptedException, JsonProcessingException {
    // arrange
    RequestRawDTO bodyOfHttpRequest = requestRawDTO;
    // prepare mocks
    doReturn(baseUrlMockWebServer)
        .when(supplierGatewayService)
        .getExternalBaseUrlForSupplier(any());
    String bodyOfHttpResponse = MAPPER.writeValueAsString(requestDTO);
    MockResponse mockResponse =
        new MockResponse()
            .addHeader("Content-Type", "application/json")
            .setResponseCode(201)
            .setBody(bodyOfHttpResponse);
    mockWebServer.enqueue(mockResponse);

    // act
    RequestDTO result =
        supplierGatewayService.sendNewRequestToSupplier(supplierVictoria, bodyOfHttpRequest);
    RecordedRequest recordedRequest = mockWebServer.takeRequest();

    // assert
    assertThat(result).isEqualTo(requestDTO);
    assertThat(recordedRequest.getMethod()).isEqualTo("POST");
    assertThat(Objects.requireNonNull(recordedRequest.getRequestUrl()).port())
        .isEqualTo(mockWebServer.getPort());
    assertThat(recordedRequest.getPath()).endsWith(supplierGatewayService.getRequestPath());
  }

  @Test
  void sendNewRequestToSupplier_whenSupplierIsVictoria_400()
      throws RestCommunicationException, InterruptedException, JsonProcessingException {
    // arrange
    RequestRawDTO bodyOfHttpRequest = requestRawDTO;
    // prepare mocks
    doReturn(baseUrlMockWebServer)
        .when(supplierGatewayService)
        .getExternalBaseUrlForSupplier(any());
    MockResponse mockResponse =
        new MockResponse().addHeader("Content-Type", "application/json").setResponseCode(400);
    mockWebServer.enqueue(mockResponse);

    // act & assert
    assertThatExceptionOfType(RestCommunicationException.class)
        .isThrownBy(
            () ->
                supplierGatewayService.sendNewRequestToSupplier(
                    supplierVictoria, bodyOfHttpRequest));
  }

  @Test
  void sendNewRequestToSupplier_whenSupplierIsVictoria_500()
      throws RestCommunicationException, InterruptedException, JsonProcessingException {
    // arrange
    RequestRawDTO bodyOfHttpRequest = requestRawDTO;
    // prepare mocks
    doReturn(baseUrlMockWebServer)
        .when(supplierGatewayService)
        .getExternalBaseUrlForSupplier(any());
    MockResponse mockResponse =
        new MockResponse().addHeader("Content-Type", "application/json").setResponseCode(500);
    mockWebServer.enqueue(mockResponse);

    // act & assert
    assertThatExceptionOfType(RestCommunicationException.class)
        .isThrownBy(
            () ->
                supplierGatewayService.sendNewRequestToSupplier(
                    supplierVictoria, bodyOfHttpRequest));
  }

  @Test
  void sendNewRequestToSupplier_whenSupplierIsNotVictoria() throws RestCommunicationException {
    // arrange
    RequestRawDTO bodyOfHttpRequest = requestRawDTO;

    // act
    supplierGatewayService.sendNewRequestToSupplier(otherSupplier, bodyOfHttpRequest);

    // assert
    verify(supplierGatewayService).mockSupplierResponse(bodyOfHttpRequest);
    verify(supplierGatewayService, never()).getExternalBaseUrlForSupplier(otherSupplier);
  }

  @Test
  void mockSupplierResponse() {
    // act
    ResponseEntity<RequestDTO> result = supplierGatewayService.mockSupplierResponse(requestRawDTO);

    // assert
    assertThat(result.getStatusCodeValue()).isEqualTo(201);
    assertThat(result.getBody()).isEqualTo(requestDTO);
  }

  @Test
  void getExternalBaseUrlForSupplier_whenSupplierIsVictoria() {
    // act
    String result = supplierGatewayService.getExternalBaseUrlForSupplier(supplierVictoria);

    // assertThat
    assertThat(result).endsWith("/api/v2/supplier/external");
  }

  @Test
  void getExternalBaseUrlForSupplier_whenSupplierIsNotVictoria() {
    // arrange

    // act & assert
    assertThatExceptionOfType(NoSuchElementException.class)
        .isThrownBy(() -> supplierGatewayService.getExternalBaseUrlForSupplier(otherSupplier))
        .withMessageContaining("No URL for supplier");
  }
}