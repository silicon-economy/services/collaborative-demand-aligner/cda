/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.model.repositories;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.producer.model.Request;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Unit Test of class {@link RequestRepository}.
 *
 * @author jhohnsel
 */
@SpringBootTest
class RequestRepositoryTest {

  // mock the repo to avoid dependencies on the database setup data
  @Spy private static RequestRepository requestRepo;
  private AutoCloseable closeable;

  @BeforeEach
  public void setup() {
    closeable = MockitoAnnotations.openMocks(this);
    // create articles for requests
    Article article1 = Article.builder().articleNumber("testArticle1").build();
    Article article2 = Article.builder().articleNumber("testArticle2").build();
    // create requests
    var year = 2022;
    var week1 = 5;
    var week2 = 6;

    Request request1 =
        Request.builder().productionYear(year).productionWeek(week1).article(article1).build();
    Request request2 =
        Request.builder().productionYear(year).productionWeek(week1).article(article2).build();
    Request request3 =
        Request.builder().productionYear(year).productionWeek(week2).article(article1).build();
    Request request4 =
        Request.builder().productionYear(year).productionWeek(week2).article(article2).build();
    Request request5 =
        Request.builder().productionYear(year).productionWeek(week2).article(article2).build();
    // mock response of RequestRepository#findAll to return requests that are created above
    when(requestRepo.findAll())
        .thenReturn(List.of(request1, request2, request3, request4, request5));
  }

  @AfterEach
  public void tearDown() throws Exception {
    closeable.close();
  }

  @Test
  void findAllByWeekAndArticle_whenOneRequestMatches() {
    // arrange
    int year = 2022;
    int week = 6;
    String articleNumber = "testArticle1";

    // act
    List<Request> requestsFound =
        requestRepo.findAllByWeekAndArticleNumber(year, week, articleNumber);

    // assert
    assertThat(requestsFound).hasSize(1); // only request3 from setup method should be found
  }

  @Test
  void findAllByWeekAndArticle_whenMultipleRequestsMatch() {
    // arrange
    int year = 2022;
    int week = 6;
    String articleNumber = "testArticle2";

    // act
    List<Request> requestsFound =
        requestRepo.findAllByWeekAndArticleNumber(year, week, articleNumber);

    // assert
    assertThat(requestsFound).hasSize(2); // request4 and request5 from setup method should be found
  }

  @Test
  void findAllByWeekAndArticle_whenNoRequestInThisWeekFound() {
    // arrange
    int year = 2022;
    int week = 1;
    String articleNumber = "testArticle2";

    // act
    List<Request> requestsFound =
        requestRepo.findAllByWeekAndArticleNumber(year, week, articleNumber);

    // assert
    assertThat(requestsFound).isEmpty();
  }

  @Test
  void findAllByWeekAndArticle_whenNoRequestWithThisArticleFound() {
    // arrange
    int year = 2022;
    int week = 5;
    String articleNumber = "wrongArticleNumber";

    // act
    List<Request> requestsFound =
        requestRepo.findAllByWeekAndArticleNumber(year, week, articleNumber);

    // assert
    assertThat(requestsFound).isEmpty();
  }
}
