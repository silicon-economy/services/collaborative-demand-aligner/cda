/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.model.repositories;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.producer.model.Demand;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Unit Test of class {@link DemandRepository}.
 *
 * @author jhohnsel
 */
@SpringBootTest
class DemandRepositoryTest {

  // mock the repo to avoid dependencies on the database setup data
  @Spy private static DemandRepository demandRepo;
  private AutoCloseable closeable;

  // needed to use @BeforeEach instead of @BeforeAll as the openMocks didn't work in a static method
  @BeforeEach
  public void setup() {
    closeable = MockitoAnnotations.openMocks(this);
    // create articles for demands
    Article article1 = Article.builder().articleNumber("testArticle1").build();
    Article article2 = Article.builder().articleNumber("testArticle2").build();
    // create demands
    LocalDateTime dayInWeek5 = LocalDateTime.of(2022, Month.FEBRUARY, 4, 0, 0);
    LocalDateTime dayInWeek6 = LocalDateTime.of(2022, Month.FEBRUARY, 11, 0, 0);
    Demand demand1 = Demand.builder().deliveryDate(dayInWeek5).article(article1).build();
    Demand demand2 = Demand.builder().deliveryDate(dayInWeek5).article(article2).build();
    Demand demand3 = Demand.builder().deliveryDate(dayInWeek6).article(article1).build();
    Demand demand4 = Demand.builder().deliveryDate(dayInWeek6).article(article2).build();
    Demand demand5 = Demand.builder().deliveryDate(dayInWeek6).article(article2).build();
    // mock response of DemandRepository#findAll to return demands that are created above
    when(demandRepo.findAll()).thenReturn(List.of(demand1, demand2, demand3, demand4, demand5));
  }

  @AfterEach
  public void tearDown() throws Exception {
    closeable.close();
  }

  @Test
  void findAllByWeekAndArticle_whenOneDemandMatches() {
    // arrange
    int year = 2022;
    int week = 5;
    String articleNumber = "testArticle2";

    // act
    List<Demand> demandsFound = demandRepo.findAllByWeekAndArticleNumber(year, week, articleNumber);

    // assert
    assertThat(demandsFound).hasSize(1); // only demand2 from setup method should be found
  }

  @Test
  void findAllByWeekAndArticle_whenMultipleDemandsMatch() {
    // arrange
    int year = 2022;
    int week = 6;
    String articleNumber = "testArticle2";

    // act
    List<Demand> demandsFound = demandRepo.findAllByWeekAndArticleNumber(year, week, articleNumber);

    // assert
    assertThat(demandsFound).hasSize(2); // demand4 and demand5 from setup method should be found
  }

  @Test
  void findAllByWeekAndArticle_whenNoDemandInThisWeekFound() {
    // arrange
    int year = 2022;
    int week = 1;
    String articleNumber = "testArticle2";

    // act
    List<Demand> demandsFound = demandRepo.findAllByWeekAndArticleNumber(year, week, articleNumber);

    // assert
    assertThat(demandsFound).isEmpty();
  }

  @Test
  void findAllByWeekAndArticle_whenNoDemandWithThisArticleFound() {
    // arrange
    int year = 2022;
    int week = 5;
    String articleNumber = "wrongArticleNumber";

    // act
    List<Demand> demandsFound = demandRepo.findAllByWeekAndArticleNumber(year, week, articleNumber);

    // assert
    assertThat(demandsFound).isEmpty();
  }
}