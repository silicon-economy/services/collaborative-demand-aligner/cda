/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.model.deserializer;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.cda.producer.model.Demand;

/**
 * Unit test of class {@link DemandDeserializer}.
 *
 * @author jhohnsel
 */
class DemandDeserializerTest {

  private static ObjectMapper mapper;
  private static DemandDeserializer deserializer;

  @BeforeAll
  public static void setup() {
    mapper = new ObjectMapper();
    deserializer = new DemandDeserializer();
  }

  /** Deserialize a {@link Demand} from JSON. */
  @Test
  void deserialize_whenCorrect() throws IOException {
    // arrange
    String json =
        "{\"Production number\":815,\"Latest date of delivery\":\"2022-02-04T12:00:00\",\"Article No\":4711,\"Quantity\":4}";

    // act
    Object deserialisedDemand = deserialiseDemand(json);

    // assert
    assertThat(deserialisedDemand).isInstanceOf(Demand.class);
    assertThat(((Demand) deserialisedDemand).getId()).isNotNull();
  }

  private Object deserialiseDemand(String json) throws IOException {
    try (InputStream stream = new ByteArrayInputStream(json.getBytes()); ) {
      JsonParser jsonParser = mapper.getFactory().createParser(stream);
      DeserializationContext context = mapper.getDeserializationContext();
      return deserializer.deserialize(jsonParser, context);
    }
  }
}