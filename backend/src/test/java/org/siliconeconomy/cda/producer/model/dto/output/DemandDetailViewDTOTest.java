/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.model.dto.output;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.common.model.Company;
import org.siliconeconomy.cda.common.model.dto.CompanyDTO;
import org.siliconeconomy.cda.producer.model.Demand;
import org.siliconeconomy.cda.producer.model.Request;
import org.siliconeconomy.cda.producer.model.enums.AggregatedRequestStatus;
import org.siliconeconomy.cda.producer.model.enums.RequestStatus;

/**
 * Unit Test of class {@link DemandDetailViewDTO}.
 *
 * @author jhohnsel
 */
class DemandDetailViewDTOTest {

  /**
   * Tests the constructing method of {@link DemandDetailViewDTO}. Implicitly tests constructors of
   * {@link DemandDTO}, {@link CompanyDTO} and {@link RequestDTO}.
   */
  @Test
  void of() {
    // arrange
    final String articleNumber = "articleNumber1";
    final String articleDescription = "articleDescription";
    final String supplierId = "supplierId";
    final String articleUnit = "unit";
    final String supplierName = "supplierName";
    final UUID demandId = UUID.randomUUID();
    final String productionNumber = "prodNo";
    final int quantity = 987;
    final int totalQuantity = 2 * quantity;
    final LocalDateTime deliveryDate = LocalDateTime.of(2022, Month.FEBRUARY, 4, 0, 0, 0);
    int year = 2022;
    int week = 5;
    final UUID requestId = UUID.randomUUID();
    final RequestStatus requestStatus = RequestStatus.NO_RESPONSE;
    final AggregatedRequestStatus aggregatedRequestStatus = AggregatedRequestStatus.NO_RESPONSE;
    final boolean requestsCompleted = true;
    // create Objects
    Company supplier = Company.builder().id(supplierId).name(supplierName).build();
    Article article =
        Article.builder()
            .articleNumber(articleNumber)
            .description(articleDescription)
            .suppliers(List.of(supplier))
            .unit(articleUnit)
            .build();
    supplier.setArticles(List.of(article));
    Demand demand =
        Demand.builder()
            .id(demandId)
            .article(article)
            .deliveryDate(deliveryDate)
            .productionNumber(productionNumber)
            .quantity(quantity)
            .build();
    Request request =
        Request.builder()
            .id(requestId)
            .article(article)
            .quantity(totalQuantity)
            .productionYear(year)
            .productionWeek(week)
            .supplier(supplier)
            .status(requestStatus)
            .build();

    // act
    DemandDetailViewDTO dto =
        new DemandDetailViewDTO()
            .of(
                year,
                week,
                article,
                List.of(demand, demand),
                List.of(request),
                aggregatedRequestStatus,
                requestsCompleted);

    // assert
    // assertions on DemandDetailViewDTO (top level)
    assertThat(dto.getYear()).isEqualTo(year);
    assertThat(dto.getWeek()).isEqualTo(week);
    assertThat(dto.getArticleNumber()).isEqualTo(articleNumber);
    assertThat(dto.getArticleDescription()).isEqualTo(articleDescription);
    assertThat(dto.getTotalQuantity()).isEqualTo(totalQuantity);
    assertThat(dto.getTotalStatus()).isEqualTo(aggregatedRequestStatus);
    assertThat(dto.isRequestsCompleted()).isTrue();
    assertThat(dto.getDemands()).hasSize(2);
    assertThat(dto.getPossibleSuppliers()).hasSize(1);
    assertThat(dto.getRequests()).hasSize(1);
    assertThat(dto.getUnit()).isEqualTo(articleUnit);
    // assertions for DemandDTO
    assertThat(dto.getDemands().get(0).getId()).isEqualTo(demandId);
    assertThat(dto.getDemands().get(0).getProductionNumber()).isEqualTo(productionNumber);
    assertThat(dto.getDemands().get(0).getQuantity()).isEqualTo(quantity);
    assertThat(dto.getDemands().get(0).getDeliveryDate())
        .isEqualTo(deliveryDate.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
    // assertions for CompanyDTO
    assertThat(dto.getPossibleSuppliers().get(0).getId()).isEqualTo(supplierId);
    assertThat(dto.getPossibleSuppliers().get(0).getName()).isEqualTo(supplierName);
    // assertions for RequestDTO
    assertThat(dto.getRequests().get(0).getId()).isEqualTo(requestId);
    assertThat(dto.getRequests().get(0).getArticleNumber()).isEqualTo(articleNumber);
    assertThat(dto.getRequests().get(0).getQuantity()).isEqualTo(totalQuantity);
    assertThat(dto.getRequests().get(0).getYear()).isEqualTo(year);
    assertThat(dto.getRequests().get(0).getWeek()).isEqualTo(week);
    assertThat(dto.getRequests().get(0).getSupplierId()).isEqualTo(supplierId);
    assertThat(dto.getRequests().get(0).getStatus()).isEqualTo(requestStatus);
  }
}
