/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.siliconeconomy.cda.common.exception.EntityNotFoundException;
import org.siliconeconomy.cda.common.exception.IllegalDataFormatException;
import org.siliconeconomy.cda.common.exception.RestCommunicationException;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.common.model.Company;
import org.siliconeconomy.cda.common.model.dto.CompanyDTO;
import org.siliconeconomy.cda.common.model.dto.RequestRawDTO;
import org.siliconeconomy.cda.common.model.repositories.ArticleRepository;
import org.siliconeconomy.cda.common.model.repositories.CompanyRepository;
import org.siliconeconomy.cda.common.service.DataRefreshStompService;
import org.siliconeconomy.cda.producer.model.Request;
import org.siliconeconomy.cda.producer.model.dto.input.DataForRequestDTO;
import org.siliconeconomy.cda.producer.model.enums.AggregatedRequestStatus;
import org.siliconeconomy.cda.producer.model.enums.RequestStatus;
import org.siliconeconomy.cda.producer.model.repositories.RequestRepository;
import org.siliconeconomy.cda.supplier.model.dto.output.RequestDTO;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

/**
 * Unit Test of class {@link RequestService}.
 *
 * @author Steffen Biehs
 * @author jhohnsel
 */
@SpringBootTest
class RequestServiceTest {
  @Mock private static ArticleRepository articleRepo;
  @Mock private static RequestRepository requestRepo;
  @Mock private static CompanyRepository companyRepo;
  @Mock private static DataRefreshStompService dataRefreshStompService;
  @Mock private static DeliveryWeekService deliveryWeekService;
  @Mock private static SupplierGatewayService supplierGatewayService;

  private RequestService requestService;
  private List<String> emittedStompMessages = new ArrayList<>();
  private DataForRequestDTO dataForRequestDTO;
  private RequestRawDTO requestRawDTO;
  private RequestDTO requestDTO;
  private Request request;
  private final String articleNumber = "0815";
  private Article article;
  private final String supplierId = "12345";
  private Company supplier;
  // pauls company id according to application properties and mockdata
  private final String customerPaulId = "3";
  private final int quantity = 18;
  private final int[] deliveryYearAndWeek = {2022, 1};
  private final int[] productionYearAndWeek = {2023, 2};
  private final UUID requestId = UUID.randomUUID();

  @BeforeEach
  public void setup() throws RestCommunicationException {
    // inject mocks into a spy
    requestService =
        Mockito.spy(
            new RequestService(
                articleRepo,
                requestRepo,
                companyRepo,
                dataRefreshStompService,
                deliveryWeekService,
                supplierGatewayService));
    // set field in RequestService directly as it is not loaded from the properties file in tests
    ReflectionTestUtils.setField(requestService, "myCompanyId", customerPaulId);

    article = Article.builder().articleNumber(articleNumber).build();
    when(articleRepo.findById(article.getArticleNumber())).thenReturn(Optional.of(article));

    supplier = Company.builder().id(supplierId).build();
    when(companyRepo.findById(supplier.getId())).thenReturn(Optional.of(supplier));

    dataForRequestDTO =
        DataForRequestDTO.builder()
            .articleId(articleNumber)
            .quantity(quantity)
            .year(productionYearAndWeek[0])
            .week(productionYearAndWeek[1])
            .supplierId(supplierId)
            .build();

    request =
        Request.builder()
            .id(requestId)
            .article(article)
            .quantity(quantity)
            .productionYear(productionYearAndWeek[0])
            .productionWeek(productionYearAndWeek[1])
            .supplier(supplier)
            .status(RequestStatus.NO_RESPONSE)
            .build();

    requestRawDTO =
        RequestRawDTO.builder()
            .id(requestId)
            .articleNumber(articleNumber)
            .quantity(quantity)
            .year(deliveryYearAndWeek[0])
            .week(deliveryYearAndWeek[1])
            .customerId(customerPaulId)
            .build();

    CompanyDTO customerPaulDTO = CompanyDTO.builder().id(customerPaulId).build();
    requestDTO =
        RequestDTO.builder()
            .id(requestId)
            .articleNumber(articleNumber)
            .quantity(quantity)
            .year(deliveryYearAndWeek[0])
            .week(deliveryYearAndWeek[1])
            .customer(customerPaulDTO)
            .status(org.siliconeconomy.cda.supplier.model.enums.RequestStatus.NEW_REQUEST)
            .build();

    when(deliveryWeekService.productionToDeliveryWeek(
            productionYearAndWeek[0], productionYearAndWeek[1]))
        .thenReturn(deliveryYearAndWeek);
    when(deliveryWeekService.deliveryToProductionWeek(
            deliveryYearAndWeek[0], deliveryYearAndWeek[1]))
        .thenReturn(productionYearAndWeek);
    when(supplierGatewayService.sendNewRequestToSupplier(any(), any())).thenReturn(requestDTO);
    when(requestRepo.save(request)).thenReturn(request);

    // to be able to test stomp communication in this unit test, alter the action to writing the
    // method name of the called method into the List whenever a stomp message would be sent
    doAnswer(invocation -> emittedStompMessages.add("triggerRefresh"))
        .when(dataRefreshStompService)
        .triggerRefresh();
    emittedStompMessages.clear();
  }

  @Test
  void createBodyForRequest() throws EntityNotFoundException {
    // act
    RequestRawDTO result = requestService.createBodyForRequest(dataForRequestDTO);

    // assert
    assertThat(result.getId()).isNotNull();
    assertThat(result.getArticleNumber()).isEqualTo(articleNumber);
    assertThat(result.getQuantity()).isEqualTo(quantity);
    assertThat(result.getYear()).isEqualTo(deliveryYearAndWeek[0]);
    assertThat(result.getWeek()).isEqualTo(deliveryYearAndWeek[1]);
    assertThat(result.getCustomerId()).isEqualTo(customerPaulId);
  }

  @Test
  void createBodyForRequest_whenThrows() {
    // arrange
    when(articleRepo.findById(dataForRequestDTO.getArticleId())).thenReturn(Optional.empty());

    // act & assert
    assertThatExceptionOfType(EntityNotFoundException.class)
        .isThrownBy(() -> requestService.createBodyForRequest(dataForRequestDTO))
        .withMessage("Unknown article in request to be sent.");
  }

  @Test
  void validateSupplierResponseAgainstRequestOrThrow() throws IllegalDataFormatException {
    // act & assert
    assertThatNoException()
        .isThrownBy(
            () ->
                requestService.validateSupplierResponseAgainstRequestOrThrow(
                    requestRawDTO, requestDTO));
  }

  @Test
  void validateSupplierResponseAgainstRequestOrThrow_whenDifferentId() {
    // arrange
    requestDTO.setId(UUID.randomUUID());

    // act & assert
    assertThatExceptionOfType(IllegalDataFormatException.class)
        .isThrownBy(
            () ->
                requestService.validateSupplierResponseAgainstRequestOrThrow(
                    requestRawDTO, requestDTO));
  }

  @Test
  void validateSupplierResponseAgainstRequestOrThrow_whenDifferentArticle() {
    // arrange
    requestDTO.setArticleNumber("different article");

    // act & assert
    assertThatExceptionOfType(IllegalDataFormatException.class)
        .isThrownBy(
            () ->
                requestService.validateSupplierResponseAgainstRequestOrThrow(
                    requestRawDTO, requestDTO));
  }

  @Test
  void validateSupplierResponseAgainstRequestOrThrow_whenDifferentQuantity() {
    // arrange
    requestDTO.setQuantity(12789);

    // act & assert
    assertThatExceptionOfType(IllegalDataFormatException.class)
        .isThrownBy(
            () ->
                requestService.validateSupplierResponseAgainstRequestOrThrow(
                    requestRawDTO, requestDTO));
  }

  @Test
  void validateSupplierResponseAgainstRequestOrThrow_whenDifferentYear() {
    // arrange
    requestDTO.setYear(111);

    // act & assert
    assertThatExceptionOfType(IllegalDataFormatException.class)
        .isThrownBy(
            () ->
                requestService.validateSupplierResponseAgainstRequestOrThrow(
                    requestRawDTO, requestDTO));
  }

  @Test
  void validateSupplierResponseAgainstRequestOrThrow_whenDifferentWeek() {
    // arrange
    requestDTO.setWeek(49);

    // act & assert
    assertThatExceptionOfType(IllegalDataFormatException.class)
        .isThrownBy(
            () ->
                requestService.validateSupplierResponseAgainstRequestOrThrow(
                    requestRawDTO, requestDTO));
  }

  @Test
  void validateSupplierResponseAgainstRequestOrThrow_whenDifferentCustomer() {
    // arrange
    requestDTO.setCustomer(CompanyDTO.builder().id("other id").build());

    // act & assert
    assertThatExceptionOfType(IllegalDataFormatException.class)
        .isThrownBy(
            () ->
                requestService.validateSupplierResponseAgainstRequestOrThrow(
                    requestRawDTO, requestDTO));
  }

  @Test
  void createRequestObjectFromSentRequest() throws EntityNotFoundException {
    // act
    Request result = requestService.createRequestObjectFromSentRequest(requestRawDTO, supplier);

    // assert
    assertThat(result).isEqualTo(request);
  }

  @Test
  void createRequestObjectFromSentRequest_whenThrows() {
    // arrange
    when(articleRepo.findById(requestRawDTO.getArticleNumber())).thenReturn(Optional.empty());

    // act & assert
    assertThatExceptionOfType(EntityNotFoundException.class)
        .isThrownBy(
            () -> requestService.createRequestObjectFromSentRequest(requestRawDTO, supplier));
  }

  @Test
  void sendRequest()
      throws RestCommunicationException, IllegalDataFormatException, EntityNotFoundException {
    // arrange
    // mock this as the request id would be created randomly and validation would always fail
    doReturn(requestRawDTO).when(requestService).createBodyForRequest(any());

    // act
    Request result = requestService.sendRequest(dataForRequestDTO);

    // assert
    verify(supplierGatewayService).sendNewRequestToSupplier(supplier, requestRawDTO);
    verify(deliveryWeekService)
        .deliveryToProductionWeek(requestRawDTO.getYear(), requestRawDTO.getWeek());
    verify(requestRepo).save(request);
    verify(dataRefreshStompService).triggerRefresh();
    assertThat(result).isEqualTo(request);
    assertThat(emittedStompMessages).hasSize(1).containsExactly("triggerRefresh");
  }

  @Test
  void sendRequest_whenThrows() {
    // arrange
    when(companyRepo.findById(supplierId)).thenReturn(Optional.empty());

    // act & assert
    assertThatExceptionOfType(EntityNotFoundException.class)
        .isThrownBy(() -> requestService.sendRequest(dataForRequestDTO))
        .withMessage("The supplier this request should be sent to is unknown.");
    assertThat(emittedStompMessages).isEmpty();
  }

  @Test
  void updateRequestStatus_sameStatus() {
    // arrange
    var statusBeforeUpdate = request.getStatus();

    // act
    var result = requestService.updateRequestStatus(request, statusBeforeUpdate);

    // assert
    assertThat(result.getStatus()).isEqualTo(statusBeforeUpdate);
    assertThat(emittedStompMessages).hasSize(1).containsExactly("triggerRefresh");
  }

  @Test
  void updateRequestStatus_differentStatus() {
    // arrange
    var newStatus = RequestStatus.POSITIVE_FEEDBACK;

    // act
    var result = requestService.updateRequestStatus(request, newStatus);

    // assert
    assertThat(result.getStatus()).isEqualTo(newStatus);
    assertThat(emittedStompMessages).hasSize(1).containsExactly("triggerRefresh");
  }

  @Test
  void getCompositeRequestStatus() {
    // arrange
    String articleNumber = "0001";
    var art1 = Article.builder().articleNumber(articleNumber).build();

    // week 1: one positive confirmed + one else
    var req1a =
        Request.builder()
            .article(art1)
            .productionYear(2022)
            .productionWeek(01)
            .status(RequestStatus.POSITIVE_FEEDBACK_CONFIRMED)
            .build();
    var req1b =
        Request.builder()
            .article(art1)
            .productionYear(2022)
            .productionWeek(01)
            .status(RequestStatus.NO_RESPONSE)
            .build();
    when(requestRepo.findAllByWeekAndArticleNumber(2022, 1, articleNumber))
        .thenReturn(List.of(req1a, req1b));

    // week 2: one positive + one else
    var req2 =
        Request.builder()
            .article(art1)
            .productionYear(2022)
            .productionWeek(02)
            .status(RequestStatus.POSITIVE_FEEDBACK)
            .build();
    var req3 =
        Request.builder()
            .article(art1)
            .productionYear(2022)
            .productionWeek(02)
            .status(RequestStatus.NO_RESPONSE)
            .build();
    when(requestRepo.findAllByWeekAndArticleNumber(2022, 2, articleNumber))
        .thenReturn(List.of(req2, req3));

    // week 3: only negative
    var req4 =
        Request.builder()
            .article(art1)
            .productionYear(2022)
            .productionWeek(03)
            .status(RequestStatus.NEGATIVE_FEEDBACK)
            .build();
    when(requestRepo.findAllByWeekAndArticleNumber(2022, 3, articleNumber))
        .thenReturn(List.of(req4));

    // week 4: one no response + one negative
    var req5 =
        Request.builder()
            .article(art1)
            .productionYear(2022)
            .productionWeek(04)
            .status(RequestStatus.NO_RESPONSE)
            .build();
    var req6 =
        Request.builder()
            .article(art1)
            .productionYear(2022)
            .productionWeek(04)
            .status(RequestStatus.NEGATIVE_FEEDBACK)
            .build();
    when(requestRepo.findAllByWeekAndArticleNumber(2022, 4, articleNumber))
        .thenReturn(List.of(req5, req6));

    // week 5: no requests
    when(requestRepo.findAllByWeekAndArticleNumber(2022, 5, articleNumber)).thenReturn(List.of());

    // act
    var result1 = requestService.getCompositeRequestStatus(2022, 01, articleNumber);
    var result2 = requestService.getCompositeRequestStatus(2022, 02, articleNumber);
    var result3 = requestService.getCompositeRequestStatus(2022, 03, articleNumber);
    var result4 = requestService.getCompositeRequestStatus(2022, 04, articleNumber);
    var result5 = requestService.getCompositeRequestStatus(2022, 05, articleNumber);

    // assert
    assertThat(result1).isEqualTo(AggregatedRequestStatus.POSITIVE_FEEDBACK_CONFIRMED);
    assertThat(result2).isEqualTo(AggregatedRequestStatus.POSITIVE_FEEDBACK);
    assertThat(result3).isEqualTo(AggregatedRequestStatus.NEGATIVE_FEEDBACK);
    assertThat(result4).isEqualTo(AggregatedRequestStatus.NO_RESPONSE);
    assertThat(result5).isEqualTo(AggregatedRequestStatus.NO_INTERACTION);
  }

  @Test
  void getCompositeRequestStatus_whenThrows() {
    // arrange
    String articleNumber = "0001";
    var art1 = Article.builder().articleNumber(articleNumber).build();
    // only rejected request but no confirmed requests is invalid state
    var rejectedRequest =
        Request.builder()
            .article(art1)
            .productionYear(2022)
            .productionWeek(1)
            .status(RequestStatus.POSITIVE_FEEDBACK_REJECTED)
            .build();
    when(requestRepo.findAllByWeekAndArticleNumber(2022, 1, articleNumber))
        .thenReturn(List.of(rejectedRequest));

    // act & assert
    assertThatExceptionOfType(IllegalStateException.class)
        .isThrownBy(() -> requestService.getCompositeRequestStatus(2022, 01, articleNumber));
  }

  @Test
  void areRequestsCompleted_whenNoPossibleSuppliers() {
    // arrange
    int year = 2022;
    int week = 10;
    String articleNumber = "08-15";
    // create objects
    var articleNoSuppliers =
        Article.builder().articleNumber(articleNumber).suppliers(Collections.EMPTY_LIST).build();
    var request =
        Request.builder()
            .article(articleNoSuppliers)
            .productionYear(year)
            .productionWeek(week)
            .status(RequestStatus.NO_RESPONSE)
            .build();
    // stub methods
    when(articleRepo.findById(articleNumber)).thenReturn(Optional.of(articleNoSuppliers));
    when(requestRepo.findAllByWeekAndArticleNumber(year, week, articleNumber))
        .thenReturn(List.of(request));

    // act
    boolean areRequestsCompleted = requestService.areRequestsCompleted(year, week, articleNumber);

    // assert
    assertThat(areRequestsCompleted).isTrue();
  }

  @Test
  void areRequestsCompleted_whenNoRequests() {
    // arrange
    int year = 2022;
    int week = 10;
    String articleNumber = "08-15";
    // create objects
    var supplier1 = new Company();
    var articleWithSuppliers =
        Article.builder().articleNumber(articleNumber).suppliers(List.of(supplier1)).build();
    // stub methods
    when(articleRepo.findById(articleNumber)).thenReturn(Optional.of(articleWithSuppliers));
    when(requestRepo.findAllByWeekAndArticleNumber(year, week, articleNumber))
        .thenReturn(Collections.EMPTY_LIST);

    // act
    boolean areRequestsCompleted = requestService.areRequestsCompleted(year, week, articleNumber);

    // assert
    assertThat(areRequestsCompleted).isFalse();
  }

  @Test
  void areRequestsCompleted_whenFirmConfirmation() {
    // arrange
    int year = 2022;
    int week = 10;
    String articleNumber = "08-15";
    // create objects
    var supplier1 = new Company();
    var articleWithSuppliers =
        Article.builder().articleNumber(articleNumber).suppliers(List.of(supplier1)).build();
    var request =
        Request.builder()
            .article(articleWithSuppliers)
            .productionYear(year)
            .productionWeek(week)
            .status(RequestStatus.POSITIVE_FEEDBACK_CONFIRMED)
            .build();
    // stub methods
    when(articleRepo.findById(articleNumber)).thenReturn(Optional.of(articleWithSuppliers));
    when(requestRepo.findAllByWeekAndArticleNumber(year, week, articleNumber))
        .thenReturn(List.of(request));

    // act
    boolean areRequestsCompleted = requestService.areRequestsCompleted(year, week, articleNumber);

    // assert
    assertThat(areRequestsCompleted).isTrue();
  }

  @Test
  void areRequestsCompleted_whenMultipleSuppliersAndAUnrequestedSupplier() {
    // arrange
    int year = 2022;
    int week = 10;
    String articleNumber = "08-15";
    // create objects
    var supplier1 = new Company();
    var supplier2 = new Company();
    var articleWithSuppliers =
        Article.builder()
            .articleNumber(articleNumber)
            .suppliers(List.of(supplier1, supplier2))
            .build();
    var requestToSupplier1 =
        Request.builder()
            .article(articleWithSuppliers)
            .productionYear(year)
            .productionWeek(week)
            .status(RequestStatus.NO_RESPONSE)
            .supplier(supplier1)
            .build();
    // stub methods
    when(articleRepo.findById(articleNumber)).thenReturn(Optional.of(articleWithSuppliers));
    when(requestRepo.findAllByWeekAndArticleNumber(year, week, articleNumber))
        .thenReturn(List.of(requestToSupplier1));

    // act
    boolean areRequestsCompleted = requestService.areRequestsCompleted(year, week, articleNumber);

    // assert
    assertThat(areRequestsCompleted).isFalse();
  }
}
