/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.producer.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.common.util.YearAndWeek;
import org.siliconeconomy.cda.producer.model.Demand;
import org.siliconeconomy.cda.producer.model.dto.output.DemandOverviewDTO;
import org.siliconeconomy.cda.producer.model.dto.output.DemandOverviewItem;
import org.siliconeconomy.cda.producer.model.enums.AggregatedRequestStatus;
import org.siliconeconomy.cda.producer.model.repositories.DemandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * Unit Test of class {@link DemandOverviewService}.
 *
 * @author Steffen Biehs
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
class DemandOverviewServiceTest {

  @MockBean DemandRepository mockDemandRepository;

  @MockBean RequestService mockRequestService;

  @Autowired DemandOverviewService demandOverviewService;
  @Autowired ApplicationContext context;

  @Test
  void getAllDemandOverviewDTOs_demandsAreEmpty() {
    // arrange
    Mockito.when(mockDemandRepository.findAll()).thenReturn(Collections.emptyList());
    Mockito.when(mockRequestService.getCompositeRequestStatus(anyInt(), anyInt(), any()))
        .thenReturn(AggregatedRequestStatus.NO_INTERACTION);
    var expectedResult = new DemandOverviewDTO();
    expectedResult.setDemandOverviewItems(new LinkedHashMap<>());

    // act
    var result = demandOverviewService.getAllDemandOverviewDTOs();

    // assert
    assertThat(result).isEqualTo(expectedResult);
  }

  @Test
  void getAllDemandOverviewDTOs_demandsWithSameWeekAndSameArticleNumber() {
    // arrange
    Article article = Article.builder().articleNumber("0815").build();

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    Demand demand1 =
        Demand.builder()
            .article(article)
            .quantity(1)
            .productionNumber("123")
            .deliveryDate(LocalDate.parse("03.02.2022", formatter).atStartOfDay())
            .build();

    Demand demand2 =
        Demand.builder()
            .article(article)
            .quantity(2)
            .productionNumber("456")
            .deliveryDate(LocalDate.parse("04.02.2022", formatter).atStartOfDay())
            .build();

    List<Demand> demands = new ArrayList<>();
    demands.add(demand1);
    demands.add(demand2);

    Mockito.when(mockDemandRepository.findAll()).thenReturn(demands);
    Mockito.when(mockRequestService.getCompositeRequestStatus(anyInt(), anyInt(), any()))
        .thenReturn(AggregatedRequestStatus.NO_INTERACTION);

    var expectedResult = new DemandOverviewDTO();

    int[] yearWeek = YearAndWeek.of(demand2.getDeliveryDate());

    Map<String, List<DemandOverviewItem>> map = new LinkedHashMap<>();
    var dto =
        DemandOverviewItem.builder()
            .quantity(demand1.getQuantity() + demand2.getQuantity())
            .year(yearWeek[0])
            .week(yearWeek[1])
            .status(AggregatedRequestStatus.NO_INTERACTION)
            .requestsCompleted(false)
            .build();
    List<DemandOverviewItem> list2 = new ArrayList<>();
    list2.add(dto);
    map.put(demand1.getArticle().getArticleNumber(), list2);
    expectedResult.setDemandOverviewItems(map);

    // act
    var result = demandOverviewService.getAllDemandOverviewDTOs();

    // assert
    assertThat(result).isEqualTo(expectedResult);
  }

  @Test
  void getAllDemandOverviewDTOs_demandsWithDifferentDateAndSameArticleNumber() {
    // arrange
    Article article = Article.builder().articleNumber("0815").build();

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    Demand demand1 =
        Demand.builder()
            .article(article)
            .quantity(1)
            .productionNumber("123")
            .deliveryDate(LocalDate.parse("11.02.2022", formatter).atStartOfDay())
            .build();

    Demand demand2 =
        Demand.builder()
            .article(article)
            .quantity(2)
            .productionNumber("456")
            .deliveryDate(LocalDate.parse("04.02.2022", formatter).atStartOfDay())
            .build();

    List<Demand> demands = new ArrayList<>();
    demands.add(demand1);
    demands.add(demand2);

    Mockito.when(mockDemandRepository.findAll()).thenReturn(demands);
    Mockito.when(mockRequestService.getCompositeRequestStatus(anyInt(), anyInt(), any()))
        .thenReturn(AggregatedRequestStatus.NO_INTERACTION);

    var expectedResult = new DemandOverviewDTO();

    int[] yearWeek1 = YearAndWeek.of(demand1.getDeliveryDate());
    int[] yearWeek2 = YearAndWeek.of(demand2.getDeliveryDate());

    Map<String, List<DemandOverviewItem>> map = new LinkedHashMap<>();
    var dto1 =
        DemandOverviewItem.builder()
            .quantity(demand1.getQuantity())
            .year(yearWeek1[0])
            .week(yearWeek1[1])
            .status(AggregatedRequestStatus.NO_INTERACTION)
            .requestsCompleted(false)
            .build();
    var dto2 =
        DemandOverviewItem.builder()
            .quantity(demand2.getQuantity())
            .year(yearWeek2[0])
            .week(yearWeek2[1])
            .status(AggregatedRequestStatus.NO_INTERACTION)
            .requestsCompleted(false)
            .build();
    List<DemandOverviewItem> list2 = new ArrayList<>();
    list2.add(dto1);
    list2.add(dto2);
    map.put(demand1.getArticle().getArticleNumber(), list2);
    expectedResult.setDemandOverviewItems(map);

    // act
    var result = demandOverviewService.getAllDemandOverviewDTOs();

    // assert
    assertThat(result).isEqualTo(expectedResult);
  }

  @Test
  void getAllDemandOverviewDTOs_demandsWithSameDateAndDifferentArticleNumber() {
    // arrange
    Article article1 = Article.builder().articleNumber("0815").build();
    Article article2 = Article.builder().articleNumber("4711").build();

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    Demand demand1 =
        Demand.builder()
            .article(article1)
            .quantity(1)
            .productionNumber("123")
            .deliveryDate(LocalDate.parse("04.02.2022", formatter).atStartOfDay())
            .build();

    Demand demand2 =
        Demand.builder()
            .article(article2)
            .quantity(2)
            .productionNumber("456")
            .deliveryDate(LocalDate.parse("04.02.2022", formatter).atStartOfDay())
            .build();

    List<Demand> demands = new ArrayList<>();
    demands.add(demand1);
    demands.add(demand2);

    Mockito.when(mockDemandRepository.findAll()).thenReturn(demands);
    Mockito.when(mockRequestService.getCompositeRequestStatus(anyInt(), anyInt(), any()))
        .thenReturn(AggregatedRequestStatus.NO_INTERACTION);

    var expectedResult = new DemandOverviewDTO();

    int[] yearWeek = YearAndWeek.of(demand1.getDeliveryDate());

    Map<String, List<DemandOverviewItem>> map = new LinkedHashMap<>();
    var dto1 =
        DemandOverviewItem.builder()
            .quantity(demand1.getQuantity())
            .year(yearWeek[0])
            .week(yearWeek[1])
            .status(AggregatedRequestStatus.NO_INTERACTION)
            .requestsCompleted(false)
            .build();
    List<DemandOverviewItem> list1 = new ArrayList<>();
    list1.add(dto1);
    map.put(demand1.getArticle().getArticleNumber(), list1);

    var dto2 =
        DemandOverviewItem.builder()
            .quantity(demand2.getQuantity())
            .year(yearWeek[0])
            .week(yearWeek[1])
            .status(AggregatedRequestStatus.NO_INTERACTION)
            .requestsCompleted(false)
            .build();
    List<DemandOverviewItem> list2 = new ArrayList<>();
    list2.add(dto2);
    map.put(demand2.getArticle().getArticleNumber(), list2);
    expectedResult.setDemandOverviewItems(map);

    // act
    var result = demandOverviewService.getAllDemandOverviewDTOs();

    // assert
    assertThat(result).isEqualTo(expectedResult);
  }
}
