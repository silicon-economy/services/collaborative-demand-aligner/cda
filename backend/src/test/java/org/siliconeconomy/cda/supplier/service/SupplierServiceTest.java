/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.supplier.service;

import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import lombok.extern.log4j.Log4j2;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.siliconeconomy.cda.common.exception.EntityNotFoundException;
import org.siliconeconomy.cda.common.exception.IllegalDataFormatException;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.common.model.Company;
import org.siliconeconomy.cda.supplier.model.Request;
import org.siliconeconomy.cda.common.model.dto.RequestRawDTO;
import org.siliconeconomy.cda.supplier.model.enums.RequestStatus;
import org.siliconeconomy.cda.supplier.model.repositories.RequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SupplierService.class)
@Log4j2
class SupplierServiceTest {

  // Class under test
  @Autowired private SupplierService supplierService;

  // Dependencies
  @MockBean private RequestService requestServiceMock;
  @MockBean private RequestRepository requestRepositoryMock;

  @Test
  void validateAgainstExistingRequest_allEqual() {
    // Arrange
    var id = UUID.randomUUID();
    var articleNumber = "articleNumber";
    var quantity = 1;
    var week = 1;
    var year = 2022;
    var customerId = "customerId";
    var requestRawDTO =
        RequestRawDTO.builder()
            .id(id)
            .articleNumber(articleNumber)
            .quantity(quantity)
            .year(year)
            .week(week)
            .customerId(customerId)
            .build();
    var request =
        Request.builder()
            .id(id)
            .article(Article.builder().articleNumber(articleNumber).build())
            .quantity(quantity)
            .deliveryYear(year)
            .deliveryWeek(week)
            .customer(Company.builder().id(customerId).build())
            .status(RequestStatus.NEW_REQUEST)
            .build();

    // Act & Assert
    assertThatNoException()
        .isThrownBy(() -> supplierService.validateAgainstExistingRequest(requestRawDTO, request));
  }

  @Test
  void validateAgainstExistingRequest_differentArticle() {
    // Arrange
    var id = UUID.randomUUID();
    var quantity = 1;
    var week = 1;
    var year = 2022;
    var customerId = "customerId";
    var requestRawDTO =
        RequestRawDTO.builder()
            .id(id)
            .articleNumber("article one")
            .quantity(quantity)
            .year(year)
            .week(week)
            .customerId(customerId)
            .build();
    var request =
        Request.builder()
            .id(id)
            .article(Article.builder().articleNumber("different article").build())
            .quantity(quantity)
            .deliveryYear(year)
            .deliveryWeek(week)
            .customer(Company.builder().id(customerId).build())
            .status(RequestStatus.NEW_REQUEST)
            .build();

    // Act & Assert
    assertThatNoException()
        .isThrownBy(() -> supplierService.validateAgainstExistingRequest(requestRawDTO, request));
  }

  @Test
  void validateAgainstExistingRequest_differentQuantity() {
    // Arrange
    var id = UUID.randomUUID();
    var articleNumber = "articleNumber";
    var week = 1;
    var year = 2022;
    var customerId = "customerId";
    var requestRawDTO =
        RequestRawDTO.builder()
            .id(id)
            .articleNumber(articleNumber)
            .quantity(1)
            .year(year)
            .week(week)
            .customerId(customerId)
            .build();
    var request =
        Request.builder()
            .id(id)
            .article(Article.builder().articleNumber(articleNumber).build())
            .quantity(2)
            .deliveryYear(year)
            .deliveryWeek(week)
            .customer(Company.builder().id(customerId).build())
            .status(RequestStatus.NEW_REQUEST)
            .build();

    // Act & Assert
    assertThatNoException()
        .isThrownBy(() -> supplierService.validateAgainstExistingRequest(requestRawDTO, request));
  }

  @Test
  void validateAgainstExistingRequest_differentWeek() {
    // Arrange
    var id = UUID.randomUUID();
    var articleNumber = "articleNumber";
    var quantity = 1;
    var year = 2020;
    var customerId = "customerId";
    var requestRawDTO =
        RequestRawDTO.builder()
            .id(id)
            .articleNumber(articleNumber)
            .quantity(quantity)
            .year(year)
            .week(1)
            .customerId(customerId)
            .build();
    var request =
        Request.builder()
            .id(id)
            .article(Article.builder().articleNumber(articleNumber).build())
            .quantity(quantity)
            .deliveryYear(year)
            .deliveryWeek(2)
            .customer(Company.builder().id(customerId).build())
            .status(RequestStatus.NEW_REQUEST)
            .build();

    // Act & Assert
    assertThatExceptionOfType(IllegalDataFormatException.class)
        .isThrownBy(() -> supplierService.validateAgainstExistingRequest(requestRawDTO, request));
  }

  @Test
  void validateAgainstExistingRequest_differentYear() {
    // Arrange
    var id = UUID.randomUUID();
    var articleNumber = "articleNumber";
    var quantity = 1;
    var week = 1;
    var customerId = "customerId";
    var requestRawDTO =
        RequestRawDTO.builder()
            .id(id)
            .articleNumber(articleNumber)
            .quantity(quantity)
            .year(2021)
            .week(week)
            .customerId(customerId)
            .build();
    var request =
        Request.builder()
            .id(id)
            .article(Article.builder().articleNumber(articleNumber).build())
            .quantity(quantity)
            .deliveryYear(2022)
            .deliveryWeek(week)
            .customer(Company.builder().id(customerId).build())
            .status(RequestStatus.NEW_REQUEST)
            .build();

    // Act & Assert
    assertThatExceptionOfType(IllegalDataFormatException.class)
        .isThrownBy(() -> supplierService.validateAgainstExistingRequest(requestRawDTO, request));
  }

  @Test
  void validateAgainstExistingRequest_differentCustomer() {
    // Arrange
    var id = UUID.randomUUID();
    var articleNumber = "articleNumber";
    var quantity = 1;
    var year = 2022;
    var week = 1;
    var requestRawDTO =
        RequestRawDTO.builder()
            .id(id)
            .articleNumber(articleNumber)
            .quantity(quantity)
            .year(year)
            .week(week)
            .customerId("customerId-1")
            .build();
    var request =
        Request.builder()
            .id(id)
            .article(Article.builder().articleNumber(articleNumber).build())
            .quantity(quantity)
            .deliveryYear(year)
            .deliveryWeek(week)
            .customer(Company.builder().id("customerId-2").build())
            .status(RequestStatus.NEW_REQUEST)
            .build();

    // Act & Assert
    assertThatExceptionOfType(IllegalDataFormatException.class)
        .isThrownBy(() -> supplierService.validateAgainstExistingRequest(requestRawDTO, request));
  }

  @Test
  void validateNewRequest_whenValid() throws IllegalDataFormatException {
    // Arrange
    var requestRawDTO =
        RequestRawDTO.builder()
            .id(UUID.randomUUID())
            .articleNumber("articleNumber")
            .quantity(1)
            .year(2022)
            .week(1)
            .customerId("customerId")
            .build();
    when(requestRepositoryMock.findAllByWeekAndArticleAndCustomer(
            anyInt(), anyInt(), anyString(), anyString()))
        .thenReturn(new ArrayList<>());

    // Act & Assert
    assertThatNoException().isThrownBy(() -> supplierService.validateNewRequest(requestRawDTO));
    verify(requestRepositoryMock)
        .findAllByWeekAndArticleAndCustomer(anyInt(), anyInt(), anyString(), anyString());
    verifyNoMoreInteractions(requestRepositoryMock);
  }

  @Test
  void validateNewRequest_whenThrows() {
    // Arrange
    var articleNumber = "articleNumber";
    var year = 2022;
    var week = 1;
    var customerId = "customerId";
    RequestRawDTO requestRawDTO =
        RequestRawDTO.builder()
            .id(UUID.randomUUID())
            .articleNumber(articleNumber)
            .quantity(100)
            .year(year)
            .week(week)
            .customerId(customerId)
            .build();
    Request requestMatchingArticleWeekCustomer =
        Request.builder()
            .id(UUID.randomUUID())
            .article(Article.builder().articleNumber(articleNumber).build())
            .quantity(20)
            .deliveryYear(year)
            .deliveryWeek(week)
            .customer(Company.builder().id(customerId).build())
            .status(RequestStatus.NEGATIVE_FEEDBACK)
            .build();
    when(requestRepositoryMock.findAllByWeekAndArticleAndCustomer(
            anyInt(), anyInt(), anyString(), anyString()))
        .thenReturn(List.of(requestMatchingArticleWeekCustomer));

    // Act & Assert
    assertThatExceptionOfType(IllegalDataFormatException.class)
        .isThrownBy(
            () -> {
              supplierService.validateNewRequest(requestRawDTO);
            });
    // Assert
    verify(requestRepositoryMock)
        .findAllByWeekAndArticleAndCustomer(anyInt(), anyInt(), anyString(), anyString());
    verifyNoMoreInteractions(requestRepositoryMock);
  }

  @Test
  void processIncomingRequest_validRequestWithSameIdExists()
      throws IllegalDataFormatException, EntityNotFoundException {
    // Arrange
    var id = UUID.randomUUID();
    var week = 1;
    var year = 2022;
    var customerId = "customerId";
    var requestRawDTO =
        RequestRawDTO.builder()
            .id(id)
            .articleNumber("different article") // diff
            .quantity(1) // diff
            .year(year)
            .week(week)
            .customerId(customerId)
            .build();
    var originalRequest =
        Request.builder()
            .id(id)
            .article(Article.builder().articleNumber("original article").build()) // diff
            .quantity(2) // diff
            .deliveryYear(year)
            .deliveryWeek(week)
            .customer(Company.builder().id(customerId).build())
            .status(RequestStatus.NEGATIVE_FEEDBACK) // diff
            .build();
    when(requestRepositoryMock.findById(id)).thenReturn(Optional.of(originalRequest));
    when(requestServiceMock.updateRequestStatus(any(), any()))
        .thenReturn(requestRawDTO.toRequest());

    // Act
    var actual = supplierService.processIncomingRequest(requestRawDTO);

    // Assert
    SoftAssertions.assertSoftly(
        softly -> {
          assertThat(actual.getId()).isEqualTo(requestRawDTO.getId());
          assertThat(actual.getArticle().getArticleNumber())
              .isEqualTo(requestRawDTO.getArticleNumber());
          assertThat(actual.getQuantity()).isEqualTo(requestRawDTO.getQuantity());
          assertThat(actual.getDeliveryYear()).isEqualTo(requestRawDTO.getYear());
          assertThat(actual.getDeliveryWeek()).isEqualTo(requestRawDTO.getWeek());
          assertThat(actual.getCustomer().getId()).isEqualTo(requestRawDTO.getCustomerId());
          assertThat(actual.getStatus()).isEqualTo(RequestStatus.NEW_REQUEST);
        });
    verify(requestRepositoryMock).findById(any());
    verify(requestServiceMock).updateRequestStatus(any(), any());
    verifyNoMoreInteractions(requestRepositoryMock, requestServiceMock);
  }

  @Test
  void processIncomingRequest_noRequestHasSameId()
      throws IllegalDataFormatException, EntityNotFoundException {
    // Arrange
    var id = UUID.randomUUID();
    var articleNumber = "articleNumber";
    var quantity = 1;
    var week = 1;
    var year = 2022;
    var customerId = "customerId";
    var requestRawDTO =
        RequestRawDTO.builder()
            .id(id)
            .articleNumber(articleNumber)
            .quantity(quantity)
            .year(year)
            .week(week)
            .customerId(customerId)
            .build();
    when(requestRepositoryMock.findById(id)).thenReturn(Optional.empty());
    when(requestRepositoryMock.findAllByWeekAndArticleAndCustomer(
            anyInt(), anyInt(), anyString(), anyString()))
        .thenReturn(new ArrayList<>());
    when(requestServiceMock.saveRequest(any())).thenReturn(requestRawDTO.toRequest());

    // Act
    var actual = supplierService.processIncomingRequest(requestRawDTO);

    // Assert
    SoftAssertions.assertSoftly(
        softly -> {
          assertThat(actual.getId()).isEqualTo(requestRawDTO.getId());
          assertThat(actual.getArticle().getArticleNumber())
              .isEqualTo(requestRawDTO.getArticleNumber());
          assertThat(actual.getQuantity()).isEqualTo(requestRawDTO.getQuantity());
          assertThat(actual.getDeliveryYear()).isEqualTo(requestRawDTO.getYear());
          assertThat(actual.getDeliveryWeek()).isEqualTo(requestRawDTO.getWeek());
          assertThat(actual.getCustomer().getId()).isEqualTo(requestRawDTO.getCustomerId());
          assertThat(actual.getStatus()).isEqualTo(RequestStatus.NEW_REQUEST);
        });
    verify(requestRepositoryMock).findById(any());
    verify(requestRepositoryMock)
        .findAllByWeekAndArticleAndCustomer(anyInt(), anyInt(), anyString(), anyString());
    verify(requestServiceMock).saveRequest(any());
    verifyNoMoreInteractions(requestRepositoryMock, requestServiceMock);
  }
}
