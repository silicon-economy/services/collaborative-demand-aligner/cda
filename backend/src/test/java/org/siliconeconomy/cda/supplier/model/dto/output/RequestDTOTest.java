/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.supplier.model.dto.output;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.common.model.Company;
import org.siliconeconomy.cda.supplier.model.Request;
import org.siliconeconomy.cda.supplier.model.enums.RequestStatus;

class RequestDTOTest {

  @Test
  void testConstructorFromRequest() {
    Article art = Article.builder().articleNumber("0815").build();
    Company customer = Company.builder().id("customer").build();
    Request req =
        Request.builder()
            .id(UUID.randomUUID())
            .deliveryWeek(12)
            .deliveryYear(2022)
            .quantity(42)
            .status(RequestStatus.NEGATIVE_FEEDBACK)
            .article(art)
            .customer(customer)
            .build();
    RequestDTO dto = new RequestDTO(req);

    assertThat(dto.getId()).isEqualTo(req.getId());
    assertThat(dto.getArticleNumber()).isEqualTo(art.getArticleNumber());
    assertThat(dto.getCustomer().getId()).isEqualTo(req.getCustomer().getId());
    assertThat(dto.getQuantity()).isEqualTo(req.getQuantity());
    assertThat(dto.getWeek()).isEqualTo(req.getDeliveryWeek());
    assertThat(dto.getYear()).isEqualTo(req.getDeliveryYear());
    assertThat(dto.getStatus()).isEqualTo(req.getStatus());
  }
}
