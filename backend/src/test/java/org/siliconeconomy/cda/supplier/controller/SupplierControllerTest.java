/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.supplier.controller;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.UUID;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.siliconeconomy.cda.common.exception.EntityNotFoundException;
import org.siliconeconomy.cda.common.exception.IllegalDataFormatException;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.common.model.Company;
import org.siliconeconomy.cda.common.model.dto.CompanyDTO;
import org.siliconeconomy.cda.supplier.model.Request;
import org.siliconeconomy.cda.common.model.dto.RequestRawDTO;
import org.siliconeconomy.cda.supplier.model.dto.output.RequestDTO;
import org.siliconeconomy.cda.supplier.model.enums.RequestStatus;
import org.siliconeconomy.cda.supplier.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@WebMvcTest(SupplierController.class)
class SupplierControllerTest {

  private static final ObjectMapper MAPPER = new ObjectMapper();

  // Main entry point for server-side Spring MVC test support
  @Autowired private MockMvc mockMvc;

  // API endpoint supplier controller
  private final String pathSupplierExternal = "/api/v2/supplier/external";
  private final String pathRequest = "/request";

  // Class under test
  @Autowired SupplierController supplierController;

  // Dependencies
  @MockBean SupplierService supplierServiceMock;

  // Test Data
  // Input DTO
  RequestRawDTO requestRawDTO;
  // Input DTO Json
  String rawRequestDTOJson;
  // Output DTO
  RequestDTO requestDTO;
  // Output DTO Json
  String requestDTOJson;
  // Model
  Request request;

  @BeforeEach
  void setUp() throws JsonProcessingException {
    var articleNumber = "4711";
    var customerId = "1";
    var customerName = "Paul";
    var quantity = 15;
    var year = 2022;
    var week = 7;
    var status = RequestStatus.NEW_REQUEST;
    var id = UUID.randomUUID();
    var article = Article.builder().articleNumber(articleNumber).build();
    var company = Company.builder().id(customerId).name(customerName).build();
    // SetUp input dto
    requestRawDTO =
        RequestRawDTO.builder()
            .id(id)
            .articleNumber(articleNumber)
            .quantity(quantity)
            .year(year)
            .week(week)
            .customerId(customerId)
            .build();
    // SetUp output dto
    requestDTO =
        RequestDTO.builder()
            .id(id)
            .articleNumber(articleNumber)
            .quantity(quantity)
            .year(year)
            .week(week)
            .customer(new CompanyDTO(company))
            .status(status)
            .build();
    // SetUp model
    request =
        Request.builder()
            .id(id)
            .article(article)
            .quantity(quantity)
            .deliveryYear(year)
            .deliveryWeek(week)
            .customer(company)
            .status(status)
            .build();
    // setup jsons
    rawRequestDTOJson = MAPPER.writeValueAsString(requestRawDTO);
    requestDTOJson = MAPPER.writeValueAsString(requestDTO);
  }

  @Test
  void createRequest_201() throws EntityNotFoundException, IllegalDataFormatException {
    // arrange
    when(supplierServiceMock.processIncomingRequest(any())).thenReturn(request);
    // Act
    RequestDTO createdRequestDTO = supplierController.createRequest(requestRawDTO);
    // Assert
    assertThat(createdRequestDTO).isNotNull();
    SoftAssertions.assertSoftly(
        softly -> {
          softly.assertThat((createdRequestDTO).getId()).isEqualTo(requestRawDTO.getId());
          softly
              .assertThat((createdRequestDTO).getQuantity())
              .isEqualTo(requestRawDTO.getQuantity());
          softly
              .assertThat((createdRequestDTO).getArticleNumber())
              .isEqualTo(requestRawDTO.getArticleNumber());
          softly.assertThat((createdRequestDTO).getYear()).isEqualTo(requestRawDTO.getYear());
          softly.assertThat((createdRequestDTO).getWeek()).isEqualTo(requestRawDTO.getWeek());
          softly
              .assertThat((createdRequestDTO).getCustomer().getId())
              .isEqualTo(requestRawDTO.getCustomerId());
          softly.assertThat((createdRequestDTO).getStatus()).isEqualTo(RequestStatus.NEW_REQUEST);
        });
  }

  @Test
  void createRequest_201_validateRestEndpoint() throws Exception {
    // arrange
    when(supplierServiceMock.processIncomingRequest(any())).thenReturn(request);
    // act & assert
    mockMvc
        .perform(
            post(pathSupplierExternal + pathRequest)
                .contentType(MediaType.APPLICATION_JSON)
                .content(rawRequestDTOJson))
        .andExpect(status().isCreated())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().json(requestDTOJson));
    // assert
    verify(supplierServiceMock).processIncomingRequest(any());
    verifyNoMoreInteractions(supplierServiceMock);
  }

  @Test
  void createRequest_400_validateRestEndpoint() throws Exception {
    // arrange
    when(supplierServiceMock.processIncomingRequest(any()))
        .thenThrow(new IllegalDataFormatException("Exception Message"));
    // act & assert
    mockMvc
        .perform(
            post(pathSupplierExternal + pathRequest)
                .contentType(MediaType.APPLICATION_JSON)
                .content(rawRequestDTOJson))
        .andExpect(status().isBadRequest())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(jsonPath("$.timestamp", is(notNullValue())))
        .andExpect(jsonPath("$.message", is("Exception Message")))
        .andExpect(jsonPath("$.requestUri", is("uri=" + pathSupplierExternal + pathRequest)));
    // assert
    verify(supplierServiceMock).processIncomingRequest(any());
    verifyNoMoreInteractions(supplierServiceMock);
  }

  @Test
  void createRequest_404_validateRestEndpoint() throws Exception {
    // arrange
    when(supplierServiceMock.processIncomingRequest(any()))
        .thenThrow(new EntityNotFoundException("Exception Message"));
    // act & assert
    mockMvc
        .perform(
            post(pathSupplierExternal + pathRequest)
                .contentType(MediaType.APPLICATION_JSON)
                .content(rawRequestDTOJson))
        .andExpect(status().isNotFound())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(jsonPath("$.timestamp", is(notNullValue())))
        .andExpect(jsonPath("$.message", is("Exception Message")))
        .andExpect(jsonPath("$.requestUri", is("uri=" + pathSupplierExternal + pathRequest)));
    // assert
    verify(supplierServiceMock).processIncomingRequest(any());
    verifyNoMoreInteractions(supplierServiceMock);
  }
}
