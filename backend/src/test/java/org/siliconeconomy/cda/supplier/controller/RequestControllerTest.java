/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.supplier.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.cda.common.exception.EntityNotFoundException;
import org.siliconeconomy.cda.common.exception.RestCommunicationException;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.common.model.dto.ConfirmationResponseDTO;
import org.siliconeconomy.cda.supplier.model.Request;
import org.siliconeconomy.cda.supplier.model.dto.output.RequestDTO;
import org.siliconeconomy.cda.supplier.model.dto.output.RequestDetailViewDTO;
import org.siliconeconomy.cda.supplier.model.dto.output.RequestOverviewDTO;
import org.siliconeconomy.cda.supplier.model.dto.output.RequestOverviewItem;
import org.siliconeconomy.cda.supplier.model.enums.AggregatedRequestStatus;
import org.siliconeconomy.cda.supplier.service.RequestOverviewService;
import org.siliconeconomy.cda.supplier.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
class RequestControllerTest {
  private static final String SEND_CONFIRMATION_PATH = "/api/v2/supplier/requests/confirmation";
  private static final ObjectMapper MAPPER = new ObjectMapper();

  @MockBean private RequestOverviewService requestOverviewService;
  @MockBean private RequestService requestService;
  @Autowired private MockMvc mockMvc;
  @Autowired private RequestController requestController;

  ConfirmationResponseDTO confirmationResponseDTO;
  ConfirmationResponseDTO confirmationResponseDTOFalse;

  @BeforeEach
  void setUp() throws JsonProcessingException {
    var requestId = UUID.randomUUID();
    confirmationResponseDTO =
        ConfirmationResponseDTO.builder().requestId(requestId).confirmed(true).build();
    confirmationResponseDTOFalse =
        ConfirmationResponseDTO.builder().requestId(requestId).confirmed(false).build();
  }

  @Test
  void testGetRequestOverviewData_500() {
    // arrange
    when(requestOverviewService.getAllRequestOverviewDTOs())
        .thenThrow(new IllegalArgumentException());
    var expectedResult =
        new ResponseEntity<>(
            "An internal server error occurs while fetching data for the request overview",
            HttpStatus.INTERNAL_SERVER_ERROR);

    // act
    var result = requestController.getRequestOverviewData();

    // assert
    assertThat(result).isEqualTo(expectedResult);
  }

  @Test
  void testGetRequestOverviewData_200() {
    // arrange
    Article article = Article.builder().articleNumber("0815").build();

    Request request1 =
        Request.builder().article(article).quantity(1).deliveryYear(2022).deliveryWeek(8).build();

    var requestOverviewDTO = new RequestOverviewDTO();

    int year = request1.getDeliveryYear();
    int week = request1.getDeliveryWeek();

    Map<String, List<RequestOverviewItem>> map = new LinkedHashMap<>();
    var dto = RequestOverviewItem.builder().quantity(3).year(year).week(week).build();
    List<RequestOverviewItem> list2 = new ArrayList<>();
    list2.add(dto);
    map.put(request1.getArticle().getArticleNumber(), list2);
    requestOverviewDTO.setRequestOverviewItems(map);

    when(requestOverviewService.getAllRequestOverviewDTOs()).thenReturn(requestOverviewDTO);
    var expectedResult = new ResponseEntity<>(requestOverviewDTO, HttpStatus.OK);

    // act
    var result = requestController.getRequestOverviewData();

    // assert
    assertThat(result).isEqualTo(expectedResult);
  }

  @Test
  void getRequestDetailData_200() {
    // arrange
    RequestDTO reqDTO =
        RequestDTO.builder().articleNumber("7321").id(UUID.randomUUID()).quantity(18).build();
    RequestDetailViewDTO dto =
        RequestDetailViewDTO.builder()
            .articleNumber("7321")
            .articleDescription("<empty>")
            .totalQuantity(18)
            .totalStatus(AggregatedRequestStatus.ALL_ANSWERED)
            .requests(Arrays.asList(reqDTO))
            .build();
    when(requestService.getRequestsDetailData(2022, 23, "7321")).thenReturn(dto);
    // act
    var result = requestController.getRequestDetailData(2022, 23, "7321");

    // assert
    assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(result.getBody()).isEqualTo(dto);
  }

  @Test
  void getRequestDetailData_null() {
    // arrange
    when(requestService.getRequestsDetailData(2022, 23, "7321")).thenReturn(null);
    // act
    var result = requestController.getRequestDetailData(2022, 23, "7321");

    // assert
    assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(result.getBody()).isNull();
  }

  @Test
  void getRequestDetailData_400() {
    // arrange
    when(requestService.getRequestsDetailData(2022, 23, "7321"))
        .thenThrow(IllegalArgumentException.class);
    // act
    var result = requestController.getRequestDetailData(2022, 23, "7321");

    // assert
    assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

  @Test
  void getRequestDetailData_500() {
    // arrange
    when(requestService.getRequestsDetailData(2022, 23, "7321"))
        .thenThrow(NullPointerException.class);
    // act
    var result = requestController.getRequestDetailData(2022, 23, "7321");

    // assert
    assertThat(result.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @Test
  void confirmationClicked_200() throws Exception {
    // arrange
    doNothing().when(requestService).sendConfirmationToProducer(any());

    // act & assert
    mockMvc
        .perform(
            post(SEND_CONFIRMATION_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(MAPPER.writeValueAsString(confirmationResponseDTO)))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    verify(requestService).sendConfirmationToProducer(confirmationResponseDTO);
  }

  @Test
  void confirmationClicked_400() throws Exception {
    // arrange
    doThrow(RestCommunicationException.class)
        .when(requestService)
        .sendConfirmationToProducer(any());

    // act & assert
    mockMvc
        .perform(
            post(SEND_CONFIRMATION_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(MAPPER.writeValueAsString(confirmationResponseDTO)))
        .andExpect(status().isBadRequest())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON));
  }

  @Test
  void confirmationClicked_404() throws Exception {
    // arrange
    doThrow(EntityNotFoundException.class).when(requestService).sendConfirmationToProducer(any());

    // act & assert
    mockMvc
        .perform(
            post(SEND_CONFIRMATION_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(MAPPER.writeValueAsString(confirmationResponseDTO)))
        .andExpect(status().isNotFound())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON));
  }
}
