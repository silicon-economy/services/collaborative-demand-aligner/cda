/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.supplier.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.UUID;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.common.model.Company;
import org.siliconeconomy.cda.supplier.model.Request;
import org.siliconeconomy.cda.supplier.model.dto.output.RequestOverviewDTO;
import org.siliconeconomy.cda.supplier.model.repositories.RequestRepository;

/**
 * Unit Test of class {@link RequestOverviewService}.
 *
 * @author Bernd Breitenbach
 */
class RequestOverviewServiceTest {

  @Mock RequestRepository mockRequestRepository;

  @Mock RequestService requestService;

  @InjectMocks RequestOverviewService requestOverviewService;

  private AutoCloseable closeable;

  @BeforeEach
  public void openMocks() {
    closeable = MockitoAnnotations.openMocks(this);
  }

  @AfterEach
  public void close() throws Exception {
    closeable.close();
  }

  @Test
  void testGetAllRequestOverviewDTOs() {
    // arrange
    Article art1 = Article.builder().articleNumber("001").description("art1").build();
    Article art2 = Article.builder().articleNumber("002").description("art2").build();
    Article art3 = Article.builder().articleNumber("003").description("art3").build();
    Company customer1 = Company.builder().name("bem-vindo").id("customer1").build();
    Company customer2 = Company.builder().name("de nada").id("customer2").build();

    Request req1 =
        Request.builder()
            .id(UUID.randomUUID())
            .customer(customer1)
            .article(art1)
            .deliveryWeek(3)
            .deliveryYear(2022)
            .quantity(11)
            .build();
    Request req2 =
        Request.builder()
            .id(UUID.randomUUID())
            .customer(customer2)
            .article(art1)
            .deliveryWeek(3)
            .deliveryYear(2022)
            .quantity(22)
            .build();
    Request req3 =
        Request.builder()
            .id(UUID.randomUUID())
            .customer(customer1)
            .article(art2)
            .deliveryWeek(3)
            .deliveryYear(2022)
            .quantity(3)
            .build();
    Request req4 =
        Request.builder()
            .id(UUID.randomUUID())
            .customer(customer1)
            .article(art2)
            .deliveryWeek(4)
            .deliveryYear(2022)
            .quantity(73)
            .build();
    Request req5 =
        Request.builder()
            .id(UUID.randomUUID())
            .customer(customer2)
            .article(art3)
            .deliveryWeek(4)
            .deliveryYear(2022)
            .quantity(5)
            .build();

    when(mockRequestRepository.findAll()).thenReturn(Arrays.asList(req1, req2, req3, req4, req5));
    when(mockRequestRepository.findAllByWeekAndArticleNumber(2022, 3, "001"))
        .thenReturn(Arrays.asList(req1, req2));
    when(mockRequestRepository.findAllByWeekAndArticleNumber(2022, 3, "002"))
        .thenReturn(Arrays.asList(req3));
    when(mockRequestRepository.findAllByWeekAndArticleNumber(2022, 4, "002"))
        .thenReturn(Arrays.asList(req4));
    when(mockRequestRepository.findAllByWeekAndArticleNumber(2022, 4, "003"))
        .thenReturn(Arrays.asList(req5));

    // act
    RequestOverviewDTO dto = requestOverviewService.getAllRequestOverviewDTOs();

    // assert
    assertThat(dto.getRequestOverviewItems()).hasSize(3);
  }
}
