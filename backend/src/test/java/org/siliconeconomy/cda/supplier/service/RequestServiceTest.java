/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.supplier.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.siliconeconomy.cda.common.exception.EntityNotFoundException;
import org.siliconeconomy.cda.common.exception.IllegalDataFormatException;
import org.siliconeconomy.cda.common.exception.RestCommunicationException;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.common.model.Company;
import org.siliconeconomy.cda.common.model.dto.ConfirmationResponseDTO;
import org.siliconeconomy.cda.common.model.repositories.ArticleRepository;
import org.siliconeconomy.cda.common.model.repositories.CompanyRepository;
import org.siliconeconomy.cda.common.service.DataRefreshStompService;
import org.siliconeconomy.cda.supplier.model.Request;
import org.siliconeconomy.cda.supplier.model.dto.output.RequestDTO;
import org.siliconeconomy.cda.supplier.model.enums.AggregatedRequestStatus;
import org.siliconeconomy.cda.supplier.model.enums.RequestStatus;
import org.siliconeconomy.cda.supplier.model.repositories.RequestRepository;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
class RequestServiceTest {
  private static MockWebServer mockWebServer;
  private static String baseUrlMockWebServer;
  private static final String PAULS_ID = "3";
  private static final ObjectMapper MAPPER = new ObjectMapper();

  @Mock private RequestRepository requestRepo;
  @Mock private ArticleRepository articleRepository;
  @Mock private CompanyRepository companyRepository;
  @Mock private DataRefreshStompService dataRefreshStompService;
  private RequestService requestService;
  private ConfirmationResponseDTO confirmationResponse;
  private ConfirmationResponseDTO rejectionResponse;
  private Request requestWithProducerPaul;
  private Request requestWithOtherProducer;
  private final List<String> emittedStompMessages = new ArrayList<>();

  @BeforeAll
  static void startMockWebServer() throws IOException {
    mockWebServer = new MockWebServer();
    mockWebServer.start();
    baseUrlMockWebServer = String.format("http://localhost:%s", mockWebServer.getPort());
  }

  @BeforeEach
  public void setup() {
    // inject mocks into a spy
    requestService =
        Mockito.spy(
            new RequestService(
                requestRepo, articleRepository, companyRepository, dataRefreshStompService));
    // set field in requestService directly as it is not loaded from the properties file in tests
    ReflectionTestUtils.setField(requestService, "producerPaulId", PAULS_ID);

    // create requests
    UUID requestId = UUID.randomUUID();
    Article article = Article.builder().articleNumber("12").unit("11").build();
    requestWithProducerPaul =
        Request.builder()
            .id(requestId)
            .customer(Company.builder().id(PAULS_ID).build())
            .status(RequestStatus.NEW_REQUEST)
            .article(article)
            .build();
    requestWithOtherProducer =
        Request.builder()
            .id(requestId)
            .customer(Company.builder().id("1234").build())
            .status(RequestStatus.NEW_REQUEST)
            .article(article)
            .build();

    // create responses
    confirmationResponse =
        ConfirmationResponseDTO.builder().requestId(requestId).confirmed(true).build();
    rejectionResponse =
        ConfirmationResponseDTO.builder().requestId(requestId).confirmed(false).build();

    // to be able to test stomp communication in this unit test, alter the action to writing the
    // method name of the called method into the List whenever a stomp message would be sent
    doAnswer(invocation -> emittedStompMessages.add("triggerRefresh"))
        .when(dataRefreshStompService)
        .triggerRefresh();
    emittedStompMessages.clear();
  }

  @AfterAll
  static void shutDown() throws IOException {
    mockWebServer.shutdown();
  }

  @Test
  void updateRequestStatus_sameStatus() throws EntityNotFoundException, IllegalDataFormatException {
    // arrange
    var id = UUID.randomUUID();
    var articleNumber = "articleNumber";
    var quantity = 1;
    var week = 1;
    var year = 2022;
    var companyId = "companyId";
    var request =
        Request.builder()
            .id(id)
            .article(Article.builder().articleNumber(articleNumber).build())
            .quantity(quantity)
            .deliveryYear(year)
            .deliveryWeek(week)
            .customer(Company.builder().id(companyId).build())
            .status(RequestStatus.NEW_REQUEST)
            .build();
    when(requestRepo.save(any())).thenReturn(request);
    when(articleRepository.findById(any()))
        .thenReturn(Optional.of(Article.builder().articleNumber(articleNumber).build()));
    when(companyRepository.findById(any()))
        .thenReturn(Optional.of(Company.builder().id(companyId).build()));
    var statusBeforeUpdate = request.getStatus();

    // act
    var result = requestService.updateRequestStatus(request, statusBeforeUpdate);

    // assert
    assertThat(result.getStatus()).isEqualTo(statusBeforeUpdate);
    assertThat(emittedStompMessages).hasSize(1).containsExactly("triggerRefresh");
  }

  @Test
  void updateRequestStatus_differentStatus()
      throws EntityNotFoundException, IllegalDataFormatException {
    // Arrange
    var newStatus = RequestStatus.POSITIVE_FEEDBACK;
    var id = UUID.randomUUID();
    var articleNumber = "articleNumber";
    var quantity = 1;
    var week = 1;
    var year = 2022;
    var companyId = "companyId";
    var request =
        Request.builder()
            .id(id)
            .article(Article.builder().articleNumber(articleNumber).build())
            .quantity(quantity)
            .deliveryYear(year)
            .deliveryWeek(week)
            .customer(Company.builder().id(companyId).build())
            .status(RequestStatus.NEW_REQUEST)
            .build();
    when(requestRepo.save(any())).thenReturn(request);
    when(articleRepository.findById(any()))
        .thenReturn(Optional.of(Article.builder().articleNumber(articleNumber).build()));
    when(companyRepository.findById(any()))
        .thenReturn(Optional.of(Company.builder().id(companyId).build()));

    // act
    var result = requestService.updateRequestStatus(request, newStatus);

    // assert
    assertThat(result.getStatus()).isEqualTo(newStatus);
    assertThat(emittedStompMessages).hasSize(1).containsExactly("triggerRefresh");
  }

  @Test
  void getCompositeRequestStatus_nothing_found() {
    // arrange
    when(requestRepo.findAllByWeekAndArticleNumber(2022, 22, "0815"))
        .thenReturn(Collections.emptyList());
    // act
    AggregatedRequestStatus status = requestService.getCompositeRequestStatus(2022, 22, "0815");
    // assert
    assertThat(status).isEqualByComparingTo(AggregatedRequestStatus.ALL_ANSWERED);
  }

  @Test
  void getCompositeRequestStatus_new() {
    // arrange
    Article art = Article.builder().articleNumber("0815").build();
    Request req1 =
        Request.builder()
            .article(art)
            .status(RequestStatus.NEW_REQUEST)
            .id(UUID.randomUUID())
            .deliveryWeek(22)
            .deliveryYear(2022)
            .build();
    Request req2 =
        Request.builder()
            .article(art)
            .status(RequestStatus.NEW_REQUEST)
            .id(UUID.randomUUID())
            .deliveryWeek(22)
            .deliveryYear(2022)
            .build();

    when(requestRepo.findAllByWeekAndArticleNumber(2022, 22, "0815"))
        .thenReturn(Arrays.asList(req1, req2));
    // act
    AggregatedRequestStatus status = requestService.getCompositeRequestStatus(2022, 22, "0815");
    // assert
    assertThat(status).isEqualByComparingTo(AggregatedRequestStatus.NEW_REQUEST);
  }

  @Test
  void getCompositeRequestStatus_incomplete() {
    // arrange
    Article art = Article.builder().articleNumber("0815").build();
    Request req1 =
        Request.builder()
            .article(art)
            .status(RequestStatus.NEW_REQUEST)
            .id(UUID.randomUUID())
            .deliveryWeek(22)
            .deliveryYear(2022)
            .build();
    Request req2 =
        Request.builder()
            .article(art)
            .status(RequestStatus.POSITIVE_FEEDBACK)
            .id(UUID.randomUUID())
            .deliveryWeek(22)
            .deliveryYear(2022)
            .build();

    when(requestRepo.findAllByWeekAndArticleNumber(2022, 22, "0815"))
        .thenReturn(Arrays.asList(req1, req2));
    // act
    AggregatedRequestStatus status = requestService.getCompositeRequestStatus(2022, 22, "0815");
    // assert
    assertThat(status).isEqualByComparingTo(AggregatedRequestStatus.FEEDBACK_INCOMPLETE);
  }

  @Test
  void getCompositeRequestStatus_answered() {
    // arrange
    Article art = Article.builder().articleNumber("0815").build();
    Request req1 =
        Request.builder()
            .article(art)
            .status(RequestStatus.NEGATIVE_FEEDBACK)
            .id(UUID.randomUUID())
            .deliveryWeek(22)
            .deliveryYear(2022)
            .build();
    Request req2 =
        Request.builder()
            .article(art)
            .status(RequestStatus.POSITIVE_FEEDBACK)
            .id(UUID.randomUUID())
            .deliveryWeek(22)
            .deliveryYear(2022)
            .build();

    when(requestRepo.findAllByWeekAndArticleNumber(2022, 22, "0815"))
        .thenReturn(Arrays.asList(req1, req2));
    // act
    AggregatedRequestStatus status = requestService.getCompositeRequestStatus(2022, 22, "0815");
    // assert
    assertThat(status).isEqualByComparingTo(AggregatedRequestStatus.ALL_ANSWERED);
  }

  @Test
  void getRequestsDetailData_nothing_found() {
    // arrange
    when(requestRepo.findAllByWeekAndArticleNumber(2022, 20, "2222")).thenReturn(new ArrayList<>());

    // act
    var result = requestService.getRequestsDetailData(2022, 20, "2222");

    // assert
    assertThat(result).isNull();
  }

  @Test
  void getRequestsDetailData() {
    // arrange
    Company comp1 = Company.builder().id(UUID.randomUUID().toString()).name("Comp 1").build();
    Company comp2 = Company.builder().id(UUID.randomUUID().toString()).name("Comp 1").build();
    Article art =
        Article.builder().articleNumber("0815").description(("airhook")).unit("Piece").build();
    Request req1 =
        Request.builder()
            .article(art)
            .status(RequestStatus.NEGATIVE_FEEDBACK)
            .id(UUID.randomUUID())
            .deliveryWeek(22)
            .deliveryYear(2022)
            .quantity(7)
            .customer(comp1)
            .build();
    Request req2 =
        Request.builder()
            .article(art)
            .status(RequestStatus.POSITIVE_FEEDBACK)
            .id(UUID.randomUUID())
            .deliveryWeek(22)
            .deliveryYear(2022)
            .quantity(11)
            .customer(comp2)
            .build();

    when(requestRepo.findAllByWeekAndArticleNumber(2022, 20, "0815"))
        .thenReturn(Arrays.asList(req1, req2));

    // act
    var result = requestService.getRequestsDetailData(2022, 20, "0815");

    // assert
    assertThat(result.getArticleNumber()).isEqualTo("0815");
    assertThat(result.getUnit()).isEqualTo("Piece");
    assertThat(result.getArticleDescription()).isEqualTo("airhook");
    assertThat(result.getRequests()).contains(new RequestDTO(req1), new RequestDTO(req2));
    assertThat(result.getTotalQuantity()).isEqualTo(18);
  }

  @Test
  void saveRequest_dataIsAppropriate() throws IllegalDataFormatException, EntityNotFoundException {
    // Arrange
    var id = UUID.randomUUID();
    var articleNumber = "articleNumber";
    var quantity = 1;
    var week = 1;
    var year = 2022;
    var companyId = "companyId";
    var request =
        Request.builder()
            .id(id)
            .article(Article.builder().articleNumber(articleNumber).build())
            .quantity(quantity)
            .deliveryYear(year)
            .deliveryWeek(week)
            .customer(Company.builder().id(companyId).build())
            .status(RequestStatus.NEW_REQUEST)
            .build();
    when(requestRepo.save(any())).thenReturn(request);
    when(articleRepository.findById(any()))
        .thenReturn(Optional.of(Article.builder().articleNumber(articleNumber).build()));
    when(companyRepository.findById(any()))
        .thenReturn(Optional.of(Company.builder().id(companyId).build()));
    // Act
    var actual = requestService.saveRequest(request);
    // Assert
    SoftAssertions.assertSoftly(
        softly -> {
          assertThat(actual.getId()).isEqualTo(request.getId());
          assertThat(actual.getArticle().getArticleNumber())
              .isEqualTo(request.getArticle().getArticleNumber());
          assertThat(actual.getQuantity()).isEqualTo(request.getQuantity());
          assertThat(actual.getDeliveryYear()).isEqualTo(request.getDeliveryYear());
          assertThat(actual.getDeliveryWeek()).isEqualTo(request.getDeliveryWeek());
          assertThat(actual.getCustomer().getId()).isEqualTo(request.getCustomer().getId());
          assertThat(actual.getStatus()).isEqualTo(request.getStatus());
        });
    verify(requestRepo).save(any());
    verify(articleRepository).findById(any());
    verify(companyRepository).findById(any());
    verify(dataRefreshStompService).triggerRefresh();
    verifyNoMoreInteractions(
        requestRepo, articleRepository, companyRepository, dataRefreshStompService);
  }

  @Test
  void saveRequest_throwExceptionDueArticleIsNotPresent() {
    // Arrange
    var id = UUID.randomUUID();
    var articleNumber = "articleNumber";
    var quantity = 1;
    var week = 1;
    var year = 2022;
    var companyId = "companyId";
    var request =
        Request.builder()
            .id(id)
            .article(Article.builder().articleNumber(articleNumber).build())
            .quantity(quantity)
            .deliveryYear(year)
            .deliveryWeek(week)
            .customer(Company.builder().id(companyId).build())
            .status(RequestStatus.NEW_REQUEST)
            .build();
    when(articleRepository.findById(any())).thenReturn(Optional.empty());
    when(companyRepository.findById(any()))
        .thenReturn(Optional.of(Company.builder().id(companyId).build()));
    // Act & Assert
    assertThatExceptionOfType(EntityNotFoundException.class)
        .isThrownBy(
            () -> {
              requestService.saveRequest(request);
            });
    // Assert
    verify(articleRepository).findById(any());
    verifyNoMoreInteractions(articleRepository);
    // Interaction with CompanyRepository does not matter at this point
    verifyNoInteractions(requestRepo);
    verifyNoInteractions(dataRefreshStompService);
  }

  @Test
  void saveRequest_throwsExceptionDueCompanyIsNotPresent() {
    // Arrange
    var id = UUID.randomUUID();
    var articleNumber = "articleNumber";
    var quantity = 1;
    var week = 1;
    var year = 2022;
    var companyId = "companyId";
    var request =
        Request.builder()
            .id(id)
            .article(Article.builder().articleNumber(articleNumber).build())
            .quantity(quantity)
            .deliveryYear(year)
            .deliveryWeek(week)
            .customer(Company.builder().id(companyId).build())
            .status(RequestStatus.NEW_REQUEST)
            .build();
    when(articleRepository.findById(any()))
        .thenReturn(Optional.of(Article.builder().articleNumber(articleNumber).build()));
    when(companyRepository.findById(any())).thenReturn(Optional.empty());
    // Act & Assert
    assertThatExceptionOfType(EntityNotFoundException.class)
        .isThrownBy(
            () -> {
              requestService.saveRequest(request);
            });
    // Assert
    verify(companyRepository).findById(any());
    verifyNoMoreInteractions(companyRepository);
    // Interaction with ArticleRepository does not matter at this point
    verifyNoInteractions(requestRepo);
    verifyNoInteractions(dataRefreshStompService);
  }

  @Test
  void saveRequest_throwsExceptionDueValidationConstraints() {
    // Arrange
    var id = UUID.randomUUID();
    var articleNumber = "articleNumber";
    var quantity = -1;
    var week = 1;
    var year = 2022;
    var companyId = "companyId";
    var request =
        Request.builder()
            .id(id)
            .article(Article.builder().articleNumber(articleNumber).build())
            .quantity(quantity)
            .deliveryYear(year)
            .deliveryWeek(week)
            .customer(Company.builder().id(companyId).build())
            .status(RequestStatus.NEW_REQUEST)
            .build();
    when(articleRepository.findById(any()))
        .thenReturn(Optional.of(Article.builder().articleNumber(articleNumber).build()));
    when(companyRepository.findById(any()))
        .thenReturn(Optional.of(Company.builder().id(companyId).build()));
    when(requestRepo.save(any())).thenThrow(new RuntimeException(""));
    // Act & Assert
    assertThatExceptionOfType(IllegalDataFormatException.class)
        .isThrownBy(
            () -> {
              requestService.saveRequest(request);
            });
    // Assert
    verify(companyRepository).findById(any());
    verify(articleRepository).findById(any());
    verify(requestRepo).save(any());
    verifyNoMoreInteractions(companyRepository, articleRepository, requestRepo);
    verifyNoInteractions(dataRefreshStompService);
  }

  @Test
  void sendConfirmationToProducer_whenRequestIdUnknown() {
    // arrange
    UUID unknownId = UUID.randomUUID();
    ConfirmationResponseDTO responseWithUnknownId = new ConfirmationResponseDTO(unknownId, true);

    // act & assert
    assertThatExceptionOfType(EntityNotFoundException.class)
        .isThrownBy(() -> requestService.sendConfirmationToProducer(responseWithUnknownId))
        .withMessageContaining(
            "There is no entity with id '%s' present in the related %s",
            unknownId, "RequestRepository");
  }

  @Test
  void sendConfirmationToProducer_withProducerPaul_withResponse200() throws Exception {
    // arrange
    when(requestRepo.findById(any())).thenReturn(Optional.of(requestWithProducerPaul));
    when(requestService.getConfirmationBaseUrlForProducerPaul()).thenReturn(baseUrlMockWebServer);
    // the returned mock response has the wrong request status but it is ignored anyway
    doReturn(requestWithProducerPaul).when(requestService).updateRequestStatus(any(), any());
    // prepare mocks
    MockResponse mockResponse =
        new MockResponse().addHeader("Content-Type", "application/json").setResponseCode(200);
    mockWebServer.enqueue(mockResponse);

    // act
    requestService.sendConfirmationToProducer(confirmationResponse);
    RecordedRequest recordedRequest = mockWebServer.takeRequest();

    // assert
    assertThat(recordedRequest.getMethod()).isEqualTo("POST");
    assertThat(recordedRequest.getBody().readUtf8())
        .isEqualTo(MAPPER.writeValueAsString(confirmationResponse));
    verify(requestRepo).findById(confirmationResponse.getRequestId());
    verify(requestService).getConfirmationBaseUrlForProducerPaul();
    verify(requestService)
        .updateRequestStatus(requestWithProducerPaul, RequestStatus.POSITIVE_FEEDBACK);
  }

  @Test
  void sendConfirmationToProducer_withProducerPaul_withResponse400() throws Exception {
    // arrange
    when(requestRepo.findById(any())).thenReturn(Optional.of(requestWithProducerPaul));
    when(requestService.getConfirmationBaseUrlForProducerPaul()).thenReturn(baseUrlMockWebServer);
    // prepare mocks
    MockResponse mockResponse =
        new MockResponse().addHeader("Content-Type", "application/json").setResponseCode(400);
    mockWebServer.enqueue(mockResponse);

    // act & assert
    assertThatExceptionOfType(RestCommunicationException.class)
        .isThrownBy(() -> requestService.sendConfirmationToProducer(confirmationResponse))
        .withMessageContaining("HttpClientErrorException$BadRequest: 400 Client Error");
    verify(requestRepo).findById(confirmationResponse.getRequestId());
    verify(requestService).getConfirmationBaseUrlForProducerPaul();
    verify(requestService, never()).changeRequestStatus(any(), any());
  }

  @Test
  void sendConfirmationToProducer_withProducerPaul_withResponse500() throws Exception {
    // arrange
    when(requestRepo.findById(any())).thenReturn(Optional.of(requestWithProducerPaul));
    when(requestService.getConfirmationBaseUrlForProducerPaul()).thenReturn(baseUrlMockWebServer);
    // prepare mocks
    MockResponse mockResponse =
        new MockResponse().addHeader("Content-Type", "application/json").setResponseCode(500);
    mockWebServer.enqueue(mockResponse);

    // act & assert
    assertThatExceptionOfType(RestCommunicationException.class)
        .isThrownBy(() -> requestService.sendConfirmationToProducer(confirmationResponse))
        .withMessageContaining("HttpServerErrorException$InternalServerError: 500 Server Error");
    verify(requestRepo).findById(confirmationResponse.getRequestId());
    verify(requestService).getConfirmationBaseUrlForProducerPaul();
    verify(requestService, never()).changeRequestStatus(any(), any());
  }

  @Test
  void sendConfirmationToProducer_withOtherProducer() throws Exception {
    // arrange
    when(requestRepo.findById(any())).thenReturn(Optional.of(requestWithOtherProducer));
    // the returned mock response has the wrong request status but it is ignored anyway
    doReturn(requestWithOtherProducer).when(requestService).updateRequestStatus(any(), any());

    // act
    requestService.sendConfirmationToProducer(rejectionResponse);

    // assert
    verify(requestRepo).findById(rejectionResponse.getRequestId());
    verify(requestService, never()).getConfirmationBaseUrlForProducerPaul();
    verify(requestService)
        .updateRequestStatus(requestWithOtherProducer, RequestStatus.NEGATIVE_FEEDBACK);
  }

  @Test
  void changeRequestStatus_confirmedTrue()
      throws IllegalDataFormatException, EntityNotFoundException {
    // arrange
    // the returned mock response has the wrong request status but it is ignored anyway
    doReturn(requestWithProducerPaul).when(requestService).updateRequestStatus(any(), any());

    // act
    requestService.changeRequestStatus(confirmationResponse, requestWithProducerPaul);

    // assert
    verify(requestService)
        .updateRequestStatus(requestWithProducerPaul, RequestStatus.POSITIVE_FEEDBACK);
  }

  @Test
  void changeRequestStatus_confirmedFalse()
      throws IllegalDataFormatException, EntityNotFoundException {
    // arrange
    // the returned mock response has the wrong request status but it is ignored anyway
    doReturn(requestWithProducerPaul).when(requestService).updateRequestStatus(any(), any());

    // act
    requestService.changeRequestStatus(rejectionResponse, requestWithProducerPaul);

    // assert
    verify(requestService)
        .updateRequestStatus(requestWithProducerPaul, RequestStatus.NEGATIVE_FEEDBACK);
  }
}
