/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.supplier.model.repositories;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.common.model.Company;
import org.siliconeconomy.cda.supplier.model.Request;
import org.siliconeconomy.cda.supplier.model.enums.RequestStatus;

class RequestRepositoryTest {

  private final int week = 7;
  private final int year = 2022;
  private final String articleNumber = "articleX";
  private final String companyId = "companyX";

  @Spy private static RequestRepository requestRepository;
  private AutoCloseable closeable;

  @BeforeEach
  public void setup() {
    closeable = MockitoAnnotations.openMocks(this);
  }

  @AfterEach
  public void tearDown() throws Exception {
    closeable.close();
  }

  @Test
  void findAllByWeekAndArticleAndCustomer_noRequests() {
    // arrange
    when(requestRepository.findAll()).thenReturn(List.of());

    // act
    List<Request> result =
        requestRepository.findAllByWeekAndArticleAndCustomer(year, week, articleNumber, companyId);

    // assert
    assertThat(result).isEmpty();
  }

  @Test
  void findAllByWeekAndArticleAndCustomer_otherCustomer() {
    // arrange
    Request request =
        Request.builder()
            .id(UUID.randomUUID())
            .article(Article.builder().articleNumber(articleNumber).build())
            .quantity(1)
            .deliveryYear(year)
            .deliveryWeek(week)
            .customer(Company.builder().id("otherCustomer").build())
            .status(RequestStatus.NEW_REQUEST)
            .build();
    when(requestRepository.findAll()).thenReturn(List.of(request));

    // act
    List<Request> result =
        requestRepository.findAllByWeekAndArticleAndCustomer(year, week, articleNumber, companyId);

    // assert
    assertThat(result).isEmpty();
  }

  @Test
  void findAllByWeekAndArticleAndCustomer_matchingRequest() {
    // arrange
    Request request =
        Request.builder()
            .id(UUID.randomUUID())
            .article(Article.builder().articleNumber(articleNumber).build())
            .quantity(1)
            .deliveryYear(year)
            .deliveryWeek(week)
            .customer(Company.builder().id(companyId).build())
            .status(RequestStatus.NEW_REQUEST)
            .build();
    when(requestRepository.findAll()).thenReturn(List.of(request));

    // act
    List<Request> result =
        requestRepository.findAllByWeekAndArticleAndCustomer(year, week, articleNumber, companyId);

    // assert
    assertThat(result).containsExactly(request);
  }
}