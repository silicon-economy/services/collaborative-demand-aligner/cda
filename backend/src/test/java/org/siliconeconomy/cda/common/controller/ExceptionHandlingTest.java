/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.common.controller;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.NoSuchElementException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.siliconeconomy.cda.common.exception.EntityNotFoundException;
import org.siliconeconomy.cda.common.exception.IllegalDataFormatException;
import org.siliconeconomy.cda.common.exception.InvalidStatusChangeException;
import org.siliconeconomy.cda.common.exception.RestCommunicationException;
import org.siliconeconomy.cda.common.model.dto.ConfirmationResponseDTO;
import org.siliconeconomy.cda.common.model.dto.RequestRawDTO;
import org.siliconeconomy.cda.producer.controller.ProducerController;
import org.siliconeconomy.cda.producer.controller.RequestController;
import org.siliconeconomy.cda.producer.model.dto.input.DataForRequestDTO;
import org.siliconeconomy.cda.supplier.controller.SupplierController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

/**
 * Unit test of {@link ExceptionHandling}.
 *
 * @author jhohnsel
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = {SupplierController.class, RequestController.class})
class ExceptionHandlingTest {
  private static final ObjectMapper MAPPER = new ObjectMapper();

  // Main entry point for server-side Spring MVC test support
  @Autowired private MockMvc mvc;

  // Dependencies
  @MockBean RequestController requestControllerProducer;
  @MockBean org.siliconeconomy.cda.supplier.controller.RequestController requestControllerSupplier;
  @MockBean SupplierController supplierControllerMock;
  @MockBean ProducerController producerControllerMock;

  @Test
  void handleJsonMappingException() throws Exception {
    // throw exception
    doAnswer(
            invocation -> {
              throw JsonMappingException.fromUnexpectedIOE(new IOException(""));
            })
        .when(requestControllerProducer)
        .sendRequest(any());
    var body = "{\"invalid body\": \"invalid body\"}";
    // act
    mvc.perform(
            MockMvcRequestBuilders.post("/api/v2/producer/requests")
                .accept("application/json")
                .contentType("application/json")
                .content(body))
        .andExpect(status().isBadRequest())
        .andExpect(content().contentType("application/json"));
  }

  @Test
  void handleEntityNotFoundException() throws Exception {
    // arrange
    var URI = "/api/v2/supplier/external/request";
    var exceptionMessage = "Exception Message";
    // force to throw exception
    doAnswer(
            invocation -> {
              throw new EntityNotFoundException(exceptionMessage);
            })
        .when(supplierControllerMock)
        .createRequest(any());
    // act & assert
    mvc.perform(
            post(URI)
                .contentType(MediaType.APPLICATION_JSON)
                .content(MAPPER.writeValueAsString(RequestRawDTO.builder().build())))
        .andExpect(status().isNotFound())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(jsonPath("$.timestamp", is(notNullValue())))
        .andExpect(jsonPath("$.message", is(exceptionMessage)))
        .andExpect(jsonPath("$.requestUri", is("uri=" + URI)));
  }

  @Test
  void handleIllegalDataFormatException() throws Exception {
    // arrange
    var URI = "/api/v2/supplier/external/request";
    var exceptionMessage = "Exception Message";
    // force to throw exception
    doAnswer(
            invocation -> {
              throw new IllegalDataFormatException(exceptionMessage);
            })
        .when(supplierControllerMock)
        .createRequest(any());
    // act & assert
    mvc.perform(
            post(URI)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(MAPPER.writeValueAsString(RequestRawDTO.builder().build())))
        .andExpect(status().isBadRequest())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(jsonPath("$.timestamp", is(notNullValue())))
        .andExpect(jsonPath("$.message", is(exceptionMessage)))
        .andExpect(jsonPath("$.requestUri", is("uri=" + URI)));
  }

  @Test
  void handleIllegalArgumentException() throws Exception {
    // arrange
    var URI = "/api/v2/supplier/requests/confirmation";
    var exceptionMessage = "Exception Message";
    var mapper = new ObjectMapper();
    // force to throw exception
    doAnswer(
            invocation -> {
              throw new IllegalArgumentException(exceptionMessage);
            })
        .when(requestControllerSupplier)
        .confirmationClicked(any());
    // act & assert
    mvc.perform(
            post(URI)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(ConfirmationResponseDTO.builder().build())))
        .andExpect(status().isBadRequest());
  }

  @Test
  void handleRestCommunicationException() throws Exception {
    // arrange
    var URI = "/api/v2/producer/requests";
    var exceptionMessage = "Exception Message";
    // force to throw exception
    doAnswer(
            invocation -> {
              throw new RestCommunicationException(new RuntimeException(exceptionMessage));
            })
        .when(requestControllerProducer)
        .sendRequest(any());

    // act & assert
    mvc.perform(
            post(URI)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(MAPPER.writeValueAsString(DataForRequestDTO.builder().build())))
        .andExpect(status().isBadRequest())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(jsonPath("$.timestamp", is(notNullValue())))
        .andExpect(jsonPath("$.message", containsString(exceptionMessage)))
        .andExpect(jsonPath("$.requestUri", is("uri=" + URI)));
  }

  @Test
  void handleNoSuchElementException() throws Exception {
    // arrange
    var URI = "/api/v2/producer/requests";
    var exceptionMessage = "Exception Message";
    // force to throw exception
    doAnswer(
            invocation -> {
              throw new NoSuchElementException(exceptionMessage);
            })
        .when(requestControllerProducer)
        .sendRequest(any());

    // act & assert
    mvc.perform(
            post(URI)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(MAPPER.writeValueAsString(DataForRequestDTO.builder().build())))
        .andExpect(status().isNotFound())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(jsonPath("$.timestamp", is(notNullValue())))
        .andExpect(jsonPath("$.message", containsString(exceptionMessage)))
        .andExpect(jsonPath("$.requestUri", is("uri=" + URI)));
  }

  @Test
  void handleInvalidStatusChangeException() throws Exception {
    // arrange
    var URI = "/api/v2/producer/external/confirmation";
    var exceptionMessage = "Exception Message";
    var mapper = new ObjectMapper();
    // force to throw exception
    doAnswer(
            invocation -> {
              throw new InvalidStatusChangeException(exceptionMessage);
            })
        .when(producerControllerMock)
        .receiveConfirmation(any());
    // act & assert
    mvc.perform(
            post(URI)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(ConfirmationResponseDTO.builder().build())))
        .andExpect(status().isBadRequest())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(jsonPath("$.timestamp", is(notNullValue())))
        .andExpect(jsonPath("$.message", is(exceptionMessage)))
        .andExpect(jsonPath("$.requestUri", is("uri=" + URI)));
  }
}
