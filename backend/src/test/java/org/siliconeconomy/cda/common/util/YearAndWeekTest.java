/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.common.util;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.time.Month;
import org.junit.jupiter.api.Test;

/**
 * Unit Test of class {@link YearAndWeek}.
 *
 * @author jhohnsel
 */
class YearAndWeekTest {

  @Test
  void getYearAndWeekOfDate() {
    // arrange
    LocalDateTime date = LocalDateTime.of(2022, Month.FEBRUARY, 4, 0, 0, 0);
    int[] expectedYearAndWeek = {2022, 5};

    // act
    var result = YearAndWeek.of(date);

    // assert
    assertThat(result).isEqualTo(expectedYearAndWeek);
  }

  @Test
  void getYearAndWeekOfDate_specialCase() {
    // arrange
    // although this date is the first day of 2022, it is contained in week 52 of year 2021
    LocalDateTime date = LocalDateTime.of(2022, Month.JANUARY, 1, 0, 0, 0);
    int[] expectedYearAndWeek = {2021, 52};

    // act
    var result = YearAndWeek.of(date);

    // assert
    assertThat(result).isEqualTo(expectedYearAndWeek);
  }

  @Test
  void containsDate_whenTrue() {
    // arrange
    LocalDateTime date = LocalDateTime.of(2022, Month.FEBRUARY, 4, 0, 0, 0);
    int year = 2022;
    int week = 5;

    // act
    var result = YearAndWeek.containsDate(year, week, date);

    // assert
    assertThat(result).isTrue();
  }

  @Test
  void containsDate_whenFalse() {
    // arrange
    LocalDateTime date = LocalDateTime.of(2022, Month.JUNE, 4, 0, 0, 0);
    int year = 2022;
    int week = 5;

    // act
    var result = YearAndWeek.containsDate(year, week, date);

    // assert
    assertThat(result).isFalse();
  }

  @Test
  void compositeKey() {
    // arrange
    int year = 2121;
    int week = 5;

    // act
    var result = YearAndWeek.composeKey(year, week);

    // assert
    assertThat(result).isEqualTo(212105);
  }
}