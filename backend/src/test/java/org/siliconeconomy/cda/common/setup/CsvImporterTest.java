/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.common.setup;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.common.model.Company;
import org.siliconeconomy.cda.producer.model.Demand;

/**
 * Unit Test of class {@link CsvImporter}.
 *
 * @author jhohnsel
 */
class CsvImporterTest {

  private final CsvImporter importer = new CsvImporter();

  /** Load articles.csv from resources. */
  @Test
  void loadObjectsFromCsv_articles() {
    // arrange
    var resourcePath = "database-initialization/articles.csv";

    // act
    List<Article> articles = importer.loadObjectsFromCsv(Article.class, resourcePath);

    // assert
    assertThat(articles).isNotEmpty();
  }

  /** Load companies.csv from resources. */
  @Test
  void loadObjectsFromCsv_companies() {
    // arrange
    var resourcePath = "database-initialization/companies.csv";

    // act
    List<Company> companies = importer.loadObjectsFromCsv(Company.class, resourcePath);

    // assert
    assertThat(companies).isNotEmpty();
  }

  /** Load demands.csv from resources. */
  @Test
  void loadObjectsFromCsv_demands() {
    // arrange
    var resourcePath = "database-initialization/demands.csv";

    // act
    List<Demand> demands = importer.loadObjectsFromCsv(Demand.class, resourcePath);

    // assert
    assertThat(demands).isNotEmpty();
  }

  /** Load outbound-requests.csv from resources. */
  @Test
  void loadObjectsFromCsv_outboundRequests() {
    // arrange
    var resourcePath = "database-initialization/outbound-requests.csv";

    // act
    List<org.siliconeconomy.cda.producer.model.Request> requests =
        importer.loadObjectsFromCsv(
            org.siliconeconomy.cda.producer.model.Request.class, resourcePath);

    // assert
    assertThat(requests).isNotEmpty();
  }

  /** Load inbound-requests.csv from resources. */
  @Test
  void loadObjectsFromCsv_inboundRequests() {
    // arrange
    var resourcePath = "database-initialization/inbound-requests.csv";

    // act
    List<org.siliconeconomy.cda.supplier.model.Request> inboundRequests =
        importer.loadObjectsFromCsv(
            org.siliconeconomy.cda.supplier.model.Request.class, resourcePath);

    // assert
    assertThat(inboundRequests).isNotEmpty();
  }

  /** Load article-supplier-matching.csv from resources. */
  @Test
  void loadManyToManyRelationships_articlesAndCompanies() {
    // arrange
    var resourcePath = "database-initialization/article-supplier-matching.csv";

    // act
    List<String[]> manyToManyRelationships = importer.loadManyToManyRelationships(resourcePath);

    // assert
    assertThat(manyToManyRelationships).isNotEmpty();
  }
}