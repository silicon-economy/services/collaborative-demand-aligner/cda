/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.common.model.dto;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.cda.supplier.model.Request;
import org.siliconeconomy.cda.supplier.model.enums.RequestStatus;

class RequestRawDTOTest {

  @Test
  void toRequest() {
    // arrange
    final UUID id = UUID.randomUUID();
    final String articleNumber = "articleX";
    final int quantity = 100;
    final int year = 2022;
    final int week = 12;
    final String customerId = "customerX";
    RequestRawDTO dto =
        RequestRawDTO.builder()
            .id(id)
            .articleNumber(articleNumber)
            .quantity(quantity)
            .year(year)
            .week(week)
            .customerId(customerId)
            .build();

    // act
    Request requestFromDto = dto.toRequest();

    // assert
    assertThat(requestFromDto.getId()).isEqualByComparingTo(id);
    assertThat(requestFromDto.getArticle().getArticleNumber()).isEqualTo(articleNumber);
    assertThat(requestFromDto.getQuantity()).isEqualByComparingTo(quantity);
    assertThat(requestFromDto.getDeliveryYear()).isEqualByComparingTo(year);
    assertThat(requestFromDto.getDeliveryWeek()).isEqualByComparingTo(week);
    assertThat(requestFromDto.getCustomer().getId()).isEqualTo(customerId);
    assertThat(requestFromDto.getStatus()).isEqualByComparingTo(RequestStatus.NEW_REQUEST);
  }
}