/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.cda.common.setup;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.NoSuchElementException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.cda.common.model.Article;
import org.siliconeconomy.cda.common.model.Company;
import org.siliconeconomy.cda.producer.model.Demand;

/**
 * Unit Test of class {@link SetupData}.
 *
 * @author jhohnsel
 */
class SetupDataTest {

  private SetupData setupData;

  @BeforeEach
  void setup() {
    setupData = new SetupData();
  }

  @Test
  void whenGettingSuppliersFromCsvFile_thenCorrect() {
    // act
    List<Company> companies = setupData.getCompanies();

    // assert
    assertThat(companies).isNotEmpty();
    assertThat(setupData.getCachedCompanies()).isNotEmpty();
  }

  @Test
  void whenGettingArticlesFromCsvFile_thenCorrect() {
    // act
    List<Article> articles = setupData.getArticles();

    // assert
    assertThat(articles).isNotEmpty();
    for (Article article : articles) {
      assertThat(article.getSuppliers()).isNotEmpty();
    }
    assertThat(setupData.getCachedArticles()).isNotEmpty();
  }

  @Test
  void whenGettingDemandsFromCsvFile_thenCorrect() {
    // act
    List<Demand> demands = setupData.getDemands();

    // assert
    assertThat(demands).isNotEmpty();
  }

  @Test
  void whenGettingOutboundRequestsFromCsvFile_thenCorrect() {
    // act
    List<org.siliconeconomy.cda.producer.model.Request> requests = setupData.getOutboundRequests();

    // assert
    assertThat(requests).isNotEmpty();
  }

  @Test
  void whenGettingInboundRequestsFromCsvFile_thenCorrect() {
    // act
    List<org.siliconeconomy.cda.supplier.model.Request> requests = setupData.getInboundRequests();

    // assert
    assertThat(requests).isNotEmpty();
  }

  @Test
  void findArticleByArticleNumber_whenPresent() {
    // arrange
    String validArticleNumber = "4711";

    // act
    Article article = setupData.findArticleByArticleNumber(validArticleNumber);

    // assert
    assertThat(article).isNotNull();
    assertThat(article.getArticleNumber()).isEqualTo(validArticleNumber);
  }

  @Test
  void findArticleByArticleNumber_whenAbsent() {
    // arrange
    String invalidArticleNumber = "invalid";

    // act & assert
    Assertions.assertThatExceptionOfType(NoSuchElementException.class)
        .isThrownBy(() -> setupData.findArticleByArticleNumber(invalidArticleNumber));
  }

  @Test
  void findSupplierById_whenPresent() {
    // arrange
    String validCompanyId = "123";

    // act
    Company company = setupData.findCompanyById(validCompanyId);

    // assert
    assertThat(company).isNotNull();
    assertThat(company.getId()).isEqualTo(validCompanyId);
  }

  @Test
  void findSupplierById_whenAbsent() {
    // arrange
    String invalidCompanyId = "invalid";

    // act & assert
    Assertions.assertThatExceptionOfType(NoSuchElementException.class)
        .isThrownBy(() -> setupData.findCompanyById(invalidCompanyId));
  }
}