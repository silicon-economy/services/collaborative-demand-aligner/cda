## Related issues

<!-- Issues that are closed by or related to this merge request, e.g. "Closes XXX" or "Related to XXX" -->

Closes SE-XXXX

*Optional: More detailed description of changes*


## Acceptance criteria

<!-- Criteria for the MR to be considered mergeable -->

- [ ] Current branch name has the correct format: `TYPE/SE-XXXX-name`
- [ ] Title of the MR summarizes the work
- [ ] MR is not marked as `Draft`
- [ ] Target branch is the main branch
- [ ] Both checkboxes "Delete source branch" and "Squash commits" are checked
- [ ] All acceptance criteria of the related issue in Jira are met
- [ ] Documentation reflects the status quo after merge
- [ ] Changelog contains entries reflecting all changes of this MR
- [ ] Only released versions of (external) dependencies are used (i.e. no snapshot versions)
- [ ] Code is tested in Unit Tests and they run successfully
- [ ] The Git Pipeline runs successfully 
- [ ] License headers are used
- [ ] SonarQube Coverage, Bugs, Code Smells (severity 'Info' is okay) etc. are checked
- [ ] MR is assigned to a person ("Assignee", performs the merge)
- [ ] MR is assigned to a reviewer ("Reviewers", approval is required)
- [ ] The squash commit message is exchanged by the suggested message below

## Proposed squash commit message

<!--
A proposed message for the eventual squashed commit.
Please stick to the following pattern:

- A short one-line summary (max. 50 characters).
- A blank line.
- A detailed explanation of the changes introduced by this merge request.
  Each line should not exceed 72 characters.
*********1*********2*********3*********4*********5*********6*********7** (<-- Ruler for line width assistance)
-->
```
A short one-line summary (max. 50 characters)

* A more detailed explanation of the changes introduced by this merge
  request (e.g. in bullet points).
* Each line should not exceed 72 characters.

Co-authored-by: NAME <EMAIL>
```
<!--
*********1*********2*********3*********4*********5*********6*********7** (<-- Ruler for line width assistance)
-->
